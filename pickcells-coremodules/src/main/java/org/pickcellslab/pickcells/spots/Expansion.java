package org.pickcellslab.pickcells.spots;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.ImageDot;

import edu.mines.jtk.util.Array;
import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessible;
import net.imglib2.algorithm.region.localneighborhood.EllipsoidNeighborhood;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.IntervalView;


public class Expansion<T extends RealType<T> & NativeType<T>> implements Runnable{

	private static final long[] span = {1,1,1};
	private final T sub;

	private final float id;


	private final LinkedList<long[]> front = new LinkedList<>();
	private final LinkedList<long[]> nextFront = new LinkedList<>();
	private final Set<long[]> domain = new HashSet<>();

	/**
	 * Neighbourhood Iterator on the original image
	 */
	private final EllipsoidNeighborhood<T> k;
	/**
	 * RandomAccess on the final labelled image
	 */
	private final RandomAccess<T> origin;
	/**
	 * RandomAccess on the final labelled image
	 */
	private final RandomAccess<T> label;		
	/**
	 * The ImageDot for which the expansion is performed
	 */
	private final ImageDot dot;
	/**
	 * The location of the dot
	 */
	private final long[] center;




	/**
	 * Tests whether or not to accept the tested voxel
	 */
	private final Predicate<Expansion<T>> predicate;


	/**
	 * The value of the voxel 0 (dot location)
	 */
	private final float V0;
	/**
	 * The currently tested neighbour value in the original image
	 */
	private float nValue = 0;
	/**
	 * The value of the voxel currently being expanded in the original image
	 */
	private float pValue = 0;

	/**
	 * The location of the accessibles
	 */
	private long[] pos = new long[3];



	Expansion(ImageDot dot, IntervalView<T> img, RandomAccessible<T> labelled, float id, Predicate<Expansion<T>> predicate){

		this.dot = dot;			
		center = dot.location();

		front.add(center);

		k = new EllipsoidNeighborhood<T>(img, center, span);
		origin = img.randomAccess();
		origin.setPosition(center);
		V0 = origin.get().getRealFloat();

		label = labelled.randomAccess();
		label.setPosition(center);
		label.get().setReal(id);
		this.id = id;

		//Init sub which is used to label rejected voxels
		sub = img.firstElement().createVariable();
		sub.setReal(-1f);

		this.predicate = predicate;
	}









	@Override
	public void run() {

		//

		while(!front.isEmpty()){

			long[] p = front.poll();
			domain.add(p);

			//Get the value at this position in the original image
			origin.setPosition(p);
			pValue = origin.get().getRealFloat();

			//Move the Neighborhood to this position
			k.setPosition(p);
			Cursor<T> c = k.localizingCursor();
			while(c.hasNext()){//Iterate over neighbours

				c.fwd();
				c.localize(pos);

				//Check if the neighbour may be accepted using its label
				label.setPosition(pos);
				float l = label.get().getRealFloat();
				if(l>0)continue;
				if(l == 0f || l == -1f){//0 is unknown, -1 is rejected once

					//Get the value in the original image to be tested
					origin.setPosition(pos);
					nValue = origin.get().getRealFloat(); 

					if(predicate.test(this)){
						//Mark in label image
						label.get().setReal(id);
						//Add to front
						nextFront.add(Array.copy(pos));
					}
				}else{
					// Subtract 1
					label.get().sub(sub);
				}
			}

		}
		front.addAll(nextFront);
		nextFront.clear();			

	}

	

	public ImageDot getDot() {
		return dot;
	}


	public long[] origin(){
		return center;
	}

	public float originSignal(){
		return V0;
	}

	/**
	 * The value of the voxel currently being expanded in the original image
	 */
	public float pSignal(){
		return pValue;
	}

	/**
	 * The currently tested neighbour value in the original image
	 */
	public float nSignal(){
		return nValue;
	}


	
	/**
	 * @return The list of positions in the image which constitute the currently expanded region
	 */
	public List<long[]> currentDomain(){
		List<long[]> d = new ArrayList<>(domain);
		d.addAll(nextFront);
		return d;
	}
	

	public Vector3D axis(){

		Vector3D c = new Vector3D(dot.getAttribute(Keys.centroid).get());
		Vector3D avg = new Vector3D(0,0,0);
		for(long[] l : domain){
			Vector3D v = new Vector3D(l[0],l[1],l[2]);
			v = v.subtract(c);
			avg = avg.add(v);
		}
		//TODO check avg is not 0
		avg = avg.normalize();

		return avg;
	}



}
