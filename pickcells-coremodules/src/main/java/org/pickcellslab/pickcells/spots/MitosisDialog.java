package org.pickcellslab.pickcells.spots;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.IOException;
import java.text.Format;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.log4j.Logger;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.pickcells.api.app.ui.UI;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Centrosome;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageDot;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;
import org.pickcellslab.pickcells.api.img.view.ImageViewer;
import org.pickcellslab.pickcells.api.img.view.Lut8;

import net.imglib2.Cursor;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.gauss3.Gauss3;
import net.imglib2.algorithm.region.localneighborhood.EllipsoidNeighborhood;
import net.imglib2.exception.IncompatibleTypeException;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;





@SuppressWarnings("serial")
public class MitosisDialog<T extends RealType<T> & NativeType<T>> extends JDialog{



	private static Logger log = Logger.getLogger(MitosisDialog.class);

	
	private final DataAccess<?,?> access;
		

	//Dialog Status
	private boolean wasCancelled = true;
	private boolean wasPreviewed = false;

	//Controllers
	private JFormattedTextField sigmaField;
	private JFormattedTextField threshField;
	JLabel waitLabel = new JLabel("...");

	//Preview inputs
	private String choice;
	private Image image;
	private List<ImageDot> cts;
	private long channel;
	/**
	 * Overlay channel index
	 */
	private long overlayIndex;
	/**
	 * The copy of the channel image of interest
	 */
	private RandomAccessibleInterval<T> cImg = null;



	//Detection parameters
	/**
	 * Noise scale
	 */
	private double sigma = 0.5;	

	private MitoticSpots detector = new MitoticSpots();

	//Filter on Response Parameters
	private double minVolume = 150;
	private double minSkew = 0.5;




	//preview state
	private boolean removeNoise = true;



	//Preview Image	
	/**
	 * The viewer frame
	 */
	private JFrame vf;
	/**
	 * The open image + one overlay channel
	 */
	private RandomAccessibleInterval<T> copy;
	private ImageViewer<T> view;




	// IO Factory
	private final ImgIO ioFactory;



	public MitosisDialog(DataAccess<?,?> access, ImgIO factory,String[] names, String choice) throws DataAccessException {

		setModal(true);

		this.access = access;
		this.ioFactory = factory;
		this.choice = choice;

		//Layout UI
		//--------------------------------------------------------------------------------------------


		NumberFormat f = NumberFormat.getNumberInstance(Locale.ENGLISH);


		getContentPane().setLayout(new BorderLayout(0, 0));		


		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Spot Detecor Options", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(panel, BorderLayout.CENTER);

		JLabel label = new JLabel("");

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Region Growth", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(51, 51, 51)));

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Spot filter", TitledBorder.LEADING, TitledBorder.TOP, null, null));



		JButton prevBtn = new JButton("Preview");
		prevBtn.setSelected(false);
		prevBtn.setToolTipText("Preview the result of the mitosis detector on the selected plane of the image");
		prevBtn.setHorizontalAlignment(SwingConstants.RIGHT);


		JButton btnLoad = new JButton("Load another Image");

		JComboBox<String> imagesBox = new JComboBox<>(names);




		JButton runBtn = new JButton("Run");

		JButton cancelBtn = new JButton("Cancel");







		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
										.addGroup(gl_panel.createSequentialGroup()
												.addGap(262)
												.addComponent(label))
										.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
												.addGap(147)
												.addComponent(imagesBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addGroup(gl_panel.createSequentialGroup()
												.addComponent(prevBtn)
												.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(waitLabel, GroupLayout.PREFERRED_SIZE, 174, GroupLayout.PREFERRED_SIZE)))
								.addComponent(btnLoad))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
								.addGroup(gl_panel.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(prevBtn)
										.addPreferredGap(ComponentPlacement.RELATED))
								.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addComponent(waitLabel)
										.addGap(11)))
						.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
								.addComponent(btnLoad, Alignment.LEADING)
								.addComponent(imagesBox, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(label))
				);




		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panel_2.rowHeights = new int[]{0, 0, 0};
		gbl_panel_2.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_2.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel_2);

		JLabel lblMinVolume = new JLabel("Min Volume");
		lblMinVolume.setToolTipText("Gaussian convolution sigma to remove small scale noise");
		GridBagConstraints gbc_lblMinVolume = new GridBagConstraints();
		gbc_lblMinVolume.anchor = GridBagConstraints.WEST;
		gbc_lblMinVolume.insets = new Insets(0, 0, 5, 5);
		gbc_lblMinVolume.gridx = 0;
		gbc_lblMinVolume.gridy = 0;
		panel_2.add(lblMinVolume, gbc_lblMinVolume);

		JFormattedTextField minVField = new JFormattedTextField((Format) null);
		minVField.setText(""+minVolume);
		minVField.setColumns(10);
		GridBagConstraints gbc_minVField = new GridBagConstraints();
		gbc_minVField.insets = new Insets(0, 0, 5, 0);
		gbc_minVField.fill = GridBagConstraints.HORIZONTAL;
		gbc_minVField.gridx = 2;
		gbc_minVField.gridy = 0;
		panel_2.add(minVField, gbc_minVField);

		JLabel lblMinSkew = new JLabel("Min Skew");
		lblMinSkew.setToolTipText("Gaussian convolution sigma to remove small scale noise");
		GridBagConstraints gbc_lblMinSkew = new GridBagConstraints();
		gbc_lblMinSkew.anchor = GridBagConstraints.WEST;
		gbc_lblMinSkew.insets = new Insets(0, 0, 0, 5);
		gbc_lblMinSkew.gridx = 0;
		gbc_lblMinSkew.gridy = 1;
		panel_2.add(lblMinSkew, gbc_lblMinSkew);

		JFormattedTextField minSField = new JFormattedTextField((Format) null);
		minSField.setText(""+minSkew);
		minSField.setColumns(10);
		GridBagConstraints gbc_minSField = new GridBagConstraints();
		gbc_minSField.fill = GridBagConstraints.HORIZONTAL;
		gbc_minSField.gridx = 2;
		gbc_minSField.gridy = 1;
		panel_2.add(minSField, gbc_minSField);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_panel_1.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);

		JLabel lblMinScale = new JLabel("Noise scale");
		GridBagConstraints gbc_lblMinScale = new GridBagConstraints();
		gbc_lblMinScale.anchor = GridBagConstraints.WEST;
		gbc_lblMinScale.insets = new Insets(0, 0, 5, 5);
		gbc_lblMinScale.gridx = 0;
		gbc_lblMinScale.gridy = 0;
		panel_1.add(lblMinScale, gbc_lblMinScale);
		lblMinScale.setToolTipText("Gaussian convolution sigma to remove small scale noise");


		sigmaField = new JFormattedTextField(f);
		GridBagConstraints gbc_sigmaField = new GridBagConstraints();
		gbc_sigmaField.fill = GridBagConstraints.HORIZONTAL;
		gbc_sigmaField.insets = new Insets(0, 0, 5, 0);
		gbc_sigmaField.gridx = 2;
		gbc_sigmaField.gridy = 0;
		panel_1.add(sigmaField, gbc_sigmaField);
		sigmaField.setText(""+sigma);
		sigmaField.setColumns(10);


		JLabel lblMaxScale = new JLabel("Threshold");
		GridBagConstraints gbc_lblMaxScale = new GridBagConstraints();
		gbc_lblMaxScale.anchor = GridBagConstraints.WEST;
		gbc_lblMaxScale.insets = new Insets(0, 0, 5, 5);
		gbc_lblMaxScale.gridx = 0;
		gbc_lblMaxScale.gridy = 1;
		panel_1.add(lblMaxScale, gbc_lblMaxScale);
		lblMaxScale.setToolTipText("Determines the amplitude of the weighting process");

		threshField = new JFormattedTextField(f);
		GridBagConstraints gbc_threshField = new GridBagConstraints();
		gbc_threshField.fill = GridBagConstraints.HORIZONTAL;
		gbc_threshField.insets = new Insets(0, 0, 5, 0);
		gbc_threshField.gridx = 2;
		gbc_threshField.gridy = 1;
		panel_1.add(threshField, gbc_threshField);
		threshField.setText(""+detector.getT());
		threshField.setColumns(10);

		JLabel lblMaxDistance = new JLabel("Max Distance");
		GridBagConstraints gbc_lblMaxDistance = new GridBagConstraints();
		gbc_lblMaxDistance.anchor = GridBagConstraints.WEST;
		gbc_lblMaxDistance.insets = new Insets(0, 0, 5, 5);
		gbc_lblMaxDistance.gridx = 0;
		gbc_lblMaxDistance.gridy = 2;
		panel_1.add(lblMaxDistance, gbc_lblMaxDistance);
		lblMaxDistance.setToolTipText("The size of a typical spot");




		JFormattedTextField itrField = new JFormattedTextField((Format) null);
		GridBagConstraints gbc_itrField = new GridBagConstraints();
		gbc_itrField.fill = GridBagConstraints.HORIZONTAL;
		gbc_itrField.insets = new Insets(0, 0, 5, 0);
		gbc_itrField.gridx = 2;
		gbc_itrField.gridy = 2;
		panel_1.add(itrField, gbc_itrField);
		itrField.setText(""+detector.getItr());
		itrField.setColumns(10);



		JLabel lblDelta = new JLabel("Delta");
		lblDelta.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblDelta = new GridBagConstraints();
		gbc_lblDelta.anchor = GridBagConstraints.WEST;
		gbc_lblDelta.insets = new Insets(0, 0, 5, 5);
		gbc_lblDelta.gridx = 0;
		gbc_lblDelta.gridy = 3;
		panel_1.add(lblDelta, gbc_lblDelta);
		lblDelta.setToolTipText("The size of a typical spot");



		JFormattedTextField deltaField = new JFormattedTextField((Format) null);
		GridBagConstraints gbc_deltaField = new GridBagConstraints();
		gbc_deltaField.fill = GridBagConstraints.HORIZONTAL;
		gbc_deltaField.insets = new Insets(0, 0, 5, 0);
		gbc_deltaField.gridx = 2;
		gbc_deltaField.gridy = 3;
		panel_1.add(deltaField, gbc_deltaField);
		deltaField.setText(""+detector.getDelta());
		deltaField.setColumns(10);

		JLabel Lowest = new JLabel("Min Intensity");
		Lowest.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_Lowest = new GridBagConstraints();
		gbc_Lowest.insets = new Insets(0, 0, 0, 5);
		gbc_Lowest.gridx = 0;
		gbc_Lowest.gridy = 4;
		panel_1.add(Lowest, gbc_Lowest);
		Lowest.setToolTipText("The size of a typical spot");


		JFormattedTextField lowestField = new JFormattedTextField((Format) null);
		GridBagConstraints gbc_lowestField = new GridBagConstraints();
		gbc_lowestField.fill = GridBagConstraints.HORIZONTAL;
		gbc_lowestField.gridx = 2;
		gbc_lowestField.gridy = 4;
		panel_1.add(lowestField, gbc_lowestField);
		lowestField.setText(""+detector.getLowest());
		lowestField.setColumns(10);


		gl_panel.setHonorsVisibility(false);
		panel.setLayout(gl_panel);

		JPanel okCancelPanel = new JPanel();
		getContentPane().add(okCancelPanel, BorderLayout.SOUTH);
		okCancelPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		okCancelPanel.add(runBtn);
		okCancelPanel.add(cancelBtn);






		// Listeners
		//----------------------------------------------------------------------------------------------------
		lowestField.addPropertyChangeListener(e-> {			
			try {
				detector.setLowest(f.parse(lowestField.getText()).floatValue());
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null,"Unable to parse " + lowestField.getText());
			}
		});
		deltaField.addPropertyChangeListener(e-> {			
			try {
				detector.setDelta(f.parse(deltaField.getText()).floatValue());
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null,"Unable to parse " + deltaField.getText());
			}
		});
		itrField.addPropertyChangeListener(e-> {			
			try {
				detector.setItr(f.parse(itrField.getText()).intValue());
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null,"Unable to parse " + itrField.getText());
			}
		});
		threshField.addPropertyChangeListener(e-> {	
			try {
				detector.setT(f.parse(threshField.getText()).doubleValue());
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null,"Unable to parse "+threshField.getText());
			}
		});
		sigmaField.addPropertyChangeListener(e-> {			
			double newSigma = Double.parseDouble(sigmaField.getText());
			if(newSigma!=sigma){
				removeNoise=true;
				sigma = newSigma;
			}
		});
		minVField.addPropertyChangeListener(e-> {			
			minVolume = Double.parseDouble(minVField.getText());			
		});
		minSField.addPropertyChangeListener(e-> {			
			minSkew = Double.parseDouble(minSField.getText());			
		});


		runBtn.addActionListener(e->{
			// Cleanup and dispose;			
			wasCancelled = false;
			view = null;
			copy = null;
			cImg = null;
			vf.dispose();
			this.dispose();
		});


		cancelBtn.addActionListener(e->{
			wasCancelled = true;
			//cleanup
			view = null;
			copy = null;
			cImg = null;
			vf.dispose();
			this.dispose();
		});


		prevBtn.addActionListener(e->{
			waitLabel.setText("Please wait, running preview...");
			new Thread(new Runnable(){
				@Override
				public void run() {
					prevBtn.setEnabled(false);
					waitLabel.setText("Number of detected mitosis: "+runPreview(image));	
					prevBtn.setEnabled(true);
				}				
			}).start();			
		});



		btnLoad.addActionListener(l-> {
			try {
				if(imagesBox.getSelectedItem().equals(this.choice))
					return;
				vf.dispose();
				loadImage(access, (String)imagesBox.getSelectedItem());
			} catch (Exception e1) {
				UI.display("Error","Unable to read images!", e1, Level.WARNING);
				return;
			}
		});



	}









	private void loadImage(DataAccess<?, ?> access, String choice) throws DataAccessException {

		wasPreviewed = false;
		removeNoise = true;
		
		this.choice = choice;

		image = access.queryFactory()
				.regenerate(Image.class)
				.toDepth(1)
				.traverseLink(ImageDot.origin,Direction.INCOMING)//To Centrosomes
				.includeAllNodes()
				.regenerateAllKeys()
				.doNotGetAll().useFilter(P.keyTest(Keys.name, P.stringEquals(choice)))
				.run().getOneTarget(Image.class).get();




		//Get the centrosomes which need to be tested
		this.cts = image.getLinks(Direction.INCOMING, ImageDot.origin)
				.map(l->(ImageDot)l.source()).collect(Collectors.toList());


		//Channel containing centrosomes
		this.channel = image.getAttribute(AKey.get(Centrosome.class.getSimpleName(),Long.class)).get();




		//Open an Image Viewer for preview
		String[] c = image.channels();
		String [] l = image.getAttribute(Image.cLuts).get();

		//Add + 3 for the overlays
		String [] channels = Arrays.copyOf(c,c.length+3);
		channels[c.length] = "Regions";
		channels[c.length+1] = "Filtered Dots";
		channels[c.length+2] = "Detected Mitoses";
		overlayIndex = c.length;

		String [] luts = Arrays.copyOf(l,l.length+3);
		luts[luts.length-3] = Lut8.Grays.name();
		luts[luts.length-2] = Lut8.Green.name();
		luts[luts.length-1] = Lut8.Yellow.name();


		//Open an image and add a channel
		// TODO only open the channel required for the detection
		RandomAccessibleInterval<T> img;
		try {
			img = ioFactory.open(image);
		} catch (IOException e) {
			log.error("Unable to open the image in path : "+image.path());
			return;
		}
		this.copy = ImgDimensions.increase(img, image.channelIndex(), 3);
		img = null;


		this.view = new ImageViewer<>(copy,ImgDimensions.createColorTables(copy,luts), image.order());
		this.vf = view.displayWithChannelsController("Spot Detection Preview ",channels);
		vf.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		vf.setAlwaysOnTop(true);
		vf.setLocationRelativeTo(null);
		
	//	for(int i = 0; i<c.length; i++)
	//		view.adjustContrast(i);


	}












	private String runPreview(Image image) {
		

		System.out.println("===========================");
		System.out.println("Mitosis Dialog options:");
		System.out.println("Remove Noise: "+removeNoise);
		System.out.println("Noise reduction: "+this.sigma);
		System.out.println("Threshold: "+detector.getT());
		System.out.println("Delta: "+detector.getDelta());
		
		//Erase the labels if this is not the first run
		if(cImg != null){
			for(int i = 0; i<3; i++){
				Cursor<T> c = Views.hyperSlice(copy, image.channelIndex(), overlayIndex+i).cursor();
				while(c.hasNext()){
					c.fwd();
					c.get().setZero();
				}
			}
			//Also delete the previous detections
			detector.deleteSideEffects(cts);
		}





		if(removeNoise){

			try{
				
				IntervalView<T> itv = Views.hyperSlice(copy, image.channelIndex(), channel);
				
				//Copy the centrosome channel
				cImg = ImgDimensions.hardCopy(itv, itv.firstElement().createVariable());
				Gauss3.gauss(sigma, Views.extendMirrorSingle(cImg), cImg);

				removeNoise = false;
			} catch (IncompatibleTypeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "0 ! An Error occured while performing the gaussian convolution !";
			}

		}





		//TODO condition to growRegion or not
		detector.growRegions(image, cts, cImg, Views.hyperSlice(copy, image.channelIndex(), overlayIndex));


		//Filter dots and create an overlay to show the ones passing the filter
		List<ImageDot> filtered = cts.stream()
				.filter(c-> c.getAttribute(MitoticSpots.volume).get() >= minVolume
				&& new Vector3D(c.getAttribute(MitoticSpots.skew).get()).getNorm() >= minSkew)
				.collect(Collectors.toList());


		EllipsoidNeighborhood<T> f =
				new EllipsoidNeighborhood<>(Views.hyperSlice(copy, image.channelIndex(), overlayIndex+1), new long[]{0,0,0}, new long[]{3,3,3});
		for(ImageDot d : filtered){
			long[] p = d.location();
			f.setPosition(p);
			f.forEach(t->t.setReal(5000));
		}




		//Find mitoses

		List<Link> mitoses = detector.findMitoses(image, Views.interval(cImg,cImg), filtered);


		//Draw Mitoses
		EllipsoidNeighborhood<T> m =
				new EllipsoidNeighborhood<>(Views.hyperSlice(copy, image.channelIndex(), overlayIndex+2), new long[]{0,0,0}, new long[]{3,3,3});


		for(Link l : mitoses){
			m.setPosition(l.getAttribute(Keys.location).get());
			m.forEach(t->t.setReal(5000));
		}


		wasPreviewed = true;

		//view.adjustContrast((int) overlayIndex);
		//view.adjustContrast((int) overlayIndex+1);
		//view.adjustContrast((int) overlayIndex+2);

		return mitoses.size() + " ! ";

	}








	@Override
	public void setVisible(boolean b){
		//Get the chosen image
		try {
			loadImage(access, choice);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		vf.setLocationRelativeTo(null);
		vf.setVisible(true);
		pack();
		this.setLocation(vf.getLocation().x-this.getWidth(), vf.getLocation().y);		
		super.setVisible(b);
	}


	boolean wasCancelled(){
		return wasCancelled;
	}





	public double getSigma(){
		return sigma;
	}



	public boolean wasPreviewed(Image i){
		boolean wp = wasPreviewed && choice.equals(i.getAttribute(Keys.name).get());
		System.out.println(i.toString()+" was previewed? "+wp);
		return wp;
	}



	public Consumer<Image> getMethod() {

		Predicate<ImageDot> filter = getFilter();


		return i ->{

			//Load the image
			Img<T> img = null;
			try {
				img = this.ioFactory.open(i);
			} catch (IOException e) {
				UI.display("Error", "IO Exception: "+i.toString()+" could not be opened", e, Level.WARNING);			
				return;
			}			

			//Copy channel and Remove noise
			try{
				//Copy the centrosome channel
				img = ImgDimensions.hardCopy(Views.hyperSlice(img, i.channelIndex(), channel),img.firstElement().createVariable());
				Gauss3.gauss(sigma, Views.extendMirrorSingle(img), img);
			} catch (IncompatibleTypeException e) {
				UI.display("Error", "Noise Reduction Exception: "+i.toString()+" could not be filtered", e, Level.WARNING);			
				return;
			}

			//Create a temporary label image
			Img<T> label = img.factory().create(img, img.firstElement().createVariable());



			//Get the centrosomes which need to be tested
			List<ImageDot> dots = i.getLinks(Direction.INCOMING, ImageDot.origin)
					.map(l->(ImageDot)l.source()).collect(Collectors.toList());

			
			System.out.println("Number of centrosomes in "+i.toString()+" : "+dots.size());

			detector.growRegions(i, dots, img, Views.interval(label,label));

			label = null;



			//Filter dots to select the mitotic dots
			List<ImageDot> filtered = dots.stream()
					.filter(filter)
					.collect(Collectors.toList());

			System.out.println("Number of filtered centrosomes in "+i.toString()+" : "+filtered.size());
			

			//Find mitoses
			System.out.println("Number of detected mitoses in "+i.toString()+" : "+
					detector.findMitoses(i, Views.interval(img,img), filtered).size());
			
			img = null;

		};
	}

 



	public Predicate<ImageDot> getFilter(){
		return c -> c.getAttribute(MitoticSpots.volume).get() >= minVolume
				&& new Vector3D(c.getAttribute(MitoticSpots.skew).get()).getNorm() >= minSkew;
	}

	public MitoticSpots getDetector() {
		return detector;
	}


	public Image getPreviewedImage() {
		return image;
	}
}
