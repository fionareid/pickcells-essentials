package org.pickcellslab.pickcells.spots;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Component;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import org.apache.log4j.Logger;
import org.pickcellslab.foundationj.datamodel.DataTypes;
import org.pickcellslab.foundationj.ui.renderers.ClassOrLinkRenderer;
import org.pickcellslab.pickcells.api.app.ui.UI;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageDot;
import org.pickcellslab.pickcells.api.img.detector.DogDetect;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;
import org.pickcellslab.pickcells.api.img.view.ImageViewer;
import org.pickcellslab.pickcells.api.img.view.Lut8;

import net.imglib2.Point;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;

@SuppressWarnings("serial")
public class SpotDetectorDialog<T extends RealType<T> & NativeType<T>> extends JDialog{

	private static Logger log = Logger.getLogger(SpotDetectorDialog.class);

	private JComboBox<String> channelBox;
	private JComboBox<Class<?>> typeBox;

	private double radius = 1;
	private double sigma = 0.2;
	private double threshold = 5;
	private boolean applyMedian = false;
	private boolean findMinima = false;

	private boolean wasCancelled = true;

	private JFrame vf;


	private RandomAccessibleInterval<T> fullWithPreview;
	private RandomAccessibleInterval<T> prevInterval;

	private ImageViewer<T> view;

	private ArrayList<Point> peaks;


	private final int channelIndex, tDim;

	private final int prevChannel;

	private final ImgIO ioFactory;

	private T type;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public SpotDetectorDialog(Image image, ImgIO factory) {

		this.ioFactory = factory;

		setModal(true);



		String[] c = image.channels();
		String [] l = image.getAttribute(Image.cLuts).get();

		//Add + 1 for the overlay
		String [] channels = Arrays.copyOf(c,c.length+1);
		channels[c.length] = "Overlay";
		prevChannel = c.length;

		String [] luts = Arrays.copyOf(l,l.length+1);
		luts[luts.length-1] = Lut8.Yellow.name();


		//Open an image
		Img<T> img;
		try {
			img = ioFactory.open(image);
			System.out.println("SpotDetectorDialog : Image opened... ");
			type = img.firstElement();
		} catch (IOException e) {
			log.error("Unable to open the image in path : "+image.path());
			tDim = -1;
			channelIndex = -1;
			UI.display("Error", "Unable to load the image", e, Level.SEVERE);
			return;
		}



		//Add a channel
		int[] order = image.order();	

		if(image.channelIndex()==-1){//If not multichannel
			System.out.println("Image is not multi channel -> adding 1 channel");
			fullWithPreview = ImgDimensions.addChannel(img, order, 1);
			channelIndex = order[Image.c];
		}
		else{
			channelIndex = image.channelIndex();
			fullWithPreview = ImgDimensions.increaseImg(img, channelIndex, 1);
		}


		System.out.println("Channel Index : "+ channelIndex);
		System.out.println("Channels :" + Arrays.toString(channels));

		img = null;



		view = new ImageViewer(fullWithPreview,ImgDimensions.createColorTables(fullWithPreview,luts), order);
		vf = view.displayWithChannelsController("Spot Detection Preview ",channels);
		vf.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		vf.setAlwaysOnTop(true);
		vf.setLocationRelativeTo(null);


		// Now get the index of the time dimension in the overlay (so channel removed)
		tDim = Image.removeDimension(order, Image.c)[Image.t];





		//Define Controllers and event handlers
		//--------------------------------------------------------------------------------------------

		JLabel waitLabel = new JLabel("");
		waitLabel.setToolTipText("Minimum Signal to noise ratio");



		JButton prevCheck = new JButton("Preview");
		prevCheck.setSelected(false);
		prevCheck.setToolTipText("Preview the result of the spot detector on the selected plane of the image");
		prevCheck.setHorizontalAlignment(SwingConstants.RIGHT);
		prevCheck.addActionListener(e->{
			waitLabel.setText("Please wait, running preview...");
			new Thread(new Runnable(){
				@Override
				public void run() {
					prevCheck.setEnabled(false);
					waitLabel.setText("Number of detected peaks: "+runPreview(image.calibration(false, false)));	
					prevCheck.setEnabled(true);
				}				
			}).start();			
		});


		//Type of ImageDot to use
		JLabel lblObjectType = new JLabel("Channel");
		lblObjectType.setToolTipText("Select the channel to segment");
		typeBox = new JComboBox<>();
		typeBox.setRenderer(new ClassOrLinkRenderer());
		DataTypes.dataTypes()
		.filter(dt->ImageDot.class.isAssignableFrom(dt))
		.forEach(clazz->typeBox.addItem(clazz));


		//Channel
		JLabel label = new JLabel("Type");
		label.setToolTipText("The Type of object which should be associated with the spots");
		channelBox = new JComboBox<>(c);

		JLabel lblMinScale = new JLabel("Radius");
		lblMinScale.setToolTipText("Radius of the structures to detect in pixel coordinates");

		final SpinnerNumberModel radiusModel = new SpinnerNumberModel(radius, 0.01, 500, 0.1);
		final JSpinner radiusField = new JSpinner(radiusModel);
		radiusField.addChangeListener(e -> radius = radiusModel.getNumber().doubleValue());

		JLabel lblMaxScale = new JLabel("Sigma");
		lblMaxScale.setToolTipText("Estimated Sigma of the image in pixel coordinate");

		final SpinnerNumberModel sigmaModel = new SpinnerNumberModel(sigma, 0.01, 10, 0.01);
		final JSpinner sigmaField = new JSpinner(sigmaModel);
		sigmaField.addChangeListener(e -> sigma = sigmaModel.getNumber().doubleValue());

		JLabel lblThreshold = new JLabel("min intensity");
		lblThreshold.setToolTipText("Threshold below which maxima will be ignored");

		final SpinnerNumberModel threshModel = new SpinnerNumberModel(threshold, 0, 65000, 1);
		final JSpinner threshField = new JSpinner(threshModel);
		threshField.addChangeListener(e -> threshold = threshModel.getNumber().doubleValue());

		JCheckBox medianBox = new JCheckBox("Apply Median Filter");
		medianBox.setSelected(applyMedian);
		medianBox.addActionListener(e-> {
			applyMedian = medianBox.isSelected();
		});


		JCheckBox minimaCheckBox = new JCheckBox("Find Minima");
		minimaCheckBox.setSelected(findMinima);
		minimaCheckBox.addActionListener(e-> {
			findMinima = minimaCheckBox.isSelected();
		});
		minimaCheckBox.setToolTipText("Tick this box if the spots to detect are dark objects over a bright background");



		JButton runBtn = new JButton("Run");
		runBtn.addActionListener(e->{
			wasCancelled = false;
			//cleanup
			view = null;
			fullWithPreview = null;
			prevInterval = null;
			peaks = null;
			vf.dispose();
			this.dispose();
		});


		JButton cancelBtn = new JButton("Cancel");
		cancelBtn.addActionListener(e->{
			wasCancelled = true;
			//cleanup
			view = null;
			fullWithPreview = null;
			prevInterval = null;
			peaks = null;
			vf.dispose();
			this.dispose();
		});


		//Layout UI
		//--------------------------------------------------------------------------------------------

		getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Spot Detecor Options", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(panel, BorderLayout.CENTER);



		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel.createSequentialGroup()
										.addGap(161)
										.addComponent(minimaCheckBox))
								.addComponent(medianBox)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
										.addGroup(gl_panel.createSequentialGroup()
												.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
														.addComponent(lblThreshold)
														.addComponent(lblMaxScale)
														.addComponent(lblMinScale)
														.addComponent(label, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
														.addComponent(lblObjectType))
												.addGap(92)
												.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
														.addComponent(threshField, 132, 132, 132)
														.addComponent(sigmaField, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
														.addComponent(radiusField, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
														.addComponent(typeBox, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
														.addComponent(channelBox, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)))
										.addGroup(gl_panel.createSequentialGroup()
												.addComponent(prevCheck)
												.addGap(29)
												.addComponent(waitLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
						.addContainerGap(135, Short.MAX_VALUE))
				);
		gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel.createSequentialGroup()
										.addGap(20)
										.addComponent(channelBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel.createSequentialGroup()
										.addGap(25)
										.addComponent(lblObjectType)))
						.addGap(18)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(label)
								.addComponent(typeBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(16)
						.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
								.addGroup(gl_panel.createSequentialGroup()
										.addComponent(radiusField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
										.addGap(6))
								.addGroup(gl_panel.createSequentialGroup()
										.addComponent(lblMinScale)
										.addGap(11)))
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(sigmaField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblMaxScale))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(threshField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblThreshold))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(medianBox)
								.addComponent(minimaCheckBox))
						.addGap(103)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel.createSequentialGroup()
										.addPreferredGap(ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
										.addComponent(prevCheck)
										.addContainerGap())
								.addGroup(gl_panel.createSequentialGroup()
										.addGap(23)
										.addComponent(waitLabel)
										.addContainerGap())))
				);
		gl_panel.linkSize(SwingConstants.VERTICAL, new Component[] {waitLabel, prevCheck});
		panel.setLayout(gl_panel);

		JPanel okCancelPanel = new JPanel();
		getContentPane().add(okCancelPanel, BorderLayout.SOUTH);


		GroupLayout gl_okCancelPanel = new GroupLayout(okCancelPanel);
		gl_okCancelPanel.setHorizontalGroup(
				gl_okCancelPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_okCancelPanel.createSequentialGroup()
						.addContainerGap(152, Short.MAX_VALUE)
						.addComponent(runBtn)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(cancelBtn)
						.addContainerGap())
				);
		gl_okCancelPanel.setVerticalGroup(
				gl_okCancelPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_okCancelPanel.createSequentialGroup()
						.addGroup(gl_okCancelPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(runBtn)
								.addComponent(cancelBtn))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);
		okCancelPanel.setLayout(gl_okCancelPanel);
	}




	private int runPreview(double[]  cal) {


		//If not the first preview, erase the previous one
		if(prevInterval!=null)
			Views.iterable(prevInterval).forEach(t->t.setZero());


		// Now create the interval on which to run the preview		
		IntervalView<T> slice = Views.hyperSlice(fullWithPreview, channelIndex, channelBox.getSelectedIndex()); //cut along selected channel
		if(tDim!=-1){
			slice = Views.hyperSlice(slice, tDim, view.getCurrentFrame()); //cut along frame if we have a time lapse
		}


		long[] min = new long[slice.numDimensions()];
		long[] max = new long[slice.numDimensions()];
		slice.dimensions(max);
		for(int i = 0; i<max.length; i++)
			max[i]--;

		view.getDisplayedInterval(min,max);
		for(int i = 0; i<max.length; i++)
			max[i]-=min[i];


		if(max[0] <= 0 || max[1] <= 0){
			JOptionPane.showMessageDialog(null, "The view is outside the bounds of the image!");
			return 0;
		}

		IntervalView<T> plane = Views.offsetInterval(slice, min, max); // Preview only on the displayed interval


		System.out.println("Initiating DoG detection");
		System.out.println("Plane dimensions = "+ plane.numDimensions());
		System.out.println("Apply Median = "+ applyMedian);
		System.out.println("Find Minima = "+ findMinima);
		System.out.println("Spot Radius = "+ radius);
		System.out.println("Sigma = "+ sigma);
		System.out.println("Threshold = "+ threshold);

		peaks = DogDetect.detect(plane, applyMedian, findMinima, radius, sigma, threshold, cal);

		log.info("Number of detected peaks : "+peaks.size());


		// Get the preview channel (also works if single channel due to preview channel added to the preview)
		RandomAccessibleInterval<T> prevSlice = Views.hyperSlice(fullWithPreview, channelIndex, prevChannel);
		if(tDim!=-1)
			prevSlice = Views.hyperSlice(prevSlice, tDim, view.getCurrentFrame());

		prevInterval = Views.offsetInterval(prevSlice, min, max);		


		RandomAccess<T> previewAccess = Views.extendZero(prevInterval).randomAccess();		
		long[] l = new long[previewAccess.numDimensions()];
		T ovl = type.createVariable();
		ovl.setReal(255f);
		for(Point p : peaks){
			//p.move(min[0],0);
			//p.move(min[1],1);
			p.localize(l);			
			ImgGeometry.drawCircle(l, 3, previewAccess, ovl);
			previewAccess.setPosition(p);
			previewAccess.get().setReal(255f);
		}

		view.adjustContrast(prevChannel);

		return peaks.size();
	}




	public ArrayList<Point> run(RandomAccessibleInterval<T> plane, double[] cal){
		System.out.println("Initiating DoG detection");
		System.out.println("Plane dimensions = "+ plane.numDimensions());
		System.out.println("Apply Median = "+ applyMedian);
		System.out.println("Spot Radius = "+ radius);
		System.out.println("Sigma = "+ sigma);
		System.out.println("Threshold = "+ threshold);
		return DogDetect.detect(plane, applyMedian, findMinima, radius, sigma, threshold, cal);

	}




	@Override
	public void setVisible(boolean b){
		vf.setLocationRelativeTo(null);
		vf.setVisible(true);
		pack();
		this.setLocation(vf.getLocation().x-this.getWidth(), vf.getLocation().y);		
		super.setVisible(b);
	}


	boolean wasCancelled(){
		return wasCancelled;
	}


	public double getRadius() {
		return radius;
	}

	public double getThreshold(){
		return threshold;
	}

	public double getSigma(){
		return sigma;
	}

	public long selectedChannel(){
		return channelBox.getSelectedIndex();
	}

	public ImageDot getPrototype() throws InstantiationException, IllegalAccessException{
		return (ImageDot) ((Class<?>) typeBox.getSelectedItem()).newInstance();
	}
}
