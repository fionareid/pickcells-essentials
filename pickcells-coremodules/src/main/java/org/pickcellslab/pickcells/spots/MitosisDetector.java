package org.pickcellslab.pickcells.spots;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.Meta;
import org.pickcellslab.foundationj.dbm.modules.SessionConsumer;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.app.modules.Analysis;
import org.pickcellslab.pickcells.api.app.ui.UI;
import org.pickcellslab.pickcells.api.datamodel.conventions.DataModel;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Centrosome;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageDot;
import org.pickcellslab.pickcells.api.img.io.ImgIO;

@MetaInfServices(Module.class)
public class MitosisDetector extends AbstractAnalysis implements Analysis, SessionConsumer{

	private Logger log = Logger.getLogger(MitosisDetector.class);

	private boolean isActivable = false;


	//Session reference
	private DataAccess<?,?> session;
	private final ImgIO ioFactory;



	public MitosisDetector(ImgIO factory){
		this.ioFactory = factory;
	}


	@Override
	public void sessionClosed(DataAccess<?,?> source) {
		session = null;
		isActivable = false;
		this.fireIsNowActive();
	}




	@Override
	public void setSession(DataAccess<?,?> session) {
		this.session = session;
		session.addSessionListener(this);		
		isActivable =  Meta.get().getMetaModel("Centrosome") == null;
		this.fireIsNowActive();
	}



	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}

	@Override
	public String name() {
		return "Mitosis Detector";
	}

	@Override
	public String description() {
		return "Identifies Mitosis events in images";
	}



	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void launch() {

		try{

			//Get images names
			//Read one image to obtain the name and luts of the channels
			List<String> rt =
					session.queryFactory().read(Image.class)
					.makeList(Keys.name).inOneSet().getAll().run();

			if(rt.isEmpty()){
				JOptionPane.showMessageDialog(null, "There are no images in the database");
				return;
			}
			
			//Display a dialog to allow the user to choose which image to load for preview
			String choice = (String) JOptionPane.showInputDialog(
					null,
					"Choose an image to preview",
					"Mitosis Detection...",
					JOptionPane.PLAIN_MESSAGE,
					icon(),
					rt.toArray(),
					rt.get(0)
					);
			
			
			if(choice == null)
				return;


			System.out.println("Choice = "+choice);
			
			setStep(0);


			//Now show the mitosis dialog

			// User inputs
			MitosisDialog dialog = new MitosisDialog(session, ioFactory, rt.toArray(new String[rt.size()]),choice);
			dialog.setVisible(true);

			if(dialog.wasCancelled()){
				setCancelled();
				return;
			}

			//Now run for every image:

			Consumer<Image> detector = dialog.getMethod();

			setStep(1);
			int count = 1;

			for(String name : rt){

				Image i = 
						session.queryFactory()
						.regenerate(Image.class)
						.toDepth(1)
						.traverseLink(ImageDot.origin,Direction.INCOMING)//To Centrosomes
						.includeAllNodes()
						.regenerateAllKeys()
						.doNotGetAll().useFilter(P.keyTest(Keys.name, P.stringEquals(name)))
						.run().getOneTarget(Image.class).get();

				if(dialog.wasPreviewed(i))
					saveImage(dialog.getPreviewedImage());
				else{
					detector.accept(i);
					saveImage(i);
				}

				setProgress((float)++count/(float)rt.size());
			}	

			setStep(2);

		}
		catch(DataAccessException e ){
			UI.display("Error", "An error while processing images occured, see the log", e, Level.WARNING);
		}


	}






	private void saveImage(Image i) {

		try {
			session.queryFactory().store().add(i)
			.feed(MitoticSpots.skew, new double[]{1,1,1}, "Mitoses Detector", 
					DataModel.DIRECTIONAL+" vector which summarises the anisotropy of "
							+ "an expanded region with respect to the origin", Centrosome.class, "Centrosome")
			.feed(MitoticSpots.volume,1, "Mitoses Detector", " The volume of the expanded region", Centrosome.class,"Centrosome")
			.run();

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//TODO  Feed on link when this will be possible

	}

	@Override
	public boolean isActive() {
		return isActivable;
	}



	@Override
	public Object[] categories() {
		return new String[]{"Segmentation"};
	}

	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/Mitoses_24.png"));
	}


	@Override
	public Icon[] icons() {
		return new Icon[3];
	}


	@Override
	public String[] steps() {
		return new String[]{
				"Inputs",
				"Finding Mitoses",
				"Done"
		};
	}


}
