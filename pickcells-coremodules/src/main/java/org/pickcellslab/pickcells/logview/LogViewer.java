package org.pickcellslab.pickcells.logview;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.pickcells.api.app.modules.DefaultDocument;
import org.pickcellslab.pickcells.api.app.ui.Icons;

@MetaInfServices(Module.class)
public class LogViewer extends AppenderSkeleton implements DefaultDocument {

	private static final Logger log = Logger.getLogger(LogViewer.class);

	private static JTextPane pane;
	private static StyledDocument doc;
	private static Map<Level,Style> styles;
	private static Map<Level,Style> icons;

	public LogViewer(){

		
		pane = new JTextPane();
		doc = pane.getStyledDocument();
		styles = new HashMap<>();
		icons = new HashMap<>();

		Font font = new Font("Serif", Font.BOLD, 16);		
		pane.setFont(font);
		pane.setEditable(false);

		//Setup the different style for each level
		Style debug = pane.addStyle("debug", null);
		StyleConstants.setForeground(debug, Color.GRAY);
		styles.put(Level.DEBUG, debug);

		//Info
		Style info = pane.addStyle("info", null);
		StyleConstants.setForeground(info, Color.BLACK);	
		styles.put(Level.INFO, info);

		Style infoIcon =  pane.addStyle("info_icon", null);
		StyleConstants.setIcon(infoIcon, Icons.info_16());
		icons.put(Level.INFO, infoIcon);

		//Warning
		Style warn = pane.addStyle("warn", null);
		StyleConstants.setForeground(warn, Color.ORANGE);	
		styles.put(Level.WARN, warn);

		Style warnIcon =  pane.addStyle("warn_icon", null);
		StyleConstants.setIcon(warnIcon, Icons.warn_16());
		icons.put(Level.WARN, warnIcon);

		//Error
		Style error = pane.addStyle("error", null);
		StyleConstants.setForeground(error, Color.RED);	
		styles.put(Level.ERROR, error);

		Style errorIcon =  pane.addStyle("error_icon", null);
		StyleConstants.setIcon(errorIcon, Icons.error_16());
		icons.put(Level.ERROR, errorIcon);


		


	}

	@Override
	public Component getScene() {
		JScrollPane scroll = new JScrollPane(pane);
		scroll.setName("Events Log");
		return scroll;
	}

	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}

	@Override
	public String name() {
		return "Event Log";
	}

	@Override
	public void close() {
		//Just clear the log
		pane.setText("");
	}

	@Override
	public boolean requiresLayout() {
		return false;
	}


	@Override
	protected void append(LoggingEvent loggingEvent) {


		System.out.println("Logging event received :"+loggingEvent.getMessage());

		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run() {
				try {	
					doc.insertString(doc.getLength(), "\n",null);
					doc.insertString(doc.getLength(), " ", icons.get(loggingEvent.getLevel()));
					doc.insertString(doc.getLength(), (String) loggingEvent.getMessage(), styles.get(loggingEvent.getLevel()));
				} catch (BadLocationException e) {
					e.printStackTrace();
				}				
			}

		});

	}

	public static void main(String[] args) throws InterruptedException {

		JFrame frame = new JFrame("LogViewer...");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(new LogViewer().getScene());
		frame.pack();
		frame.setVisible(true);

		log.debug("debug");
		Thread.sleep(1000);
		log.info("info");
		Thread.sleep(1000);
		log.warn("warn");
		Thread.sleep(1000);
		log.error("error");

	}

  @Override
  public String description() {
    return "Logs events to a JTextPane";
  }



@Override
public Icon icon() {
	return new ImageIcon(getClass().getResource("/icons/info_16.png"));
}



@Override
public int preferredLocation() {
	return DefaultDocument.DOWN;
}


}
