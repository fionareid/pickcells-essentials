package org.pickcellslab.pickcells.tools;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.Icon;

import org.apache.commons.math3.util.FastMath;
import org.apache.log4j.Logger;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.foundationj.modules.ModuleActivationListener;
import org.pickcellslab.pickcells.api.app.modules.DesktopModule;
import org.pickcellslab.pickcells.api.app.ui.Icons;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.io.ImgsChecker;
import org.pickcellslab.pickcells.api.img.io.ImgsChooser;

import edu.mines.jtk.util.Array;
import net.imglib2.Cursor;
import net.imglib2.Interval;
import net.imglib2.RandomAccess;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.ComplexType;
import net.imglib2.type.numeric.real.FloatType;
import net.imglib2.util.Intervals;
import net.imglib2.util.Util;
import net.imglib2.view.Views;



@MetaInfServices(Module.class)
public class Relabelling<T extends NativeType<T> & ComplexType<T>> implements DesktopModule{

	private static final Logger log = Logger.getLogger(Relabelling.class);

	private ImgIO io;

	private final double maxD = 20;


	public Relabelling(ImgIO io) {

		this.io = io;

	}

	@Override
	public Object[] categories() {
		return new String[]{"ToolBox"};
	}

	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}

	@Override
	public String name() {
		return "Labels Doctor";
	}

	@Override
	public String description() {
		return "Relabels 2 stitched segmented images to ensure that 2 nuclei with the same label on the original images are given a unique color";
	}

	@Override
	public void launch() {


		//Load the image
		ImgsChooser chooser = io.createChooserBuilder().build();
		chooser.setModal(true);
		chooser.setVisible(true);




		ImgFileList list = chooser.getChosenList();

		if(list == null) return;

		ImgsChecker checker = io.createCheckerBuilder()
				.addCheck(ImgsChecker.calibration)
				.addCheck(ImgsChecker.ordering)
				.build(list);

		int[] order = ImgsChecker.convert(checker.value(ImgsChecker.ordering));
		double[] rawCal = checker.value(ImgsChecker.calibration);
		double[] cal = ImgsChecker.convert(order, rawCal);



		//Check dimensions
		for(int i = 0;i<list.numDataSets();i++){

			@SuppressWarnings("unchecked")
			Img<T>[] imgs = new Img[list.numImages(i)];
			long[] offsets = new long[list.numImages(i)];

			long finalWidth = 0;

			for(int j = 0; j<list.numImages(i); j++){

				imgs[j] = list.load(i,j);
				offsets[j] = finalWidth;
				finalWidth += imgs[j].dimension(0);

			}
			finalWidth--;



			//Create the recipient image as a float image
			System.out.println("Final Width : " + (finalWidth+1));
			Interval size = Intervals.createMinMax(new long[]{0,0,0, finalWidth, imgs[0].dimension(1)-1, imgs[0].dimension(2)-1});			
			Img<FloatType> stitched =  Util.getArrayOrCellImgFactory(size,new FloatType()).create(size, new FloatType());


			//Label generator
			AtomicInteger generator = new AtomicInteger();


			//ExecutorService service = Executors.newFixedThreadPool(list.numImages(i));


			//Now relabel original and fill the stitched image

			for(int j = 0; j<list.numImages(i); j++){


				final int index = j;
				final long[] max = new long[3];
				imgs[j].dimensions(max);

				Cursor<T> c = imgs[j].localizingCursor();

				//service.execute( () -> {


				final Map<Float,RegionStats> labels = new HashMap<Float,RegionStats>();	
				final Map<Float, List<long[]>> split = new HashMap<>();

				RandomAccess<FloatType> access = null;
				if(index == 0)
					access = Views.interval(stitched, new long[3], max).randomAccess();
				else
					access = Views.offsetInterval(stitched, new long[]{offsets[index],0,0}, max).randomAccess();


				long[] pos = new long[access.numDimensions()];

				while(c.hasNext()){
					c.fwd();
					float current = c.get().getRealFloat();
					if(current!=0){// if not background

						RegionStats region = labels.get(current);
						c.localize(pos);
						if(region == null){
							region = new RegionStats(generator.incrementAndGet(), pos, rawCal);
							labels.put(current, region);
							access.setPosition(c);
							access.get().setReal(region.getLabel());
						}						
						else if(region.accept(pos, maxD)){
							access.setPosition(c);
							access.get().setReal(region.getLabel());
						}
						else{
							//If not accepted then we need to throw this position in the pool of yet unlabelled
							List<long[]> splitPool = split.get(current);
							if(splitPool == null){
								splitPool = new ArrayList<>();
								split.put(current, splitPool);
							}							
							splitPool.add(Array.copy(pos));
						}
					}
				}
				
				imgs[j] = null;
				c = null;
				
				log.info("Number of split regions in img " +j+ ": "+ split.size());
				
				//Now relabel split regions
				for(List<long[]> pool : split.values()){
					float l = generator.incrementAndGet();
					for(long[] p : pool){
						access.setPosition(p);
						access.get().setReal(l);
					}
				}

				//});

			}


			//service.shutdown();
			//try {
			//	service.awaitTermination(20, TimeUnit.MINUTES);
			//} catch (InterruptedException e1) {
			//	log.error("Error during relabelling", e1);
			//}



			imgs = null;

			log.info("Number of labels : "+generator);


			//Save the stitched image
			long[] dims = new long[stitched.numDimensions()];
			stitched.dimensions(dims);
			MinimalImageInfo info = new MinimalImageInfo(order, dims, cal);
			try {
				io.save(info, stitched, list.path(i)+File.separator+"Relabelled_"+i+".tif");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}//END of Iteration through datasets



	}









	@Override
	public Icon icon() {
		return Icons.info_32();
	}

	@Override
	public void registerListener(ModuleActivationListener lst) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isActive() {
		// TODO Auto-generated method stub
		return false;
	}






	private class RegionStats{

		private final int label;
		private int N = 0;
		private final double[] center;
		private final double[] cal;


		RegionStats(final int label, long[] first, final double[] calibration){
			this.label = label;
			cal = calibration;
			center = new double[cal.length];
			for(int i = 0; i<cal.length; i++)
				center[i] = ((double)first[i]) * cal[i];
		}


		float getLabel(){
			return label;
		}


		/**
		 * Accepts the given position if its distance to the center of this region is less than max.
		 * If accepted center of this region is updated.
		 * @param pos
		 * @param max
		 * @return
		 */
		boolean accept(long[] pos, double max){

			//Calibrate
			double[] p = new double[pos.length];
			for(int i = 0; i<p.length; i++)
				p[i] = ((double)pos[i])*cal[i];

			//Check distance			
			double squares = 0;
			for(int i = 0; i<p.length; i++)
				squares += FastMath.pow(p[i]-center[i], 2);
			if(FastMath.sqrt(squares)>max)
				return false;

			//update center
			for(int i = 0; i<p.length; i++)
				center[i] = (center[i]*N + p[i]) / ++N;


			return true;
		}



	}


	



}
