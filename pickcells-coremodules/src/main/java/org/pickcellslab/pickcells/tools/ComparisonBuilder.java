package org.pickcellslab.pickcells.tools;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

public class ComparisonBuilder {

	private final String creator;
	private final Map<String, SummaryStatistics> stats = new HashMap<>();
	private final Map<String, Number> values = new HashMap<>();
	
	public ComparisonBuilder(Class<?> creator) {
		this.creator = creator.getSimpleName();
	}
	
	/**
	 * Adds the given value into the group of values with name : '{@code name}' for which {@link SummaryStatistics} should be computed.
	 * If the name does not yet exists, a new group will be created.
	 * @param name The name of the group of values
	 * @param value The value to add
	 */
	public void addVariable(String name, double value){
		SummaryStatistics summary = stats.get(name);
		if(summary == null){
			summary = new SummaryStatistics();
			stats.put(name, summary);
		}
		summary.addValue(value);
	}
	
	
	/**
	 * Set a value for a given name. No stats are computed, the value is kept as is.
	 * @param name
	 * @param value
	 */
	public void setValue(String name, Number value){
		values.put(name, value);
	}
	
	public Comparison build(){
		return new Comparison(this);
	}

	
	public static class Comparison{
		
		private final String creator;
		private final Map<String, Number> values;
		
		
		private Comparison(ComparisonBuilder b){
			this.creator = b.creator;			
			this.values = new HashMap<>(b.values);
			b.stats.forEach((k,v)->{
				
				values.put(k+" N", v.getN());
				values.put(k+" Mean", v.getMean());
				values.put(k+" Std", v.getStandardDeviation());
				
			});
		}
		
		
		public String creator(){
			return creator;
		}
		
		
		public List<String> computedMeasures(Modus modus){
			List<String> list = new ArrayList<>(values.keySet());
			Collections.sort(list);
			return list;
		};
		
		public Number getValueFor(String measure){
			return values.get(measure);
		};
		
		
		
	}
	
	
}
