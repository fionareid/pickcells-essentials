package org.pickcellslab.pickcells.tools;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.pickcells.tools.ComparisonBuilder.Comparison;

import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

public class ClusteringComparator implements SegmentationComparator{

	private ComparisonBuilder builder = new ComparisonBuilder(ClusteringComparator.class);

	@Override
	public boolean requiresMatching(){
		return false;
	}

	@Override
	public <T extends RealType<T>> void addTest(RandomAccessibleInterval<T> ref, RandomAccessibleInterval<T> tested) {
		
		long TP = 0;	long FP = 0;
		long TN = 0;	long FN = 0;
		
		Cursor<T> refC = Views.iterable(ref).localizingCursor();
		RandomAccess<T> testA = tested.randomAccess();
		
		T zero = testA.get().createVariable();
		boolean rZero, tZero;
		while(refC.hasNext()){
			refC.fwd();
			testA.setPosition(refC);
			rZero = refC.get().compareTo(zero) == 0;
			tZero = testA.get().compareTo(zero) == 0;
			if(rZero && tZero)
				TN++;
			else if(rZero && !tZero)
				FP++;
			else if(!rZero && tZero)
				FN++;
			else
				TP++;
		}
		
		double RI = ((double) TP + TN) / ((double)(TP+TN+FP+FN));
		double JI = ((double) TP ) / ((double)(TP+FP+FN));
	
		builder.setValue("Rand Index", RI);
		builder.setValue("Jaccard Index", JI);
		
	}

	
	
	@Override
	public Comparison getResult() {
		return builder.build();
	}

	@Override
	public void reset() {
		builder = new ComparisonBuilder(ClusteringComparator.class);
	}

}
