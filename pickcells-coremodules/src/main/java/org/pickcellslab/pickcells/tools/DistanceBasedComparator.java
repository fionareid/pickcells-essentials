package org.pickcellslab.pickcells.tools;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.pickcellslab.pickcells.api.img.process.DistanceTransform;
import org.pickcellslab.pickcells.api.img.process.Outline;
import org.pickcellslab.pickcells.tools.ComparisonBuilder.Comparison;

import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.Img;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

public class DistanceBasedComparator implements SegmentationComparator{

	private static final String NSD = "NSD";
	private static final String Hausdorff = "Hausdorff";
	
	private ComparisonBuilder builder = new ComparisonBuilder(DistanceBasedComparator.class);

	@Override
	public boolean requiresMatching(){
		return true;
	}

	@Override
	public <T extends RealType<T>> void addTest(RandomAccessibleInterval<T> ref, RandomAccessibleInterval<T> tested) {
		
		// Create a copy of the ref and draw the outline only
		final Img<T> refDistance = (Img)Outline.run((RandomAccessibleInterval)ref);
		DistanceTransform.run(refDistance);
		
		// Now iterate over voxels of the symetric difference between the 2 shapes
		final Cursor<T> refCursor = Views.iterable(ref).localizingCursor();
		
		final RandomAccess<T> distRA = refDistance.randomAccess();
		final RandomAccess<T> testRA = tested.randomAccess();
		
		final T zero = distRA.get().createVariable();
		
		double distSum = 0;
		double nsd = 0;
		double hausdorff = 0;
		
		while(refCursor.hasNext()){
			
			T refT = refCursor.next();
			testRA.setPosition(refCursor);
			T testT = testRA.get();
				
			
			if (refT.compareTo(zero)!=0 || testT.compareTo(zero)!=0){ // Union
				distRA.setPosition(refCursor);
				distSum += distRA.get().getRealDouble();
				
				if(refT.compareTo(zero)==0 || testT.compareTo(zero)==0){ // Symetric difference					
					hausdorff = Math.max(hausdorff, distRA.get().getRealDouble());
					nsd += distRA.get().getRealDouble();
				}
			}
		}
		
		nsd /= distSum;
		
		builder.addVariable(NSD, nsd);
		builder.addVariable(Hausdorff, hausdorff);
		
	}

	
	
	@Override
	public Comparison getResult() {
		return builder.build();
	}

	@Override
	public void reset() {
		builder = new ComparisonBuilder(ClusteringComparator.class);
	}
	
}
