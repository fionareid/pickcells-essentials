package org.pickcellslab.pickcells.clustering;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.apache.log4j.Logger;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.modules.SessionConsumer;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.foundationj.queryUI.swing.QueryableChoiceDialog;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.app.ui.UI;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.UsefulQueries;
import org.pickcellslab.pickcells.api.datamodel.types.ClusterableAdapter;
import org.pickcellslab.pickcells.api.datamodel.types.Clustering;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.assign_nc.CentrosomeVectorFinder;

@MetaInfServices(Module.class)
public class EuclidianClustering extends AbstractAnalysis implements SessionConsumer {

	private Logger log = Logger.getLogger(CentrosomeVectorFinder.class);

	private boolean isActivable = true;
	private boolean isRunning = false;
	private DataAccess<?,?> session;

	private int numImages;

	private static final int cancel = 0, ok = 1, redef = 2;





	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}
	@Override
	public String name() {
		return "Clustering";
	}
	@Override
	public String description() {
		return "Uses the DBScan algorithm to cluster objects in space";
	}




	@Override
	public void launch() {

		try {

			isRunning = true;

			setStep(0);


			//Get images in experiment
			List<Image> images = null;
			try {
				images = session.queryFactory()
						.regenerate(Image.class).toDepth(0)
						.traverseAllLinks().includeAllNodes()
						.regenerateAllKeys().getAll().getTargets(Image.class).collect(Collectors.toList());
			} catch (DataAccessException e) {
				log.error("Unable to read the database");
				setFailure("Unable to read the database");
				return;
			}


			if(images.isEmpty()){
				JOptionPane.showMessageDialog(null,"There are no data in the experiment");
				log.info("Euclidian clustering was cancelled");
				setFailure("No image in the database");
				return;
			}


			QueryableChoiceDialog<MetaClass> sd =new QueryableChoiceDialog<>(
					session,
					"Choose which objects to cluster",
					(mq) -> {
						if(!MetaClass.class.isAssignableFrom(mq.getClass()))
							return false;
						return mq.hasKey(Keys.centroid);
					}, false);

			sd.setModal(true);
			sd.pack();
			sd.setLocationRelativeTo(null);
			sd.setVisible(true);



			Optional<MetaClass> sOpt = sd.getChoice();

			if(!sOpt.isPresent()){
				log.info("Euclidian clustering was cancelled");
				setCancelled();
				return;
			}



			String name = JOptionPane.showInputDialog("Give a name to this Clustering", "DBSCAN");
			if(name == null) {
				log.info("Euclidian clustering was cancelled");
				setCancelled();
				return;
			}


			
			
			//Display a dialog to allow the user to choose which image to load for preview
			Image choice = (Image) JOptionPane.showInputDialog(
					null,
					"Choose an image to load for the preview",
					"DBSCAN...",
					JOptionPane.PLAIN_MESSAGE,
					icon(),
					images.toArray(),
					images.get(0)
					);
	
			if(choice == null){
				setCancelled();
				return;
			}



			@SuppressWarnings("unchecked")
			List<ImageLocated> sources =
			UsefulQueries.imageToImageLocated(
					session,
					sOpt.get(),
					choice.getAttribute(WritableDataItem.idKey).get())
			.getAllItemsFor(ImageLocated.class).collect(Collectors.toList());

			//Adapt sources to Clusterable objects
			List<ClusterableAdapter<ImageLocated>> list = 
					sources.stream()
					.map(s->new ClusterableAdapter<>(s,(o)->o.centroid()))
					.collect(Collectors.toList());

			DBSCANDialog<ClusterableAdapter<ImageLocated>> dialog = new DBSCANDialog<>(list);
			dialog.setModal(true);
			dialog.pack();
			dialog.setLocationRelativeTo(null);
			dialog.setVisible(true);

			if(dialog.wasCancelled()){
				setCancelled();
				return;
			}

			
			
			//debug only

			try {
				session.queryFactory().delete(Clustering.class).completely().getAll().run();
				session.queryFactory().delete(org.pickcellslab.pickcells.api.datamodel.types.Cluster.class).completely().getAll().run();
			} catch (DataAccessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			
			
			



			//Create 1 clustering node for all clusters
			Clustering clustering = new Clustering(name,"DBSCAN","minPts = "+dialog.minPts()+" ; eps = "+dialog.eps());

			int counter = 0;
			for(Image i : images){

				setStep(++counter);


				sources = 
						UsefulQueries.imageToImageLocated(
								session,
								sOpt.get(),
								i.getAttribute(WritableDataItem.idKey).get())
						.getAllItemsFor(ImageLocated.class).collect(Collectors.toList());


				//Adapt sources to Clusterable objects
				list = sources.stream()
						.map(s->new ClusterableAdapter<ImageLocated>(s,(o)->o.centroid()))
						.collect(Collectors.toList());

				DBSCANClusterer<ClusterableAdapter<ImageLocated>> clusterer = new DBSCANClusterer<>(dialog.eps(),dialog.minPts());
				List<Cluster<ClusterableAdapter<ImageLocated>>> clusters = clusterer.cluster(list);

				int id = 0;
				for(Cluster<ClusterableAdapter<ImageLocated>> c : clusters)				
					new org.pickcellslab.pickcells.api.datamodel.types.Cluster(i, clustering,
							c.getPoints().stream().map(a->a.object()).collect(Collectors.toList()), id++);				


			}


			//Store to the database
			try {
				session.queryFactory().store().add(clustering).run();

				/*
			//Create default filters

			// To Find this clustering
			ExplicitPredicate<DataItem> p = P.keyTest(DataItem.idKey,
					P.equalsTo(clustering.getAttribute(DataItem.idKey).get()));
			MetaFilter fitF = 
					new MetaFilter(
							name,
							"Clustering Analysis",
							"Find a Clustering with name : " + name,
							p, Subtype.ATTRIBUTES, Meta.getMetaModel(Clustering.class, session));
			session.runQuery(QueryFactory.meta(fitF));
				 */
				// To Find each individual cluster


			} catch (DataAccessException e) {
				JOptionPane.showMessageDialog(null,"An error occured while saving to the database");
				e.printStackTrace();
			}

			setStep(numImages+1);
			isRunning = false;

			

		}catch(Exception e){
			UI.display("Error", "An exception occured while running DBSCAN", e, Level.WARNING);
		}
		
		
	}

		



		@Override
		public boolean isActive() {
			return isActivable;
		}




		@Override
		public Object[] categories() {
			return new String[]{"Clustering"};
		}

		@Override
		public Icon icon() {
			return new ImageIcon(getClass().getResource("/icons/Cluster.png"));
		}



		private class Choice{

			private int c;

			Choice(int c){
				this.c = c;
			}

			void set(int o){
				c = o;
			}

			int get(){
				return c;
			}
		}


		@Override
		public void sessionClosed(DataAccess<?,?> source) {
			session = null;
		}




		@Override
		public void setSession(DataAccess<?,?> session) {
			this.session = session;
			session.addSessionListener(this);

			//Check if Objects have centroids in the database
			isActivable = !Meta.get().getQueryable(p->p.hasKey(Keys.centroid)).isEmpty();

			if(isActivable){
				//Get the number of images
				try {
					numImages = session.queryFactory().read(Image.class).makeList(Keys.name).inOneSet().getAll().run().size();
				} catch (DataAccessException e) {
					UI.display("Error", "Euclidian Clustering unable to read the database", e, Level.SEVERE);
					isActivable = false;
				}
			}		
			this.fireIsNowActive();		
		}



		@Override
		public Icon[] icons() {
			return new Icon[3];
		}



		@Override
		public String[] steps() {		
			String[]  steps = new String[numImages+2]; 
			for(int i = 0; i<numImages; i++)
				steps[i+1] = "I"+i;
			steps[0] = "Preview";
			steps[steps.length-1] = "Done";
			return steps;
		}










	}
