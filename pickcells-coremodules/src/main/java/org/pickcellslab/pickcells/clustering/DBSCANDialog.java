package org.pickcellslab.pickcells.clustering;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.Clusterable;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.pickcellslab.pickcells.api.app.charts.Charts;
import org.pickcellslab.pickcells.api.app.charts.ImplementationUnavailableException;
import org.pickcellslab.pickcells.api.app.charts.SimpleChart;
import org.pickcellslab.pickcells.api.app.charts.SimpleScatter2D;
import org.pickcellslab.pickcells.api.app.charts.SimpleScatter3D;
import org.pickcellslab.pickcells.api.app.charts.SimpleSeries;

import com.alee.extended.panel.GroupPanel;

@SuppressWarnings("serial")
public class DBSCANDialog<T extends Clusterable> extends JDialog {

	private JTextField minField;
	private JTextField epsField;

	private int minPts = 20;
	private double eps = 10;
	private boolean wasCancelled = true;;



	public DBSCANDialog(List<T> points) throws ImplementationUnavailableException {





		// Controllers for user inputs	

		JLabel lblMin = new JLabel("Min Number of Points in cluster : ");

		minField = new JTextField(""+minPts);
		minField.setColumns(10);

		JLabel lblRadius = new JLabel("Radius of search for each point :   ");

		epsField = new JTextField(""+eps);
		epsField.setColumns(10);

		//Chart to display the result
		SimpleChart<double[]> chart = null;

		if(points.get(0).getPoint().length == 3)
			chart = Charts.createSimpleChart(SimpleScatter3D.class);
		else
			chart = Charts.createSimpleChart(SimpleScatter2D.class);

		//Fill the chart with all the points
		Cluster<T> cluster = new Cluster<>();
		for(int i = 0; i< points.size(); i++){
			cluster.addPoint(points.get(i));
		}
		final ClusterSeries all = new ClusterSeries("zero ", cluster);
		chart.getModel().addSeries(all);

		JButton prevButton = new JButton("preview");
		JLabel info = new JLabel();
		
		final SimpleChart<double[]> fChart = chart;
		
		prevButton.addActionListener(l->{

			try{
				minPts = Integer.parseInt(minField.getText());
				eps = Double.parseDouble(epsField.getText());
			}catch(NumberFormatException e ){
				JOptionPane.showMessageDialog(null, "Inputs are invalid");
				return;
			}

			// First remove previous clusters
			new ArrayList<>(fChart.getModel().getSeries())
			.forEach(s->{
				if(s!=all)
					fChart.getModel().removeSeries(s.id());
			});

			// Now run the clustering
			DBSCANClusterer<T> clusterer = new DBSCANClusterer<>(eps,minPts);
			List<Cluster<T>> clusters = clusterer.cluster(points);

			for(int i = 0; i< clusters.size(); i++){
				ClusterSeries series = new ClusterSeries("Cluster "+i, clusters.get(i));
				fChart.getModel().addSeries(series);
			}
			info.setText("Number of Clusters : "+clusters.size());

		});



		JButton okButton = new JButton("OK");
		okButton.addActionListener(l->{
			try{
				minPts = Integer.parseInt(minField.getText());
				eps = Double.parseDouble(epsField.getText());
			}catch(NumberFormatException e ){
				JOptionPane.showMessageDialog(null, "Inputs are invalid");
				return;
			}
			wasCancelled = false;
			this.dispose();
		});


		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(l->this.dispose());
		getRootPane().setDefaultButton(cancelButton);




		// =========================================================================================================
		// Layout

		setIconImage(new ImageIcon(getClass().getResource("/icons/Cluster.png")).getImage());
		setTitle("DBSCAN Clustering...");

		getContentPane().setLayout(new BorderLayout(0, 0));

		GroupPanel row1 = new GroupPanel(lblMin, minField);
		GroupPanel row2 = new GroupPanel(lblRadius, epsField);
		JPanel row3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		row3.add(prevButton);
		row3.add(info);

		GroupPanel inputPanel = new GroupPanel(false, row1, row2, row3);
		inputPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		getContentPane().add(inputPanel, BorderLayout.NORTH);

		Component chartPanel = chart.getScene();
		getContentPane().add(chartPanel, BorderLayout.CENTER);

		JPanel okCancelPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		okCancelPanel.add(okButton);
		okCancelPanel.add(cancelButton);
		getContentPane().add(okCancelPanel, BorderLayout.SOUTH);






	}


	public boolean wasCancelled(){
		return wasCancelled;
	}

	public int minPts(){
		return minPts;
	}


	public double eps(){
		return eps;
	}


	private class ClusterSeries implements SimpleSeries<double[]>{

		private final String id;
		private final Cluster<T> cluster;

		public ClusterSeries(String id, Cluster<T> cluster) {
			this.id = id;
			this.cluster = cluster;
		}

		@Override
		public String id() {
			return id;
		}

		@Override
		public void addObservation(double[] value) {
			// Not implemented
		}

		@Override
		public int numValues() {
			return cluster.getPoints().size();
		}

		@Override
		public double[] getValue(int i) {
			return cluster.getPoints().get(i).getPoint();
		}

	}



}
