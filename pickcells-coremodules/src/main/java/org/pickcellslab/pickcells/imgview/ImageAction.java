package org.pickcellslab.pickcells.imgview;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.pickcellslab.foundationj.datamodel.DataTypes;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.Path;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.ImageDot;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;
import org.pickcellslab.pickcells.api.img.providers.SegmentationManagementException;
import org.pickcellslab.pickcells.api.img.providers.SegmentationManager;
import org.pickcellslab.pickcells.api.img.view.AnnotationAppearance;

import net.imglib2.RandomAccess;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.ExtendedRandomAccessibleInterval;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;




public class ImageAction<T extends RealType<T> & NativeType<T>> implements Action<Path<NodeItem,Link>,Void> {

	//private static final Logger log = Logger.getLogger(ImageAction.class);

	private final AnnotationAppearance colors;

	private final SegmentationManager segs;

	private final Map<Object,ImageMechanism> coords = new HashMap<>();
	private final boolean linkOnly;
	private final int tDim;

	private final ExtendedRandomAccessibleInterval<T,?> view;

	private final T typeFactory;

	private final Map<String, Map<Float, Integer>> ids;



	public ImageAction(IntervalView<T> view, SegmentationManager manager, AnnotationAppearance colors, Map<String,Map<Float,Integer>> ids, boolean linkOnly, int timeDim) {
		this.view = Views.extendZero(view);
		this.ids = ids;
		this.segs = manager;
		this.colors = colors;
		this.linkOnly = linkOnly;
		this.tDim = timeDim;
		this.typeFactory = view.firstElement();
		//System.out.println("Link Only? "+linkOnly);
	}

	@Override
	public ActionMechanism<Path<NodeItem, Link>> createCoordinate(Object key) {
		ImageMechanism m = new ImageMechanism(Objects.toString(key));
		coords.put(Objects.toString(key),m);
		return m;
	}

	@Override
	public Set<Object> coordinates() {
		return coords.keySet();
	}



	@Override
	public Void createOutput(Object key) throws IllegalArgumentException {
		return null;
	}

	@Override
	public String description() {
		return "Draw objects on an image";
	}






	private class ImageMechanism implements ActionMechanism<Path<NodeItem,Link>>{


		final Map<String, String> series = new HashMap<>();
		final Set<WritableDataItem> processed = new HashSet<>(); 
		final RandomAccess<T> access;
		private String k;


		public ImageMechanism(String k) {
			this.k = k;
			access = view.randomAccess();
		}



	
		private String getSeriesID(String type){
			String id = series.get(type);
			if(id==null){
				id = k+" "+type;
				series.put(type,id);
			}
			return id;
		}



		@Override
		public void performAction(Path<NodeItem, Link> path) throws DataAccessException {
			try {
				

				//Get the last node
				if(!linkOnly){
					NodeItem node = path.last();
					if(processed.add(node)){
						
						String typeId = node.typeId();
						String declaredType = node.declaredType();
						
						Class<? extends NodeItem> clazz = DataTypes.classOf(typeId);

						if(ImageDot.class.isAssignableFrom(clazz)){	
							T app = typeFactory.createVariable();
							app.setReal(colors.getAppearance(getSeriesID(declaredType)));
							createDot(node, app);
						}
						else{
							createOutline(declaredType, node, colors.getAppearance(getSeriesID(declaredType)));
						}
					}
				}

				//Get the last link
				Link l = path.lastEdge();
				if(l != null ){
					createLink(l);
				}

			} catch (SegmentationManagementException e) {
				throw new DataAccessException("",e);
			}

		}



		private void createLink(Link l) {

			//origin
			long[] o = l.source().getAttribute(Keys.location).orElse(null);
			if(o == null)
				return;
			
			if(tDim!=-1)
				access.setPosition(l.target().getAttribute(ImageLocated.frameKey).orElse(0), tDim); // target to see trace in time lapse

			long[] h = l.target().getAttribute(Keys.location).orElse(null);
			if(h == null)
				return;
			
			int app = colors.getAppearance(getSeriesID(l.declaredType()));

			//Create a vector
			double[] v = new double[o.length];
			double sumSqr = 0;
			for(int i = 0; i<o.length; i++){
				v[i] = o[i]-h[i];
				sumSqr+=Math.pow(v[i], 2);
			}
			//Normalize
			double d = (int) Math.sqrt(sumSqr);
			for(int i = 0; i<o.length; i++)
				v[i]/=d;

			//System.out.println("ImageAction : Vector : "+Arrays.toString(v));
			//System.out.println("ImageAction : Length : "+d);

			long[] p = new long[o.length];

			for(long x = 0; x<Math.ceil(d); x++){
				for(int i = 0; i<o.length; i++){
					p[i] = (long) (h[i] + x*v[i]);
				}
				access.setPosition(p);				
				access.get().setReal(app);
				if(p.length==3){ // if 3d
					p[2] = p[2]-1;
					access.setPosition(p);				
					access.get().setReal(app);
					p[2] = p[2]+2;
					access.setPosition(p);				
					access.get().setReal(app);
				}
			}

		}




		private void createOutline(String clazz, NodeItem node, Integer appearanceCode) throws SegmentationManagementException {

			if(tDim!=-1)
				access.setPosition(node.getAttribute(ImageLocated.frameKey).orElse(0), tDim);

			segs.getFor(clazz).processOutline(
					node.getAttribute(Keys.bbMin).get(),
					node.getAttribute(Keys.bbMax).get(),
					node.getAttribute(SegmentedObject.labelKey).get(),
					node.getAttribute(ImageLocated.frameKey).orElse(0),
					(p)->{
						access.setPosition(p);
						access.get().setReal(appearanceCode);
					});

			Map<Float,Integer> map = ids.get(clazz);
			if(map == null){
				map = new HashMap<>();
				ids.put(clazz, map);
			}
			map.put(node.getAttribute(SegmentedObject.labelKey).get(), node.getAttribute(WritableDataItem.idKey).get());
		}

		
		

		private void createDot(NodeItem node, T appearance) {

			if(tDim!=-1)
				access.setPosition(node.getAttribute(ImageLocated.frameKey).orElse(0), tDim);

			long[] center = node.getAttribute(Keys.location).get();
			int radius = 4;			
			ImgGeometry.drawCircle(center, radius, access, appearance);
			if(center.length==3){ 
				center[2] = center[2]-1;
				ImgGeometry.drawCircle(center, radius, access, appearance);
				center[2] = center[2]+2;
				ImgGeometry.drawCircle(center, radius, access, appearance);
			}


		}



	}

}







