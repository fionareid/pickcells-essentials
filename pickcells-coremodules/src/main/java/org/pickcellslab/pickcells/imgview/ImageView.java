package org.pickcellslab.pickcells.imgview;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.foundationj.queryUI.config.QueryConfigs;
import org.pickcellslab.pickcells.api.app.charts.DBViewFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.charts.ViewFactoryException;
import org.pickcellslab.pickcells.api.app.modules.DefaultUIDocument;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;
import org.pickcellslab.pickcells.api.app.ui.Icons;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.img.view.AnnotationAppearanceDialog;
import org.pickcellslab.pickcells.api.img.view.ImageContentModel;







@MetaInfServices(Module.class)
public class ImageView implements DBViewFactory{ 


	private final ImgIO io;
	private final ProviderFactoryFactory pff;




	public ImageView(ImgIO io, ProviderFactoryFactory pff) {
		this.io = io;
		this.pff = pff;
	}


	@Override
	public String toString() {
		return "Image Viewer";
	}




	@Override
	public String description() {
		return "This module lets you load a specific image to visualise and select objects in the image";
	}



	@Override
	public ViewType getType() {
		return ViewType.Numerical;
	}




	@Override
	public String name() {
		return "Image View";
	}




	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}




	@Override
	public UIDocument createView(DataAccess<?, ?> access) throws ViewFactoryException {

		/*
		// 1- Ask the user which image to load if more than one image
				List<String> images = null;

				try {

					images = access.queryFactory().read(Image.class).makeList(Keys.name).inOneSet().getAll().run();

				} catch (DataAccessException e) {
					throw new ViewFactoryException("Unable to access the database",e);
				}

				String choice = null;
				if(images.isEmpty()){
					JOptionPane.showMessageDialog(null, "There are no image loaded into this experiment");
					return null;
				}

				choice = images.get(0);

				if(images.size()>1){	
					choice =
				(String) JOptionPane.showInputDialog(null, 
						"Select the image to display",
						"Image View...",
						JOptionPane.PLAIN_MESSAGE,
						null,
						images.toArray(), 
						choice);		
				}

				if(choice == null)
					return null;

		 */

		//Create the model
		ImageContentModel model = new ImageContentModel(access, pff);

		//Create the view
		ImgView<?> view = new ImgView<>(io, model);

		// Create Controllers
		JPanel toolBar = DBViewHelp.newToolBar()		 
				.addChangeDataSetButton(() -> QueryConfigs.newPathOnlyConfig("")
						.setSynchronizeRoots(true)
						.setFixedRootKey(ImageContentModel.fixedRootKey(access))
						.setAllowedQueryables(ImageContentModel.renderableTypes())
						.build(),
						view.getModel())
				.addButton(new ImageIcon(getClass().getResource("/icons/contrast.png")), "Brightness and Contrast", l->view.showChannelControl())				
				
				// Color Control
				.addButton(DBViewHelp.paletteIcon(), "Colors...", 
						l->{
							if(view.getAppearance().registeredSeries().isEmpty()){
								JOptionPane.showMessageDialog(null, "There is no dataset loaded yet");
								return;
							}
							AnnotationAppearanceDialog dg = new AnnotationAppearanceDialog(view.getAppearance());
							dg.pack();
							dg.setLocationRelativeTo(null);
							dg.setModal(true);
							dg.setVisible(true);
							
						})	
				
				// Picking Control
				.addButton(DBViewHelp.singleMultiPickerIcons()[0], "Choose the picking target type", l->{

					if(model.getDataSet().numQueries() == 0){
						JOptionPane.showMessageDialog(null, "There is no dataset loaded yet");
						return;
					}

					Object[] av = model.getSegmentationManager().getIncluded().toArray();

					if(av.length == 0){
						JOptionPane.showMessageDialog(null,"There is no pickable data in the image");
						return;
					}
					
					Object choice = 
							JOptionPane.showInputDialog(null, 
							"Pickable Objects",
							"Loaded :",
							JOptionPane.QUESTION_MESSAGE, 
							null, 
							av, 
							av[0]);
					
					if(choice == null)
						return;
					
					else
						view.setPickingType((String)choice);
				})
				.addButton(Icons.save_16(), "Save the overlayed channel", l->view.saveOverlay())
				.build();


		JPanel scene = new JPanel();
		scene.setLayout(new BorderLayout());
		scene.add(toolBar, BorderLayout.NORTH);
		scene.add(new JScrollPane(view.getView()), BorderLayout.CENTER);

		return new DefaultUIDocument(scene, name(), icon());


	}




	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/view.png"));
	}

}
