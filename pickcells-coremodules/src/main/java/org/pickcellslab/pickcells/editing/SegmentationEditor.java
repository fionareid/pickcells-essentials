package org.pickcellslab.pickcells.editing;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.modules.SessionConsumer;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.foundationj.modules.ModuleActivationListener;
import org.pickcellslab.pickcells.api.app.modules.AnalysisException;
import org.pickcellslab.pickcells.api.app.modules.Organiser;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.RawFile;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;
import org.pickcellslab.pickcells.api.img.editors.SegmentationEditorControllers;
import org.pickcellslab.pickcells.api.img.editors.SegmentedObjectManager;
import org.pickcellslab.pickcells.api.img.io.StaticImgIO;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.img.view.ImageDisplay;

import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;

@MetaInfServices(Module.class)
public class SegmentationEditor<S extends RealType<S> & NativeType<S>, T extends RealType<T> & NativeType<T>> implements Organiser, MetaModelListener, SessionConsumer{



	private ModuleActivationListener al;

	private boolean isActive = false;
	private DataAccess<?,?> access;

	private final ProviderFactoryFactory pff;


	public SegmentationEditor(ProviderFactoryFactory pff) {
		this.pff = pff;
	}


	@Override
	public void launch() throws AnalysisException {

		// First Allow the user to choose a segmented result

		// Regenerate all the Segmented Results which do not yet have an associated type up to their raw file


		try {

			RegeneratedItems items = 
					access.queryFactory().regenerate(SegmentationResult.class)
					.toDepth(2)
					.traverseLink(Links.COMPUTED_FROM, Direction.OUTGOING)
					.and(Image.RAW_FILE_LINK,  Direction.OUTGOING)
					.includeAllNodes().regenerateAllKeys().doNotGetAll()
					.useFilter(P.hasKey(SegmentationResult.associated).negate())
					.run();


			if(items.getAllTargets().isEmpty()){
				JOptionPane.showMessageDialog(null, "There are no editable Segmentation in the database");
				return;
			}


			List<RawFile> rt = items.getAllItemsFor(RawFile.class).collect(Collectors.toList());

			RawFile raw = (RawFile) JOptionPane.showInputDialog(
					null,
					"Choose the image to be edited",
					"Segmentation Editor...",
					JOptionPane.PLAIN_MESSAGE,
					icon(),
					rt.toArray(),
					rt.get(0)
					);

			if(null == raw)
				return;
			
			//RawFile raw = rt.stream().filter(rf->rf.toString().equals(choice)).findFirst().get();
			Image image = raw.getImage();
			
			List<SegmentationResult> segs = image.segmentations();		// no need to filter for no associations, this was done when regenerating from the database	
			SegmentationResult sr = segs.get(0);
			
			if(segs.size()>1){
				sr = (SegmentationResult) JOptionPane.showInputDialog(
						null,
						"Several results are associated with this image, choose the one to edit",
						"Segmentation Editor...",
						JOptionPane.PLAIN_MESSAGE,
						icon(),
						segs.toArray(),
						segs.get(0)
						);
				if(sr==null)
					return;
			}
			
			
			
			
			// Now launch the display
			
			//Create the display
			final ImageDisplay display = ImageDisplay.createDisplay();
			
			//Add the color image
			final Img<T> color = StaticImgIO.open(image);
			display.addImage(image.getMinimalInfo(), color);
			//Name channels
			String[] channelNames = image.channels();
			for(int i=0; i<channelNames.length; i++){
				display.getChannel(0, i).setChannelName(channelNames[i]);
				//TODO display.getChannel(0, i).getAvailableLuts()
			}
			
			// Add annotations
			final SegmentedObjectManager<S> mgr = new SegmentedObjectManager<S>(pff, sr, access);
			display.addAnnotationLayer("Annotations", mgr);
			
			/*
			
			//Save color changes in preferences
			final UserPreferences prefs = CrossDBRessources.getPrefsForCurrentUser(access);
			ColorTable[] luts = ImgDimensions.createColorTables(color.firstElement(), image.luts());			
			AnnotationEditorView<T> view = new AnnotationEditorView<>(manager, color, luts, image.order(), prefs);
			*/
								
			JToolBar toolBar = SegmentationEditorControllers.createControls(mgr, display);
			
			JPanel panel = new JPanel();
			panel.setLayout(new BorderLayout());
			panel.add(toolBar,BorderLayout.NORTH);
			panel.add(display.getView(), BorderLayout.CENTER);
			
			JFrame f = new JFrame();
			f.setContentPane(panel);
			f.pack();
			f.setLocationRelativeTo(null);
			f.setVisible(true);
			
			display.refreshDisplay();	
			

		} catch (DataAccessException e) {
			throw new AnalysisException("Unable to read the database", e);
		} catch (IOException e) {
			throw new AnalysisException("Unable to open the color image", e);
		}

	}




	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/SegEditor_32.png"));
	}



	@Override
	public Object[] categories() {
		return new String[]{"Editing"};
	}

	@Override
	public String name() {
		return "Segmentation Editor";
	}

	@Override
	public String description() {
		return "<HTML>"
				+ "<h3>Segmentation Editor :</h3>"
				+ "<p>This module will display an image with an overlay containing the boundaries of segmented shapes</p>"
				+ "<p>contained in a segmented result. It then provides multiple interactive tools for you to correct and at</p>"
				+ "<p>the same time quantify the accuracy of the original segmentation (The number of edits will be stored</p>"
				+ "<p>in the corresponding SegmentationResult node in the database).</p>"
				+ "</HTML>";
	}

	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}





	@Override
	public void registerListener(ModuleActivationListener lst) {
		al = lst;
	}



	@Override
	public boolean isActive() {
		return isActive;
	}




	@Override
	public void sessionClosed(DataAccess<?, ?> access) {
		this.access = null;
		isActive = false;
		if(al!=null)
			al.isNowActive(this, isActive);
	}




	@Override
	public void setSession(DataAccess<?, ?> access) {

		this.access = access;

		// Check if there exists SegmentationResult which have no associated types yet
		try {

			isActive = 
					! access.queryFactory().read(SegmentationResult.class).makeList(WritableDataItem.idKey)
					.inOneSet().useFilter(P.hasKey(SegmentationResult.associated)).run().isEmpty();

			if(al!=null)
				al.isNowActive(this, isActive);

		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//.inGroups().getAll().groupBy()

	}



	@Override
	public void metaEvent(RegeneratedItems meta, MetaChange evt) {

		try {

			isActive = 
					! access.queryFactory().read(SegmentationResult.class).makeList(WritableDataItem.idKey)
					.inOneSet().useFilter(P.hasKey(SegmentationResult.associated).negate()).run().isEmpty();


			if(al!=null)
				al.isNowActive(this, isActive);


		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
