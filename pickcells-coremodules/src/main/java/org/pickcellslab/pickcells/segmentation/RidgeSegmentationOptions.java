package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.function.Consumer;

import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathUtils;

public class RidgeSegmentationOptions {

	
	private double searchRadius = 3;
	private double searchThreshold = 1;
	private double delta = 0.10;
	private double minRadius = 3;
	private double maxRadius = 15;
	private double minCompactness = 0.4;
	private double cornerThreshold = 0.75;
	private double minClosingRatio = 8;
	private double minScore = -30;
	
	private int childDepth = -1;
	private double maxLength = -1;
	
	
	private boolean obsoleteMaxima = true;
	private boolean cancel = false;
	
	
	// Classification fields
	static final String[] classes = {"Valid", "Invalid"};
	private boolean useClassifier = true;
	private NaiveBayesClassifier classifier;
		
	
	public enum RSMode{
		DO_RUN,
		DEBUG_TRACK		
	}
	
	
	private RSMode mode = RSMode.DO_RUN;
	private boolean drawLeaves;
	private boolean drawCenters;
	

	
	
	public boolean useClassifier() {
		return useClassifier;
	}
	
	public void setUseClassifier(boolean use){
		useClassifier = use;
	}
	
	
	public NaiveBayesClassifier getClassifier(){
		return classifier;
	}
	
	
	public void setClassifier(NaiveBayesClassifier cl) {
		classifier = cl;		
	}
	
	
	
	public String validClass() {
		return classes[0];
	}
	
	public String invalidClass() {
		return classes[1];
	}
	
	
	public RSMode getMode() {
		return mode;
	}
	
	
	void setMode(RSMode m){
		mode = m;
		if(m == RSMode.DO_RUN){
			drawLeaves = false;
			drawCenters = false;
		}
		else{
			drawLeaves = true;
			drawCenters = true;
		}
	}
	
	
	public boolean drawLeaves() {
		return drawLeaves;
	}
	
	public void drawLeaves(boolean draw) {
		drawLeaves = draw;
	}


	public boolean drawCenters() {
		return drawCenters;
	}

	public void drawCenters(boolean draw) {
		drawCenters = draw;
	}
	
	
	
	void setCancelled(boolean cancel){
		this.cancel = cancel;
	}
	
	
	public boolean isCancelled(){
		return cancel;
	}
	
	
	
	
	public double getMinScore() {
		return minScore; 
	}
	
	public void setMinScore(double v) {
		minScore = v;
	}


	
	public double getSigma() {
		return 0.1;
	}
	
	
	double getSearchRadius() {
		return searchRadius;
	}
	
	
	void setSearchRadius(double searchRadius) {
		this.searchRadius = searchRadius;
	}
	
	double getSearchThreshold() {
		return searchThreshold;
	}
	
	void setSearchThreshold(double threshold) {
		this.searchThreshold = threshold;
		this.obsoleteMaxima = true;
	}
	
	
	
	boolean minMaxAreObsolete(){
		return this.obsoleteMaxima;
	}
	

	
	double getRidgeThreshold() {
		return searchThreshold;
	}
	
	
	void setRidgeThreshold(double threshold) {
		//this.ridgeThreshold = threshold;
	}
	
	double getDelta() {
		return delta;
	}
	
	void setDelta(double delta) {
		this.delta = delta;
	}
	
	double getMinRadius() {
		return minRadius;
	}
	
	void setMinRadius(double minRadius) {
		this.minRadius = minRadius;
	}
	
	double getMaxRadius() {
		return maxRadius;
	}
	
	void setMaxRadius(double maxRadius) {
		this.maxRadius = maxRadius;
		this.childDepth = -1;
		computeMaxLength();
	}
	double getMinCompactness() {
		return minCompactness;
	}
	void setMinCompactness(double minCompactness) {
		this.minCompactness = minCompactness;
		this.childDepth = -1;
		computeMaxLength();
	}
	
	
	double getMinClosingRatio() {
		return minClosingRatio;
	}
	
	void setMinClosingRatio(double ratio){
		minClosingRatio = ratio;
	}
	
	double getMinLength(){
		return MathUtils.TWO_PI * getMinRadius();
	}
	
	double getMinArea(){
		return Math.PI * FastMath.pow(getMinRadius(),2);
	}
	
	double getMaxArea(){
		return Math.PI * FastMath.pow(getMaxRadius(),2);
	}
	
	double getMaxLength(){
		if(maxLength==-1)
			computeMaxLength();
		return maxLength;
	}
	
	private void computeMaxLength(){
		maxLength = FastMath.sqrt(4*Math.PI*getMaxArea()/getMinCompactness());
	}
	
	
	public static double compactness(double perimeter, double area){
		return (area - perimeter) / (area - FastMath.sqrt(area)); 
		//return 4*Math.PI*area/Math.pow(perimeter,2);
	}
	
	public int getMaxTotalNumLeaves() {
		return 20;//(int) (maxLength/(getSearchRadius()*5));
	}
	
	
	public int getChildDepth(){
		if(childDepth == -1){
			double d = getSearchRadius() * 2;
			childDepth = (int) ((getMaxLength() - d) / (d*4));
		}
		return childDepth;
	}
	
	
	/**
	 * @return The maximum total length of all the tracks in a ridge tracing process
	 */
	public double getMaxTotalLength() {		
		return getMaxLength()*2;
	}

	
	
	public double getCornerThreshold() {
		return cornerThreshold;
	}
	
	void setCornerThreshold(double value){
		cornerThreshold =  value;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public void description(Consumer<String> consumer){
		
		consumer.accept(" ************ Ridge Segmentation Options ***********");
		consumer.accept(" == User Defined ==");
		consumer.accept("Search Radius = "+ searchRadius);
		consumer.accept("Min Radius = "+ minRadius);
		consumer.accept("Max Radius = "+ maxRadius);		
		consumer.accept("Min Compactness = "+ minCompactness);	
		consumer.accept("Corner Threshold = "+ cornerThreshold);	
		consumer.accept("Min Closing Ratio = "+ minClosingRatio);		
		consumer.accept("Threshold = "+ searchThreshold);
		consumer.accept("Delta Max = "+ delta);
		consumer.accept(" == Computed ==");
		consumer.accept("Min Length = "+ getMinLength());
		consumer.accept("Max Length = "+ getMaxLength());
		consumer.accept("Min Area = "+ getMinArea());
		consumer.accept("Max Area = "+ getMaxArea());
		consumer.accept("Child Depth = "+ getChildDepth());
		consumer.accept("Max Number of Leaves = "+ this.getMaxTotalNumLeaves());
		consumer.accept(" ************ Ridge Segmentation Options ***********");
		
		
	}

	

	

	

	


	

	


	
	
	
	
	
}
