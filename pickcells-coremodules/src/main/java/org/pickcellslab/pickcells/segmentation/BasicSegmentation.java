package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.modules.SessionConsumer;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.app.modules.Analysis;
import org.pickcellslab.pickcells.api.app.modules.AnalysisException;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Experiment;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;
import org.pickcellslab.pickcells.api.img.detector.LabelGenerator;
import org.pickcellslab.pickcells.api.img.io.StaticImgIO;
import org.pickcellslab.pickcells.api.img.pipeline.DimensionalityEffect;
import org.pickcellslab.pickcells.api.img.pipeline.ExtendedImageInfo;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;
import org.pickcellslab.pickcells.api.img.pipeline.PipelineBuilder;
import org.pickcellslab.pickcells.api.img.pipeline.ProcessingPipeline;
import org.pickcellslab.pickcells.api.img.pipeline.Skip;
import org.pickcellslab.pickcells.api.img.process.AnisotropicDiffusion;
import org.pickcellslab.pickcells.api.img.process.BinaryMorphology;
import org.pickcellslab.pickcells.api.img.process.ChannelExtractor;
import org.pickcellslab.pickcells.api.img.process.GaussianFilter;
import org.pickcellslab.pickcells.api.img.process.Thresholding;
import org.pickcellslab.pickcells.api.img.steerable.SteerableFilter;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.labeling.ConnectedComponents;
import net.imglib2.algorithm.labeling.ConnectedComponents.StructuringElement;
import net.imglib2.img.Img;
import net.imglib2.roi.labeling.ImgLabeling;
import net.imglib2.type.NativeType;
import net.imglib2.type.logic.BitType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.UnsignedShortType;
import net.imglib2.type.numeric.real.FloatType;


@MetaInfServices(Module.class)
public class BasicSegmentation extends AbstractAnalysis implements Analysis, SessionConsumer {

	private static Logger log = Logger.getLogger(BasicSegmentation.class);

	//Session reference
	private DataAccess<?,?> session;

	//Activation related Fields
	private boolean isActivable = true;


	private File home;
	private String resultsName = "Segmentation";



	

	@Override
	public void  launch() throws AnalysisException {


		setStep(0);

		//First get the image data
		RegeneratedItems set;
		try {
			if(home == null){
				//Get the image folder
				String f = session.queryFactory().read(Experiment.class).makeList(Experiment.dbPathKey).inOneSet().getAll().run().get(0);
				home = new File(f);		
			}

			set = session.queryFactory().regenerate(Image.class)
					.toDepth(2)
					.traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
					.includeAllNodes()
					.regenerateAllKeys()
					.getAll();

		} catch ( Exception e1) {
			throw new AnalysisException("Unable to read the database", e1);
		}

		final List<Image> images = set.getTargets(Image.class).collect(Collectors.toList());		
		//Sort the list to maintain the same indexing as original images
		Collections.sort(images, (i1,i2)->i1.getAttribute(Keys.name).get().compareTo(i2.getAttribute(Keys.name).get()));

		if(images.isEmpty()){
			log.warn("No images were found in the database");
			return;
		}


		resultsName = getName();
		if(resultsName == null){
			resultsName = "Segmentation";
			return;
		}

		try {

			process(images);
		} catch (IOException e) {
			throw new AnalysisException("Unable to read the database", e);		
		}

	}






	private <T extends NativeType<T> & RealType<T>> void process(List<Image> images) throws IOException{


		List<ImgProcessing<T,FloatType>> enhancer = new ArrayList<>();
		enhancer.add(new GaussianFilter<>());
		enhancer.add(new SteerableFilter<>());
		enhancer.add(new AnisotropicDiffusion<>());
		enhancer.add(new Skip<>(new FloatType(), DimensionalityEffect.FLAT_TO_FLAT));

		setStep(1);
		
		
		Optional<ProcessingPipeline<T,BitType>> pipeline = 
				new PipelineBuilder<T,T>()
				.step(new ChannelExtractor<T>(), "Channel Choice")	//TODO add channel combiner
				.steps(enhancer, "Feature Enhancement / Noise Reduction")
				.step(new Thresholding<FloatType>(), "Threshodling")
				.steps(Arrays.asList(new BinaryMorphology().processors()), "Morphology Operations")// Add watershed?
				.buildForDBImages(session, icon(), "Basic Segmentation...");
		
		
		if(pipeline.isPresent()){
			ProcessingPipeline<T, UnsignedShortType> pl = pipeline.get().appendStep(labelling(), new UnsignedShortType());
			final Consumer<ExtendedImageInfo<UnsignedShortType>> save = (info)->{
				new SegmentationResult(info.getAssociatedImage(), 
						info.getName() + StaticImgIO.standard(info.getInfo().imageDimensions(),						
						info.getType().getBitsPerPixel()),
						resultsName,
						3,
						"Computed with Basic Segmentation");
				try {
					session.queryFactory().store().add(info.getAssociatedImage()).run();
				} catch (DataAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			};
			setStep(2);
			pl.runPipeline((n)->resultsName+"_"+n, save);
		}
		
		
		setStep(3);
		 
	}
	
	
	private Function<RandomAccessibleInterval<BitType>, RandomAccessibleInterval<UnsignedShortType>> labelling(){
		return (segmented) ->{
			final Img<UnsignedShortType> result = StaticImgIO.createImg(segmented, new UnsignedShortType());
			ImgLabeling<Integer,UnsignedShortType> labels = new ImgLabeling<>(result);
			ConnectedComponents.labelAllConnectedComponents(segmented, labels,  new LabelGenerator(0), StructuringElement.FOUR_CONNECTED);
			return result;
		};
	}



	private String getName() {
		String name = JOptionPane.showInputDialog(null, "Choose a name for the segmentation you will generate",resultsName);
		if(name == ""){
			JOptionPane.showMessageDialog(null, "Please enter a valid name");
			return getName();
		}
		return name;
	}








	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}




	@Override
	public String name() {
		return "Basic Segmentation";
	}





	@Override
	public boolean isActive() {
		return isActivable;
	}





	@Override
	public Object[] categories() {
		return new String[]{"Segmentation"};
	}







	@Override
	public String description() {
		return "<HTML>Allows you to identify individual objects in your images using basic segmentation operations"
				+ "<br> such as thresholding and morphological filtering...</HTML>";
	}



	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/segs.png"));
	}




	@Override
	public void sessionClosed(DataAccess<?,?> source) {
		session = null;
	}




	@Override
	public void setSession(DataAccess<?,?> session) {
		this.session = session;
		session.addSessionListener(this);
	}




	@Override
	public Icon[] icons() {
		return new Icon[7];
	}




	@Override
	public String[] steps() {
		return new String[]{
				"Read DB",
				"Protocol",
				"Segmenting",
				"Done"
		};
	}






}

