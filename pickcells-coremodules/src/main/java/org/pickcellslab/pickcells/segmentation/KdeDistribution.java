package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.math3.distribution.ConstantRealDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.commons.math3.random.EmpiricalDistribution;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

public class KdeDistribution extends EmpiricalDistribution {

	
	
	 public KdeDistribution(int i) {
		super(i);
	}

	/**
     * The within-bin smoothing kernel. Returns a Gaussian distribution
     * parameterized by {@code bStats}, unless the bin contains only one
     * observation, in which case a constant distribution is returned.
     *
     * @param bStats summary statistics for the bin
     * @return within-bin kernel parameterized by bStats
     */
    protected RealDistribution getKernel(SummaryStatistics bStats) {
        if (bStats.getN() <= 1 || bStats.getStandardDeviation() == 0) {
            return new ConstantRealDistribution(bStats.getMean());
        } else {
            return new NormalDistribution(randomData.getRandomGenerator(),
                bStats.getMean(), bStats.getStandardDeviation(),
                NormalDistribution.DEFAULT_INVERSE_ABSOLUTE_ACCURACY);
        }
    }
	
}
