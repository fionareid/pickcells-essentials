package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

public class ATVOptions implements Cloneable{


	double minVolume = 2000;
	double maxVolume = 20000;
	
	
	
	boolean finalize = true;
	private boolean removeFlat = true;
	private double minOvl = 0.7;
	double maxdcc = 20;
	int maxJump = 2;

	
	PostProcessSplit splitType = null; //SplitTypes.values()[SplitTypes.values().length-1];
	double split = 4;
	
	
	private double mean = 11000;
	private double twoSigmas = 6000;
	

	public ATVOptions(RidgeSegmentationOptions rbsOptions) {
		setMinVolume((int)((rbsOptions.getMinArea()*6 + 99) / 100 ) * 100);
		setMaxVolume((int)((rbsOptions.getMaxArea()*3 + 99) / 100 ) * 100);
		maxdcc = rbsOptions.getMaxRadius() * 1.2;
	}

	double getMinVolume() {
		return minVolume;
	}

	void setMinVolume(double minVolume) {
		this.minVolume = minVolume;
		mean = (maxVolume + minVolume) / 2;
		twoSigmas = (maxVolume - minVolume) * 2 / 3; 
	}

	double getMaxVolume() {
		return maxVolume;
	}

	void setMaxVolume(double maxVolume) {
		this.maxVolume = maxVolume;
		mean = (maxVolume + minVolume) / 2;
		twoSigmas = (maxVolume - minVolume) / 3; 
	}

	int getMaxJump() {
		return maxJump;
	}

	void setMaxJump(int maxJump) {
		this.maxJump = maxJump;
	}

	boolean isFinalize() {
		return finalize;
	}
	void setFinalize(boolean finalize) {
		this.finalize = finalize;
	}



	public double getMaxInterCentroidDistance() {
		return maxdcc;
	}

	void setMaxInterCentroidDistance(double maxDcc) {
		maxdcc = maxDcc;
	}

	public double getMinOverlap() {
		return minOvl;
	}

	void setMinOverlap(double ovl){
		minOvl = ovl;
	}


	public double splitTolerance() {
		return split;
	}

	public void setSplitTolerance(double doubleValue) {
		split = doubleValue;
	}
	
	public <V extends VolumeSliceFeature, O> PostProcessSplit<V,O> splitType(){
		return splitType;
	}
	
	public void setSplitType(PostProcessSplit type){
		splitType = type;
	}
	
	


	/**
	 * @param v
	 * @return true if the given volume is below mean + 2 sigmas (lower bound not tested).
	 */
	public boolean acceptVolume2Sigmas(double v){
		return v<= mean + twoSigmas;
	}


	/**
	 * @param v
	 * @return true if the given volume is within the distribution bounds (min and max)
	 */
	public boolean acceptVolumeExtrema(double v){
		double threeSigmas = twoSigmas + twoSigmas/2; 
		return v >= mean - threeSigmas  && v<= mean + threeSigmas;
	}


	public void setRemoveFlat(boolean remove){
		removeFlat = remove;
	}


	public boolean removeFlat() {
		return removeFlat;
	}


	@Override
	public ATVOptions clone(){
		try {
			return (ATVOptions) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}




}
