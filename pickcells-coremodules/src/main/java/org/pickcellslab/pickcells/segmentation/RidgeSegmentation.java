package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.pickcellslab.pickcells.api.img.detector.DogDetect;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;

import net.imglib2.Point;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.region.localneighborhood.EllipseNeighborhood;
import net.imglib2.outofbounds.OutOfBoundsConstantValueFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.util.Util;

public class RidgeSegmentation {






	public static < V extends RealType<V> & NativeType<V>> List<Point>  maximaForPlane(
			RandomAccessibleInterval<V> graysPlane,
			RidgeSegmentationOptions options){

		if(options.isCancelled())
			return Collections.emptyList();

		// ****************************************** Find maxima ******************************************************

		final ArrayList<Point> peaks = DogDetect.detect(graysPlane, false, false, 2.5, options.getSigma(), options.getSearchThreshold(), new double[]{1,1,1,1,1});


		// ********************* Sort maxima in descending order of average signal in neighborhood *************************
		final Map<Point,Double> neighborhood = new HashMap<>();
		final EllipseNeighborhood<V> n = 
				new EllipseNeighborhood<>(
						graysPlane,
						new long[2],
						new long[]{(long) options.getSearchRadius(), (long) options.getSearchRadius()},
						new OutOfBoundsConstantValueFactory<>(graysPlane.randomAccess().get().createVariable()));
		peaks.forEach(p->{
			double avg = 0;
			n.setPosition(p);
			for(V t : n){
				avg+=t.getRealDouble();
			}
			neighborhood.put(p, avg/(double)n.size());
		});

		peaks.sort((p1,p2)-> Double.compare(neighborhood.get(p2), neighborhood.get(p1)));			

		return peaks;

	}







	/**
	 * @param graysPlane
	 * @param skelPlane
	 * @param labelsPlane
	 * @param peaks can be null -> will be computed if null
	 * @param options
	 * @param gen
	 * @return
	 */
	static <O extends RealType<O> & NativeType<O>, T extends RealType<T> & NativeType<T>, V extends RealType<V> & NativeType<V>> Map<Float, Ridge<T, V>> processPlane(
			RandomAccessibleInterval<O> channelPlane,
			RandomAccessibleInterval<V> graysPlane,
			RandomAccessibleInterval<T> skelPlane,
			RandomAccessibleInterval<T> labelsPlane,
			List<Point> peaks,
			RidgeSegmentationOptions options,
			Iterator<T> gen){

		
		// Compute maxima if null
		if(peaks == null)
			peaks = maximaForPlane(graysPlane, options);
		
		

		// ********************For each maximum initiate tracing *******************************


		//Create the sampling kernel
		long[][] kernel = ImgGeometry.clockwiseKernel((long)options.getSearchRadius());
		if(!peaks.isEmpty()){
			switch(options.getMode()){
			case DEBUG_TRACK: return runAsDebugTrack((RandomAccessibleInterval)channelPlane, graysPlane, skelPlane, labelsPlane, peaks, gen, kernel, options);
			case DO_RUN: return run(channelPlane, graysPlane, skelPlane, labelsPlane, peaks, gen, kernel, options);
			}

		}


		return Collections.emptyMap();

	}




	static <O extends RealType<O> & NativeType<O>, T extends RealType<T> & NativeType<T>, V extends RealType<V> & NativeType<V>> Map<Float, Ridge<T, V>> processPlane(
			RandomAccessibleInterval<O> channelPlane,
			RandomAccessibleInterval<V> graysPlane,
			RandomAccessibleInterval<T> labelsPlane,
			List<Point> peaks,
			RidgeSegmentationOptions options,
			Iterator<T> gen){

		//Create the sampling kernel
		long[][] kernel = ImgGeometry.clockwiseKernel((long)options.getSearchRadius());
		if(!peaks.isEmpty()){
			T type = labelsPlane.randomAccess().get().createVariable();
			RandomAccessibleInterval<T> tracks = Util.getArrayOrCellImgFactory(graysPlane, type).create(graysPlane, type);
			return run(channelPlane, graysPlane, tracks, labelsPlane, peaks, gen, kernel, options);
		}

		return Collections.emptyMap();

	}






	private static <O extends RealType<O> & NativeType<O>, T extends RealType<T> & NativeType<T>, V extends RealType<V> & NativeType<V>> Map<Float, Ridge<T, V>> run(
			RandomAccessibleInterval<O> channelPlane,
			RandomAccessibleInterval<V> graysPlane, RandomAccessibleInterval<T> skelPlane, RandomAccessibleInterval<T> labelsPlane,
			List<Point> peaks, Iterator<T> gen, long[][] kernel, RidgeSegmentationOptions options) {


		// Turn off visualisation
		RidgePoint.printFailed = false;
		RidgePoint.printLures = false;



		T v = skelPlane.randomAccess().get().createVariable();
		int ids = 10;


		RandomAccess<T> labelsAccess = labelsPlane.randomAccess();
		RandomAccess<T> skelAccess = skelPlane.randomAccess();

		final Map<Float,Ridge<T,V>> chosen = new HashMap<>();

		for(Point p : peaks){

			if(options.isCancelled())
				return Collections.emptyMap();

			T id = v.createVariable();
			id.setReal(ids++);

			RidgePoint<T,V> root = new RidgePoint<>((RandomAccessibleInterval)channelPlane, graysPlane, skelPlane, labelsPlane, p, id, kernel, options);



			List<Ridge<T,V>> ridges = Tracing.getMostProbableTracks(root, chosen, options);

			root.clearAllConnectedTracks();

			if(!ridges.isEmpty() && ridges.get(0).score(options, chosen) > options.getMinScore()){
				ridges.get(0).drawBorderAndArea(skelAccess, labelsAccess, ridges.get(0).id());
				chosen.put(ridges.get(0).id().getRealFloat(),ridges.get(0));
				//System.out.println(ridges.get(0).toString());
				//System.out.println("Score written");
			}



		}

		// we need to rewrite the ridge
		chosen.forEach((t,r)->r.drawBorder(skelAccess));

		//System.out.println("Number of objects : "+chosen.size());
		return chosen;
	}




	private static <T extends RealType<T> & NativeType<T>, V extends RealType<V> & NativeType<V>> Map<Float, Ridge<T, V>> runAsDebugTrack(
			RandomAccessibleInterval<V> channelPlane,
			RandomAccessibleInterval<V> graysPlane, RandomAccessibleInterval<T> skelPlane, RandomAccessibleInterval<T> labelsPlane,
			List<Point> peaks, Iterator<T> gen, long[][] kernel, RidgeSegmentationOptions options) {



		RidgePoint.printFailed = true;
		RidgePoint.printLures = true;
		Map<Float,Ridge<T,V>> chosen = new HashMap<>();

		int ids = 10;

		Point p= peaks.get(peaks.size()-1);
		T v = skelPlane.randomAccess().get().createVariable();
		v.setReal(62000);

		T id = v.createVariable();
		id.setReal(ids++);

		RidgePoint<T,V> root = new RidgePoint<>((RandomAccessibleInterval)channelPlane, graysPlane, skelPlane, labelsPlane, p, id, kernel, options);


		//if(ridge.isOn()){
		//Traverse the tree and add a point at the center of each child
		RandomAccess<T> skelAccess = skelPlane.randomAccess();
		RandomAccess<T> labelsAccess = labelsPlane.randomAccess();

		//Erase non selected labels
		T label = id.createVariable();
		label.setReal(ids++);

		T leaf = id.createVariable();
		leaf.setReal(65000);

		T leaf2 = id.createVariable();
		leaf2.setReal(65002);

		List<Ridge<T,V>> ridges = Tracing.getMostProbableTracks(root, chosen, options);
		if(!ridges.isEmpty()){

			for(int i = 1; i<ridges.size(); i++){
				if(options.drawLeaves()){
					ImgGeometry.drawCircle(ridges.get(i).getFirst(), 3, skelAccess, leaf);
					ImgGeometry.drawCircle(ridges.get(i).getLast(), 3, skelAccess, leaf);
				}
				//ridges.get(i).drawBorder(skelAccess, v.createVariable());
				//System.out.println("Rejected : \n"+ridges.get(i).toString());
			}

			if(options.drawLeaves()){
				ImgGeometry.drawCircle(ridges.get(0).getFirst(), 3, skelAccess, leaf2);
				ImgGeometry.drawCircle(ridges.get(0).getLast(), 3, skelAccess, leaf2);
			}

			if(!ridges.isEmpty() && ridges.get(0).score(options, chosen) > options.getMinScore()){
				ridges.get(0).drawBorderAndArea(skelAccess, labelsAccess, r->r.getCurvatureAverageNonZero()*100, true);
				System.out.println(ridges.get(0).toString());
				//System.out.println("Score = "+ridges.get(0).score(options) );
				//System.out.println("Accepted : \n"+ridges.get(0).toString());
			}


			if(options.drawCenters()){

				ImgGeometry.drawCircle(root.center(), (long)options.getSearchRadius(), skelAccess, v);	
				skelAccess.setPosition(root.center());
				skelAccess.get().set(v);

				Deque<RidgePoint<T,V>> queue = new LinkedList<>();
				queue.add(root);

				while(!queue.isEmpty()){
					RidgePoint<T,V> node = queue.poll();

					//lAccess.get().set(v);
					v.setReal(63000);
					//ImgGeometry.drawCircle(node.center(), (long)options.getSearchRadius(), skelAccess, v);	
					skelAccess.setPosition(node.center());
					skelAccess.get().set(v);

					v.setReal(64000);
					skelAccess.setPosition(node.getRidgeSection().cp());
					skelAccess.get().set(v);
					for(int c = 0 ; c<node.numChildren(); c++){
						if(node.get(c)!=null)
							queue.add(node.get(c));
					}

				}

			}
		}

		return chosen;

	}






}
