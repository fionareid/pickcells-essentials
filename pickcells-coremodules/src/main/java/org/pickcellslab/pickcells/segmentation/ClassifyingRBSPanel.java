package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dialog.ModalExclusionType;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;

import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;
import org.pickcellslab.pickcells.api.img.pipeline.ImageViewState;
import org.pickcellslab.pickcells.api.img.pipeline.PreviewBag;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;
import org.pickcellslab.pickcells.api.img.process.LabelGen;
import org.pickcellslab.pickcells.api.img.view.AnnotationAppearance;
import org.pickcellslab.pickcells.api.img.view.Lut16;
import org.pickcellslab.pickcells.segmentation.NaiveBayesClassifier.ClassifierBuilder;

import com.alee.extended.panel.GroupPanel;
import com.alee.laf.filechooser.WebFileChooser;
import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotification;
import com.alee.utils.ThreadUtils;

import net.imglib2.Point;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

public class ClassifyingRBSPanel<O extends NativeType<O> & RealType<O>, V extends NativeType<V> & RealType<V>, T extends NativeType<T> & RealType<T>>  extends ValidityTogglePanel{


	private static final String maxima = "MAXIMA", parameters = "PARAMETERS", training = "TRAINING";

	private static final String unclassified = "UNCLASSIFIED", valid = "VALID", invalid = "INVALID", ambiguous = "AMBIGUOUS";

	private final RidgeSegmentationOptions options;


	private static final AnnotationAppearance mgr = new AnnotationAppearance();

	private ImageViewState<T> view;


	private RandomAccessibleInterval<V> steered;
	private RandomAccessibleInterval<O> channel;
	private RandomAccessibleInterval<T> labeled;
	private RandomAccessibleInterval<T> ovl;

	private final int zDim, tDim;
	private final Map<Integer, Map<Float,Ridge<T,V>>> ridges = new HashMap<>();


	private JComboBox<NaiveBayesClassifier> classifierTypeBox;
	private ClassifierBuilder cBuilder;
	private int currentClasse = 0;



	public ClassifyingRBSPanel(ImageViewState<T> img, PreviewBag<V> bag, int zDim, int tDim, RidgeSegmentationOptions options) {

		this.options = options;
		
		//Setup appearances for training a classifier
		mgr.createSeriesSilent(unclassified, Color.blue.getRGB());
		mgr.createSeriesSilent(valid, Color.green.getRGB());
		mgr.createSeriesSilent(invalid, Color.red.getRGB());
		mgr.createSeriesSilent(ambiguous, Color.yellow.getRGB());


		//Add the channel dimension
		this.view = img;
		this.zDim = zDim;
		this.tDim = tDim;

		this.channel = bag.getFirstFlat();
		this.steered = bag.getInput();

		assert channel!=null;
		assert steered!=null;

		this.ovl = img.getAnnotationImage();
		this.labeled = ImgDimensions.copy(ovl);

		CardLayout card1 = new CardLayout();
		this.setLayout(card1);





		//-----------------------------------------------------------------------------------------------------
		// Maxima Panel
		//-----------------------------------------------------------------------------------------------------



		JLabel lblThreshold = new JLabel("Maxima Threshold");
		lblThreshold.setToolTipText("Minimum Signal to noise ratio for detecting maxima from which the ridge following procedures are initiated");

		final SpinnerNumberModel threshModel = new SpinnerNumberModel(options.getSearchThreshold(),-50,65500,0.1);
		final JSpinner threshField = new JSpinner(threshModel);
		threshField.addChangeListener(e-> {			
			options.setSearchThreshold(threshModel.getNumber().doubleValue());			
		});



		JButton maxBtn = new JButton("Show Maxima");
		maxBtn.addActionListener(l->showMaxima());


		JButton toParamBtn = new JButton("Next");
		toParamBtn.addActionListener(l->card1.next(ClassifyingRBSPanel.this));


		GroupPanel maximaPanel = new GroupPanel(
				10,
				false,
				new GroupPanel(10, lblThreshold, threshField),
				maxBtn,
				toParamBtn);		
		maximaPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

		//card.addLayoutComponent(maximaPanel, maxima);
		this.add(maximaPanel, maxima);




		//-----------------------------------------------------------------------------------------------------
		// Parameters Panel
		//-----------------------------------------------------------------------------------------------------



		//=================== Tracing Parameters Panel ==========================

		JPanel parametersPanel = new JPanel();
		parametersPanel.setBorder(BorderFactory.createTitledBorder("Tracing Parameters..."));
		parametersPanel.setLayout(new GridLayout(4, 2, 10, 10));



		JLabel lblSearchR = new JLabel("Search Radius");
		lblSearchR.setToolTipText("Resolution of the probing circle. It is advised to use a radius which is \naround 1/3 of the smallest nucleus radius.");

		final SpinnerNumberModel searchModel = new SpinnerNumberModel(options.getSearchRadius(),3,100,1);
		final JSpinner searchRadiusField = new JSpinner(searchModel);
		searchRadiusField.getModel().setValue(options.getSearchRadius());
		searchRadiusField.addChangeListener(e-> {			
			options.setSearchRadius(searchModel.getNumber().doubleValue());		
		});
		parametersPanel.add(lblSearchR);
		parametersPanel.add(searchRadiusField);


		JLabel lblBackgroundSignal = new JLabel("Delta");
		lblBackgroundSignal.setToolTipText("A percentage of the origin intensity below which the ridge following procedure will stop");

		final SpinnerNumberModel deltaModel = new SpinnerNumberModel(options.getDelta(),0.001,5,0.1);
		final JSpinner deltaField = new JSpinner(deltaModel);
		deltaField.addChangeListener(e-> {	
			options.setDelta(deltaModel.getNumber().doubleValue());				
		});		
		parametersPanel.add(lblBackgroundSignal);
		parametersPanel.add(deltaField );


		JLabel lblMinR = new JLabel("Min Radius");
		lblMinR.setToolTipText("Radius of the smallest nucleus in the image (in pixel)");

		final SpinnerNumberModel minRadiusModel = new SpinnerNumberModel(options.getMinRadius(),3,1000,1);
		final JSpinner minRadiusField = new JSpinner(minRadiusModel);
		minRadiusField.addChangeListener(e-> {		
			options.setMinRadius(minRadiusModel.getNumber().doubleValue());			
		});
		parametersPanel.add(lblMinR);
		parametersPanel.add(minRadiusField);


		JLabel lblMaxR = new JLabel("Max Radius");
		lblMaxR.setToolTipText("Radius of the largest nucleus in the image (in pixel)");

		final SpinnerNumberModel maxRadiusModel = new SpinnerNumberModel(options.getMaxRadius(),3,1000,1);
		final JSpinner maxRadiusField = new JSpinner(maxRadiusModel);
		maxRadiusField.addChangeListener(e-> {			
			options.setMaxRadius(maxRadiusModel.getNumber().doubleValue());					
		});
		parametersPanel.add(lblMaxR);
		parametersPanel.add(maxRadiusField);


		//=================== Classifier Panel ==========================

		JPanel lowerPanel = new JPanel();
		CardLayout lowerCard = new CardLayout();
		lowerPanel.setLayout(lowerCard);
		
		

		JLabel typeLbl = new JLabel("Classifier : ");
		classifierTypeBox = new JComboBox<>(this.getPresetCassifiers()); //TODO default classifiers

		options.setClassifier((NaiveBayesClassifier) classifierTypeBox.getSelectedItem());

		JButton loadBtn = new JButton("Load a Classifier");
		loadBtn.addActionListener(l->{
			File f = WebFileChooser.showOpenDialog();
			if(f==null)
				return;
			try{
				NaiveBayesClassifier loaded = ClassifierBuilder.fromFile(f);
				classifierTypeBox.addItem(loaded);
				classifierTypeBox.setSelectedItem(loaded);
				options.setMinScore(0);
			}
			catch(Exception e ){
				JOptionPane.showMessageDialog(null, "The provided file is invalid");
				return;
			}
		});


		JButton createBtn = new JButton("Train a New Classifier...");
		createBtn.addActionListener(l->{
			options.setUseClassifier(false); // Simply generate shape with no classification
			options.setMinScore(-1000);
			cBuilder = new ClassifierBuilder(RidgeSegmentationOptions.classes); // instantiate the builder
			// Redraw shapes with the "unclassified" color
			T color = view.getType().createVariable();
			color.setReal(mgr.getAppearance(unclassified));
			ridges.forEach((z,map) -> map.forEach((f,r)->r.drawBorder(color)));
			//Set the Lut for annotated shapes
			view.setLut(view.getAnnotationIndex(), mgr);
			lowerCard.previous(lowerPanel);
		});


		
		JButton editBtn = new JButton("Edit the Selected Classifier...");
		editBtn.addActionListener(l->{
			if(classifierTypeBox.getSelectedItem() == null){
				JOptionPane.showMessageDialog(null, "No classifier selected");
				return;
			}
			if(((NaiveBayesClassifier)classifierTypeBox.getSelectedItem()).getFile() == null){
				JOptionPane.showMessageDialog(null, "Default Classifiers cannot be edited");
				return;
			}
			options.setUseClassifier(true); // Simply generate shape with no classification
			options.setMinScore(-1000);
			cBuilder = ClassifierBuilder.builderFromClassifier((NaiveBayesClassifier)classifierTypeBox.getSelectedItem()); // instantiate the builder
			// Redraw shapes with the "unclassified" color
			T color = view.getType().createVariable();
			color.setReal(mgr.getAppearance(unclassified));
			ridges.forEach((z,map) -> map.forEach((f,r)->r.drawBorder(color)));
			//Set the Lut for annotated shapes
			view.setLut(view.getAnnotationIndex(), mgr);
			lowerCard.previous(lowerPanel);
		});

		
		



		JButton prevBtn = new JButton("Preview");
		JLabel infoLbl = new JLabel("");
		Preview prev = new Preview(prevBtn, infoLbl);


		
		

		GroupPanel csfChoicePanel = new GroupPanel(
				10,
				false, 
				new GroupPanel(10, typeLbl, classifierTypeBox),
				loadBtn,
				createBtn,
				editBtn,
				new GroupPanel(10, prevBtn, infoLbl));

		csfChoicePanel.setBorder(BorderFactory.createTitledBorder("Shapes Classification..."));
		lowerPanel.add(csfChoicePanel, "choice");

		GroupPanel protocolPanel = new GroupPanel(10, false, parametersPanel, lowerPanel);
		protocolPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		//card.addLayoutComponent(parameterPanel, parameters);
		this.add(protocolPanel, parameters);




		//-----------------------------------------------------------------------------------------------------
		// Training panel
		//-----------------------------------------------------------------------------------------------------

		JButton  genBtn = new JButton("Generate Shapes");
		genBtn.addActionListener(l->{
			SwingUtilities.invokeLater(()->{
				int z = view.getSelectedZ();
				if(cBuilder.hasTrainingSet()){
					options.setUseClassifier(true);					
					options.setClassifier(cBuilder.build(new double[]{0.5,0.5}));
				}
				options.setMinScore(-1000);
				processPlane(bag.getFirstFlat(), bag.getInput(), ovl, labeled, z, view.getSelectedT());
				T label = view.getType().createVariable();
				label.setReal(mgr.getAppearance(unclassified));
				ridges.get(z).forEach((f,r)->r.drawBorder(label));
				view.refreshView();
			});
		});


		JButton  bestBtn = new JButton("Create Best Guess");
		bestBtn.addActionListener(l->{
			SwingUtilities.invokeLater(()->{

				if(!cBuilder.hasTrainingSet()){
					requestMoreShapes();
					return;					
				}

				int z = view.getSelectedZ();
				options.setUseClassifier(true);
				options.setMinScore(0);
				options.setClassifier(cBuilder.build(new double[]{0.5,0.5}));

				processPlane(bag.getFirstFlat(), bag.getInput(), ovl, labeled, z, view.getSelectedT());
				T label = view.getType().createVariable();
				label.setReal(mgr.getAppearance(unclassified));
				ridges.get(z).forEach((f,r)->r.drawBorder(label));
				view.refreshView();
			});
		});




		ButtonGroup btnGroup = new ButtonGroup();
		JCheckBox validBox = new JCheckBox("Select Valid Shapes");
		validBox.addActionListener(l->setCurrentClasse(0));
		validBox.setSelected(true);
		btnGroup.add(validBox);

		JCheckBox invalidBox = new JCheckBox("Select Invalid Shapes");
		invalidBox.addActionListener(l->setCurrentClasse(1));
		btnGroup.add(invalidBox);

		JCheckBox undoBox = new JCheckBox("Undo");
		undoBox.addActionListener(l->setCurrentClasse(2));
		btnGroup.add(undoBox);

		GroupPanel firstPanel = new GroupPanel(10, false, genBtn, bestBtn, validBox, invalidBox, undoBox);




		JButton predictButton = new JButton("Show Prediction");
		predictButton.addActionListener(l->{
			if(!cBuilder.hasTrainingSet()){
				requestMoreShapes();
				return;				
			}
			doPrediction();
		});


		JButton highlightButton = new JButton("Highlight Lowest Scores");
		highlightButton.addActionListener(l->{
			if(!cBuilder.hasTrainingSet()){
				requestMoreShapes();
				return;					
			}
			showAmbiguous();
		});


		JButton stsBtn = new JButton("Save Classifier");
		stsBtn.addActionListener(l->{	
			
			if(!cBuilder.hasTrainingSet()){
				requestMoreShapes();
				return;					
			}

			File f = WebFileChooser.showSaveDialog();
			if(f!=null)
				cBuilder.saveToFile(f);
			else 
				return;

			WebNotification wn = NotificationManager.showNotification("Classifier saved!");
			wn.setDisplayTime(1000);

			NaiveBayesClassifier custom = cBuilder.build(new double[]{0.5,0.5});
			classifierTypeBox.addItem(custom); 

		});


		GroupPanel scdPanel = new GroupPanel(10, predictButton, highlightButton, stsBtn);

		JButton backBtn = new JButton("Back To Classification Choices");
		backBtn.addActionListener(l->{
			options.setUseClassifier(true);
			options.setClassifier((NaiveBayesClassifier) classifierTypeBox.getSelectedItem());
			options.setMinScore(0);
			view.setLut(view.getAnnotationIndex(), Lut16.RANDOM.name());
			cBuilder = null; //cBuilder is used by the mouse listener to determine how to handle the click
			ridges.forEach((z,map) -> map.forEach((f,r)->r.drawBorder()));
			lowerCard.previous(lowerPanel);
		});



		GroupPanel trainingPanel = new GroupPanel(10, false, firstPanel, scdPanel, backBtn);	
		trainingPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

		lowerPanel.add(trainingPanel, training);

		card1.show(this, maxima);

	}

	
	
	private void requestMoreShapes(){
		int validSize = cBuilder.getCurrentTrainingSetSize(options.validClass());
		int invalidSize = cBuilder.getCurrentTrainingSetSize(options.invalidClass());
		JOptionPane.showMessageDialog(null, "At least 10 shapes for each classe is required: (Valid: "+validSize+"; Invalid: "+invalidSize+")");
	}
	
	
	


	private void setCurrentClasse(int i) {
		currentClasse = i;
	}






	private void showAmbiguous() {

		
		NaiveBayesClassifier csf = cBuilder.build(new double[]{0.5,0.5});
		RandomAccessibleInterval<T> skelPlane = ovl;

		if(tDim != -1){					
			skelPlane = Views.hyperSlice(skelPlane, tDim, view.getSelectedT());//Get One frame
		}
		if(zDim != -1){					
			skelPlane = Views.hyperSlice(skelPlane, zDim, view.getSelectedZ());//Get One slice
		}

		final RandomAccess<T> access = skelPlane.randomAccess();
		final T color = access.get().createVariable();
		color.setReal(mgr.getAppearance(ambiguous));

		// Get the score of the "valid" posterior for each shape and compute stats
		List<Ridge<T,V>> list = new ArrayList<>(ridges.get(view.getSelectedZ()).values());
		Collections.sort(list, (r1, r2)->{
			return Double.compare(csf.getPosterior(options.validClass(), r1.getFeatures()),
					csf.getPosterior(options.validClass(), r2.getFeatures()));
		});

		//Now color in yellow the 5% lowest scores
		list.subList(0, (int)(0.05f*(float)list.size())).forEach((r)->{
				color.setReal(mgr.getAppearance(ambiguous));
				r.drawBorder(access, color);			
		});		

		view.refreshView();
	}





	private void doPrediction() {

		NaiveBayesClassifier csf = cBuilder.build(new double[]{0.5,0.5});

		RandomAccessibleInterval<T> skelPlane = ovl;


		if(tDim != -1){					
			skelPlane = Views.hyperSlice(skelPlane, tDim, view.getSelectedT());//Get One frame
		}
		if(zDim != -1){					
			skelPlane = Views.hyperSlice(skelPlane, zDim, view.getSelectedZ());//Get One slice
		}

		final RandomAccess<T> access = skelPlane.randomAccess();
		final T color = access.get().createVariable();
		ridges.get(view.getSelectedZ()).forEach((f,r)->{
			String classe = csf.getPrediction(r.getFeatures());
			if(classe == options.validClass())
				color.setReal(mgr.getAppearance(valid));
			else
				color.setReal(mgr.getAppearance(invalid));
			r.drawBorder(access, color);
		});

		view.refreshView();
	}







	private void showMaxima() {

		RandomAccessibleInterval<V> graysPlane = steered;
		RandomAccessibleInterval<T> ovlPlane = ovl;


		if(tDim != -1){					
			graysPlane = Views.hyperSlice(graysPlane, tDim, view.getSelectedT());//Get One frame
			ovlPlane = Views.hyperSlice(ovlPlane, tDim, view.getSelectedT());
		}
		if(zDim != -1){					
			graysPlane = Views.hyperSlice(graysPlane, zDim, view.getSelectedZ());//Get One slice
			ovlPlane = Views.hyperSlice(ovlPlane, zDim, view.getSelectedZ());
		}

		Views.iterable(ovlPlane).forEach(t->t.setZero());

		final List<Point> peaks = RidgeSegmentation.maximaForPlane(graysPlane, options);
		final RandomAccess<T> access = Views.extendZero(ovlPlane).randomAccess();
		final long[] center = new long[2];
		final T value = access.get().createVariable();
		value.setReal(value.getMaxValue());
		for(Point p : peaks){
			p.localize(center);
			ImgGeometry.drawCircle(center, 3, access, value);
		}

		view.refreshView();

	}







	public void mouseClicked(MouseEvent e) {

		RandomAccessibleInterval<T> skelPlane = ovl;
		if(tDim != -1)			
			skelPlane = Views.hyperSlice(skelPlane, tDim, view.getSelectedT());//Get One frame

		if(zDim != -1){				
			skelPlane = Views.hyperSlice(skelPlane, zDim, view.getSelectedZ());//Get One slice

			final RandomAccess<T> access = skelPlane.randomAccess();
			final T label = access.get().createVariable();

			long[] pos = view.getImageCoordinates(e.getX(), e.getY());

			Ridge r = getRidge(pos);
			if(r== null)
				return;

			if(cBuilder == null){//We are in the preview panels

				final JFrame popOver = new JFrame();
				popOver.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
				popOver.setAlwaysOnTop(true);
				popOver.setLayout ( new FlowLayout () );

				//TextPane for the description of the object
				JTextPane pane = new JTextPane();
				pane.setPreferredSize(new Dimension(250,600));
				pane.setText(r.toString());

				popOver.setTitle("Area Features...");
				popOver.add ( new JScrollPane(pane) );
				popOver.pack();
				popOver.setLocation(e.getLocationOnScreen());
				popOver.setVisible(true);


			}
			else{// We are in the trainingPanel



				if(currentClasse == 0){
					cBuilder.addTrainingValue(options.validClass(), r.getFeatures());
					label.setReal(mgr.getAppearance(valid));
				}
				else if(currentClasse == 1){
					cBuilder.addTrainingValue(options.invalidClass(), r.getFeatures());
					label.setReal(mgr.getAppearance(invalid));
				}
				else{
					cBuilder.removeTrainingValue(options.invalidClass(), r.getFeatures());
					cBuilder.removeTrainingValue(options.validClass(), r.getFeatures());
					label.setReal(mgr.getAppearance(unclassified));
				}


				r.drawBorder(access, label);
				view.refreshView();
			}
		}

	}





	public Ridge<T,V> getRidge(long[] point) {

		RandomAccessibleInterval<T> labels = labeled;

		// Get the label under the mouse click
		// Remove channel dimension
		long[] result = new long[point.length-1];
		int[] order = view.getOrder();
		int p = 0;
		for(int i = 0; i<order.length; i++){
			if(order[i]!=-1 && i!=Image.c){
				result[p] = point[order[i]];
				p++;				
			}
		}

		RandomAccess<T> access = labels.randomAccess();
		access.setPosition(result);
		T value = access.get();

		// Return the corresponding Ridge
		Map<Float,Ridge<T,V>> map = null;
		if(zDim != -1)
			map = ridges.get((int)result[2]);
		else
			map = ridges.get(-1);

		if(map == null)
			return null;


		return map.get(value.getRealFloat());
	}








	RidgeSegmentationOptions getOptions(){
		return options;
	}





	@Override
	public boolean validity() {
		return true;
	}


	public void updateInput(PreviewBag<V> current) {
		this.steered = current.getInput();
		this.channel = current.getFirstFlat();
	}



	public void stopPreview() {
		// TODO Auto-generated method stub

	}







	private class Preview implements Runnable{

		private JButton prevButton;
		private JLabel info;
		private boolean done = true;
		private ActionListener start = l -> new Thread(this).start();
		private ActionListener cancel = l -> this.cancel();

		public Preview(JButton prevButton, JLabel info) {
			this.prevButton = prevButton;	
			this.prevButton.addActionListener(start);
			this.info = info;
		}


		public void run(){

			options.setClassifier((NaiveBayesClassifier)classifierTypeBox.getSelectedItem());

			done = false;
			prevButton.setText("Cancel");
			this.prevButton.removeActionListener(start);
			this.prevButton.addActionListener(cancel);

			info.setText("Running preview...");


			options.description(System.out::println);


			info.setText("Processing Plane...");
			int z = view.getSelectedZ();
			processPlane(channel, steered, ovl, labeled, z, view.getSelectedT());

			// clean plane before drawing			
			//Views.iterable(fskelPlane).forEach(p->p.setZero());
			ridges.get(z).forEach((f,r)->r.drawBorderAndArea());

			view.refreshView();





			if(!options.isCancelled()){
				view.refreshView();
				prevButton.setText("Preview");
				this.prevButton.removeActionListener(cancel);
				this.prevButton.addActionListener(start);
				info.setText("Done !");
			}


			done = true;
		}




		void cancel(){
			options.setCancelled(true);	
			new Thread(()->{

				prevButton.setText("Cancelling...");
				prevButton.setEnabled(false);

				while(!done){
					ThreadUtils.sleepSafely(200);
				}

				this.prevButton.removeActionListener(cancel);
				this.prevButton.addActionListener(start);
				view.refreshView();
				options.setCancelled(false);
				prevButton.setText("Run Preview...");
				this.prevButton.setEnabled(true);
				info.setText("Preview cancelled");

			}).start();


		}


	}





	private void processPlane(RandomAccessibleInterval<O> channels, RandomAccessibleInterval<V> grays,RandomAccessibleInterval<T> tracks, RandomAccessibleInterval<T> labels, int z, int t){


		RandomAccessibleInterval<O> channelPlane = channels;
		RandomAccessibleInterval<V> graysPlane = grays;
		RandomAccessibleInterval<T> skelPlane = tracks;
		RandomAccessibleInterval<T> labelsPlane = labels;


		if(tDim != -1){					
			channelPlane = Views.hyperSlice(channelPlane, tDim, t);//Get One frame
			graysPlane = Views.hyperSlice(graysPlane, tDim, t);//Get One frame
			skelPlane = Views.hyperSlice(skelPlane, tDim, t);//Get One frame
			labelsPlane = Views.hyperSlice(labelsPlane, tDim, t);//Get One frame
			System.out.println("Grays dimension after frame extract : "+graysPlane.numDimensions());
		}
		if(zDim != -1){		
			channelPlane = Views.hyperSlice(channelPlane, zDim, z);//Get One slice
			graysPlane = Views.hyperSlice(graysPlane, zDim, z);//Get One slice
			skelPlane = Views.hyperSlice(skelPlane, zDim, z);//Get One slice
			labelsPlane = Views.hyperSlice(labelsPlane, zDim, z);//Get One slice
			System.out.println("Grays dimension after slice extract : "+graysPlane.numDimensions());
		}



		//Ensure the planes are not labeled already
		Views.iterable(skelPlane).forEach(p->p.setZero());
		Views.iterable(labelsPlane).forEach(p->p.setZero());



		Iterator<T> gen = new LabelGen<T>(skelPlane.randomAccess().get().createVariable());


		List<Point> peaks = RidgeSegmentation.maximaForPlane(graysPlane, options);

		ridges.put(z, RidgeSegmentation.processPlane(channelPlane, graysPlane, skelPlane, labelsPlane, peaks, options, gen));



		System.out.println("Plane " + z + " done !!!");


	}



	
	private NaiveBayesClassifier[] getPresetCassifiers(){
		/*
		File folder = new File(getClass().getResource("/classifiers/").getFile());
		System.out.println("Classifier folder "+folder);
		File[] cfs = folder.listFiles();
		NaiveBayesClassifier[] nbcs = new NaiveBayesClassifier[cfs.length];
		for(int i = 0; i<cfs.length; i++)
			nbcs[i] = ClassifierBuilder.fromFile(cfs[i]);
		
		*/
		
		NaiveBayesClassifier[] nbcs = new NaiveBayesClassifier[1];
		nbcs[0] =  ClassifierBuilder.fromResource(new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/classifiers/E70_Classifier"))), "E70_Classifier");
	
		return nbcs;
	}
	




}
