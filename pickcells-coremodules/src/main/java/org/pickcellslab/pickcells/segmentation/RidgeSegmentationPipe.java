package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import org.apache.log4j.Logger;
import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.pipeline.DimensionalityEffect;
import org.pickcellslab.pickcells.api.img.pipeline.ImageBag;
import org.pickcellslab.pickcells.api.img.pipeline.ImageViewState;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;
import org.pickcellslab.pickcells.api.img.pipeline.PreviewBag;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;
import org.pickcellslab.pickcells.api.img.process.LabelGen;
import org.pickcellslab.pickcells.api.img.view.Lut16;

import com.alee.utils.SwingUtils;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.img.ImgFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;
import net.imglib2.util.Util;
import net.imglib2.view.Views;

public class RidgeSegmentationPipe<V extends RealType<V> & NativeType<V>> extends MouseAdapter implements ImgProcessing<V,FloatType>{

	private static final Logger log = Logger.getLogger(RidgeSegmentationPipe.class);

	private final RidgeSegmentationOptions options = new RidgeSegmentationOptions();
	private ClassifyingRBSPanel dialog;
	private PreviewBag<V> bag;
	private RandomAccessibleInterval<FloatType> result;
	private ImageViewState<?> state;
	private int zDim, tDim;


	@Override
	public String name() {
		return "Ridge Segmentation";
	}
	
	
	

	@Override
	public <T extends RealType<T>> ValidityTogglePanel inputDialog(ImageViewState<T> state, PreviewBag<V> input) {

		this.bag = input;
		// Make sure that this input is not using any negative values	

		this.state = state;


		int[] noC = Image.removeDimension(state.getOrder(), Image.c);
		zDim = noC[Image.z];
		tDim = noC[Image.t];


		this.dialog = new ClassifyingRBSPanel(state, bag, zDim, tDim, options);	//FIXME assuming NativeType

		return dialog;

	}



	@Override
	public void changeInput(PreviewBag<V> current) {
		this.bag = current;	
		dialog.updateInput(current);
	}




	@Override
	public RandomAccessibleInterval<FloatType> validate() {


		// input -> stack of gray values
		// ovl -> overlay of the view (holds tracks)
		// result -> to be returned and written in preview

		RandomAccessibleInterval<V> input = bag.getInput();
		
		dialog.getOptions().setUseClassifier(true);
		
		
		Views.iterable(state.getAnnotationImage()).forEach(t->t.setZero());

		Iterator<FloatType> gen = new LabelGen<>(new FloatType());
		FloatType type = gen.next();
		RandomAccessibleInterval<FloatType> tracks = Util.getArrayOrCellImgFactory(input, type).create(input, type);
		result = Util.getArrayOrCellImgFactory(input, type).create(input, type); 

		
		
		final RidgeSegmentationOptions options = dialog.getOptions();


		int frameNumber = tDim != -1 ? (int)input.dimension(tDim) : 1;
		int sliceNumber = zDim != -1 ? (int)input.dimension(zDim) : 1;

		IntStream.range(0, frameNumber).forEach(t->{
			
			
			IntStream.range(0, sliceNumber).parallel().forEach(z->{

				RandomAccessibleInterval<V> graysPlane = input;
				RandomAccessibleInterval<V> channelPlane = bag.getFirstFlat();
				RandomAccessibleInterval<FloatType> skelPlane = tracks;
				RandomAccessibleInterval<FloatType> labelsPlane = result;


				if(tDim != -1){				
					channelPlane = Views.hyperSlice(channelPlane, tDim, t);//Get One frame
					graysPlane = Views.hyperSlice(graysPlane, tDim, t);//Get One frame
					skelPlane = Views.hyperSlice(skelPlane, tDim, t);//Get One frame
					labelsPlane = Views.hyperSlice(labelsPlane, tDim, t);//Get One frame
				}
				if(zDim != -1){					
					channelPlane = Views.hyperSlice(channelPlane, zDim, z);//Get One frame
					graysPlane = Views.hyperSlice(graysPlane, zDim, z);//Get One slice
					skelPlane = Views.hyperSlice(skelPlane, zDim, z);//Get One slice
					labelsPlane = Views.hyperSlice(labelsPlane, zDim, z);//Get One slice
				}


				RidgeSegmentation.processPlane(channelPlane, graysPlane, skelPlane, labelsPlane, null, options, gen);
				//log.info("Slice "+z+" of Frame "+t+" Done !");


			});
			
			
		});

		state.setLut(state.getPreviewIndex(), Lut16.RANDOM.name());

		ImgDimensions.copyRealAndContrast(tracks, state.getAnnotationImage());

		
		
		
		return result; 
	}







	@Override
	public String description() {
		return "Ridge Based Segmentation";
	}

	@Override
	public boolean repeatable() {
		return false;
	}

	@Override
	public DimensionalityEffect dimensionalityEffect() {
		return DimensionalityEffect.FLAT_TO_FLAT;
	}

	@Override
	public boolean requiresAnnotationChannel() {
		return true;
	}



	@Override
	public Function<ImageBag<V>, RandomAccessibleInterval<FloatType>> toPipe() {
		return new RSPipe(zDim, dialog.getOptions());
	}
	
	
	
	RidgeSegmentationOptions getOptions(){
		return options;
	}
	


	private class RSPipe<O extends RealType<O> & NativeType<O>> implements Function<ImageBag<V>, RandomAccessibleInterval<FloatType>>{

		private final RidgeSegmentationOptions options;

		public RSPipe(int zDim, RidgeSegmentationOptions options) {
			this.options = options;
		}


		@Override
		public RandomAccessibleInterval<FloatType> apply( final ImageBag<V> bag) {

			final MinimalImageInfo info = bag.getInputFrameInfo();
			final RandomAccessibleInterval<V> input = bag.getInputFrame(0);
			final RandomAccessibleInterval<O> channel = bag.getFirstFlat(0);
			
			// Make sure options is in run mode
			options.setCancelled(false);
			Ridge.updater = null;
			RidgePoint.update = null;



			Iterator<FloatType> gen = new LabelGen<>(new FloatType());
			FloatType type = gen.next();
			ImgFactory<FloatType> fctry =  Util.getArrayOrCellImgFactory(input, type);
			RandomAccessibleInterval<FloatType> result = fctry.create(input, type);	//FIXME very large datasets issue!




			//TODO Check if result fits in memory			
				
				LongStream.range(0, info.dimension(Image.z)).parallel().forEach(z->{

					RandomAccessibleInterval<O> channelPlane = channel;		
					RandomAccessibleInterval<V> graysPlane = input;					
					RandomAccessibleInterval<FloatType> labelsPlane = result;


					if(zDim != -1){			
						channelPlane = Views.hyperSlice(channelPlane, info.index(Image.z), z);//Get One frame
						graysPlane = Views.hyperSlice(graysPlane, info.index(Image.z), z);//Get One slice
						labelsPlane = Views.hyperSlice(labelsPlane, info.index(Image.z), z);//Get One slice
					}

					final RandomAccessibleInterval<FloatType> skelPlane = fctry.create(labelsPlane, type); // Created on the fly to save memory

					RidgeSegmentation.processPlane(channelPlane, graysPlane, skelPlane, labelsPlane, null, options, gen);
					//TODO show progress log.info("Slice "+z+" of Frame "+t+" Done !");

				});

						
			return result;
		}
	}
	
	
	


	@Override
	public void stop() {
		if(dialog!=null)
			dialog.stopPreview();		
	}


	public RandomAccessibleInterval<FloatType> getResult(){
		return result;
	}



	@Override
	public void mouseClicked(MouseEvent e) {

		if(SwingUtils.isRightMouseButton(e)){
			dialog.mouseClicked(e);
		}
		

	}

	@Override
	public boolean requiresOriginal() {
		return false;
	}

	@Override
	public boolean requiresFlat() {
		return true;
	}




	@Override
	public FloatType outputType(V inputType) {
		return new FloatType();
	}




}
