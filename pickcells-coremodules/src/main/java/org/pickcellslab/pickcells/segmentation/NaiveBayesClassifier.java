package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.distribution.BetaDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.commons.math3.distribution.WeibullDistribution;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

public class NaiveBayesClassifier {

	public static final String UNKNOWN = "Unknown";

	private String name;
	private File file;

	private final String[] classes;
	private final double[] priors;
	private final RealDistribution[][] distris;

	private final Map<String,Integer> indices = new HashMap<>();


	private NaiveBayesClassifier(String[] classes, double[] priors, RealDistribution[][] distris) {
		this.classes = classes;
		this.priors = priors;
		this.distris = distris;
		for(int i = 0; i<classes.length; i++)
			indices.put(classes[i], i);
	}


	public double getPosterior(String c, double[] value){
		return getPosterior(indices.get(c), value);		
	}


	public List<double[]> random(String classe, int n){
		int c = indices.get(classe);
		List<double[]> list = new ArrayList<>();
		for(int i = 0;i<n; i++){
			double[] v = new double[distris[c].length];
			for(int d = 0; d<distris[c].length; d++){
				v[d] = distris[c][d].sample();
			}
			list.add(v);
		}
		return list;
	}


	public String getPrediction(double[] value){

		double maxPost = Double.NEGATIVE_INFINITY;
		int max = -1;
		for(int i = 0; i<classes.length; i++){
			double p = getPosterior(i, value);

			if(p>maxPost){
				maxPost = p;
				max = i;
			}
		}
		if(max==-1 || maxPost == 0d)
			return UNKNOWN;			
		else
			return classes[max];
	}


	public double getPosterior(int i, double[] value){
		RealDistribution[] ds = distris[i];
		double post = priors[i];
		for(int d = 0; d<ds.length; d++){
			double prob = ds[d].density(value[d]);
			//	System.out.println(prob);
			post *= prob;
		}
		//System.out.println("Posterior = "+post);

		return post;
	}



	public File getFile() {
		return file;
	}



	public void setFile(File file){
		this.file = file;
		this.name = file.getName();
	}



	@Override
	public String toString(){
		return name == null ? super.toString() : name;
	}






	public static class ClassifierBuilder{

		private final Map<String, List<double[]>> trainingSet; 

		private int length = -1;

		private String[] classes;

		private File file;

		public ClassifierBuilder(String[] classes) {
			this.classes = Arrays.copyOf(classes, classes.length);
			this.trainingSet = new HashMap<>();
			for(String s : classes)
				trainingSet.put(s, new ArrayList<>());
		}



		public void addTrainingValue(String set, double[] value){

			if(length!=-1)
				assert value.length == length : "The provided value length is not consistent with previously entered values: "+value.length;
			else
				length = value.length;

			assert trainingSet.containsKey(set) : "Unknown class : "+set;

			trainingSet.get(set).add(value);

		}


		public void removeTrainingValue(String set, double[] value){
			assert trainingSet.containsKey(set) : "Unknown class : "+set;
			Iterator<double[]> list = trainingSet.get(set).iterator();
			while(list.hasNext()){
				double[] n = list.next();
				if(Arrays.equals(n, value)){
					list.remove();
					System.out.println("Value removed from "+set);
					break;
				}
			}
		}



		public NaiveBayesClassifier build(double[] priors){
			RealDistribution[][] distris = new RealDistribution[classes.length][length];
			for(int i = 0; i<classes.length; i++){
				List<double[]> list = trainingSet.get(classes[i]);

				for(int j = 0; j<length; j++){					
					// compute mean and sigma
					SummaryStatistics stats = new SummaryStatistics();
					for(double[] array : list)
						stats.addValue(array[j]);


					distris[i][j] = new NormalDistribution(stats.getMean(), stats.getStandardDeviation());
					//	System.out.println(distris[i][j]);
				}
			}		
			NaiveBayesClassifier nbc = new NaiveBayesClassifier(classes, priors, distris);
			if(file!=null)
				nbc.setFile(file);
			return nbc;
		}


/*

		public NaiveBayesClassifier build(double[] priors){

			RealDistribution[][] distris = new RealDistribution[classes.length][length];

			for(int i = 0; i<classes.length; i++){

				//System.out.println("Class : "+classes[i]);

				for(int j = 0; j<length; j++){	

					//System.out.println("Param : "+j);

					List<double[]> list = trainingSet.get(classes[i]);
					double[] arr = new double[list.size()];
					//DescriptiveStatistics stats = new DescriptiveStatistics();
					for(int s = 0; s<list.size(); s++){					
						arr[s] = list.get(s)[j];
						//	stats.addValue(arr[s]);
					}

					//	System.out.println(stats.getSkewness());
					//	if(stats.getSkewness()<-0.2 || stats.getSkewness()>0.2){
					int bin = (int) Math.max(Math.sqrt(arr.length), 3);
					EmpiricalDistribution d = new KdeDistribution(bin);
					distris[i][j] = d;
					d.load(arr);
					//System.out.println("Kde chosen");
					//	}
					//	else{
					//		distris[i][j] = new NormalDistribution(null, stats.getMean(), stats.getStandardDeviation());
					//		System.out.println("Normal chosen");
					//	}
				}

			}		
			return new NaiveBayesClassifier(classes, priors, distris);
		}

*/



		public void saveToFile(File file){

			try {

				PrintWriter pw = new PrintWriter(file);
				//Header
				pw.print("Class");
				for(String s : Ridge.getFeatureNames()){
					pw.print("\t"+s);
				}


				trainingSet.forEach((classe, list)->{

					for(double[] arr : list){
						pw.println("");
						pw.print(classe);
						for(double d : arr)
							pw.print("\t"+d);
					}
				});

				pw.flush();
				pw.close();

				this.file = file;

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}



		}




		public static NaiveBayesClassifier fromFile(File file){		

			ClassifierBuilder cBuilder = ClassifierBuilder.builderFromFile(file);
			NaiveBayesClassifier classifier = cBuilder.build(new double[]{0.5,0.5});
			classifier.setFile(file);
			return classifier;

		}




		public boolean hasTrainingSet() {
			for(String s : classes)
				if(trainingSet.get(s).size()<10)
					return false;
			return true;
		}


		public int getCurrentTrainingSetSize(String classe){
			final List<double[]> set = trainingSet.get(classe);
			return set == null ? 0 : set.size();
		}
		


		public static ClassifierBuilder builderFromClassifier(NaiveBayesClassifier nbc) {
			assert nbc != null : "Classifier is null";			
			return ClassifierBuilder.builderFromFile(nbc.file);
		}



		private static ClassifierBuilder builderFromFile(File file) {

			String line = "";
			String cvsSplitBy = "\t";

			ClassifierBuilder cBuilder = new ClassifierBuilder(RidgeSegmentationOptions.classes);

			try (BufferedReader br = new BufferedReader(new FileReader(file))) {

				//headers
				br.readLine();

				while ((line = br.readLine()) != null) {

					// use comma as separator
					String[] row = line.split(cvsSplitBy);
					double[] value = new double[row.length-1];
					for(int i = 1; i<row.length; i++)
						value[i-1] = Double.parseDouble(row[i]);

					cBuilder.addTrainingValue(row[0], value);

				}

				return cBuilder;

			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}




		public static NaiveBayesClassifier fromResource(BufferedReader br, String name) {



			String line = "";
			String cvsSplitBy = "\t";

			ClassifierBuilder cBuilder = new ClassifierBuilder(RidgeSegmentationOptions.classes);

			try {

				//headers
				br.readLine();

				while ((line = br.readLine()) != null) {

					// use comma as separator
					String[] row = line.split(cvsSplitBy);
					double[] value = new double[row.length-1];
					for(int i = 1; i<row.length; i++)
						value[i-1] = Double.parseDouble(row[i]);

					cBuilder.addTrainingValue(row[0], value);

				}

				NaiveBayesClassifier nbc = cBuilder.build(new double[]{0.5,0.5});
				nbc.name = name;

				return nbc;

			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}




	}



	public static void main(String[] args) {

		RealDistribution invalid1 = new BetaDistribution(2, 2);
		RealDistribution valid1 = new BetaDistribution(0.5, 0.5);
		RealDistribution invalid2 = new WeibullDistribution(0.5, 1);
		RealDistribution valid2 = new WeibullDistribution(1.5, 1);
		RealDistribution invalid3 = new NormalDistribution(1000, 500);
		RealDistribution valid3 = new NormalDistribution(1250,500);

		ClassifierBuilder cBuilder = new ClassifierBuilder(RidgeSegmentationOptions.classes);


		for(int i = 0; i<30; i++){
			double[] v = new double[3];
			v[0] = valid1.sample();
			v[1] = valid2.sample();
			v[2] = valid3.sample();	
			cBuilder.addTrainingValue(RidgeSegmentationOptions.classes[0], v);
			double[] w = new double[3];
			w[0] = invalid1.sample();
			w[1] = invalid2.sample();
			w[2] = invalid3.sample();
			cBuilder.addTrainingValue(RidgeSegmentationOptions.classes[1], w);
		}


		NaiveBayesClassifier classifier = cBuilder.build(new double[]{0.5, 0.5});

		int valid = 0, invalid = 0, unknown = 0;
		for(int i = 0; i<1000; i++){

			double[] v = new double[3];
			v[0] = valid1.sample();
			v[1] = valid2.sample();
			v[2] = valid3.sample();

			double[] w = new double[3];
			w[0] = invalid1.sample();
			w[1] = invalid2.sample();
			w[2] = invalid3.sample();

			String correct = classifier.getPrediction(v);
			String wrong = classifier.getPrediction(w);
			if(correct == RidgeSegmentationOptions.classes[0])
				valid++;
			else if(correct == UNKNOWN)
				unknown++;
			if(wrong == RidgeSegmentationOptions.classes[1])
				invalid++;
			else if(wrong == UNKNOWN)
				unknown++;
		}



		System.out.println("Valid accuracy "+valid);
		System.out.println("Invalid accuracy "+invalid);
		System.out.println("Number of unknown "+unknown);
	}







}
