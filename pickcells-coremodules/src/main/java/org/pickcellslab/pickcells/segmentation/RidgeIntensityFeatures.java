package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.pickcellslab.foundationj.datamodel.tools.MutableInt;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;

import net.imglib2.outofbounds.OutOfBounds;
import net.imglib2.type.numeric.RealType;

/**
 * 
 * An immutable class which computes and stores intensity features
 * 
 * @author Guillaume Blin
 */
public class RidgeIntensityFeatures {











	private final double borderSize, areaSC;
	private final double sBorderMean,sBorderMedian, sBorderIQR;
	//private final double sInnerMean, sInnerMedian, sInnerIQR;
	
	private final double cBorderMean, cBorderMedian, cBorderIQR;
	private final double cInnerMean, cInnerMedian, cInnerIQR;


	<T extends RealType<T>, V extends RealType<V>> RidgeIntensityFeatures(OutOfBounds<V> steeredAccess, OutOfBounds<V> channelAccess, Ridge<T,V> ridge) {

		OutOfBounds<V> sAccess = steeredAccess.copy();
		OutOfBounds<V> cAccess = channelAccess.copy();

		
		MutableInt ar = new MutableInt();
		
		// Border
		final DescriptiveStatistics sStats = new DescriptiveStatistics();
		final DescriptiveStatistics cStats = new DescriptiveStatistics();
		for(long[] l : ridge){
			sAccess.setPosition(l);								cAccess.setPosition(l);
			sStats.addValue(sAccess.get().getRealDouble());		cStats.addValue(cAccess.get().getRealDouble());
			ar.increment();
		}
		
		sBorderMean = sStats.getMean();	sBorderMedian = sStats.getPercentile(2); 	sBorderIQR = (sStats.getPercentile(3) - sStats.getPercentile(1))/sBorderMean;
		cBorderMean = cStats.getMean();	cBorderMedian = cStats.getPercentile(2); 	cBorderIQR = (cStats.getPercentile(3) - cStats.getPercentile(1))/cBorderMean;
		
		borderSize = ar.doubleValue();

		// Inner
		sStats.clear();
		cStats.clear();
		
		List<long[]> polygon = ridge.toPolyLineSampled(3);
		ImgGeometry.FillPolygon(polygon, (l1,l2)->{
			ImgGeometry.linePath(l1,l2, (pos)->{
				cAccess.setPosition(pos);							//sAccess.setPosition(pos);							
				cStats.addValue(cAccess.get().getRealDouble());		//sStats.addValue(sAccess.get().getRealDouble());		
				ar.increment();
			});
		});

		//sInnerMean = sStats.getMean();	sInnerMedian = sStats.getPercentile(2); 	sInnerIQR = sStats.getPercentile(3) - sStats.getPercentile(1);
		cInnerMean = cStats.getMean();	cInnerMedian = cStats.getPercentile(2); 	cInnerIQR = (cStats.getPercentile(3) - cStats.getPercentile(1))/cInnerMean;
		
		areaSC = ar.doubleValue();
	}

	
	
	public double getBorderSize() {
		return areaSC;
	}
	
	

	public double getAreaSC() {
		return areaSC;
	}



	public double getsBorderMean() {
		return sBorderMean;
	}







	




	public double getcBorderMean() {
		return cBorderMean;
	}







	public double getcInnerMean() {
		return cInnerMean;
	}


	

	public double getsBorderMedian() {
		return sBorderMedian;
	}



	public double getsBorderIQR() {
		return sBorderIQR;
	}

/*
	public double getsInnerMean() {
		return sInnerMean;
	}

	public double getsInnerMedian() {
		return sInnerMedian;
	}



	public double getsInnerIQR() {
		return sInnerIQR;
	}
*/


	public double getcBorderMedian() {
		return cBorderMedian;
	}



	public double getcBorderIQR() {
		return cBorderIQR;
	}



	public double getcInnerMedian() {
		return cInnerMedian;
	}



	public double getcInnerIQR() {
		return cInnerIQR;
	}

	
	
	public double getBorderChannelDistanceFromMean(){
		return Math.abs(cBorderMedian - cBorderMean)/cBorderMean;
	}
	
	public double getBorderSteeredDistanceFromMean(){
		return Math.abs(sBorderMedian - sBorderMean)/sBorderMean;
	}
	
	public double getInnerChannelDistanceFromMean(){
		return Math.abs(cInnerMedian - cInnerMean)/cInnerMean;
	}
	
	/*
	public double getInnerSteeredDistanceFromMean(){
		return (sInnerMedian - sInnerMean)/sInnerMean;
	}
	*/







	@Override
	public String toString() {
		String s = "RidgeIntensityFeatures : \n";
		s+="Path Size :"+borderSize;
		s+="\nArea (ScanLine) "+areaSC;	
		s+="\n---- Steered Intensities -----";	
		s+="\nBorder Mean "+sBorderMean;
		s+="\nBorder Median "+sBorderMedian;
		s+="\nBorder IQR "+sBorderIQR;
		//s+="\nInner Mean "+sInnerMean;
		//s+="\nInner Median "+sInnerMedian;
		//s+="\nInner IQR "+sInnerIQR;
		s+="\n---- Channel Intensities -----";	
		s+="\nBorder Mean "+cBorderMean;
		s+="\nBorder Median "+cBorderMedian;
		s+="\nBorder IQR "+cBorderIQR;
		s+="\nInner Mean "+cInnerMean;
		s+="\nInner Median "+cInnerMedian;
		s+="\nInner IQR "+cInnerIQR;
		return s;
	}

}
