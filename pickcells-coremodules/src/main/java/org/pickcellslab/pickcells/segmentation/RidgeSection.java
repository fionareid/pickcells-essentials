package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;

import net.imglib2.Point;
import net.imglib2.type.numeric.RealType;

/**
 * 
 * A RidgeSection stores locations of a ridge from a parent {@link RidgePoint} to a child {@link RidgePoint}.
 * It also stores the location of the end of the parent zone of influence (crossing point)
 * 
 * @author Guillaume Blin
 *
 * @param <T>
 */
public class RidgeSection<T extends RealType<T>, V extends RealType<V>> {



	/**
	 * Polyline from parent to child
	 */
	private final List<long[]> track;
	private double length = -1;


	private int cp;




	/**
	 * @param access Access on the original image (non filtered)
	 * @param track The polyline defining this section, from the parent to the child end point
	 * @param cp
	 */
	public RidgeSection(List<long[]> track, int cp) {

		this.track = track;

		//TODO debug only
		Tracing.toChainCode(track); 

		if(track.isEmpty() || cp>=track.size() || track.size() == 1)
			throw new IllegalArgumentException("cp = "+cp+" ; track size : "+track.size());
		this.cp = cp;			
	}


	public RidgeSection(Point p) {		

		long[] loc = new long[2];
		p.localize(loc);
		track = new ArrayList<>(1);		
		track.add(loc);
		cp = 0;	
	}

	/**
	 * Write in pos the location of the crossing point + the given offset
	 * @param offset
	 * @param pos
	 */
	public void offsetCp(long[] offset, long[] pos){
		pos[0] = track.get(cp)[0] + offset[0];		
		pos[1] = track.get(cp)[1] + offset[1];
	}

	/**
	 * Write in pos the location of the parent center + the given offset
	 * @param offset
	 * @param pos
	 */
	public void offsetOrigin(long[] offset, long[] pos){
		pos[0] = track.get(0)[0] + offset[0];		
		pos[1] = track.get(0)[1] + offset[1];
	}


	/**
	 * @return An {@link Iterator} over the locations defining this section from child to parent
	 */
	public Iterator<long[]> childToParent(){
		return new Iterator<long[]>(){

			int i = track.size() -1;

			@Override
			public boolean hasNext() {
				return i >= 0;
			}

			@Override
			public long[] next() {
				return track.get(i--);
			}

			@Override
			public void remove(){
				track.remove(i+1);				
			}

		};
	}


	/**
	 * @return An {@link Iterator} over the locations defining this section from child to parent excluding
	 * the parent location from the track
	 */
	public Iterator<long[]> childToParentExcl(){
		return new Iterator<long[]>(){

			int i = track.size() - 1;

			@Override
			public boolean hasNext() {
				return i >= 1;
			}

			@Override
			public long[] next() {
				return track.get(i--);
			}


			@Override
			public void remove(){
				track.remove(i+1);				
			}

		};
	}


	/**
	 * @return An {@link Iterator} over the locations defining this section from parent to child (remove unsuported)
	 */
	public Iterator<long[]> parentToChild(){
		return Collections.unmodifiableList(track).iterator();
	}

	/**
	 * @return An {@link Iterator} over the locations defining this section from parent to child (remove unsuported)
	 * excluding the child center.
	 */
	public Iterator<long[]> parentToChildExcl(){

		return new Iterator<long[]>(){

			int i = 0;

			@Override
			public boolean hasNext() {
				return i <= track.size() - 2;
			}

			@Override
			public long[] next() {
				return track.get(i++);
			}

		};
	}



	/**
	 * @return The location of the child center
	 */
	public long[] childCenter() {
		return  track.get(track.size()-1);
	}


	public long[] nextAfterChild(){
		return track.get(track.size()-2);
	}


	/**
	 * @return The location of the crossing point (where the parent has initialised a blind track to find the child)
	 */
	public long[] cp() {
		return track.get(cp);
	}



	/**
	 * @return The number of points in the track
	 */
	public int size(){
		return track.size();
	}


	/**
	 * @return The location of the parent center
	 */
	public long[] parentCenter() {
		return  track.get(0);
	}


	public long[] nextAfterParent(){
		return track.get(1);
	}






	public Vector2D parentVector(){
		if(track.size()<4)
			return Vector2D.NaN;
		return new Vector2D(track.get(0)[0]-track.get(cp)[0], track.get(0)[1]-track.get(cp)[1]);
	}

	public Vector2D childVector(){
		if(track.size()<3)
			return Vector2D.NaN;
		return new Vector2D(track.get(track.size()-1)[0]-track.get(cp)[0], track.get(track.size()-1)[1]-track.get(cp)[1]);
	}


	public double parentChildAngle(){
		Vector2D v1 = parentVector();
		if(v1.getNorm() == 0)
			return 0;
		Vector2D v2 = parentVector();
		if(v2.getNorm() == 0)
			return 0;
		return Vector2D.angle(v1,v2);
	}



	/**
	 * @return The thength of the is section
	 */
	public double pathLength() {
		if(length == -1){
			long[] last = track.get(0);
			for(long[] pos : track){
				length += ImgGeometry.distance(last, pos);
				last = pos;
			}
		}
		return length;
	}




	/**
	 * 
	 * Rebases the childCenter to the given index in the current childToParent path
	 * @param furthest the index in the path where the child should be rebased
	 * @return A List of coordinates which were trimmed from the path (in child to parent order) including the
	 * new child center coordinate (last point of the list)
	 */
	public ArrayList<long[]> rebase(int furthest) {

		int count = 0;
		ArrayList<long[]> trimmed = new ArrayList<>(furthest);		
		Iterator<long[]> it = this.childToParent();

		while(count <= furthest){
			trimmed.add(it.next());
			it.remove();
			count++;
		}

		//System.out.println("After Rebase");
		track.add(trimmed.get(trimmed.size()-1));
		//track.forEach(l->System.out.println(Arrays.toString(l)));					
		//Tracing.toChainCode(track); //TODO debug only


		//rebase cp as well
		cp = track.size()/2;


		return trimmed;
	}


	public void addParentTrim(ArrayList<long[]> trim) {
		for(int i = 1; i<trim.size(); i++)
			track.add(0,trim.get(i));


		//track.forEach(l->System.out.println(Arrays.toString(l)));					
		//Tracing.toChainCode(track); //TODO debug only

		//rebase cp as well
		cp = track.size()/2;
	}


}
