package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.math3.util.Pair;
import org.apache.log4j.Logger;
import org.pickcellslab.foundationj.datamodel.graph.TreeNode;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;

import net.imglib2.Point;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.outofbounds.OutOfBounds;
import net.imglib2.outofbounds.OutOfBoundsConstantValueFactory;
import net.imglib2.outofbounds.OutOfBoundsFactory;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

public class RidgePoint<T extends RealType<T>, V extends RealType<V>> implements TreeNode<RidgePoint<T,V>> {

	private final static Logger log = Logger.getLogger(RidgePoint.class);

	private final long[][] branchKernel; 


	private final long childD; //Distance to next Child

	private static final long[][] maskKernel = {
			{-1,1},	{0,1}, {1,1},
			{-1,0},{0,0},{1,0},
			{-1,-1},{0,-1}, {1,-1} 
	};
	private static final long[][] traceKernel = {
			{-1,1},	{0,1}, {1,1},
			{-1,0},/*skip*/{1,0},
			{-1,-1},{0,-1}, {1,-1}
	};


	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static final OutOfBoundsFactory oos = new OutOfBoundsConstantValueFactory(null);


	// Debugging and visualisation

	static Consumer<RidgePoint<?,?>> update;
	static boolean printFailed = false;
	static boolean printLures = false;


	// Parameters

	private final RidgeSegmentationOptions o;


	// Tree Node fields

	private final RidgePoint<T,V> parent;		
	private RidgePoint<T,V>[] children;
	private HashSet<RidgePoint<T,V>> ancestors;



	// Ridge Point Specific variables

	private final float threshold;
	final float intensity;
	private final int depth;
	private LeafCase isLeaf;

	private final T id;
	private final V highestIntensity;

	/**
	 * The Ridge Section linking the center of the parent RidgePoint to this RidgePoint,
	 * If this RidgePoint is the root the path will have only one point
	 */
	private final RidgeSection<T,V> path;



	// Image accessors

	private final OutOfBounds<V> cAccess;
	private final OutOfBounds<V> rAccess;
	private final OutOfBounds<T> sAccess;
	private final OutOfBounds<T> lAccess;



	// Tree structure

	private final List<RidgePoint<T,V>> tree;
	private final Set<Integer> intersected;
	private final Map<RidgePoint<T,V>,List<long[]>> turns;



	private static int gen = 0;
	final int identifier;











	@SuppressWarnings("unchecked")
	public RidgePoint(RandomAccessibleInterval<V> channel, RandomAccessibleInterval<V> ridges, RandomAccessibleInterval<T> skeleton, RandomAccessibleInterval<T> labels, Point p, T id, long[][] branchKernel, RidgeSegmentationOptions o) {

		tree = new ArrayList<>();
		intersected = new HashSet<>();
		turns = new HashMap<>();

		this.tree.add(this);


		isLeaf = null;

		//First check that we are not already on another RidgePoint
		boolean setOff = false;


		// Get accessors
		V outValue = Views.iterable(ridges).firstElement().createVariable();
		outValue.setReal(id.getMinValue());
		rAccess = Views.extendValue(ridges, outValue ).randomAccess();
		cAccess = Views.extendZero(channel).randomAccess();
		sAccess = Views.extendZero(skeleton).randomAccess();
		lAccess = Views.extendZero(labels).randomAccess();


		rAccess.setPosition(p);	
		intensity = rAccess.get().getRealFloat();

		long[] center = new long[2];
		p.localize(center);
		long[] pos = new long[2];

		for(int k = 0; k<traceKernel.length; k++){
			pos[0] = center[0] + traceKernel[k][0];			pos[1] = center[1] + traceKernel[k][1];	
			sAccess.setPosition(pos);
			if(sAccess.get().getRealDouble()!=0){
				setOff = true;
				break;
			}
		}

		this.id = id;
		this.identifier = gen++;
		this.highestIntensity = rAccess.get().createVariable();
		this.parent = null;
		this.depth = 0;
		this.branchKernel = branchKernel;
		this.childD = (long) (o.getSearchRadius()*2);
		this.o = o;

		if(setOff){
			this.path = null;
			this.threshold = 0;
			this.children = new RidgePoint[0];
		}

		else{

			this.path = new RidgeSection<>(p);



			this.threshold = (float) (intensity*o.getDelta());


			// ********** Find children ************
			// 1- Find maxima at the periphery of the zone of influence
			List<long[]> maxima = sampleNeighborhood(path.parentCenter());



			// 2- For each maximum track edge from maximum to origin
			List<List<long[]>> tracks = tracksToOriginAsParent(maxima, path.parentCenter());


			// 4- Create children
			if(tracks.size() == 0){
				//Set to off
				this.children = new RidgePoint[0];	
				//log.warn("No neighborhood track identified for RidgePoint "+id);
				log.trace("No neighborhood track identified for RidgePoint "+id);
			}

			else{

				//Create children
				children = new RidgePoint[tracks.size()];

				for(int c = 0; c<tracks.size(); c++){

					//				Tracing.toChainCode(tracks.get(c)); //DEBUG remove

					long[] max = tracks.get(c).get(0);
					// Keep the index of the crossing point in the track as we
					// are going to extend the track towards the origin of the next child
					int cp = tracks.get(c).size()-1; 
					// Make the track move outward
					Collections.reverse(tracks.get(c));
					// Extend the track towards the origin of the next child
					Pair<LeafCase,List<long[]>>pair = trackBlind(path.parentCenter(), max, id, tracks.get(c));

					//					Tracing.toChainCode(pair.getSecond()); //DEBUG remove

					if(pair.getSecond().size()>0)
						pair.getSecond().remove(0);

					tracks.get(c).addAll(pair.getSecond());

					//					Tracing.toChainCode(tracks.get(c)); //DEBUG remove

					//Create a new RidgeSection and a new Child
					RidgeSection<T,V> tr = new RidgeSection<>(tracks.get(c), cp);					
					children[c] = new RidgePoint<>(this, depth+1, pair.getFirst(), tr);
				}
			}


			// Now create children of childrens BFS - wise
			LinkedList<RidgePoint<T,V>> queue = new LinkedList<>();
			queue.add(this);

			Comparator<RidgePoint<T,V>> comp = (rp1,rp2)->{
				return Float.compare(rp2.intensity,rp1.intensity);
			};



			double totalLength = 1;
			int count = 0;
			while(!queue.isEmpty()){
				RidgePoint<T,V> node = queue.poll();
				node.createChildren(totalLength, count);

				for(int c = 0; c<node.numChildren(); c++){
					totalLength+=node.get(c).getRidgeSection().pathLength();
					if(node.get(c).isLeaf!=null)
						count++;
					else
						queue.add(node.get(c));

					Collections.sort(queue,comp);
				}
			}

			// Update turns
			this.updateTurns();

			// Finally update intersections
			this.updateIntersections();

		}

	}






	@SuppressWarnings("unchecked")
	public RidgePoint(RidgePoint<T,V> parent, int depth, LeafCase isLeaf, RidgeSection<T,V> track) {

		this.identifier = gen++;

		this.parent = parent;
		this.o = parent.o;
		this.tree = parent.tree;
		this.tree.add(this);
		this.intersected = parent.intersected;
		this.turns = parent.turns;
		this.cAccess = parent.cAccess;
		this.rAccess = parent.rAccess;
		this.sAccess = parent.sAccess;
		this.lAccess = parent.labelsAccess();
		this.path = track;
		this.childD = parent.childD;
		this.branchKernel = parent.branchKernel;
		this.id = parent.id;
		this.highestIntensity = rAccess.get().createVariable();
		this.depth = depth;
		this.isLeaf = isLeaf;

		rAccess.setPosition(path.childCenter());
		//float t = rAccess.get().getRealFloat() * delta;
		this.threshold = (float) Math.max(parent.threshold,o.getRidgeThreshold());

		this.intensity = rAccess.get().getRealFloat();


		if(isLeaf != null)
			this.children = new RidgePoint[0];

	}





	@SuppressWarnings("unchecked")
	private void createChildren(double totalLength, int numLeaves){

		if(children == null){  

			//Check depth
			if(depth > o.getChildDepth()){
				this.isLeaf = LeafCase.Path_End;
				children = new RidgePoint[0];
				//System.out.println("Cut on depth");
				return;
			}

			// Check Total number of children
			/*
			if(numLeaves>=o.getMaxTotalNumLeaves()){
				this.isLeaf = LeafCase.Path_End;
				children = new RidgePoint[0];
				System.out.println("Cut on max total number of leaves");
				return;
			}
			 */

			//Check totalLength
			if(totalLength>o.getMaxTotalLength()){
				this.isLeaf = LeafCase.Path_End;
				children = new RidgePoint[0];
				//System.out.println("Cut on max total length");
				return;
			}


			//Check length from root TODO remove!!
			double length = 0;
			for(RidgePoint<T,V> p : this.pathToParent(tree.get(0))){
				length += p.getRidgeSection().pathLength();
			}

			if(length > o.getMaxLength()/3){
				log.trace(length);
				this.isLeaf = LeafCase.Path_End;
				children = new RidgePoint[0];
				//System.out.println("Cut on max length from root");
				return;
			}





			// ********** Find children ************

			// 1- Find maxima at the periphery of the zone of influence
			List<long[]> maxima = sampleNeighborhood(path.childCenter());

			log.trace("Number of identified maxima : "+maxima.size());
			//long[] last = new long[]{-5,-5};

			Iterator<long[]> it = maxima.iterator();
			while(it.hasNext()){

				long[] m = it.next();
				labelsAccess().setPosition(m);
				sAccess.setPosition(m);


				// If a maxima already belongs to the track
				// remove and create children as intersections
				if(sAccess.get().equals(id)){

					for(int i = 0; i<tree.size(); i++){
						if(tree.get(i)!=this)
							if(ImgGeometry.distance(m, tree.get(i).center())<o.getSearchRadius()){
								log.trace("Creating an intersection based on maxima");
								it.remove();
								this.intersected.add(tree.indexOf(this));
								this.intersected.add(i);
								break;
							}
					}
				}
				/*
				else if(getlAccess().get().equals(zero))
					continue;
				else if(getlAccess().get().equals(sAccess.get())){ // If we are on an edge (same in labels and tracks)
					continue;
				}
				else{
					// Remove as we are inside a labelled area and not on its edge
					it.remove();
					log.trace("maxima removed because it was stepping inside a labelled area");
					//log.trace("maxima removed because it was stepping inside a labelled area");
				}

				 */


			}




			// 2- For each maximum track edge from maximum to origin
			List<List<long[]>> tracks = tracksToOriginAsChild(maxima);


			// 3- Create children
			if(tracks.size() == 0){
				children = new RidgePoint[0];
				//log.warn("No neighborhood track identified for RidgePoint "+id);
				log.trace("No neighborhood track identified for RidgePoint "+id);
			}
			else{

				//Create children

				List<Pair<LeafCase,RidgeSection<T,V>>> LR = childCase(path.childCenter(), tracks);
				children = new RidgePoint[LR.size()];

				for(int c = 0; c<LR.size(); c++){
					children[c] = new RidgePoint<>(this, depth+1, LR.get(c).getFirst(),LR.get(c).getSecond());
				}
			}

		}

	}









	private List<long[]> sampleNeighborhood(long[] orig){

		//Sample the gray profile in the neighborhood

		final List<Double> profile = new ArrayList<>(branchKernel.length);

		long[] pos= new long[2];
		for(int k = 0; k<branchKernel.length; k++){
			pos[0] = orig[0] + branchKernel[k][0];		pos[1] = orig[1] + branchKernel[k][1];
			rAccess.setPosition(pos);
			profile.add(rAccess.get().getRealDouble());
		}

		final double limit = childD/4d;

		//Find maxima in the profile of gray values
		final T zero = id.createVariable(); zero.setZero();
		List<Integer> peaks = FindPeaks.peak1dCircular(profile, threshold)
				//remove maxima if already labbeled
				.stream().filter(i->

				// *********************************** This is where we remove the cp **********************************

				{// i index in branchKernel

					//k index in maskKernel (neighborhood of current maximum)
					for(int k = 0;k<maskKernel.length; k++){ 

						pos[0] = orig[0] + branchKernel[i][0] + maskKernel[k][0];		pos[1] = orig[1] + branchKernel[i][1] + maskKernel[k][1];
						sAccess.setPosition(pos);

						// Remove the peak which comes from the crossing point
						if(sAccess.get().equals(id)){							

							if(ImgGeometry.distance(pos,path.cp())<limit){ // this is our cp sp remove
								log.trace("maxima removed based on proximity with label "+sAccess.get());
								//log.debug("maxima removed based on proximity with label "+sAccess.get());
								return false;									
							}			
						}


					}

					return true;
				}

						).sorted((p1,p2) -> profile.get(p2).compareTo(profile.get(p1)))
				.collect(Collectors.toList()); //descending order of intensity


		//return the 3 most intense peaks
		int spacing = branchKernel.length / 12;
		final List<long[]> hotspots = new ArrayList<>(4);
		long[] last = new long[]{-5,-5};
		for(int i=0, j = 0; j<4 && i<peaks.size(); i++){
			long[] p = new long[2];
			p[0] = orig[0] + branchKernel[peaks.get(i)][0];		p[1] = orig[1] + branchKernel[peaks.get(i)][1];


			if(ImgGeometry.distance(p, last)> spacing){
				hotspots.add(p);
				last = p;
				j++;
			}


			//	else
			//		log.debug("maxima removed because it was too close from previous one : "+spacing);


		}

		return hotspots; 
	}













	/**
	 * @param maxima
	 * @param orig
	 * @param ridges
	 * @param labels
	 * @param threshold
	 * @return A List where each entry represents the List of positions to move from a given maximum to the given origin.
	 * Positions will include the maximum but not the origin.
	 */
	private List<List<long[]>> tracksToOriginAsParent(List<long[]> maxima, long[] orig) {


		// List of identified tracks
		final List<List<long[]>> tracks = new ArrayList<>(maxima.size());
		//final List<List<long[]>> failed = new ArrayList<>(maxima.size());


		// For each maximum -> move towards the origin (center of the circle)


		for(long[] maximum : maxima){


			List<long[]> track = new ArrayList<>();

			int maxTrackSize = (int) (ImgGeometry.distance(maximum, orig)*2);
			log.trace("Max track size = "+maxTrackSize);


			// temporarily write id on the opposite side of origin to init the method
			long[] lure = Tracing.getLure(maximum, orig);
			sAccess.setPosition(lure);
			T prevLabel = sAccess.get().copy();
			sAccess.get().set(id);

			//Debug maxima identification only
			//lAccess.setPosition(Tracing.getLure(lure, orig));
			//lAccess.get().setReal(id.getMaxValue());


			long[] pos = new long[2]; // temp array to iterate over positions
			long[] lastHighest = new long[2]; // holds the position with highest intensity	
			long[] current = Arrays.copyOf(maximum,maximum.length); // holds the current position


			while(true){


				if(update!=null)
					update.accept(this);


				// 1- Reset highest intensity				
				highestIntensity.setReal(highestIntensity.getMinValue());

				// 2- lookup available positions
				long[][] kern = Tracing.getFront(sAccess, current, pos, id);

				for(int k = 0; k<kern.length; k++){

					pos[0] = current[0] + kern[k][0];		pos[1] = current[1] + kern[k][1];

					//Check intensity in ridges
					rAccess.setPosition(pos);
					if(rAccess.get().compareTo(highestIntensity)>=0){
						highestIntensity.set(rAccess.get()); // update intensity
						lastHighest[0] = pos[0]; 	lastHighest[1] = pos[1]; // update last highest location 
					}

				}


				// Remove id from lure
				sAccess.setPosition(lure);		sAccess.get().set(prevLabel);

				// Test stop conditions				
				if(highestIntensity.getRealFloat()<=threshold){
					//log.debug("Threshold reached");
					//failed.add(track);
					break;
				}

				if(track.size()+1> maxTrackSize){
					//log.debug("max track size reached");
					//failed.add(track);
					break;
				}


				if(ImgGeometry.distance(lastHighest,orig) <= 1.5){
					// Add remaining points to the track
					track.add(Arrays.copyOf(current, current.length));
					track.add(Arrays.copyOf(lastHighest, lastHighest.length));
					track.add(this.path.parentCenter());
					//Add the track to the list of tracks
					tracks.add(track);


					//System.out.println("to origin (Parent)");
					//track.forEach(l->System.out.println(Arrays.toString(l)));					
					//Tracing.toChainCode(track); //DEBUG debug only


					break;
				}



				// Update lure
				lure = Arrays.copyOf(current, current.length);
				sAccess.setPosition(lure);		sAccess.get().set(id);

				// Copy current to track
				track.add(Arrays.copyOf(current, current.length));

				// Update current
				current = Arrays.copyOf(lastHighest, lastHighest.length);				

			}




		}


		//Now set id along the tracks
		tracks.forEach(t->{
			t.forEach(p->{				
				sAccess.setPosition(p);
				sAccess.get().set(id);
			});
		});

		/* Debug only
		failed.forEach(t->{
			t.forEach(p->{
				//log.debug(Arrays.toString(p));
				lAccess.setPosition(p);
				lAccess.get().setReal(id.getMaxValue());
			});
		});
		 */

		return tracks;
	}










	private List<List<long[]>> tracksToOriginAsChild(List<long[]> maxima) {

		// First make sure we write our own path 
		Iterator<long[]> itr = path.childToParent();
		while(itr.hasNext()){
			sAccess.setPosition(itr.next());
			sAccess.get().set(id);
		}


		// List of tracks reaching the origin
		final List<List<long[]>> toOrigin = new ArrayList<>(maxima.size());

		// List of tracks reaching id on the path to parent
		final List<List<long[]>> toParent = new ArrayList<>(maxima.size());		

		// List of iindex in path to parent where current tracks touch the path to parent
		final List<Integer> touches = new ArrayList<>(maxima.size());

		// List of tracks which did not reach our id
		final List<List<long[]>> missed = new ArrayList<>(maxima.size());


		// For each maximum -> move towards the origin (center of the circle)


		for(long[] maximum : maxima){

			// Init a new track
			List<long[]> track = new ArrayList<>();

			int maxTrackSize = (int) (ImgGeometry.distance(maximum, path.childCenter())*2);
			log.trace("Child "+depth+": Max track size = "+maxTrackSize);


			// temporarily write id on the opposite side of origin to init the method
			long[] lure = Tracing.getLure(maximum, path.childCenter());
			T lureId = id.createVariable();
			lureId.setReal(id.getRealDouble()+1);

			sAccess.setPosition(lure);
			T prevLabel = sAccess.get().copy();
			sAccess.get().set(lureId);

			if(printLures){
				labelsAccess().setPosition(lure);
				labelsAccess().get().setReal(id.getMaxValue()-2);
			}

			long[] pos = new long[2]; // temp array to iterate over positions
			long[] lastHighest = new long[2]; // holds the position with highest intensity	
			long[] current = Arrays.copyOf(maximum,maximum.length); // holds the current position



			while(true){


				// ***************** Tracking Ridge ************************

				// Copy current to track
				track.add(Arrays.copyOf(current, current.length));


				if(update!=null)
					update.accept(this);


				// - Reset highest intensity				
				highestIntensity.setReal(highestIntensity.getMinValue());

				// - lookup available positions
				long[][] kern = Tracing.getFront(sAccess, current, pos, lureId);

				for(int k = 0; k<kern.length; k++){

					pos[0] = current[0] + kern[k][0];		pos[1] = current[1] + kern[k][1];

					//Check intensity in ridges
					rAccess.setPosition(pos);
					if(rAccess.get().compareTo(highestIntensity)>=0){
						highestIntensity.set(rAccess.get()); // update intensity
						lastHighest[0] = pos[0]; 	lastHighest[1] = pos[1]; // update last highest location 
					}

				}


				// Remove id from current lure
				sAccess.setPosition(lure);		sAccess.get().set(prevLabel);






				// ******************** Testing stop conditions  ****************************

				sAccess.setPosition(lastHighest);

				if(sAccess.get().equals(id)){ // Did we reach the parent?

					// Check if this is the origin
					if(Arrays.equals(lastHighest, path.childCenter())){
						track.add(path.childCenter());
						toOrigin.add(track);

						//System.out.println("to origin -> "+identifier);
						//track.forEach(l->System.out.println(Arrays.toString(l)));					
						//Tracing.toChainCode(track); //DEBUG debug only

						log.trace("to origin found");
						break;
					}


					// If not origin check where we reached the id

					// Is this on our path to parent ?
					int prev = toParent.size();					
					Iterator<long[]> it = this.path.childToParent();
					int counter = 0;
					while(it.hasNext()){
						long[] n = it.next();
						if(Arrays.equals(lastHighest, n)){
							// do not add remaining point to the track as it will be added by the rebase process
							//track.add(Arrays.copyOf(lastHighest, lastHighest.length));
							toParent.add(track);							
							touches.add(counter);
							log.trace("to parent found");
							break;
						}
						else
							counter++;
					}



					if(prev == toParent.size()){ // If we did not find ourselves

						//We reached another RidgePoint's id!
						// in this case add to intersections 
						int index = 0;
						RidgePoint<T,V> other = null;
						for(RidgePoint<T,V> t : tree){
							if(t!=this)
								if(ImgGeometry.distance(lastHighest, t.center()) <= maxTrackSize){
									other = t;
									break;
								}
							index++;
						}
						if(other != null){

							this.intersected.add(index);
							this.intersected.add(tree.indexOf(this));
							log.trace("Intersection created because we reached another part of the track");
						}
						else							
							log.trace("Reached neighbour track to origin, ignore");

					}

					break;
				}



				// If we did not reach our own id ...


				// Fail if the intensity becomes too low
				if(highestIntensity.getRealFloat()<=threshold){
					log.trace("Threshold reached");
					//failed.add(track);
					break;
				}


				// Add to missed if we reach the maximum track size
				if(track.size()+1> maxTrackSize){					
					log.trace("Child max track size reached");					
					missed.add(track);
					log.trace("Missed");
					break;					
				}


				// If we reach here it means we should continue...

				// Update lure
				lure = Arrays.copyOf(current, current.length);
				sAccess.setPosition(lure);		sAccess.get().set(lureId);

				// Update current
				current = Arrays.copyOf(lastHighest, lastHighest.length);				

			}


		} // END of iteration over maxima





		// Now solve for each case

		// List of accepted tracks
		final List<List<long[]>> accepted = new ArrayList<>(maxima.size());
		accepted.addAll(toOrigin);






		//Deal with to Parents first to see if a rebase is required.

		// Find the track with highest index in path to parent
		ArrayList<long[]> oldCenterToNewCenter = null; 
		// path.childCenter changes with rebase this is why we keep a reference here.
		long[] oldChildCenter = path.childCenter(); 



		int furthest = -1;
		while(!toParent.isEmpty()){

			furthest = Collections.max(touches);


			// DEBUG System.out.println("Rebasing "+identifier);
			// DEBUG System.out.println("furthest = "+furthest+" ; Size = "+path.size());



			//NB if furthest is the parent center then the path fully removed!
			//To avoid this, rebase parent
			if(furthest == this.path.size()-1){
				// DEBUG System.out.println("Parent path size = "+this.parent.path.size()+ "("+this.parent.identifier+")");

				if(this.parent.path.size()<3){
					int rm = touches.indexOf(furthest);
					touches.remove(rm);
					toParent.remove(rm);
					// DEBUG System.out.println("furthest removed");
				}
				else{
					this.parent.rebaseWithChildren(1);
					break;
				}
			}
			else
				break;

		}





		if(!toParent.isEmpty()){


			oldCenterToNewCenter = this.rebase(furthest, toOrigin.isEmpty());

			// Add missing elements in each toParent paths (from where it touches to the furthest)
			for(int i = 0; i<toParent.size(); i++){
				List<long[]> tp = toParent.get(i);
				for(int j = touches.get(i); j < oldCenterToNewCenter.size(); j++)
					tp.add(oldCenterToNewCenter.get(j));
				//System.out.println("toParent -> "+identifier);
				//tp.forEach(l->System.out.println(Arrays.toString(l)));					
				//Tracing.toChainCode(tp); //DEBUG debug only
			}

			// Add missing elements in each toOrigin paths (from where it touches to the furthest)
			for(int i = 0; i<toOrigin.size(); i++){
				for(int j = 1; j<oldCenterToNewCenter.size(); j++) // except first one as toOrigin already contains it.
					toOrigin.get(i).add(oldCenterToNewCenter.get(j));
				//System.out.println("toOrigin after Rebase -> "+ identifier);
				//toOrigin.get(i).forEach(l->System.out.println(Arrays.toString(l)));					
				//Tracing.toChainCode(toOrigin.get(i)); //DEBUG debug only	
			}




			accepted.addAll(toParent);

			/*
					trimmed.forEach(p->{				
						lAccess.setPosition(p);
						lAccess.get().setReal(id.getMaxValue());
					});
			 */

		}




		// Handle missed tracks (the ones that did not reach our own id and moved in a distinct direction)

		for(List<long[]> m : missed){

			//System.out.println("Missed");
			//m.forEach(l->System.out.println(Arrays.toString(l)));
			//Tracing.toChainCode(m); //DEBUG debug only

			if(ImgGeometry.distance(m.get(m.size()-1), oldChildCenter) < childD){

				// Back track to find the closest point to origin
				Pair<Integer, Double> pair = Tracing.closestPoint(oldChildCenter, m); // Can be more efficient

				if(pair.getSecond() <= o.getSearchRadius()){

					int index = pair.getFirst();

					List<long[]> missToOldChild = new ArrayList<>(m.subList(0,index+1));


					ImgGeometry.linePath(missToOldChild.get(missToOldChild.size()-1), oldChildCenter, (l)-> {						
						long[] copy = Arrays.copyOf(l, l.length);	
						missToOldChild.add(copy);
					});

					// childPath will be used to setup a child at the location of the miss (in update turns)
					// so it must move from this new child center to the end of the trim (parent to child)
					List<long[]> childPath = new ArrayList<>();

					//Handle the case when a rebase was performed
					if(oldCenterToNewCenter!=null){	

						// Remember rebaseTrim is child to parent
						// DEBUG System.out.println("Miss To Old Child before rebase");
						// DEBUG missToOldChild.forEach(l->System.out.println(Arrays.toString(l)));

						// Elongate the missToOldChild with the trim of the rebase operation
						if(Arrays.equals(missToOldChild.get(missToOldChild.size()-1),oldCenterToNewCenter.get(0))){
							missToOldChild.remove(missToOldChild.size()-1);
							// DEBUG System.out.println("Last Miss To Old Child removed!");
						}
						missToOldChild.addAll(oldCenterToNewCenter);	

						// Start childPath from new center to  oldCenter
						ListIterator<long[]> it = oldCenterToNewCenter.listIterator(oldCenterToNewCenter.size());
						while(it.hasPrevious()){
							childPath.add(it.previous());
						}



					} // If there were no rebase operation
					else{

						// DEBUG System.out.println("Missed with no trimming "+identifier);

						if(!Arrays.equals(missToOldChild.get(missToOldChild.size()-1),path.childCenter())){
							missToOldChild.add(path.childCenter());
							// DEBUG System.out.println("child center added to trimmed missed!");
						}
						childPath.add(path.childCenter());
						childPath.add(missToOldChild.get(missToOldChild.size()-2));
					}

					//	System.out.println("Missed track trimmed from index : "+index+" ("+identifier+")");
					//	System.out.println("Turn Path size : "+childPath.size());
					//	missToOldChild.forEach(l->System.out.println(Arrays.toString(l)));					
					//	Tracing.toChainCode(missToOldChild); //DEBUG debug only

					turns.put(this,childPath);
					accepted.add(missToOldChild);
					log.trace("Missed was valid");
				}
			}
		} 

		/* TODO doube check if the following is required or not
		// Add as child if no track could reach the origin
		if(accepted.isEmpty()){
			this.intersected.add(tree.indexOf(this));
			log.trace("No child reached the origin");
		}
		 */



		//Now set id along the tracks
		accepted.forEach(t->{
			t.forEach(p->{				
				sAccess.setPosition(p);
				sAccess.get().set(id);
			});
		});



		/*
		if(printFailed){
			missed.forEach(t->{
				t.forEach(p->{
					//log.debug(Arrays.toString(p));
					labelsAccess().setPosition(p);
					labelsAccess().get().setReal(id.getMaxValue());
				});
			});
		}
		 */


		return accepted;
	}






	/**
	 * @see RidgeSection#rebase(int)
	 */
	private ArrayList<long[]> rebase(int furthest, boolean createChild) {

		ArrayList<long[]> trimmed = path.rebase(furthest);

		//	System.out.println("Rebase trimmed -> "+identifier);
		//	trimmed.forEach(l->System.out.println(Arrays.toString(l)));					
		//	Tracing.toChainCode(trimmed); //DEBUG debug only

		if(createChild && trimmed.size()>1){
			List<long[]> newChild = new ArrayList<>(trimmed);
			Collections.reverse(newChild);		
			turns.put(this, newChild);		
		}

		return trimmed;
	}



	private void rebaseWithChildren(int index){
		ArrayList<long[]> trim = path.rebase(index);
		if(children!=null)
			for(int c = 0; c<children.length; c++){
				children[c].respondToParentRebase(trim);
			}
		//Also check turns
		List<long[]> turn = turns.get(this);
		if(turn != null){
			for(int i = 1; i<trim.size(); i++)
				turn.add(0,trim.get(i));
			//System.out.println("Turn updated");
			//turn.forEach(l->System.out.println(Arrays.toString(l)));					
			//Tracing.toChainCode(turn); //DEBUG debug only
		}

	}


	private void respondToParentRebase(ArrayList<long[]> trim){
		// DEBUG System.out.println("Responding to parent Rebase "+identifier);
		this.path.addParentTrim(trim);
	}





	private enum LeafCase {
		Intensity_Drop,
		Loop,
		Path_End,
		Self_Intersect,
		Unknown
	}










	/**
	 * Follow an edge from a starting location. In order to determine the direction to follow, the other direction must already be
	 * labelled with the same id (in labels). The side effect is that the id will be deposited along the track in labels.
	 * @param start The starting location
	 * @param ridges The edge containing image
	 * @param id The id of the track (same as previous)
	 * @param half current track from origin to start
	 * @return The track as List of locations
	 */
	private Pair<LeafCase,List<long[]>> trackBlind(long[] origin, long[] start, T id, List<long[]> half){


		LeafCase lc = null;


		List<long[]> track = new ArrayList<>();

		double nextCenter = o.getSearchRadius()*2;
		log.trace("Max track size = " + nextCenter);



		long[] pos = new long[2]; // temp array to iterate over positions
		long[] lastHighest = new long[2]; // holds the position with highest intensity	
		long[] current = Arrays.copyOf(start, start.length); // holds the current position


		boolean isLoop = false;

		while(true){


			if(update!=null)
				update.accept(this);

			// 1- Reset highest intensity				
			highestIntensity.setReal(highestIntensity.getMinValue());

			// 2- lookup available positions
			long[][] kern = Tracing.getFront(sAccess, current, pos, id);

			for(int k = 0; k<kern.length; k++){

				pos[0] = current[0] + kern[k][0];		pos[1] = current[1] + kern[k][1];

				//Check intensity in ridges
				rAccess.setPosition(pos);
				if(rAccess.get().compareTo(highestIntensity)>=0){
					highestIntensity.set(rAccess.get()); // update intensity
					lastHighest[0] = pos[0]; 	lastHighest[1] = pos[1]; // update last highest location 
				}

			}


			// ***************   Testing stop conditions   ************************			

			// Do we reach our own id ?

			sAccess.setPosition(lastHighest); 
			if(sAccess.get().equals(id)){


				RidgePoint<T,V> other = null;


				// If we are then we need to identify the other ridge section we are steping on
				int index = 0;
				for(RidgePoint<T,V> t : tree){
					if(t!=this)
						if(ImgGeometry.distance(lastHighest, t.center()) <= o.getSearchRadius()){//nextCenter){
							other = t;
							break;
						}
					index++;
				}

				if(other == null){
					// This means we made a loop towards ourselves
					isLoop = true;
					log.trace("Track blind --> Loop found (back to id)"+ Arrays.toString(start));

				}
				else{
					lc = LeafCase.Self_Intersect;
					intersected.add(index);
					log.trace("Track blind --> Intersection ! "+ Arrays.toString(start));
				}

				break;
			}



			if(highestIntensity.getRealFloat()<=threshold){  // Too dim to continue
				log.trace("Threshold reached"); 

				lc = LeafCase.Intensity_Drop;
				log.trace("Track blind --> Intensity drop"+ Arrays.toString(start));

				break;
			}





			if( nextCenter - ImgGeometry.distance(lastHighest, start) <= 1.5){ // max distance reached				
				log.trace("blind center found");
				break;
			}

			//write current
			sAccess.setPosition(current);
			sAccess.get().set(id);


			// Copy current to track
			track.add(Arrays.copyOf(current, current.length));

			// Update current
			current = Arrays.copyOf(lastHighest, lastHighest.length);				

		}

		// Done checking stop conditions.



		// ************************   Handle the different Cases   ***********************************

		if(isLoop){

			// If we have a loop, cut where the track is the furthest from this child center.


			if(track.isEmpty()){
				track.add(new long[]{-1,-1});		
				lc = LeafCase.Unknown;				
			}
			else{

				track.forEach(l->{
					sAccess.setPosition(l);
					sAccess.get().setZero();
					if(update!=null)
						update.accept(this);
				});



				Pair<Integer,Double> pair = Tracing.farthestPoint(this.center(), track);
				track = new ArrayList<>(track.subList(0, pair.getFirst()+1));

				track.forEach(l->{
					sAccess.setPosition(l);
					sAccess.get().set(id);
					if(update!=null)
						update.accept(this);
				});			


				// DEBUG System.out.println("loop ?");



				// Set the loop flag only if the track is very small
				// This way we give a chance to longer tracks to be considered for finding a child in the next iteration
				// This helps when the track loops at intersections that are a bit large in the image
				if(track.size()<o.getSearchRadius()/2){
					//			System.out.println("loop !");
					lc = LeafCase.Loop;
				}

				//track.forEach(l->System.out.println(Arrays.toString(l)));					
				//		Tracing.toChainCode(track); //DEBUG debug only	

			}

		}	
		else{

			if(lc != LeafCase.Self_Intersect){


				// Check to see if we came back close to our center by another way
				if(track.size()>3){

					if(ImgGeometry.distance(track.get(track.size()-1), this.center()) <= o.getSearchRadius()){


						Pair<Integer,Double> pair = Tracing.farthestPoint(this.center(), track);
						track = new ArrayList<>(track.subList(0, pair.getFirst()+1));

						track.forEach(l->{
							sAccess.setPosition(l);
							sAccess.get().set(id);
							if(update!=null)
								update.accept(this);
						});			


						//				System.out.println("loop 2");
						//				track.forEach(l->System.out.println(Arrays.toString(l)));					
						//				Tracing.toChainCode(track); //DEBUG debug only	

						lc = LeafCase.Loop;


					}


					//Finally check for a sharp turn
					else {//if(lc == null){
						double turn = ImgGeometry.distance(track.get(0), track.get(track.size()-1))/track.size();
						//	log.debug("Turn value "+turn);
						if(turn <= 0.7){
							log.trace("Turn value accepted "+turn);
							// DEBUG System.out.println("turn added ! "+ identifier);

							//Setup the turn at the site of the cp							

							turns.put(this,new ArrayList<>(half)); // Approximation!
						}
					}
				}

			}

		}

		log.trace("LeafCase = "+lc );

		return new Pair<>(lc,track);
	}















	private void addChild(RidgePoint<T,V> ridgePoint) {
		//log.debug("In !  "+this);
		//	log.debug("Adding Child!  "+Arrays.toString(children));
		children = Arrays.copyOf(children, children.length+1);
		children[children.length-1] = ridgePoint;
		//	log.debug("Child added!  "+Arrays.toString(children));
	}











	/**
	 * @param center
	 * @param maxToParent
	 * @param ridges
	 * @param labels
	 * @return A List of Pair objects where the right object corresponds to a new ridge
	 * starting from one maximum in maxToParent and the left entry return true if the ridge reaches the max distance
	 * and false if the a lowest intensity threshold was reached first
	 */
	private List<Pair<LeafCase, RidgeSection<T,V>>> childCase(long[] center, List<List<long[]>> maxToParent) {

		List<Pair<LeafCase, RidgeSection<T,V>>> cases = new ArrayList<>(maxToParent.size());

		//Store maxima in same order as the provided list

		for(int i = 0; i<maxToParent.size(); i++){	

			//		Tracing.toChainCode(maxToParent.get(i)); //DEBUG remove

			int cp = maxToParent.get(i).size()-1;

			// In addition update the lists with a track blind step			

			Collections.reverse(maxToParent.get(i));

			Pair<LeafCase,List<long[]>> pair = this.trackBlind(path.childCenter(), maxToParent.get(i).get(cp), id, maxToParent.get(i));


			//		Tracing.toChainCode(pair.getSecond()); //DEBUG remove

			if(pair.getSecond().size()>0)
				pair.getSecond().remove(0);

			maxToParent.get(i).addAll(pair.getSecond());			

			cases.add(new Pair<>(pair.getFirst(), new RidgeSection<>(maxToParent.get(i), cp)));			
		}

		return cases;

	}









	public boolean isOn() {		
		return path != null && isLeaf != LeafCase.Unknown;
	}




	/**
	 * @return The {@link RidgeSection} allowing to move from the {@link RidgePoint} to its parent
	 */
	public RidgeSection<T,V> getRidgeSection(){
		return path;
	}



	/**
	 * @return The location of the {@link RidgePoint}
	 */
	public long[] center() {
		if(path != null)
			return this.path.childCenter();
		else
			return new long[]{-1,-1};
	}




	/**
	 * @return A copy of this {@link RidgePoint} id
	 */
	public T id(){
		return id.copy();
	}



	/**
	 * @return The depth of this {@link RidgePoint}
	 */
	public int depth(){
		return depth;
	}



	/**
	 * @return a {@link RandomAccess} on the original image (1 channel)
	 */
	OutOfBounds<V> channelAccess(){
		return cAccess;
	}

	/**
	 * @return a {@link RandomAccess} on the steered image
	 */
	OutOfBounds<V> ridgesAccess(){
		return rAccess;
	}


	/**
	 * @return a {@link RandomAccess} on the track image
	 */
	OutOfBounds<T> trackAccess(){
		return sAccess;
	}




	public void clearAllConnectedTracks(){
		tree.forEach(r->r.eraseTrack());
	}


	public void eraseTrack(){
		if(path!=null)
			path.parentToChild().forEachRemaining(l->{
				sAccess.setPosition(l);
				sAccess.get().setZero();
			});
	}



	// =======================  TreeNode Impl  =====================================



	@Override
	public RidgePoint<T,V> getParent() {
		return parent;
	}


	@Override
	public int numChildren() {
		if(children == null)
			return 0;
		return children.length;
	}


	@Override
	public RidgePoint<T,V> get(int i) {
		return children[i];
	}






	@Override
	public boolean hasAncestor(RidgePoint<T,V> node) {
		if(ancestors == null){
			ancestors = new HashSet<>();
			Iterator<RidgePoint<T,V>> it = ancestorIterator();
			while(it.hasNext())
				ancestors.add(it.next());
		}
		return ancestors.contains(node);
	}



	public void updateTurns() {
		turns.forEach((rp,l)->{
			// DEBUG System.out.println("rp "+rp.identifier+" creating turn ("+gen+")");
			rp.createLeaf(l);
		});
	}



	public void updateIntersections() {
		intersected.forEach(rp->{
			tree.get(rp).createLeaf();
		});
	}






	private void createLeaf() {
		if(isLeaf == null){ // Only create if we are not already a leaf			
			// Create a RidgeSection
			// DEBUG System.out.println("Intersection created by "+this.identifier);
			// DEBUG System.out.println("Path to parent : "+this.path.size());
			// DEBUG 	this.path.childToParent().forEachRemaining(l->System.out.println(Arrays.toString(l)));
			if(this.path.size()>1){
				List<long[]> list = new ArrayList<>(2); 
				list.add(this.center());
				list.add(this.path.nextAfterChild());
				// DEBUG System.out.println("Leaf : "+gen);
				// DEBUG list.forEach(l->System.out.println(Arrays.toString(l)));
				RidgeSection<T,V> mock = new RidgeSection<>(list, 0);
				this.addChild(new RidgePoint<>(this, depth+1, LeafCase.Self_Intersect, mock));
			}
		}		
	}




	private void createLeaf(List<long[]> l) {
		// Create a RidgeSection
		l.forEach(p->{
			sAccess.setPosition(p);
			sAccess.get().set(id);
		});			

		RidgeSection<T,V> mock = new RidgeSection<>(l, 0);
		this.addChild(new RidgePoint<>(this, depth+1, LeafCase.Self_Intersect, mock));			

	}






	OutOfBounds<T> labelsAccess() {
		return lAccess;
	}





}
