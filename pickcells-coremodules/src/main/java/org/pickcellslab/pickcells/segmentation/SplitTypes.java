package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics;
import org.apache.commons.math3.util.Pair;

import net.imglib2.RandomAccess;
import net.imglib2.type.numeric.RealType;

public final class SplitTypes {


	@SuppressWarnings("rawtypes")
	private static final PostProcessSplit[] arr = new PostProcessSplit[]{null, intensityOnly(), directionalOnly(), both()};

	private SplitTypes() {/*Not instantiable*/}

	public static PostProcessSplit<?,?>[] values(){
		return arr;
	}


	// ************************************************ VolumeSliceFeature Impl ***********************************************************


	private interface Scored{
		double getScore();
	}




	private static class IntensityOnlySplitFeature implements VolumeSliceFeature, Scored{



		final long[] centroid = new long[3]; 

		double centerIntensity, rimIntensity;
		double intensityRatio;
		private double intensityRatioNorm;
		int area, contour;

		@Override
		public <T extends RealType<T>> void accept(RandomAccess<T> labels, RandomAccess<T> grays, long[] pos, long[] temp, T paint) {

			area++;
			centroid[0] += pos[0];		centroid[1] += pos[1];		centroid[2] += pos[2];

			//Keep track of the countour's intensity
			for(int i = 0; i<Volume.k.length; i++){
				temp[0] = pos[0] + Volume.k[i][0];	temp[1] = pos[1] + Volume.k[i][1];
				labels.setPosition(temp);
				if(!labels.get().equals(paint)){
					contour++;
					grays.setPosition(pos);
					rimIntensity += grays.get().getRealDouble();
					break;
				}
			}
		}





		@Override
		public <T extends RealType<T>> boolean doneWithPositions(RandomAccess<T> grays, long[] temp) {

			if(area==0){
				intensityRatio = 1;
				return false;
			}else{
				centroid[0] /= area;		centroid[1] /= area;		centroid[2] /= area;
				rimIntensity /= contour;
				//Compute intensity close to the centroid
				for(int i = 0; i<Volume.k.length; i++){
					temp[0] = centroid[0] + Volume.k[i][0];	temp[1] = centroid[1] + Volume.k[i][1];
					grays.setPosition(temp);
					centerIntensity += grays.get().getRealDouble();
				}
				centerIntensity /= 9d;
				intensityRatio = centerIntensity / rimIntensity;
				return true;
			}

		}







		void normalize(double means){
			intensityRatioNorm = (intensityRatio - means)/means;

		}



		@Override
		public int area(){
			return area;
		}

		double intensityRatio(){
			return intensityRatio;
		}

		double intensityRatioNorm(){
			return intensityRatioNorm;
		}


		@Override
		public double getScore() {
			return intensityRatioNorm;
		}


		@Override
		public String toString(){
			return "Intensity Only";
		}





		@Override
		public long[] centroid() {
			return centroid;
		}


	}



	public static final class DirectionalSliceFeature implements VolumeSliceFeature, Scored {


		final long[] centroid = new long[3]; 

		double dirChange = -1;
		int area;

		private Vector2D v;



		@Override
		public <T extends RealType<T>> void accept(RandomAccess<T> labels, RandomAccess<T> grays, long[] pos, long[] temp, T paint) {			
			area++;
			centroid[0] += pos[0];		centroid[1] += pos[1];		centroid[2] += pos[2];
		}





		@Override
		public <T extends RealType<T>> boolean doneWithPositions(RandomAccess<T> grays, long[] temp) {			
			if(area==0){
				return false;
			}else{
				centroid[0] /= area;		centroid[1] /= area;		centroid[2] /= area;
				return true;
			}			
		}



		void computeChanges(DirectionalSliceFeature before, DirectionalSliceFeature after){
			dirChange = this.computeAngle(after);
		}



		void normalize(double means){
			dirChange -= means;
		}



		@Override
		public int area(){
			return area;
		}


		double dirChange(){
			return dirChange;
		}


		void computeVector(DirectionalSliceFeature before){
			if(before!=null){
				v = new Vector2D((double) (centroid[0] - before.centroid[0]), (double) (centroid[1] - before.centroid[1]));
			}
		}


		private double computeAngle(DirectionalSliceFeature after){			
			if(v == null || after == null || v.equals(Vector2D.ZERO) || after.v.equals(Vector2D.ZERO)) // is first or last slice or alpha already computed
				dirChange = 0;
			else if(dirChange == -1){
				dirChange = Vector2D.angle(v, after.v) * v.add(after.v).getNorm();
			}
			return dirChange;
		}





		@Override
		public double getScore() {
			return Math.abs(dirChange);
		}





		@Override
		public long[] centroid() {
			return centroid;
		}





	}





	public static final class BothSliceFeature implements VolumeSliceFeature, Scored {


		final long[] centroid = new long[3]; 

		double centerIntensity, rimIntensity;
		double intensityRatio;
		private double intensityRatioNorm;

		double dirChange = -1;
		int area, contour;

		private Vector2D v;



		@Override
		public <T extends RealType<T>> void accept(RandomAccess<T> labels, RandomAccess<T> grays, long[] pos, long[] temp, T paint) {

			area++;
			centroid[0] += pos[0];		centroid[1] += pos[1];		centroid[2] += pos[2];

			//Keep track of the countour's intensity
			for(int i = 0; i<Volume.k.length; i++){
				temp[0] = pos[0] + Volume.k[i][0];	temp[1] = pos[1] + Volume.k[i][1];
				labels.setPosition(temp);
				if(!labels.get().equals(paint)){
					contour++;
					grays.setPosition(pos);
					rimIntensity += grays.get().getRealDouble();
					break;
				}
			}
		}





		@Override
		public <T extends RealType<T>> boolean doneWithPositions(RandomAccess<T> grays, long[] temp) {

			if(area==0){
				intensityRatio = 1;
				return false;
			}else{
				centroid[0] /= area;		centroid[1] /= area;		centroid[2] /= area;
				rimIntensity /= contour;
				//Compute intensity close to the centroid
				for(int i = 0; i<Volume.k.length; i++){
					temp[0] = centroid[0] + Volume.k[i][0];	temp[1] = centroid[1] + Volume.k[i][1];
					grays.setPosition(temp);
					centerIntensity += grays.get().getRealDouble();
				}
				centerIntensity /= 9d;
				intensityRatio = centerIntensity / rimIntensity;
				return true;
			}

		}



		void computeChanges(BothSliceFeature before, BothSliceFeature after){
			dirChange = this.computeAngle(after);
		}




		void normalize(double intMean, double dirMean){
			intensityRatioNorm = (intensityRatio - intMean)/intMean;
			dirChange = (dirChange - dirMean);
		}



		@Override
		public int area(){
			return area;
		}

		double intensityRatio(){
			return intensityRatio;
		}

		double intensityRatioNorm(){
			return intensityRatioNorm;
		}

		double dirChange(){
			return dirChange;
		}


		void computeVector(BothSliceFeature before){
			if(before!=null){
				v = new Vector2D((double) (centroid[0] - before.centroid[0]), (double) (centroid[1] - before.centroid[1]));
				//System.out.println("v = "+v.toString());
			}
		}


		private double computeAngle(BothSliceFeature after){			
			if(v == null || after == null || v.equals(Vector2D.ZERO) || after.v.equals(Vector2D.ZERO)) // is first or last slice or alpha already computed
				dirChange = 0;
			else if(dirChange == -1){
				dirChange = Vector2D.angle(v, after.v) * v.add(after.v).getNorm();
			}
			return dirChange;
		}





		@Override
		public double getScore() {
			return intensityRatioNorm + Math.abs(dirChange);
		}





		@Override
		public long[] centroid() {
			return centroid;
		}


	}



	// ************************************************ PostProcessSplit Impl ***********************************************************




	private static class IntensityOptions{

		/**
		 * Absolute value of the ratio
		 */
		final SynchronizedSummaryStatistics absRatioStats = new  SynchronizedSummaryStatistics();
		/**
		 * Difference to mean
		 */
		final SynchronizedSummaryStatistics normRatioStats = new  SynchronizedSummaryStatistics();

		final double splitTolerance;

		double absRatioThreshold, normRatioThreshold ;

		public IntensityOptions(double splitTolerance) {
			this.splitTolerance = splitTolerance;
		}

		void computeThresholds(){
			absRatioThreshold = getAbsRatioMean() + getAbsRatioStd()/2 * splitTolerance;
			normRatioThreshold = getNormRatioStd()/2 * splitTolerance;	

			//System.out.println("Abs Ratio Threshold = "+ absRatioThreshold);
			//System.out.println("Norm Ratio Threshold = "+ normRatioThreshold);
		}


		void addValues(double absRatio, double normRatio){
			if(!Double.isNaN(absRatio))
				absRatioStats.addValue(absRatio);
			if(!Double.isNaN(normRatio))
				normRatioStats.addValue(normRatio);
		}

		double getAbsRatioMean(){
			return absRatioStats.getMean();
		}

		double getAbsRatioStd(){
			return absRatioStats.getStandardDeviation();
		}

		double getNormRatioStd(){
			return normRatioStats.getStandardDeviation();
		}

		double getSplitTolerance(){
			return splitTolerance;
		}

		double absRatioThreshold(){
			return absRatioThreshold;
		}


		double normRatioThreshold(){
			return normRatioThreshold;
		}
	}




	private static class DirectionalOptions{

		/**
		 * Absolute value of the ratio
		 */
		final SynchronizedSummaryStatistics dirStats = new  SynchronizedSummaryStatistics();


		final double splitTolerance;

		double threshold;

		public DirectionalOptions(double splitTolerance) {
			this.splitTolerance = splitTolerance;
		}

		void addValue(double dirChange){
			if(!Double.isNaN(dirChange))
				dirStats.addValue(dirChange);
		}

		void computeThreshold(){
			threshold = dirStats.getStandardDeviation() * splitTolerance;
		}

		double getDirChangeStd(){
			return dirStats.getStandardDeviation();
		}

		double getSplitTolerance(){
			return splitTolerance;
		}

		public double getDirChangeThreshold() {
			return threshold;
		}

	}






	private static PostProcessSplit<IntensityOnlySplitFeature, IntensityOptions> intensityOnly(){
		return new PostProcessSplit<IntensityOnlySplitFeature, IntensityOptions>(){

			@Override
			public IntensityOnlySplitFeature[] createSliceFeatureArray(int length) {
				IntensityOnlySplitFeature[] arr = new IntensityOnlySplitFeature[length];
				for(int i = 0; i<length; i++)
					arr[i] = new IntensityOnlySplitFeature();
				return arr;
			}


			@Override
			public void completeInit(IntensityOnlySplitFeature[] sFts) {
				// Compute Average
				double means = 0;
				int empty = 0;
				for(int s=1; s<sFts.length-1; s++){
					if(sFts[s].area() != 0){
						means+=sFts[s].intensityRatio();
					}else
						empty++;
				}

				means /= (sFts.length-2-empty);

				// Normalize features
				for(int s=1; s<sFts.length-1; s++)
					sFts[s].normalize(means);
			}


			@Override
			public PopulationInfoCollector<IntensityOnlySplitFeature, IntensityOptions> createCollector(ATVOptions options) {

				final IntensityOptions result = new IntensityOptions(options.splitTolerance()); 

				return new PopulationInfoCollector<IntensityOnlySplitFeature,IntensityOptions>(){

					@Override
					public void accept(IntensityOnlySplitFeature v) {
						result.addValues(v.intensityRatio(), v.intensityRatioNorm());
					}

					@Override
					public IntensityOptions deliver() {
						result.computeThresholds();
						return result;
					}

				};
			}


			@Override
			public List<Integer> rankedSplitCandidates(IntensityOnlySplitFeature[] arr, IntensityOptions options) {
				// options.computeThresholds();
				//Only create list if any should be split
				List<Integer> list = null;
				for(int i = 1; i<arr.length-1; i++){
					if(arr[i].intensityRatio() > options.absRatioThreshold() && arr[i].intensityRatioNorm() > options.normRatioThreshold()){
						if(list == null)
							list = new ArrayList<>();
						list.add(i);
					}
				}			
				// Now Sort the list in descending score
				if(list!=null && list.size()>1)
					Collections.sort(list, (s1,s2) -> Double.compare(arr[s2].getScore(), arr[s1].getScore()));
				return list;
			}



			@Override
			public String toString(){
				return "Intensity Only";
			}

		};
	}




	private static PostProcessSplit<DirectionalSliceFeature, DirectionalOptions> directionalOnly(){
		return new PostProcessSplit<DirectionalSliceFeature, DirectionalOptions>(){

			@Override
			public DirectionalSliceFeature[] createSliceFeatureArray(int length) {
				DirectionalSliceFeature[] arr = new DirectionalSliceFeature[length];
				for(int i = 0; i<length; i++)
					arr[i] = new DirectionalSliceFeature();
				return arr;
			}


			@Override
			public void completeInit(DirectionalSliceFeature[] sFts) {

				// Compute Vectors
				sFts[0].computeVector(null);
				for(int s = 1; s<sFts.length; s++)
					sFts[s].computeVector(sFts[s-1]);

				//Now provide before and after to each SliceFeature to measure rate of change of angle and area
				for(int s = 1; s<sFts.length-1; s++){
					sFts[s].computeChanges(sFts[s-1], sFts[s+1]);
				}				

				// Compute Average
				double means = 0;
				int empty = 0;
				for(int s=1; s<sFts.length-1; s++){
					if(sFts[s].area() != 0){
						means+=sFts[s].dirChange();
					}
					else
						empty++;
				}
				means /= (sFts.length-2-empty);

				// Normalize features
				for(int s=1; s<sFts.length-1; s++)
					sFts[s].normalize(means);
			}



			@Override
			public PopulationInfoCollector<DirectionalSliceFeature, DirectionalOptions> createCollector(ATVOptions options) {

				final DirectionalOptions result = new DirectionalOptions(options.splitTolerance()); 

				return new PopulationInfoCollector<DirectionalSliceFeature, DirectionalOptions>(){

					@Override
					public void accept(DirectionalSliceFeature v) {
						result.addValue(v.dirChange());
					}

					@Override
					public DirectionalOptions deliver() {
						result.computeThreshold();
						return result;
					}

				};
			}


			@Override
			public List<Integer> rankedSplitCandidates(DirectionalSliceFeature[] arr, DirectionalOptions options) {

				//Only create list if any should be split
				List<Integer> list = null;
				for(int i = 2; i<arr.length-2; i++){
					if(Math.abs(arr[i].dirChange()) > options.getDirChangeThreshold()){
						if(list == null)
							list = new ArrayList<>();
						list.add(i);
					}
				}			
				// Now Sort the list in descending score
				if(list!=null && list.size()>1)
					Collections.sort(list, (s1,s2) -> Double.compare(arr[s2].getScore(), arr[s1].getScore()));
				return list;
			}



			@Override
			public String toString(){
				return "Directional Only";
			}

		};
	}





	private static PostProcessSplit<BothSliceFeature, Pair<IntensityOptions,DirectionalOptions>> both(){
		return new PostProcessSplit<BothSliceFeature, Pair<IntensityOptions,DirectionalOptions>>(){

			@Override
			public BothSliceFeature[] createSliceFeatureArray(int length) {
				BothSliceFeature[] arr = new BothSliceFeature[length];
				for(int i = 0; i<length; i++)
					arr[i] = new BothSliceFeature();
				return arr;
			}


			@Override
			public void completeInit(BothSliceFeature[] sFts) {

				// Compute Vectors
				sFts[0].computeVector(null);
				for(int s = 1; s<sFts.length; s++)
					sFts[s].computeVector(sFts[s-1]);

				//Now provide before and after to each SliceFeature to measure rate of change of angle and area
				for(int s = 1; s<sFts.length-1; s++){
					sFts[s].computeChanges(sFts[s-1], sFts[s+1]);
				}				


				// Compute Average
				double dirMean = 0, intMean= 0;
				int empty = 0;
				for(int s=1; s<sFts.length-1; s++){
					if(sFts[s].area() != 0){						
						dirMean += sFts[s].dirChange();
						intMean += sFts[s].intensityRatio();
					}
					else
						empty++;
				}

				intMean /= (sFts.length-2-empty);		dirMean /= (sFts.length-2-empty);

				// Normalize features
				for(int s=1; s<sFts.length-1; s++)
					sFts[s].normalize(intMean, dirMean);
			}


			@Override
			public PopulationInfoCollector<BothSliceFeature, Pair<IntensityOptions,DirectionalOptions>> createCollector(ATVOptions options) {

				final IntensityOptions intResult = new IntensityOptions(options.splitTolerance());
				final DirectionalOptions dirResult = new DirectionalOptions(options.splitTolerance()); 

				return new PopulationInfoCollector<BothSliceFeature, Pair<IntensityOptions, DirectionalOptions>>(){

					@Override
					public void accept(BothSliceFeature v) {
						intResult.addValues(v.intensityRatio(), v.intensityRatioNorm());
						dirResult.addValue(v.dirChange());
					}

					@Override
					public Pair<IntensityOptions,DirectionalOptions> deliver() {
						intResult.computeThresholds();
						dirResult.computeThreshold();
						return new Pair<>(intResult, dirResult);
					}

				};
			}


			@Override
			public List<Integer> rankedSplitCandidates(BothSliceFeature[] arr, Pair<IntensityOptions, DirectionalOptions> options) {

				// options.computeThresholds();
				final IntensityOptions intResult = options.getFirst();
				final DirectionalOptions dirResult = options.getSecond();				

				//Only create list if any should be split
				List<Integer> list = null;
				for(int i = 1; i<arr.length-1; i++){
					if((arr[i].intensityRatio() > intResult.absRatioThreshold() && arr[i].intensityRatioNorm() > intResult.normRatioThreshold())
							|| Math.abs(arr[i].dirChange()) > dirResult.getDirChangeThreshold()		){
						if(list == null)
							list = new ArrayList<>();
						list.add(i);
					}
				}			
				// Now Sort the list in descending score
				if(list!=null && list.size()>1)
					Collections.sort(list, (s1,s2) -> Double.compare(arr[s2].getScore(), arr[s1].getScore()));
				return list;
			}




			@Override
			public String toString(){
				return "Both";
			}

		};
	}






	/*
		NONE{

			@Override
			public boolean isSplit(BothSliceFeature<?> sf, SummaryStatistics[] stats, double sigma) {
				return false;
			}



		},
		Intensity{

			@Override
			public boolean isSplit(BothSliceFeature<?> sf, SummaryStatistics[] stats, double sigma) {
				if(stats[0].getMean() + stats[0].getStandardDeviation()/2 * sigma< sf.intensityRatio() 
						&& stats[1].getStandardDeviation()/2 * sigma < sf.intensityRatioNorm())
					return true;
				else
					return false;
			}


		},
		Directional{

			@Override
			public boolean isSplit(BothSliceFeature<?> sf, SummaryStatistics[] stats, double sigma) {
				if(stats[2].getStandardDeviation() * sigma < Math.abs(sf.dirChange()))
					return true;
				return false;
			}


		},
		Both{

			@Override
			public boolean isSplit(BothSliceFeature<?> sf, SummaryStatistics[] stats, double sigma) {
				if(stats[0].getMean() + stats[0].getStandardDeviation()/2 * sigma< sf.intensityRatio()
						&& stats[1].getStandardDeviation()/2 * sigma < sf.intensityRatioNorm())
					return true;
				else if(stats[2].getStandardDeviation() * sigma < Math.abs(sf.dirChange()))
					return true;
				return false;
			}


		};
	}
	 */



}
