package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.distribution.NormalDistribution;

public abstract class FindPeaks {

	public static <N extends Number> List<Integer> peak1dCircular(List<N> values, double threshold) {

		List<Integer> peaks = new ArrayList<>(4);

		//Check borders
		if(values.get(0).doubleValue()>threshold)
			if(values.get(0).doubleValue()>values.get(values.size()-1).doubleValue() && values.get(0).doubleValue()>=values.get(1).doubleValue())
				peaks.add(0);

		if(values.get(values.size()-1).doubleValue()>threshold)
			if(values.get(values.size()-1).doubleValue()>values.get(values.size()-2).doubleValue() && values.get(values.size()-1).doubleValue()>=values.get(0).doubleValue())
				peaks.add(values.size()-1);

		for(int i = 1; i<values.size()-1; i++){
			if(values.get(i).doubleValue()>threshold)
				if(values.get(i).doubleValue()>values.get(i-1).doubleValue() && values.get(i).doubleValue()>=values.get(i+1).doubleValue())
					peaks.add(i);
		}


		return peaks;
	}

	
	public static <N extends Number> List<Integer> peak1dLinear(List<N> values, double threshold) {

		List<Integer> peaks = new ArrayList<>(4);

		for(int i = 1; i<values.size()-1; i++){
			if(values.get(i).doubleValue()>threshold)
				if(values.get(i).doubleValue()>values.get(i-1).doubleValue() && values.get(i).doubleValue()>=values.get(i+1).doubleValue())
					peaks.add(i);
		}


		return peaks;
	}
	
	
	public static List<Integer> peak1dLinear(double[] values, double threshold, double minAmplitude) {
		
		List<Integer> peaks = new ArrayList<>(4);
		
		for(int i = 1; i<values.length-1; i++){
			if(values[i]>threshold)
				if(values[i]>values[i-1]+minAmplitude && values[i]>values[i+1]+minAmplitude)
					peaks.add(i);
		}

		return peaks;
	}

	
	public static List<Integer> minima1dLinear(List<Double> values, double threshold) {

		List<Integer> peaks = new ArrayList<>(4);

		for(int i = 1; i<values.size()-1; i++){
			if(values.get(i)<threshold)
				if(values.get(i)<values.get(i-1) && values.get(i)<=values.get(i+1))
					peaks.add(i);
		}


		return peaks;
	}
	
	
	public static List<Integer> minima1dLinear(double[] values, double threshold, double minAmplitude) {
		List<Integer> peaks = new ArrayList<>(4);

		for(int i = 1; i<values.length-1; i++){
			if(values[i]<threshold)
				if(values[i]<values[i-1]-minAmplitude && values[i]<=values[i+1]-minAmplitude)
					peaks.add(i);
		}
		
		return peaks;
	}
	
	

	public static void main(String[] args) {

		//setup
		NormalDistribution d1 = new NormalDistribution(100, 20);
		NormalDistribution d2 = new NormalDistribution(300, 15);
		NormalDistribution d3 = new NormalDistribution(900, 100);
		List<Double> values = new ArrayList<>(100);
		for(int i = 0; i<100000; i++){
			values.add(0.3*d1.density(i)+0.4*d2.density(i)+0.3*d3.density(i));
		}

		//values.forEach(System.out::println);

		System.out.println("Finding peaks");
		FindPeaks.peak1dCircular(values, 0).forEach((id)->System.out.println("id = "+id+" ; value = "+values.get(id)));
		System.out.println("===============================");
		FindPeaks.minima1dLinear(values, 1).forEach((id)->System.out.println("id = "+id+" ; value = "+values.get(id)));

	}


	


	







}
