package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.logging.Level;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.app.modules.AnalysisException;
import org.pickcellslab.pickcells.api.app.ui.UI;
import org.pickcellslab.pickcells.api.img.io.ImgsChecker;
import org.pickcellslab.pickcells.api.img.io.StaticImgIO;
import org.pickcellslab.pickcells.api.img.pipeline.DimensionalityEffect;
import org.pickcellslab.pickcells.api.img.pipeline.ExtendedImageInfo;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;
import org.pickcellslab.pickcells.api.img.pipeline.PipelineBuilder;
import org.pickcellslab.pickcells.api.img.pipeline.ProcessingPipeline;
import org.pickcellslab.pickcells.api.img.pipeline.Skip;
import org.pickcellslab.pickcells.api.img.process.ChannelExtractor;
import org.pickcellslab.pickcells.api.img.steerable.SteerableFilter;

import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotification;

import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.real.FloatType;

@MetaInfServices(Module.class)
public class RidgeBasedSegmentation extends AbstractAnalysis{


	//TODO split into 2 module detached and database connected


	private <T extends RealType<T> & NativeType<T>> void run(){



		// Create The Pipeline (UI form)
		List<ImgProcessing<T,FloatType>> preprocess = new ArrayList<>();	
		preprocess.add(new SteerableFilter<>());
		preprocess.add(new Skip<>(new FloatType(), DimensionalityEffect.FLAT_TO_FLAT));


		RidgeSegmentationPipe<FloatType> rs = new RidgeSegmentationPipe<>();
		AreaToVolume<FloatType,?,?> atv = new AreaToVolume<>(rs.getOptions());

		
		Optional<ProcessingPipeline<T,FloatType>> pipeline =	
				new PipelineBuilder<T,T>()
				.step(new ChannelExtractor<T>(), "Extract One Channel")
				.steps(preprocess, "Preprocessing ...")
				.step(rs, "Slice by Slice Segmentation")
				.step(atv, "Merge Areas to 3D Volumes")
				.buildForUserChosenFiles(StaticImgIO.createCheckerBuilder()
						.addCheck(ImgsChecker.calibration)
						.addCheck(ImgsChecker.ordering));



		if(pipeline.isPresent()){
			
			Consumer<ExtendedImageInfo<FloatType>> listener = ei->{
				System.out.println("Image saved!");
			};
			
			try {
				pipeline.get().runPipeline((n)->n+"_Segmentation", listener);
		
			} catch (IOException e) {
				UI.display("Error", "An error occured during the Ridge Based Segmentation Process", e, Level.SEVERE);
				return;
			}
			
			WebNotification wn = NotificationManager.showNotification("Images saved!");
			wn.setDisplayTime(1000);
		}


		


	}





	@Override
	public Icon[] icons() {
		return new Icon[1];
	}

	@Override
	public String[] steps() {
		return new String[]{""};
	}

	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/RBS_Icon_32.png"));
	}

	@Override
	public void launch() throws AnalysisException {
		run();
	}

	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}




	@Override
	public String name() {
		return "Ridge Based Segmentation";
	}





	@Override
	public boolean isActive() {
		return true;
	}





	@Override
	public Object[] categories() {
		return new String[]{"Segmentation"};
	}

	@Override
	public String description() {
		return "Ridge Based Segmentation";
	}







}
