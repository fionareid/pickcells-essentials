package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.math3.util.MathArrays;
import org.apache.commons.math3.util.Pair;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.ui.wizard.ValidityTogglePanel;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.pipeline.DimensionalityEffect;
import org.pickcellslab.pickcells.api.img.pipeline.ImageBag;
import org.pickcellslab.pickcells.api.img.pipeline.ImageViewState;
import org.pickcellslab.pickcells.api.img.pipeline.ImgProcessing;
import org.pickcellslab.pickcells.api.img.pipeline.PreviewBag;
import org.pickcellslab.pickcells.api.img.process.ImgDimensions;
import org.pickcellslab.pickcells.api.img.process.LabelGen;
import org.pickcellslab.pickcells.segmentation.RidgeArea.Ambiguity;

import com.alee.laf.label.WebLabel;
import com.alee.managers.popup.WebPopup;
import com.alee.utils.SwingUtils;

import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.IntervalView;
import net.imglib2.view.Views;

public class AreaToVolume<T extends RealType<T> & NativeType<T>, V extends VolumeSliceFeature, O> extends MouseAdapter implements ImgProcessing<T,T>{


	static final String link = "NEXT";
	static final String repairLink = "REPAIR";
	static final AKey<Double> overlap = AKey.get("Overlap",Double.class);
	static final AKey<Double> jaccard = AKey.get("Jaccard",Double.class);
	static final AKey<long[]> overlapCenter = AKey.get("Overlap",long[].class);
	//static final AKey<double[]> vector = AKey.get("vector",double[].class);
	static final AKey<Double> upArea = AKey.get("Upstream Area",Double.class);

	private int zDim, tDim;
	private ATVDialogAdvanced dialog;
	private final RidgeSegmentationOptions options;

	private RandomAccessibleInterval<T> input;
	private RandomAccessibleInterval<T> grays;
	private ImageViewState<?> state;

	private Map<T,Volume<T,V,O>> displayedVolumes;



	public AreaToVolume(RidgeSegmentationOptions options) { //TODO should change this to get a more generic type to deliver default values
		this.options = options;
	}


	@Override
	public String name() {
		return "Area To Volumes";
	}



	@Override
	public <V extends RealType<V>> ValidityTogglePanel inputDialog(ImageViewState<V> state,	PreviewBag<T> input) {

		int[] noC = Image.removeDimension(state.getOrder(), Image.c);
		zDim = noC[Image.z];
		tDim = noC[Image.t];

		if(dialog == null){
			dialog = new ATVDialogAdvanced(options);
		}

		T type = input.getCurrentInputType();
		this.input = ImgDimensions.hardCopy(input.getInput(), type);
		this.grays = input.getFirstFlat();
		this.state = state;

		return dialog;
	}


	@Override
	public void changeInput(PreviewBag<T> current) {
		T type = current.getCurrentInputType();
		this.input = ImgDimensions.hardCopy(current.getInput(), type);			
		this.grays = current.getFirstFlat();
	}



	@Override
	public RandomAccessibleInterval<T> validate() {

		Views.iterable(state.getAnnotationImage()).forEach(t->t.setZero());		
		
		if(tDim!=-1){
			IntStream.range(0, (int)input.dimension(tDim)).forEach(t->{
				final IntervalView<T> frame = Views.hyperSlice(input, tDim, t);
				final IntervalView<T> graysFrame = Views.hyperSlice(grays, tDim, t);
				final IntervalView<? extends RealType> annotFrame = Views.hyperSlice(state.getAnnotationImage(), tDim, t);
				displayedVolumes = runSliceConnections(frame, graysFrame, annotFrame, dialog.getOptions(), zDim);
			});

		}
		else
			displayedVolumes = runSliceConnections(input, grays, state.getAnnotationImage(), dialog.getOptions(), zDim);


		return input;
	}


	public RandomAccessibleInterval<T> getResult() {
		return input;
	}




	@Override
	public String description() {
		return "Identify Volumes from individually segmented 2D slices";
	}

	@Override
	public boolean repeatable() {
		return false;
	}

	@Override
	public DimensionalityEffect dimensionalityEffect() {
		return DimensionalityEffect.FLAT_TO_FLAT;
	}

	@Override
	public boolean requiresAnnotationChannel() {
		return true;
	}



	@Override
	public Function<ImageBag<T>, RandomAccessibleInterval<T>> toPipe() {

		return (bag)->{
			
			final RandomAccessibleInterval<T> input = bag.getInputFrame(0);
			final RandomAccessibleInterval<T> grays = bag.getFirstFlat(0);
			runSliceConnections(input, grays, null, dialog.getOptions(), zDim);

			return input;

		};
	}











	@SuppressWarnings("unchecked")
	private static <T extends RealType<T> & NativeType<T>,V extends VolumeSliceFeature, O, A extends RealType<A>> 	
	Map<T, Volume<T,V,O>>	runSliceConnections(RandomAccessibleInterval<T> labels, RandomAccessibleInterval<T> grays, RandomAccessibleInterval<A> annotations, ATVOptions options, int zDim){


		// ***************************   First create the graph of potential connexions  *****************************************


		final PostProcessSplit<V,O> splitType = options.splitType();


		IntervalView<T> prevView = Views.hyperSlice(labels, zDim, 0); // cut along z

		RidgeArea<T> prototype = new RidgeArea<>();


		List<RidgeArea<T>> prev = new ArrayList<>(SegmentationUtil.createMap(prevView, prototype).values());
		Set<RidgeArea<T>> all = new HashSet<>(prev);
		prev.forEach(a->a.setAttribute(RidgeArea.slice,0));
		//prev.forEach(a->a.drawBoundingBox(state.getOverlay()));


		for(int z = 1 ; z<labels.dimension(zDim); z++){

			//System.out.println("Connecting slice "+(z-1)+" and "+z);



			IntervalView<T> currentView = Views.hyperSlice(labels, zDim, z);
			List<RidgeArea<T>> current = new ArrayList<>(SegmentationUtil.createMap(currentView, prototype).values());

			final int s = z;
			current.forEach(a->a.setAttribute(RidgeArea.slice,s));
			//current.forEach(a->a.drawBoundingBox(state.getOverlay()));

			if(prev.isEmpty() || current.isEmpty()){
				prev.addAll(current);	
				filterPrevious(prev, options, z+1);
				all.addAll(current);
				continue;
			}



			// Create the graph


			connectSets(prev, current, options, labels);



			// Remove from prev the areas from slices too far above
			prev.addAll(current);	
			filterPrevious(prev, options, z+1);					
			all.addAll(current);			


		}







		// ***************************   Split nodes which have multiple connections above and below  *****************************************




		// Create a Labels Generator for new elements (initialize using the max found in all +1)

		T maxLabel = labels.randomAccess().get().createVariable();
		float m = 0;
		for(RidgeArea<T> a : all){
			if(a.label().get() > m)
				m = a.label().get();
		}
		maxLabel.setReal(m+1);
		Iterator<T> gen = new LabelGen<>(maxLabel);



		// Identify and split ambiguous nodes
		// Keep a link between nodes (one side ambiguity) that have been split for later repair
		final String splitLink = "splitted";
		LinkedList<RidgeArea<T>> splitted = new LinkedList<>();

		RandomAccess<T> access = labels.randomAccess();
		T zero = access.get().createVariable();

		final List<RidgeArea<T>> allList = new ArrayList<>(all);	all = null;
		final ListIterator<RidgeArea<T>> it = allList.listIterator();

		while(it.hasNext()){

			RidgeArea<T> ra = it.next();


			if(ra.getDegree(Direction.INCOMING, link) > 1){




				// ******* IF BOTH then split  **********
				//Does the area also have multiple links downstream ? then split!
				if(ra.getDegree(Direction.OUTGOING, link) > 1){

					//ra.drawBoundingBox((RandomAccessibleInterval)annotations);

					//first remove the label
					ra.repaint(zero, labels);
					//Remove the split area from the graph
					it.remove();



					// copy the areas from above or below
					final double above = 
							ra.getLinks(Direction.INCOMING, link)
							.mapToDouble(l->l.getAttribute(AreaToVolume.jaccard).get())
							.sum();

					final double below = 
							ra.getLinks(Direction.OUTGOING, link)
							.mapToDouble(l->l.getAttribute(AreaToVolume.jaccard).get())
							.sum();



					if(above >= below){
						Collection<Link> belowLinks = ra.getLinks(Direction.OUTGOING, link).collect(Collectors.toList());
						ra.getLinks(Direction.INCOMING, link).forEach(l->
							it.add(((RidgeArea<T>)l.source()).copyToSlice(labels, ra.sliceLocation(), gen.next(), belowLinks, options, l.getAttribute(jaccard).get())));
					}
					else{
						Collection<Link> aboveLinks = ra.getLinks(Direction.INCOMING, link).collect(Collectors.toList());
						ra.getLinks(Direction.OUTGOING, link).forEach(l->
							it.add(((RidgeArea<T>)l.target()).copyToSlice(labels, ra.sliceLocation(), gen.next(), aboveLinks, options, l.getAttribute(jaccard).get())));
					}

					// Finally remove potential links
					ra.removeLink(l->l.declaredType().equals(link), true);



				}// END of ambiguity on both sides


				else{ // Ambiguity above only

					//TODO create a condition to split JI of up or down < JI of merged ambiguous

					// Check condition
					// Jaccard Index from below
					boolean split = true;
					if(ra.getDegree(Direction.OUTGOING, link)!=0){
						double belowJI = ra.getLinks(Direction.OUTGOING, link).findFirst().get().getAttribute(jaccard).get();
						//Compute JI from merge above
						double intersect = 0;	double union = ra.area();
						final Iterator<Link> inIt = ra.getLinks(Direction.INCOMING, link).iterator();
						while(inIt.hasNext()){
							Link l = inIt.next();
							intersect += ((RidgeArea<T>) l.source()).intersection(ra, labels).getFirst();
							union += ((RidgeArea<T>) l.source()).area();
						}
						union -= intersect;
						double mergedJI = intersect/union;

						split = belowJI < mergedJI;
					}				

					if(split){

						//first remove the label 
						ra.repaint(zero, labels);
						//Remove the split area from the graph
						it.remove();	
						splitted.add(ra);

						ra.setAmbiguity(Ambiguity.UP);
						// copy from above
						final List<Link> belowLinks = ra.getLinks(Direction.OUTGOING, link).collect(Collectors.toList());
						final Iterator<Link> inIt = ra.getLinks(Direction.INCOMING, link).iterator();
						while(inIt.hasNext()){
							Link l = inIt.next();
							RidgeArea<T> added = ((RidgeArea<T>)l.source()).copyToSlice(labels, ra.sliceLocation(), gen.next(), belowLinks, options,l.getAttribute(jaccard).get());
							it.add(added);
							new DataLink(splitLink, ra, added, true);
						}

						/*
						// Now replace potential links by repair links
						ra.getLinks(Direction.BOTH, link).forEach(l->{
							Link rp = new DataLink(repairLink, l.source(),l.target(), true);
							rp.setAttribute(jaccard,l.getAttribute(jaccard).get());
						});
						 */
						ra.removeLink(l->l.declaredType().equals(link), true);
					}


				}

			}



			else if(ra.getDegree(Direction.OUTGOING, link) > 1){ // Ambiguity below only

				// Check condition

				// Is the ambiguous area attached to an area above
				Collection<Link> inc = ra.getLinks(Direction.INCOMING, link).collect(Collectors.toList());
				boolean split = true;
				if(!inc.isEmpty()){
					// If yes get the Jaccard Index from above
					double aboveJI = inc.iterator().next().getAttribute(jaccard).get();

					//Compute JI from merge below
					double intersect = 0;	double union = ra.area();
					final Iterator<Link> outIt = ra.getLinks(Direction.OUTGOING, link).iterator();
					while(outIt.hasNext()){
						Link l = outIt.next();
						intersect += ((RidgeArea<T>) l.target()).intersection(ra, labels).getFirst();
						union += ((RidgeArea<T>) l.target()).area();
					}
					union -= intersect;
					double mergedJI = intersect/union;

					//Split only if above JI is below merged JI
					split = aboveJI < mergedJI;
				}				

				if(split){

					//first remove the label
					ra.repaint(zero, labels);

					//Remove the split area from the included list and add the current area to the list of splitted
					it.remove();
					splitted.add(ra);

					// Set Ambiguity Type
					ra.setAmbiguity(Ambiguity.DOWN);

					// Copy from below
					List<Link> aboveLinks = ra.getLinks(Direction.INCOMING, link).collect(Collectors.toList());
					final Iterator<Link> outIt = ra.getLinks(Direction.OUTGOING, link).iterator();
					while(outIt.hasNext()){
						Link l = outIt.next();
						RidgeArea<T> added = ((RidgeArea<T>)l.target()).copyToSlice(labels, ra.sliceLocation(), gen.next(), aboveLinks, options, l.getAttribute(jaccard).get());
						it.add(added);
						new DataLink(splitLink, ra, added, true);
					}

					/*
					//Now replace potential links by repair links					
					ra.getLinks(Direction.BOTH, link).forEach(l->{
						Link rp = new DataLink(repairLink, l.source(),l.target(), true);
						rp.setAttribute(jaccard,l.getAttribute(jaccard).get());
					});
					 */
					ra.removeLink(l->l.declaredType().equals(link), true);

				}

			}			

		}










		// **********************************  Now Perform the graph colouring process  *****************************************






		// Collect all the links into a queue and sort according to the value of the jaccard index

		final LinkedList<Link> queue = new LinkedList<>();		
		allList.forEach(ra->ra.getLinks(Direction.OUTGOING, link).forEach(queue::add));
		Collections.sort( queue, (l2,l1) -> Double.compare(l1.getAttribute(jaccard).get(), l2.getAttribute(jaccard).get()) );

		// Keep track of volumes 
		Map<T,Volume<T,V,O>> volumes = new HashMap<>();

		//keep track of boundaries
		LinkedList<Link> boundaries = new LinkedList<>();

		for(Link l : queue){

			//Link l = queue.pollLast();
			//System.out.println("jaccard = "+l.getAttribute(jaccard).get());
			RidgeArea<T> source = (RidgeArea<T>) l.source(); 
			RidgeArea<T> target = (RidgeArea<T>) l.target(); 

			if(source.hasColor()){
				if(target.hasColor()){ // if both have different colors
					if(!mergeOrCreateBoundary(volumes.get(source.getColor()), volumes.get(target.getColor()), volumes, options))
						boundaries.add(l);					
				}
				else{ // if only the source has a color
					if(!propagateOrCreateBoundary(volumes.get(source.getColor()), target, gen, volumes, options))
						boundaries.add(l);
				}
			}
			else if(target.hasColor()){// if only the target has a color
				if(!propagateOrCreateBoundary(volumes.get(target.getColor()), source, gen, volumes, options))
					boundaries.add(l);
			}
			else{ // if none have color
				createAndAddToMap(source, target, gen, volumes, splitType);
			}

		}








		/*


		// **********************************  Inspect worst Jaccard indices  *****************************************
		System.out.println("********* Inspecting worst indices *********");

		final SummaryStatistics vStats = new SummaryStatistics();
		volumes.values().forEach(v->vStats.addValue(v.volume()));
		System.out.println("volume Mean = "+vStats.getMean());
		System.out.println("volume Std = "+vStats.getStandardDeviation());
		double vThresh = vStats.getStandardDeviation()/3;

		Collections.reverse(queue);		
		for(Link l : queue){
			System.out.println("jaccard = "+l.getAttribute(jaccard).get());
			if(l.getAttribute(jaccard).get()>0.33)
				break;

			RidgeArea<T> source = (RidgeArea<T>) l.source();
			RidgeArea<T> target = (RidgeArea<T>) l.target();
			Volume<T,V,O> v1 = volumes.get(source.getColor());			
			Volume<T,V,O> v2 = volumes.get(target.getColor());

			if(v1!=null && v2!=null && v1 == v2){

				T id2 =  gen.next();
				Pair<Volume<T,V,O>, Volume<T,V,O>> pair = v1.split(source, v1.getColor(), target, id2);
				if(Math.abs(pair.getFirst().volume()-vStats.getMean()) < vThresh
						&& Math.abs(pair.getSecond().volume()-vStats.getMean()) < vThresh){
					volumes.put(v1.getColor(), pair.getFirst());
					volumes.put(id2, pair.getSecond());
					System.out.println("Splitting!");
				}
			}
		}


		 */




		// **********************************  Repair splits which led to small objects  *****************************************

		/* TODO test:
		// find volume of created + other volumes from copied, if at least 3 volumes  -> check if up or down and see accordingly if volumes below and above are balanced
		// if not repair

		List<Volume<T,V,O>> vols = new ArrayList<>(4);
		while(!splitted.isEmpty()){
			RidgeArea<T> toRepair = splitted.pollLast();			
			//toRepair.drawBoundingBox((RandomAccessibleInterval)annotations);
		}
		 */








		// **********************************  Attempt to link too small volumes to larger ones at boundaries  *****************************************
		//NB: Boundaries already sorted in descending order of jaccard index
		while(!boundaries.isEmpty()){
			Link l = boundaries.pollLast();
			RidgeArea<T> source = (RidgeArea<T>) l.source();
			RidgeArea<T> target = (RidgeArea<T>) l.target();

			Volume<T,V,O> v1 = volumes.get(source.getColor());			
			Volume<T,V,O> v2 = volumes.get(target.getColor());
			if(v1!=null && v2!=null)//Can be null if from a split repair (see above) 
				mergeExtrema(v1, v2, options, volumes, l.getAttribute(jaccard).get());
		}









		// ***************************   Finalize the Segmentation  *****************************************



		// - Erase all ra with no color (not part of a volume) unless their area >= minVolume
		allList.forEach(ra->{
			if(!ra.hasColor())
				if(options.removeFlat() || ra.area()<options.minVolume)
					ra.repaint(zero, labels);
				else
					ra.repaint(gen.next(), labels);
		});

		allList.clear();



		// - Paint the volumes and get stats for splits			

		if(splitType != null){
			final PopulationInfoCollector<V,O> collector = splitType.createCollector(options);	

			volumes.values().stream().parallel().forEach(v->{
				v.applyLabelAndSmooth(labels, options);				
			});

			volumes.values().stream().parallel().filter(v->!v.wasErased()).forEach(v->{
				v.updateFeatures(labels, grays, collector);
				if(v.volume()<options.minVolume)
					v.erase(labels);
			});

			final O o = collector.deliver();


			volumes.values().stream().parallel().filter(v->!v.wasErased()).forEach(v->{
				v.split(labels, gen, annotations, o, options); 			
			});

		}
		else{
			volumes.values().stream().parallel().forEach(v->{
				v.applyLabelAndSmooth(labels, options);				
			});

			volumes.values().stream().parallel().filter(v->!v.wasErased()).forEach(v->{
				v.updateFeatures(labels, grays, null);
				if(v.volume()<options.minVolume)
					v.erase(labels);
			});
		}


		return volumes;

	}









	private static <T extends RealType<T> & NativeType<T>,V extends VolumeSliceFeature, O> void mergeExtrema(Volume<T,V,O> v1, Volume<T,V,O> v2, ATVOptions options, Map<T, Volume<T,V,O>> volumes, double jaccard) {
		if(v1!=v2)
			if(jaccard>0.9){
				v1.addVolume(v2);
				volumes.remove(v2.getColor());
			}
			else if((v1.volume()<options.getMinVolume() || v2.volume()<options.getMinVolume() ) && options.acceptVolumeExtrema(v1.volume() + v2.volume())){
				v1.addVolume(v2);
				volumes.remove(v2.getColor());
			}
	}



	private static <T extends RealType<T> & NativeType<T>, V extends VolumeSliceFeature, O> 
	void createAndAddToMap(RidgeArea<T> source, RidgeArea<T> target, Iterator<T> gen, Map<T, Volume<T,V,O>> volumes, PostProcessSplit<V,O> splitType) {
		T novelId = gen.next();
		Volume<T,V,O> v = new Volume<>(novelId, splitType);
		v.addArea(source);
		v.addArea(target);
		volumes.put(novelId, v);
	}



	/**
	 * @param v1
	 * @param v2
	 * @param volumes
	 * @param options
	 * @return true if the 2 volumes were merged
	 */
	private static <T extends RealType<T> & NativeType<T>, V extends VolumeSliceFeature, O> boolean mergeOrCreateBoundary(Volume<T,V,O> v1, Volume<T,V,O> v2, Map<T, Volume<T,V,O>> volumes,
			ATVOptions options) {
		if(v1!=v2)
			if(options.acceptVolume2Sigmas(v1.volume() + v2.volume())){
				v1.addVolume(v2);
				volumes.remove(v2.getColor());
				return true;
			}

		return false;

	}



	/**
	 * @param volume
	 * @param noColor
	 * @param gen
	 * @param volumes
	 * @param options
	 * @return true if the color was propagated
	 */
	private static <T extends RealType<T> & NativeType<T>, V extends VolumeSliceFeature, O> boolean propagateOrCreateBoundary(Volume<T,V,O> volume, RidgeArea<T> noColor, Iterator<T> gen, Map<T, Volume<T,V,O>> volumes, ATVOptions options) {

		// Propagate the color only if adding the area to the current colored volume  does not make the volume > max volume.
		if( options.acceptVolume2Sigmas(volume.volume() + noColor.area()) ){
			volume.addArea(noColor);
			return true;
		}
		else{
			//If we reach the maximum size, create a new volume and add it to the map
			T novelId = gen.next();
			Volume<T,V,O> v = new Volume<>(novelId, options.splitType());
			v.addArea(noColor);
			volumes.put(novelId,v);
			return false;
		}

	}









	static <T extends RealType<T> & NativeType<T>> void connectSets(List<RidgeArea<T>> fprev, List<RidgeArea<T>> current, ATVOptions options, RandomAccessibleInterval<T> labels) {


		IntStream.range(0, fprev.size()).forEach(p->{
			IntStream.range(0, current.size()).forEach(c->{

				// First cut on inter-centroid distance
				double[] pCentroid = fprev.get(p).centroid();
				double[] cCentroid = current.get(c).centroid();
				double dcc = MathArrays.distance(pCentroid, cCentroid);

				if(dcc < options.getMaxInterCentroidDistance()){

					// Check area of intersection
					Pair<Double, long[]> pair = fprev.get(p).intersection(current.get(c), labels);
					double itrsct = pair.getFirst();
					double pPrct = itrsct/fprev.get(p).area();
					double cPrct = itrsct/current.get(c).area();

					//double j = itrsct / (fprev.get(p).area() + current.get(c).area() - itrsct);

					if(pPrct > options.getMinOverlap() || cPrct > options.getMinOverlap()){
						//if(j > options.getMinOverlap()){

						// Jaccard Index : 
						double j = itrsct / (fprev.get(p).area() + current.get(c).area() - itrsct);
						//System.out.println("JI = "+j);


						Link cx = new DataLink(link, fprev.get(p),current.get(c),true);
						cx.setAttribute(Keys.distance, dcc);
						cx.setAttribute(overlap, (pPrct+cPrct));
						cx.setAttribute(jaccard, j);
						cx.setAttribute(overlapCenter, pair.getSecond());
						/*
						cx.setAttribute(vector, 
								new double[]{
										cCentroid[0]-pCentroid[0],
										cCentroid[1]-pCentroid[1],
										fprev.get(p).sliceLocation()-current.get(c).sliceLocation()});
						 */

					}

				}
			});
		});


	}



	private static <T extends RealType<T> & NativeType<T>> void filterPrevious(List<RidgeArea<T>> prev, ATVOptions options, int z) {


		Iterator<RidgeArea<T>> it = prev.iterator();
		while(it.hasNext()){
			RidgeArea<T> area = it.next();
			if(area.getDegree(Direction.OUTGOING, link)>0)
				it.remove();
			else if(area.getAttribute(RidgeArea.slice).get() < z-(options.getMaxJump())){
				it.remove();
			}
		}
		//System.out.println("Number of removed for "+z+" -> "+(b-prev.size()));
	}



	private WebPopup popup;


	@Override
	public void mouseClicked(MouseEvent e) {

		if(popup!=null){
			popup.hidePopup();
		}

		if(SwingUtils.isRightMouseButton(e) && displayedVolumes != null){

			RandomAccessibleInterval<T> volPlane = input;
			if(tDim != -1)			
				volPlane = Views.hyperSlice(volPlane, tDim, state.getSelectedT());//Get One frame

			if(zDim != -1)		
				volPlane = Views.hyperSlice(volPlane, zDim, state.getSelectedZ());//Get One slice

			final RandomAccess<T> access = volPlane.randomAccess();

			long[] pos = state.getImageCoordinates(e.getX(), e.getY());


			// Get the label under the mouse click
			// Remove channel dimension
			long[] result = new long[pos.length-1];
			int[] order = state.getOrder();
			int p = 0;
			for(int i = 0; i<order.length; i++){
				if(order[i]!=-1 && i!=Image.c){
					result[p] = pos[order[i]];
					p++;				
				}
			}

			access.setPosition(result);
			T value = access.get();
			Volume<T,?,?> selected = displayedVolumes.get(value);
			if(selected != null){				
				popup = new WebPopup ();
				popup.add ( new WebLabel ( "Volume = "+selected.volume(), WebLabel.CENTER ) );
				popup.setLocation( e.getX(), e.getY());
				popup.showPopup((Component) e.getSource(), e.getX(), e.getY());
				popup.setCloseOnFocusLoss(true);
			}

		}

	}






	@Override
	public void stop() {
		// TODO Auto-generated method stub

	}



	@Override
	public boolean requiresOriginal() {
		return false;
	}



	@Override
	public boolean requiresFlat() {
		return true;
	}

	@Override
	public T outputType(T inputType) {
		return inputType;
	}


	@Override
	public boolean condition(MinimalImageInfo info){
		return info.dimension(Image.z)>1;
	}
	

}
