package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.Pair;
import org.apache.log4j.Logger;
import org.pickcellslab.foundationj.datamodel.graph.TreeNode;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;

import com.alee.utils.ThreadUtils;

import net.imglib2.RandomAccess;
import net.imglib2.type.numeric.RealType;

public class Ridge<T extends RealType<T>, V extends RealType<V>> implements Iterable<long[]>{

	private static Logger log = Logger.getLogger(Ridge.class);


	@SuppressWarnings("rawtypes")
	static BiConsumer<Ridge,Ridge> updater;

	private final List<RidgePoint<T,V>> left;
	private final List<RidgePoint<T,V>> right;
	private final RidgePoint<T,V> ancestor;

	private RidgeIntensityFeatures intFeatures;
	private CurvatureFeatures curvFeatures;



	private double score = - Double.MAX_VALUE;
	private boolean scoreComputed;

	private long[] centroid;
	private double length = -1;
	private double endPointDistance;
	private double area = -1;
	private double comp = -1;







	public Ridge(float rootIntensity, RidgePoint<T,V> rp1, RidgePoint<T,V> rp2) {

		
		// First we need to identify the least common ancestor
		Optional<Pair<List<RidgePoint<T,V>>, List<RidgePoint<T,V>>>> pair = TreeNode.pathsToCommonAncestor(rp1, rp2);
		// First we need to identify the least common ancestor
		if(pair.isPresent()){

			left = pair.get().getFirst();
			right = pair.get().getSecond();
			ancestor = left.get(left.size()-1).getParent();		

		}
		else throw new IllegalArgumentException("rp1 and rp2 do not share a common ancestor");
	}




	/**
	 * @return The id of this Ridge
	 */
	public T id() {
		return left.get(0).id();
	}



	/**
	 * @return The first point in this Ridge (clockwise)
	 */
	public long[] getFirst(){
		final List<RidgePoint<T,V>> first = area > 0 ? left : right;
		return first.get(0).getRidgeSection().childCenter();
	}


	/**
	 * @return The last point in this Ridge (clockwise)
	 */
	public long[] getLast(){
		final List<RidgePoint<T,V>> last = area > 0 ? right : left;
		return last.get(0).getRidgeSection().childCenter();
	}







	public double getConcaveContribution(){
		return (getCurvature().concaveSum() );/// (double)curvFeatures.getN());
	}


	public double getCurvatureContribution(){
		return getCurvature().getAverage() ;
	}


	/*
	public double getCornersContribution(float t){
		return getCurvature(4, t).sumOfPeaks() ;
	}
	 */




	public double getCurvatureAverageNonZero() {
		return getCurvature().getAverageNonZero() ;
	}






	private double getZeroProportion() {
		return (double)getCurvature().zeroCount()/(double)curvFeatures.getN() ;
	}






	/**
	 * @return The location of the centroid of the enclosed shape defined by this ridge
	 */
	public long[] getCentroid(){
		if(centroid == null){
			Pair<Double,long[]> p = ImgGeometry.centroidArea(toPolyLineSampled(2));
			area = p.getFirst();
			centroid = p.getSecond();
		}
		return centroid;
	}



	public double getRadius(){
		double area = Math.abs(getArea());
		return FastMath.sqrt(area/Math.PI);
	}


	/**
	 * @return The area of the enclosed shape defined by this ridge (only accurate if the contour is not self-intersecting)
	 */
	public double getArea(){
		if(area == -1){
			Pair<Double,long[]> p = ImgGeometry.centroidArea(this.toPolyLineSampled(2));
			area = p.getFirst();
			centroid = p.getSecond();
		}
		return area;
	}



	/**
	 * @return An approximation of this ridge enclosed area, using the scan line algorithm. Note that the returned area will only be
	 * an approximation however it might be useful when the ridge is self intersecting.
	 */
	public double getAreaScanLine() {			
		return getIntensityFeatures().getAreaSC();
	}




	public RidgeIntensityFeatures getIntensityFeatures(){
		if(intFeatures == null)
			intFeatures = new RidgeIntensityFeatures(left.get(0).channelAccess(), left.get(0).ridgesAccess(), this);
		return intFeatures;
	}




	private CurvatureFeatures getCurvature() {
		if(curvFeatures==null){		
			List<Integer> cc = toChainCode();			
			List<Float> curv = ImgGeometry.curvature(cc, getCurvatureK());
			curvFeatures = new CurvatureFeatures(curv);			
		}		
		return curvFeatures;
	}




	private int getCurvatureK() {
		/*
		int probing = (int) (getRadius()+1);
		probing = Math.min(probing, 10);
		probing = Math.max(probing, 4);
		return probing;
		 */
		return 6;
	}




	public boolean selfIntersecting(){
		return false; //TODO
		//return ImgGeometry.selfIntersecting(toPolyline());
	}



	public double score(RidgeSegmentationOptions o, Map<Float,Ridge<T,V>> neighbours) {

		if(!scoreComputed){



			// First compute length features 
			computeLengthFeatures();



			//=======================================================================================
			// First check that minimal requirements are met
			//=======================================================================================



			double totalLength = length + endPointDistance;
			
			//System.out.println("Total length = "+totalLength);
			//System.out.println("length = "+length);
			//System.out.println("End Points Distance = "+endPointDistance);
			

			if(updater!=null){
				log.debug("*************************************");
				log.debug("Total Length "+totalLength);
				T t = left.get(0).id().createVariable();
				t.setReal(left.get(0).id().getRealDouble()+10);
				ImgGeometry.drawCircle(left.get(0).center(), 3,left.get(0).trackAccess(), t); 
				ImgGeometry.drawCircle(right.get(0).center(), 3,right.get(0).trackAccess(), t);
			}


			// Check total length		
			if(totalLength>o.getMinLength() && totalLength<o.getMaxLength()){

				if(updater!=null){
					log.debug("Total Length passed");
				}
				


				//Check if we are crossing our own track
				if(!selfIntersecting()){

					if(updater!=null){
						log.debug("Self Intersecting passed");
					}	


					// Make sure we are not closing from too far

					if(length/endPointDistance > o.getMinClosingRatio()){


						if(updater!=null){
							log.debug("Tortuosity passed");
						}

						// System.out.println("Tortuosity passed");

						// Check Area and compactness
						double posArea = Math.abs(getArea());
						if(comp == -1)
							comp = RidgeSegmentationOptions.compactness(totalLength, posArea);		



						// Are we within user defined bounds?

						if(posArea > o.getMinArea() && posArea < o.getMaxArea() && comp > o.getMinCompactness()){


							if(updater!=null){
								log.debug("Area passed");
							}


							// System.out.println("Area and compactness passed");


							//Check that we are not clipping other ridges too much
							//-----------------------------------------------------


							boolean pass = true;

							// Find neighbours we step on


							final Set<T> ids = new HashSet<>();
							final RandomAccess<T> access = left.get(0).labelsAccess().copyRandomAccess();
							for(long[] l : this){
								access.setPosition(l);
								if(access.get().getRealDouble()!=0){
									ids.add(access.get().copy());
								}
							}

							// Now check cliped area for each found neighbour

							for(T n : ids){
								//log.debug("Id -> "+n);
								Ridge<T,V> neigh = neighbours.get(n.getRealFloat());

								if(neigh!=null){

									// debug
									//	if(updater!=null)
									//		updater.accept(this,neigh);

									//Check that we are not cutting too much from neighbour
									//Count overlap
									double ovl1 = overlap(this,neigh,access);

									log.trace("Neigh Area -> " + neigh.getArea());

									if(ovl1 >= 0.2*getAreaScanLine() || ovl1 > 0.2 * (double)neigh.getAreaScanLine() || (neigh.getAreaScanLine()-ovl1)<o.getMinArea()){
										//log.debug("Clipping test failed");
										pass = false;
										break;
									}

								}
								else 
									// TODO throw new IllegalStateException("The map neighbour does not contain the label : "+n);
									log.debug("The map neighbour does not contain the label : "+n);
							}





							//=======================================================================================
							// Now compute the score of this ridge if min requirements are met
							//=======================================================================================


							if(pass){


								if(updater!=null){
									log.debug("Overlap passed");
								}
								//System.out.println("Overlap passed");


								if(o.useClassifier()){

									//System.out.println("Overlap passed");
									getIntensityFeatures(); // Make sure those are computed
									double[] features = getFeatures();
									double valid = o.getClassifier().getPosterior(o.validClass(), features);
									double invalid = o.getClassifier().getPosterior(o.invalidClass(), features);
									score = valid > invalid ? valid : -invalid;

									//System.out.println("Classifier Score = "+score);
								}
								else{									

									score = 0;

									// Intensity Feautures
									getIntensityFeatures();
									score += this.intFeatures.getBorderSteeredDistanceFromMean();
									//System.out.println("Score 1 = "+score);
									score += this.intFeatures.getcInnerIQR();
									//System.out.println("Score 2 = "+score);									
									// Curvature Features
									//--------------------------------------------------------------------------
									score += getZeroProportion();
									score -= getCurvatureContribution();
									score += getConcaveContribution(); // negative number
									score += comp;
									
									//System.out.println("Threshold Score = "+score);

								}

							}
						}
					}

				}

			}
			else
				score = Double.NEGATIVE_INFINITY;


			
			
			if(updater!=null){
				log.debug("Score = "+score);

				ThreadUtils.sleepSafely(50);
				T t = left.get(0).id().createVariable();
				if(score>-100){					
					t.setReal(100);
				}
				ImgGeometry.drawCircle(left.get(0).center(), 3,left.get(0).trackAccess(), t);
				ImgGeometry.drawCircle(right.get(0).center(), 3,right.get(0).trackAccess(), t);
			}

			scoreComputed = true;
		}


		return score;
	}






	private void computeLengthFeatures() {

		if(length == -1){

			for(RidgePoint<?,?> rp : left){
				RidgeSection<?,?> rs = rp.getRidgeSection();
				length += rs.pathLength();
			}
			for(RidgePoint<?,?> rp : right){
				RidgeSection<?,?> rs = rp.getRidgeSection();
				length += rs.pathLength();
			}

			endPointDistance = ImgGeometry.distance(left.get(0).center(), right.get(0).center()	);
		}

	}






	private double overlap(Ridge<T,V> ridge, Ridge<T,V> labeled, RandomAccess<T> access) {

		/*
		double ovl = ImgGeometry.area(ImgGeometry.clipPolygon( labeled.toPolyline(), ridge.toPolyline()));
		log.debug(ovl);
		return ovl;
		 */

		final AtomicInteger counter = new AtomicInteger();
		final T id = labeled.id();
		ImgGeometry.FillPolygon(
				ridge.toClosedPolyline(),
				(s,t) -> ImgGeometry.linePath(s, t,
						l ->{
							access.setPosition(l);
							if(access.get().equals(id))
								counter.incrementAndGet();
						})
				);

		return counter.get();

	}






	/**
	 * NB: Return the long[] positions of this Ridge from left upward to right downward except the very last position.
	 * Also does not include the positions of the closing line
	 * @see java.lang.Iterable#iterator()
	 */
	public Iterator<long[]> iterator(){
		// DEBUG System.out.println("Iterator initiated");

		final List<RidgePoint<T,V>> first = area > 0 ? left : right;
		final List<RidgePoint<T,V>> last = area > 0 ? right : left;


		return new Iterator<long[]>(){




			//Start by moving up from the left list
			boolean movingUp = true;
			Iterator<RidgePoint<T,V>> currentPath = first.iterator();
			Function<RidgePoint<T,V>,Iterator<long[]>> itChooser = (rp)-> rp.getRidgeSection().childToParentExcl();
			Iterator<long[]> currentSection = itChooser.apply(currentPath.next());

			@Override
			public boolean hasNext() {
				if(movingUp){
					if(currentSection.hasNext())
						return true;
					else if(currentPath.hasNext()){
						RidgePoint<T,V> rp = currentPath.next();
						// DEBUG System.out.println("rp "+rp.identifier);
						currentSection = itChooser.apply(rp);
						return true;
					}
					else{
						// DEBUG System.out.println("Starting right branch");
						currentPath = reverseIterator(last);
						itChooser = (rp)-> rp.getRidgeSection().parentToChildExcl();
						currentSection = itChooser.apply(currentPath.next());
						movingUp = false;
						if(!currentSection.hasNext())
							throw new RuntimeException("Encountered a section with an empty track!");
						return true;
					}
				}
				else{
					if(currentSection.hasNext())
						return true;
					else if(currentPath.hasNext()){
						RidgePoint<T,V> rp = currentPath.next();
						// DEBUG System.out.println("rp "+rp.identifier);
						currentSection = itChooser.apply(rp);
						return true;
					}
					else
						return false;
				}
			}

			@Override
			public long[] next() {
				return currentSection.next();
			}

		};
	}





	private Iterator<RidgePoint<T,V>> reverseIterator(List<RidgePoint<T,V>> right) {
		return new Iterator<RidgePoint<T,V>>(){

			int i = right.size() - 1;

			@Override
			public boolean hasNext() {
				return i >= 0;
			}

			@Override
			public RidgePoint<T,V> next() {
				return right.get(i--);
			}

		};
	}



	public List<Integer> toChainCode(){

		// DEBUG  System.out.println("Ridge Chain Code -> Ancestor is "+(ancestor == null ? 0 : ancestor.depth()));

		List<Integer> cc = new ArrayList<>();

		//Tracked ridge
		Iterator<long[]> it = this.iterator();
		long[] current = it.next();

		// DEBUG System.out.println(Arrays.toString(current));

		long[] temp = new long[2];
		try{
			while(it.hasNext()){
				long[] next = it.next();
				// DEBUG System.out.println(Arrays.toString(next));
				cc.add(Tracing.getCode(current, next, temp));
				current = next;
			}
		}catch(IllegalArgumentException e){
			System.out.println("------ Illegal Caught here -----");
			while(it.hasNext()){				
				System.out.println(Arrays.toString(it.next()));
			}
			throw new RuntimeException(e);
		}

		//Add the last position
		if(!Arrays.equals(current, getLast())){
			cc.add(Tracing.getCode(current, getLast(), temp));
			current = getLast();
		}

		// Closing line
		final long[] fc = new long[2];
		fc[0] = current[0]; 	fc[1] = current[1];
		ImgGeometry.linePath(current, getFirst(), l->{
			cc.add(Tracing.getCode(fc, l, temp));
			fc[0] = l[0];	fc[1] = l[1];
		});

		//Final position
		if(!Arrays.equals(fc, getFirst()))
			cc.add(Tracing.getCode(fc, getFirst(), temp));

		return cc;
	}




	/**
	 * @return A List representing a polyline using the centers of every {@link RidgePoint} in this Ridge.
	 * NB : from start to midle of endpoints (not closed)
	 */
	public List<long[]> toPolyLineCenter(){
		List<long[]> polygon = new ArrayList<>();
		for(RidgePoint<T,V> l : left){
			polygon.add(l.center());
		}
		Iterator<RidgePoint<T,V>> it = reverseIterator(right);
		while(it.hasNext()){
			polygon.add(it.next().center());
		}

		// Now add the midle of end points
		long[] f = getFirst();	long[] l = getLast();
		long[] m = {(f[0]+l[0])/2, (f[1]+l[1])/2};
		polygon.add(m);

		return polygon;
	}



	/**
	 * @return A List representing a polyline using the centers of every {@link RidgePoint} in this Ridge.
	 * NB : from start to midle of endpoints (not closed)
	 */
	public List<long[]> toPolyLineSampled(int sampling){
		List<long[]> polygon = new ArrayList<>();
		int c = 0;
		for(long[] l : this){
			if(c++ % sampling == 0)
				polygon.add(Arrays.copyOf(l,l.length));
		}

		Collections.reverse(polygon);

		return polygon;
	}







	/**
	 * @return The List of Contour to Centroid Distance in clockwise order.
	 */
	public List<Double> toCCD(){

		final long[] centroid = getCentroid();
		List<Double> ccd = new ArrayList<>();
		for(long[] l : this){
			ccd.add(ImgGeometry.distance(l, centroid));
		}
		ImgGeometry.linePath(getLast(), getFirst(), l->ccd.add(ImgGeometry.distance(l, centroid)));

		return ccd;
	}








	public List<double[]> toPolyLinePolar(){
		List<double[]> polar = new ArrayList<>();
		long[] ref = {getFirst()[0],getFirst()[1]};

		for(RidgePoint<T,V> l : left){
			long[] pol = {l.center()[0],l.center()[1]};
			pol[0]-=centroid[0];	pol[1]-=centroid[1];
			polar.add(new double[]{Math.sqrt(pol[0]*pol[0]+pol[1]*pol[1]), ImgGeometry.angle2PI(ref, pol)});
		}
		Iterator<RidgePoint<T,V>> it = reverseIterator(right);
		while(it.hasNext()){
			RidgePoint<T,V> l = it.next();
			long[] pol = {l.center()[0],l.center()[1]};
			pol[0]-=centroid[0];	pol[1]-=centroid[1];
			polar.add(new double[]{Math.sqrt(pol[0]*pol[0]+pol[1]*pol[1]), ImgGeometry.angle2PI(ref, pol)});
		}

		// Now add the midle of end points
		long[] f = getFirst();	long[] l = getLast();
		long[] pol = {(f[0]+l[0])/2, (f[1]+l[1])/2};
		pol[0]-=centroid[0];	pol[1]-=centroid[1];
		polar.add(new double[]{Math.sqrt(pol[0]*pol[0]+pol[1]*pol[1]), ImgGeometry.angle2PI(ref, pol)});
		return polar;
	}



	/** 
	 * @return	Same as {@link #toPolyLineCenter()} except that the first point is also the last.
	 */
	public List<long[]> toClosedPolyline(){
		List<long[]> polygon = toPolyLineCenter();
		polygon.add(getFirst());
		return polygon;
	}



	/**
	 * @return A List containing all the point in this ridge. NB the list starts and finishes with the starting point (polygon form)
	 * The points between start and end point are not included.
	 */
	public List<long[]> toClosedList(){
		List<long[]> polygon = new ArrayList<>();
		for(long[] l : this){
			polygon.add(l);
		}
		// Close the Path
		polygon.add(getFirst());
		return polygon;
	}



	public void drawBorder(){
		RandomAccess<T> access = left.get(0).trackAccess();
		drawBorder(access);
	}


	public void drawBorderAndArea(){
		drawBorderAndArea(left.get(0).trackAccess(), left.get(0).labelsAccess(), id());
	}


	public void drawBorder(T label){
		RandomAccess<T> access = left.get(0).trackAccess();
		drawBorder(access, label);
	}



	public void drawBorder(RandomAccess<T> access){
		final T id = left.get(0).id();
		drawBorder(access, id);
	}


	public void drawBorder(RandomAccess<T> access, T label){
		for(long[] l : this){
			access.setPosition(l);
			access.get().set(label);
		}
		ImgGeometry.drawLine(access, getFirst(), getLast(), label);
	}






	public void drawCurvature(RandomAccess<T> access){

		List<Float> curv = ImgGeometry.curvature(toChainCode(), 4);
		final T curvature = left.get(0).id().createVariable();
		Iterator<Float> it = curv.iterator();
		//int c = 0;
		for(long[] l : this){
			curvature.setReal((it.next()+3f)*10f);
			access.setPosition(l);
			access.get().set(curvature);
		}		

		ImgGeometry.linePath(getLast(), getFirst(), l->{
			curvature.setReal((it.next()+3f)*10f);
			access.setPosition(l);
			access.get().set(curvature);
		});


		//ImgGeometry.drawCircle(this.ancestor.center(), 3l, access, this.id());

	}




	public void drawBorderAndArea(RandomAccess<T> skeleton, RandomAccess<T> labels, T label) {

		// Draw the path
		RandomAccess<T> skelAccess = skeleton;
		List<long[]> polygon = new ArrayList<>();
		for(long[] l : this){
			polygon.add(l);
			skelAccess.setPosition(l);
			labels.setPosition(l);
			skelAccess.get().set(label);
			labels.get().set(label);
		}

		// Close the Path
		polygon.add(getFirst());
		ImgGeometry.drawLine(skelAccess, getFirst(), getLast(), label);
		ImgGeometry.drawLine(labels, getFirst(), getLast(), label);

		ImgGeometry.FillPolygon(polygon, (l1,l2)-> ImgGeometry.drawLine(labels, l1, l2, label));

		// TODO Finalise using opening

	}


	public void drawBorderAndArea(RandomAccess<T> skeleton, RandomAccess<T> labels, Function<Ridge,Double> feature, boolean curv) {

		final T label = left.get(0).id().createVariable();
		label.setReal(feature.apply(this));

		// Draw the path
		RandomAccess<T> skelAccess = skeleton;
		List<long[]> polygon = new ArrayList<>();
		for(long[] l : this){
			polygon.add(l);
			skelAccess.setPosition(l);
			labels.setPosition(l);
			skelAccess.get().set(label);
			labels.get().set(label);
		}

		// Close the Path
		polygon.add(getFirst());
		ImgGeometry.drawLine(skelAccess, getFirst(), getLast(), label);
		ImgGeometry.drawLine(labels, getFirst(), getLast(), label);

		ImgGeometry.FillPolygon(polygon, (l1,l2)-> ImgGeometry.drawLine(labels, l1, l2, label));

		if(curv)
			this.drawCurvature(skelAccess);

	}





	@Override
	public String toString(){

		String s = "";
		s+="\n	Ridge : "+id();
		s+="\n==================================";
		s+="\n Score : "+score;
		s+="\n==================================";
		s+="\n Border DMean Steered : "+ intFeatures.getBorderSteeredDistanceFromMean();
		s+="\n Inner IQR Channel : "+ intFeatures.getcInnerIQR();
		s+="\n==================================";
		s+="\n Area : "+area;
		s+="\n Area (ScanLine): "+getAreaScanLine();
		s+="\n Compactness : "+comp;
		s+="\n Radius : "+getRadius();
		s+="\n==================================";
		s+="\n"+curvFeatures.toString();
		s+="\n==================================";
		s+="\n"+intFeatures.toString();

		return s;

	}



	public static String[] getFeatureNames(){
		return new String[]{
				
				"Border DMean Steered ",
				"Border IQR Steered ",
			//	"Inner DMean Steered ",
			//	"Inner IQR Steered",
				"Border DMean Channel",
				"Border IQR Channel",
				"Inner DMean Channel ",
				"Inner IQR Channel",
				"Compactness",
				"Curvature Zeros",
				"Curvature non Zero",
				"Average Concave",
				"Average Convex",
		};
	}


	public double[] getFeatures(){
		return new double[]{
				intFeatures.getBorderSteeredDistanceFromMean(),
				intFeatures.getsBorderIQR(),
				//intFeatures.getInnerSteeredDistanceFromMean(),
				//intFeatures.getsInnerIQR(),
				intFeatures.getBorderChannelDistanceFromMean(),
				intFeatures.getcBorderIQR(),
				intFeatures.getInnerChannelDistanceFromMean(),
				intFeatures.getcInnerIQR(),
				comp,
				getZeroProportion(),
				curvFeatures.getAverageNonZero(),
				curvFeatures.concaveSum(),
				curvFeatures.convexSum(),			
		};
	}





}
