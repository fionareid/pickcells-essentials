package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

public class CurvatureFeatures {


	//private final int peaks;

	private double concaveSum;
	private double convexSum;
	private int zeroCount; 
	private int N;

	public CurvatureFeatures(List<Float> curv) {	

		
		
		for(float f : curv){
			if(f == 0f)
				zeroCount++;
			else if (f > 0){
				convexSum += f;
			}
			else{
				concaveSum += f;
			}
		}

		N = curv.size();



	}




	public double convexSum(){
		return convexSum;
	}


	public double getConvexAverage() {
		return (double)convexSum/(double)(N);
	}



	public double getConcaveAverage() {
		return (double)concaveSum/(double)(N);
	}
	
	


	public double concaveSum(){
		return concaveSum;
	}


	/**
	 * @return The number of pixels with a zero curvature
	 */
	public int zeroCount(){
		return zeroCount;
	}


	public int getN(){
		return N;
	}

	/**
	 * @return The average of the absolute curvature
	 */
	public double getAverage(){
		return (convexSum - concaveSum)/(double)N;
	}

	/*
	 * @return The average of the absolute curvature over the non zero curvature pixel
	 */
	public double getAverageNonZero() {
		return (convexSum - concaveSum)/(double)(N-zeroCount);
	}



	public String toString(){		
		String s = "Curvature Features"
				+" \n Size : "+N
				//+" \n Number of peaks : "+peaks
				//+" \n Sum of Peaks : "+sumOfPeaks
				+" \n Zero Count: "+ zeroCount
				+" \n Sum of Convex : "+convexSum
				+" \n Sum of Concave : "+concaveSum
				+" \n Average (Non Zero) : "+getAverageNonZero();
		return s;
	}



	












}
