package org.pickcellslab.pickcells.segmentation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.pickcellslab.pickcells.api.img.process.LabelledMorphology;
import org.pickcellslab.pickcells.api.img.process.NeighborhoodFunction;

import net.imglib2.Cursor;
import net.imglib2.FinalInterval;
import net.imglib2.Interval;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.algorithm.region.localneighborhood.AbstractNeighborhood;
import net.imglib2.algorithm.region.localneighborhood.RectangleNeighborhoodGPL;
import net.imglib2.outofbounds.OutOfBoundsConstantValueFactory;
import net.imglib2.outofbounds.OutOfBoundsFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.util.Intervals;
import net.imglib2.util.Util;
import net.imglib2.view.Views;

public class Volume<T extends RealType<T> & NativeType<T>, V extends VolumeSliceFeature, O> {



	private final T id;
	private final PostProcessSplit<V, O> splitType;
	private final List<RidgeArea<T>> list = new ArrayList<>();
	private V[] sFts;
	private final long[] bbMin; 
	private final long[] bbMax; 
	private double volume = 0;
	private boolean erased = false;





	Volume(T id, PostProcessSplit<V,O> splitType){

		this.id = id;
		this.splitType = splitType;

		bbMin = new long[3];	Arrays.fill(bbMin, Long.MAX_VALUE);
		bbMax = new long[3];	Arrays.fill(bbMax, Long.MIN_VALUE); 


	}



	T getColor() {
		return id;
	}


	/**
	 * Add the given area into this volume and set the color of the area to the color of this volume
	 * @param ra
	 */
	public void addArea(RidgeArea<T> ra) {
		volume+=ra.area();
		for(int i = 0; i<2; i++){
			bbMin[i] = Math.min(bbMin[i], ra.bbMin()[i]);
			bbMax[i] = Math.max(bbMax[i], ra.bbMax()[i]);
		}
		bbMin[2] = Math.min(ra.sliceLocation(), bbMin[2]);
		bbMax[2] = Math.max(ra.sliceLocation(), bbMax[2]);

		ra.setColor(id);
		list.add(ra);
	}



	public void addVolume(Volume<T,?,?> v2) {
		if(this!=v2){

			volume+=v2.volume;
			for(int i = 0; i<bbMin.length; i++){
				bbMin[i] = Math.min(bbMin[i], v2.bbMin[i]);
				bbMax[i] = Math.max(bbMax[i], v2.bbMax[i]);
			}
			list.addAll(v2.list);
			v2.list.forEach(ra->ra.setColor(id));			
		}		
	}


	public Pair<Volume<T,V,O>, Volume<T,V,O>> split(RidgeArea<T> r1, T id1, RidgeArea<T> r2, T id2){

		Volume<T,V,O> v1 = new Volume<>(id1, splitType);		Volume<T,V,O> v2 = new Volume<>(id2, splitType);
		Volume<T,V,O> current = v1;
		for(RidgeArea<T> ra : list){
			current.addArea(ra);
			if(ra == r1)
				current = v2;
		}

		return new Pair<>(v1,v2);		
	}






	public long[] bbMin(){
		return bbMin;
	}


	public long[] bbMax(){
		return bbMax;
	}

	public double volume() {
		return volume;
	}





	public void erase(RandomAccessibleInterval<T> labels){
		this.erased = true;
		if(this.volume!=0){
			//Add slices (zTips)
			bbMin[2]-=2;	bbMax[2]+=2;
			//Check bounds
			bbMin[2] = Math.max(bbMin[2], 0);		bbMax[2] = Math.min(bbMax[2], labels.dimension(2)-1);
			this.repaintVolume(labels, new FinalInterval(bbMin,bbMax), id, id.createVariable());
		}
	}



	public void applyLabelAndSmooth(RandomAccessibleInterval<T> labels, ATVOptions options){

		// *****************  Give the same color to every slice  **********************

		list.forEach(s->s.repaint(id, labels));		




		// Smooth with morphological filters
		if(options.isFinalize() && bbMax[2]-bbMin[2]+1>(options.getMaxJump()-1)*2){
			smooth(labels, id, options.getMaxJump()-1);
			//Correct bbMin and bbMax
			Cursor<T> c = Views.interval(labels, new FinalInterval(bbMin, bbMax)).localizingCursor();
			Arrays.fill(bbMin, Long.MAX_VALUE);		Arrays.fill(bbMax, 0);
			final long[] pos = new long[c.numDimensions()];
			while(c.hasNext()){
				c.fwd();
				c.localize(pos);
				if(c.get().equals(id)){
					for(int i=0; i<c.numDimensions(); i++){
						bbMin[i] = Math.min(bbMin[i], pos[i]);
						bbMax[i] = Math.max(bbMax[i], pos[i]);
					}
				}
			}
		}

		// Create Z-Tips
		createZTips(labels, id, 2);

		list.clear(); //won't be used anymore

	}





	public void updateFeatures(RandomAccessibleInterval<T> labels, RandomAccessibleInterval<T> grays, PopulationInfoCollector<V, O> collector){

		final RandomAccess<T> labelsAccess = Views.extendZero(labels).randomAccess(); 

		// Compute the new volume and slices features
		int first = (int)bbMin[2];
		int last = (int)bbMax[2];
		int initialDepth = last - first + 1;
		this.volume = 0;
		final long[] pos = new long[3];

		if(initialDepth > 4 && splitType != null){

			final long[] temp = new long[3];
			final RandomAccess<T> graysAccess = grays.randomAccess(new FinalInterval(bbMin, bbMax));	
			sFts = splitType.createSliceFeatureArray(initialDepth);

			for(int z = first, s = 0; z<=last; z++, s++){ 

				pos[2] = z;	temp[2] = z;

				for(int x = (int) bbMin[0]; x<bbMax[0]; x++){
					pos[0] = x;
					for(int y = (int) bbMin[1]; y<bbMax[1]; y++){
						pos[1] = y;
						labelsAccess.setPosition(pos);		
						if(labelsAccess.get().equals(id)){
							sFts[s].accept(labelsAccess, graysAccess, pos, temp, id);
						}
					}
				}// END of giving positions to SliceFeature

				// Notify slice feature that positions have been iterated through completely
				//if(!
				sFts[s].doneWithPositions(graysAccess, temp);
				//)
				//paint(labelsAccess, bbMin, bbMax, pos, id);
				this.volume += sFts[s].area();

			}// END of creating SliceFeatures


			splitType.completeInit(sFts);

			// Give features to collector
			for(int s=1; s<sFts.length-1; s++)
				collector.accept(sFts[s]);


		}
		else{

			//Simply compute volume
			for(int z = first; z<last; z++){ 
				pos[2] = z;
				long[] center = new long[3];	center[2] = z;
				for(int x = (int) bbMin[0]; x<bbMax[0]; x++){
					pos[0] = x;
					for(int y = (int) bbMin[1]; y<bbMax[1]; y++){
						pos[1] = y;
						labelsAccess.setPosition(pos);		
						if(labelsAccess.get().equals(id)){
							this.volume++;
						}
					}
				}
			}
		}


	}





	public <A extends RealType<A>> void split(RandomAccessibleInterval<T> labels, Iterator<T> gen, RandomAccessibleInterval<A> annotation, O populationInfo, ATVOptions options){

		if(sFts==null)
			return;

		List<Integer> ranking = splitType.rankedSplitCandidates(sFts, populationInfo);
		if(ranking == null)
			return;

		// Look for potential split points 
		if(ranking.size() == 1){
			// Get Volume on both sides
			double vol1 = getVolume(sFts, 0, ranking.get(0));
			if(options.getMinVolume()<vol1 && options.getMinVolume()<(volume-vol1)){
				repaintSplit((int)sFts[ranking.get(0)].centroid()[2], (int) bbMax[2]+2, labels, gen);

				if(annotation!=null)
					paintFrame(annotation.randomAccess(), sFts[ranking.get(0)].centroid()[2]); 
			}
		}
		else{
			//Get the 2 first ones and check the 3 possibilities
			int i1 = ranking.get(0);
			int i2 = ranking.get(1);
			if(i1>i2){
				int temp = i1;
				i1 = i2;
				i2 = temp;
			}

			double vol1 = getVolume(sFts, 0, i1);
			double vol2 = getVolume(sFts, i1+1, i2);
			double vol3 = volume - vol1 - vol2;
			if(options.getMinVolume()<vol1 && options.getMinVolume()<vol2 && options.getMinVolume()<vol3){
				repaintSplit((int)sFts[i1].centroid()[2], (int)sFts[i2].centroid()[2], labels, gen);
				repaintSplit((int)sFts[i2].centroid()[2], (int)bbMax[2]+2, labels, gen); //+2 due to ZTips
				if(annotation!=null){
					paintFrame(annotation.randomAccess(), sFts[i1].centroid()[2]); 
					paintFrame(annotation.randomAccess(), sFts[i2].centroid()[2]); 
				}
			}else if(options.getMinVolume()<vol1 + vol2 && options.getMinVolume()<vol3){
				repaintSplit((int)sFts[i2].centroid()[2], (int)bbMax[2]+2, labels, gen);
				if(annotation!=null)
					paintFrame(annotation.randomAccess(), sFts[i2].centroid()[2]); 
			}else if(options.getMinVolume()<vol1 && options.getMinVolume()<vol2 + vol3){
				repaintSplit((int)sFts[i1].centroid()[2], (int)bbMax[2]+2, labels, gen);
				if(annotation!=null)
					paintFrame(annotation.randomAccess(), sFts[i1].centroid()[2]); 
			}
		}
	}


	private void repaintSplit(int fromIndex, int toIndex, RandomAccessibleInterval<T> labels, Iterator<T> gen){
		long[] min = Arrays.copyOf(bbMin, 3);	long[] max = Arrays.copyOf(bbMax, 3);
		min[2] = fromIndex+1;	max[2] = Math.min(labels.dimension(2)-1, toIndex); 
		this.repaintVolume(labels, new FinalInterval(min, max), id, gen.next());
	}






	private double getVolume(V[] sFts,int fromIndex, int toIndex) {

		assert fromIndex>toIndex : "fromIndex larger than toIndex!";		

		double vol = 0;
		for(int i = fromIndex; i<=toIndex; i++)
			vol+=sFts[i].area();
		return vol;
	}



	private <A extends RealType<A>> void paintFrame(RandomAccess<A> ra, long l) {
		final A max = ra.get().createVariable();
		max.setReal(max.getMaxValue());
		long[] pos = new long[3];
		pos[1] = bbMin[1]; pos[2] = l;
		for(int x = (int)bbMin[0]; x<bbMax[0]; x++){
			pos[0] = x;
			ra.setPosition(pos);
			ra.get().set(max);			
		}
		pos[1] = bbMax[1]; 
		for(int x = (int)bbMin[0]; x<bbMax[0]; x++){
			pos[0] = x;
			ra.setPosition(pos);
			ra.get().set(max);			
		}
		pos[0] = bbMin[0]; 
		for(int x = (int)bbMin[1]; x<bbMax[1]; x++){
			pos[1] = x;
			ra.setPosition(pos);
			ra.get().set(max);			
		}
		pos[0] = bbMax[0]; 
		for(int x = (int)bbMin[1]; x<bbMax[1]; x++){
			pos[1] = x;
			ra.setPosition(pos);
			ra.get().set(max);			
		}
	}





	private void paint(RandomAccess<T> labelsAccess, long[] bbMin2, long[] bbMax2, long[] pos, T paint) {
		for(int x = (int) bbMin[0]; x<bbMax[0]; x++){
			pos[0] = x;
			for(int y = (int) bbMin[1]; y<bbMax[1]; y++){
				pos[1] = y;
				labelsAccess.setPosition(pos);
				labelsAccess.get().set(paint);
			}
		}
	}


	static final long[][] k = {
			{1,0},{1,1},{0,1},
			{-1,1},{-1,0},{-1,-1},
			{0,-1},{1,-1}
	};




	private void createZTips(RandomAccessibleInterval<T> labels, T paint, int max) {


		final long[] pos = new long[3];	
		final long[] w = new long[3];
		final long[] scan = new long[3];
		final long[] offset = new long[3];

		// Compute offset 
		long bbDif = bbMax[2]-bbMin[2];
		if(bbDif !=0){
			double[] c1 = list.get(0).centroid();	double[] c2 = list.get(list.size()-1).centroid();
			for(int i = 0; i<c1.length; i++)
				offset[i] = (long) (c1[i] - c2[i])/bbDif;
		}

		final RandomAccess<T> ra = labels.randomAccess();



		// Top slice
		int round = 0;
		while(round!=max && bbMin[2]>0){

			pos[2] = scan[2] = bbMin[2];		w[2] = bbMin[2]-1;

			int area = paintTip(ra, paint, pos, scan, w, offset);

			if(area>0)
				bbMin[2]--;

			if(area<25)
				round = max;
			else
				round++;

		}

		// Bottom slice
		round = 0;
		for(int i = 0; i<offset.length; i++)
			offset[i] = -offset[i];

		while(round!=max && bbMax[2]<labels.dimension(2)-1){

			pos[2] = scan[2] = bbMax[2];		w[2] = bbMax[2]+1;

			int area = paintTip(ra, paint, pos, scan, w, offset);

			if(area>0)
				bbMax[2]++;

			if(area<25)
				round = max;
			else
				round++;

		}

	}




	private int paintTip(RandomAccess<T> ra, T paint, long[] pos, long[] scan, long[] w, long[] offset){

		int area = 0;
		T zero = paint.createVariable();

		for(long x = bbMin[0]; x<bbMax[0]; x++){
			pos[0] = x;
			for(long y = bbMin[1]; y<bbMax[1]; y++){
				pos[1] = y;
				ra.setPosition(pos);
				if(ra.get().equals(paint)){
					boolean write = true;
					for(int i = 0; i<k.length; i++){
						scan[0] = pos[0]+k[i][0]+offset[0];		scan[1] = pos[1]+k[i][1]+offset[1];
						ra.setPosition(scan);
						if(!ra.get().equals(paint)){
							write = false;
							break;
						}
					}
					if(write){
						//System.out.println("Writing!");
						w[0] = pos[0]; 	w[1] = pos[1];
						ra.setPosition(w);
						if(ra.get().equals(zero)){
							ra.get().set(paint);
							area++;
						}
					}
				}
			}
		}//End of scan

		return area;		

	}




	private void repaintVolume(RandomAccessibleInterval<T> labels, Interval interval, T current, T novel){

		Cursor<T> cursor = Views.interval(Views.extendZero(labels), interval).cursor();
		while(cursor.hasNext()){
			T t = cursor.next();
			if(t.equals(current)){
				t.set(novel);
			}
		}
	}






	@Override
	public String toString(){
		return "Volume : "
				+ "\n Number of slice -> "+list.size()
				+ "\n Volume "+volume()
				+ "\n ID "+id
				+ "\n bbMin "+Arrays.toString(bbMin)
				+ "\n bbMax "+Arrays.toString(bbMax);
	}






	private void smooth(RandomAccessibleInterval<T> labels, T id, int zRadius){

		final Interval interval = Intervals.expand(new FinalInterval(bbMin,bbMax), zRadius+1);

		final OutOfBoundsFactory<T,RandomAccessibleInterval<T>> outOfBounds = new OutOfBoundsConstantValueFactory<>(id.createVariable());

		final RandomAccessibleInterval<T> input = Views.interval(Views.extendValue(labels,id.createVariable()),interval);		

		


		// First perform a closing operation in 3D
		final AbstractNeighborhood<T> xyN = new RectangleNeighborhoodGPL<>(input, outOfBounds);
		xyN.setSpan(new long[]{1,1, zRadius});
		
		final RandomAccessibleInterval<T> dilated = runFunction(input, outOfBounds, LabelledMorphology.dilate(id), xyN);
		xyN.updateSource(dilated);
		final RandomAccessibleInterval<T> closed = runFunction(dilated, outOfBounds, LabelledMorphology.erode(id), xyN);	
		
		// Then remove z sections which only span 1 slice
		final T zero = id.createVariable();
		final RandomAccessibleInterval< T > smoothed = Util.getArrayOrCellImgFactory( closed, zero).create( input, zero);
		final long[] pos = new long[smoothed.numDimensions()];
		final Cursor<T> cursorClosed = Views.iterable(closed).localizingCursor();
		final RandomAccess<T> raClosed = closed.randomAccess();
		final RandomAccess<T> raSmoothed = smoothed.randomAccess();
		
		while(cursorClosed.hasNext()){
			cursorClosed.fwd();
			if(cursorClosed.get().equals(id)){
				cursorClosed.localize(pos);
				pos[2]--; 	raClosed.setPosition(pos);
				if(raClosed.get().equals(id)){
					raSmoothed.setPosition(cursorClosed);
					raSmoothed.get().set(id);
				}
				else{
					pos[2]+=2;	raClosed.setPosition(pos);
					if(raClosed.get().equals(id)){
						raSmoothed.setPosition(cursorClosed);
						raSmoothed.get().set(id);
					}
				}
				
			}
			
		}
		
		
		//final RandomAccessibleInterval<T> eroded = runFunction(closed, outOfBounds, LabelledMorphology.erode(id), xyN);	
		//final RandomAccessibleInterval<T> opened = runFunction(eroded, outOfBounds, LabelledMorphology.dilate(id), xyN);	


		Cursor<T> cc = Views.flatIterable(smoothed).cursor();
		Cursor<T> ci = Views.flatIterable(input).cursor();
		while(cc.hasNext()){
			cc.fwd();
			ci.fwd();
			if(cc.get().equals(id))
				ci.get().set(id);
			else if(ci.get().equals(id))
				ci.get().set(zero);
		}
	}



	private RandomAccessibleInterval<T> runFunction(RandomAccessibleInterval<T> input , OutOfBoundsFactory<T,RandomAccessibleInterval<T>> outOfBounds, NeighborhoodFunction<T> f, AbstractNeighborhood<T> r){

		final RandomAccessibleInterval< T > out = Util.getArrayOrCellImgFactory( input, id.createVariable()).create( input, id.createVariable());


		Cursor<T> cursorIn = Views.flatIterable(input).cursor();
		Cursor<T> cursorOut = Views.flatIterable(out).cursor();
		while(cursorIn.hasNext()){
			cursorIn.fwd();
			cursorOut.fwd();
			r.setPosition(cursorIn);
			T result = f.apply(r, cursorIn.get());
			cursorOut.get().set(result);
		}

		return out;

	}



	public boolean wasErased() {
		return erased;
	}







}
