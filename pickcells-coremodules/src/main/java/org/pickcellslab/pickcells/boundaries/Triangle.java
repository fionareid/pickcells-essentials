package org.pickcellslab.pickcells.boundaries;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import net.imglib2.Cursor;
import net.imglib2.Dimensions;
import net.imglib2.algorithm.region.hypersphere.HyperSphere;
import net.imglib2.type.Type;

public class Triangle<T extends Type<T>> {

	private final long[] p;	
	private final T l;
	private final boolean[] border;

	final float[] a, b, c;

	Triangle( float[] a, float[] b, float[] c, T l){
		this.p = new long[a.length];
		this.a = a; this.b = b; this.c = c;
		this.l = l;
		border = new boolean[a.length];
		for(int i = 0; i<a.length; i++){
			this.p[i] = (long) a[i];//+(int)bbMin[i]; //p must be translated with respect to min bounding box
			border[i] = a[i] == b[i] && b[i] == c[i];
		}
	}	


	/**
	 * Tests whether all the summit of this triangle lie on one of the border of the provided Dimensions
	 * @param space
	 */
	boolean isBorder(Dimensions space){
		for(int i = 0; i<p.length; i++)
			if(border[i]){
				if(p[i] == 0) return true;
				if(p[i] == space.dimension(i)-1) return true;
			}
		return false;	
	}

	/**
	 * Finds the first element which is not identical to the label associated to this triangle
	 * within the given {@link HyperSphere}
	 * @param s An {@link HyperSphere} which can be moved to the location of this triangles summit to access neighbouring data
	 * @return The identified element
	 */
	public T otherLabel(HyperSphere<T> s){
		s.updateCenter(p);
		Cursor<T> cur = s.cursor();
		while(cur.hasNext()){
			cur.next();
			if(!cur.get().equals(l))
				return cur.get();
		}
		return l;
	}
	
	public T getLabel(){
		return l;
	}

	
	public double area(){		
		Point3f pA = new Point3f(a);
		Vector3f v1 = new Vector3f(b);
		v1.sub(pA);
		Vector3f v2 = new Vector3f(c);
		v2.sub(pA);
		v1.cross(v1,v2);
		return Math.abs(v1.length()/2d);
	}
	
	
	@Override
	public String toString(){
		return Arrays.toString(a)+" | "+Arrays.toString(b)+" | "+Arrays.toString(c);
	}

}
