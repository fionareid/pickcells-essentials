package org.pickcellslab.pickcells.boundaries;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;
import org.apache.log4j.Logger;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.MutableInt;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.modules.SessionConsumer;
import org.pickcellslab.foundationj.dbm.queries.StorageBox.BoxBuilder;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.foundationj.queryUI.swing.QueryableChoiceDialog;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.app.modules.Analysis;
import org.pickcellslab.pickcells.api.app.modules.AnalysisException;
import org.pickcellslab.pickcells.api.app.ui.UI;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactory;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;
import org.pickcellslab.pickcells.features.BasicComputer;
import org.pickcellslab.pickcells.features.EllipseComputer;

import net.imglib2.RandomAccess;
import net.imglib2.type.numeric.RealType;


@MetaInfServices(Module.class)
public class SegmentedObjectComparator extends AbstractAnalysis implements Analysis, SessionConsumer{

	private static Logger log = Logger.getLogger(SegmentedObjectComparator.class);

	private static float minOverlap = 0.05f;

	private static final AKey<Float> ovl = AKey.get("Overlap", Float.class);
	private static final AKey<Double> ji = AKey.get("Jaccard Index", Double.class);
	private static final AKey<Double> anisoDif = AKey.get("Anisotropy Difference", Double.class);
	private static final AKey<Double> volDif = AKey.get("Volume Difference", Double.class);
	private static final AKey<Double> pcaAngle = AKey.get("PCA1 Angle", Double.class);
	private static final AKey<Double> meanIntDis = AKey.get("Intensities Distance", Double.class);
	private static final AKey<Integer> neighD = AKey.get("Neighbourhood Difference", Integer.class);

	//Session reference
	private DataAccess<?,?> session;

	//Activation related Fields
	private boolean isActivable = true;


	private final ProviderFactoryFactory factory;


	public SegmentedObjectComparator(ProviderFactoryFactory factory){
		this.factory = factory;
	}


	@Override
	public void sessionClosed(DataAccess<?,?> source) {
		session = null;
	}




	@Override
	public void setSession(DataAccess<?,?> session) {
		this.session = session;
		session.addSessionListener(this);
	}




	@Override
	public void  launch() throws AnalysisException {


		setStep(0);


		//Check Requirements

		List<MetaClass> segs = Meta.get().classesExtending(SegmentedObject.class);

		segs = segs.stream().filter(
				mc->
				mc.hasKey(EllipseComputer.aniso) 
				&& mc.hasKey(EllipseComputer.pca1)
				&& mc.hasKey(BasicComputer.meanInt)
				&& mc.metaLinks().filter(ml -> ml.itemDeclaredType().contains(Links.ADJACENT_TO)).collect(Collectors.counting()) > 0
				)
				.collect(Collectors.toList());

		/*
		segs.forEach(mc->{
			System.out.println("Current MetaClass : " + mc.type());
			System.out.println("Has Aniso : " + mc.hasKey(EllipseComputer.aniso));
			System.out.println("Has PCA1 : " + mc.hasKey(EllipseComputer.pca1));
			System.out.println("Has Mean Int : " + mc.hasKey(BasicComputer.meanInt));
			System.out.println("Has Adjacency : " + (mc.metaLinks().filter(ml -> ml.type().contains(Links.ADJACENT_TO)).collect(Collectors.counting()) > 0));
		});
		 */

		if(segs.size()<2){
			JOptionPane.showMessageDialog(null, "At least 2 types of segmented objects are required for this module, \n"
					+ "in addition, EllipseComputer and BasicComputer must have been run.");
			return;
		}


		// Inputs

		QueryableChoiceDialog<MetaClass> sd = new QueryableChoiceDialog<>(segs,"Choose the sources", false);
		sd.setModal(true);
		sd.pack();
		sd.setLocationRelativeTo(null);
		sd.setVisible(true);

		if(sd.wasCancelled())
			return;

		MetaClass source = sd.getChoice().get();

		segs.remove(source);

		QueryableChoiceDialog<MetaClass> td = new QueryableChoiceDialog<>(segs,"Choose the targets", false);
		td.setModal(true);
		td.pack();
		td.setLocationRelativeTo(null);
		td.setVisible(true);

		if(td.wasCancelled())
			return;

		MetaClass target = td.getChoice().get();

		// Determine filters on nodes
		ExplicitPredicate<? super DataItem> srcP = P.none();
		ExplicitPredicate<? super DataItem> tgP = P.none();

		// Determine Links Adjacency types
		String srcAdjType = source.metaLinks().filter(ml->ml.itemDeclaredType().contains(Links.ADJACENT_TO)).findAny().get().itemDeclaredType();
		String tgAdjType = target.metaLinks().filter(ml->ml.itemDeclaredType().contains(Links.ADJACENT_TO)).findAny().get().itemDeclaredType();





		System.out.println("Src link types = "+srcAdjType);
		System.out.println("tg link types = "+tgAdjType);




		//Now run for each image
		Set<Image> images = null;
		try {
			images = session.queryFactory()
					.regenerate(Image.class)
					.toDepth(0).traverseAllLinks()
					.includeAllNodes().regenerateAllKeys().getAll()
					.getTargets(Image.class).collect(Collectors.toSet());
		} catch (Exception e) {
			throw new AnalysisException("Unable to read the database", e);
		}






		setStep(1);

		int c = 0;
		for(Image i : images){


			List<? extends NodeItem> sources;
			try {
				sources = session.queryFactory()
						.regenerate(Image.class)
						.toDepth(3)
						.traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
						.and(Links.CREATED_FROM, Direction.INCOMING)
						.and(srcAdjType, Direction.BOTH)
						.withEvaluations(P.isType(source.itemDeclaredType()).merge(Op.Bool.AND, srcP), Decision.INCLUDE_AND_CONTINUE, Decision.EXCLUDE_AND_CONTINUE)
						.lastEvaluation(P.isClass(LabelsImage.class), Decision.INCLUDE_AND_CONTINUE)
						.regenerateAllKeys()
						.doNotGetAll()
						.useFilter(P.keyTest(WritableDataItem.idKey, P.equalsTo(i.getAttribute(WritableDataItem.idKey).get())))
						.run()
						.getAllItemsFor(source.itemClass()).collect(Collectors.toList());
			} catch (Exception e1) {
				throw new AnalysisException("Error reading sources : "+source, e1);
			}




			if(sources.size() == 0){
				log.warn("No "+source.name()+" in "+i.toString());
				continue;
			}



			List<? extends WritableDataItem> targets;
			try {
				targets = session.queryFactory()
						.regenerate(Image.class)
						.toDepth(3)
						.traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
						.and(Links.CREATED_FROM, Direction.INCOMING)
						.and(tgAdjType, Direction.BOTH)
						.withEvaluations(P.isType(target.itemDeclaredType()).merge(Op.Bool.AND, tgP), Decision.INCLUDE_AND_CONTINUE, Decision.EXCLUDE_AND_CONTINUE)
						.lastEvaluation(P.isClass(LabelsImage.class), Decision.INCLUDE_AND_CONTINUE)
						.regenerateAllKeys()
						.doNotGetAll()
						.useFilter(P.keyTest(WritableDataItem.idKey, P.equalsTo(i.getAttribute(WritableDataItem.idKey).get())))
						.run()
						.getAllItemsFor(target.itemClass()).collect(Collectors.toList());
			} catch (Exception e1) {
				throw new AnalysisException("Error reading targets : "+target, e1);
			}



			if(targets.size() == 0){
				log.warn("No Structure in "+i.toString());
				continue;
			}


			boolean save = false;
			try{			
				save = doComparision((List)sources,(List)targets, minOverlap, !i.isZStack(), i.isTimeLapse(), srcAdjType, tgAdjType);
			}catch(InterruptedException | ExecutionException e){
				throw new AnalysisException("Error while reading images", e);
			}


			if(save){

				BoxBuilder box = session.queryFactory().store().add(((SegmentedObject) targets.get(0)).origin().get());

				try {
					box.run();
				} catch (DataAccessException e) {
					log.error("An error occured while saving the data to the database",e);
					continue;
				}

			}

			setProgress((float)++c/(float)images.size());
		}

		setStep(2);
	}







	private <T extends RealType<T>> boolean doComparision(List<SegmentedObject> srces, List<SegmentedObject> structs, float min, boolean is2D, boolean isTL, String srcAdjType, String tgAdjType) throws InterruptedException, ExecutionException{

		try{

			//Load the LabelsImage for the sources (if not ImageDot)
			ProviderFactory pf = factory.create(2);
			LabelsImage sourceLabels = srces.iterator().next().origin().get();
			pf.addToProduction(sourceLabels);	


			//Load the LabelsImage for the structures
			LabelsImage targetLabels = structs.iterator().next().origin().get();
			pf.addToProduction(targetLabels);


			SegmentationImageProvider<T> sp = pf.get(sourceLabels);
			sp.setKnownAssociatedData(Collections.singletonMap(0, srces));

			SegmentationImageProvider<T> tp = pf.get(targetLabels);
			tp.setKnownAssociatedData(Collections.singletonMap(0, structs));



			// First sort dots and structs by frame if time lapse
			Map<Integer,List<SegmentedObject>> sources;
			Map<Integer,List<SegmentedObject>> targets;
			if(isTL){
				sources = srces.stream().collect(Collectors.groupingBy(d->d.frame()));
				targets = structs.stream().collect(Collectors.groupingBy(d->d.frame()));
			}
			else{
				sources = new HashMap<>(1);	sources.put(0, srces);
				targets = new HashMap<>(1);	targets.put(0, structs);
			}


			final Image image = sourceLabels.origin();
			final double cal = image.pxWidth()*image.pxDepth()*image.pxHeight();

			for(int time : sources.keySet()){



				RandomAccess<? extends T> tA = tp.getFrame(time).randomAccess();

				// Now for each source : 
				// Get all the points of the segmented object and check the value in tA
				// Map each new value to a list of points
				// If the size of the list is above minOverlap create a new link

				for(SegmentedObject source : sources.get(time)){


					final MutableInt roiSize = new MutableInt();
					final Map<Float, MutableInt> overlaps = new HashMap<>();

					final Consumer<long[]> c = l->{
						roiSize.increment();
						tA.setPosition(l);
						float tgLabel = tA.get().getRealFloat();
						if(tgLabel != 0 && tp.getItem(tgLabel, time).isPresent()){
							MutableInt counter = overlaps.get(tgLabel);
							if(counter==null){
								counter = new MutableInt();
								overlaps.put(tgLabel, counter);
							}
							counter.increment();
						}
					};


					sp.processPoints(source.label().get(), time, c);


					// Now get the label in tA with the highest score
					if(overlaps.isEmpty())
						continue;

					Float winner = null;
					int score = 0;
					for(Entry<Float, MutableInt> i : overlaps.entrySet()){
						if(i.getValue().intValue()>score){
							score = i.getValue().intValue();
							winner = i.getKey();
						}
					}



					final int minSize = (int) (roiSize.intValue() * min);
					final RandomAccess<T> tRa = tp.getFrame(time).randomAccess();



					if(score>=minSize){

						final float tgLabel = winner;

						SegmentedObject target = tp.getItem(tgLabel, time).get();
						Link link = new DataLink("BEST_MATCH", source, target, true);
						link.setAttribute(ovl, (float)score/roiSize.floatValue());

						// Jaccard Index
						final MutableInt itrsc = new MutableInt();

						sp.processPoints(source.label().get(), time, l->{
							tRa.setPosition(l);
							if(tRa.get().getRealFloat() == tgLabel)
								itrsc.increment();
						});
						double itrV = itrsc.doubleValue() * cal;
						link.setAttribute(ji, itrV / (source.getAttribute(Keys.volume).get() + target.getAttribute(Keys.volume).get() - itrV));


						// Centroid distance
						double d = MathArrays.distance(
								link.source().getAttribute(Keys.centroid).get(), 
								link.target().getAttribute(Keys.centroid).get());							
						link.setAttribute(Keys.distance, d);

						// Anisotropy Difference
						double aD = link.source().getAttribute(EllipseComputer.aniso).get() - link.target().getAttribute(EllipseComputer.aniso).get();
						link.setAttribute(anisoDif, aD); //Math.abs(aD));

						// Volume Difference
						double volS = link.source().getAttribute(Keys.volume).get();
						double volT = link.target().getAttribute(Keys.volume).get();
						link.setAttribute(volDif, (volS-volT)/volT); //Math.abs(volS-volT)/volT);

						// Mean Intensities Distance
						double[] mIs = link.source().getAttribute(BasicComputer.meanInt).get();
						double[] mIt = link.target().getAttribute(BasicComputer.meanInt).get();
						link.setAttribute(meanIntDis, MathArrays.distance(mIs, mIt));

						// PCA1 Angle
						double[] pcas = link.source().getAttribute(EllipseComputer.pca1).get();
						double[] pcat = link.target().getAttribute(EllipseComputer.pca1).get();
						Vector3D vs = new Vector3D(pcas);
						Vector3D vt = new Vector3D(pcat);
						double angle = FastMath.toDegrees(Vector3D.angle(vs, vt));
						if(angle>90)
							angle = 180 - angle;
						link.setAttribute(pcaAngle, angle);

						// Number of Neighbours difference
						int ns = link.source().getDegree(Direction.BOTH, srcAdjType);
						int nt = link.target().getDegree(Direction.BOTH, tgAdjType);
						link.setAttribute(neighD, ns-nt); //Math.abs(ns-nt));
					}






				}

			}

		}catch(Exception e){
			UI.display("Error", "An error occured while identifying memberships", e, Level.WARNING);
			return false;
		}


		return true;
	}






	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}




	@Override
	public String name() {
		return "Segmentated Shape Comparator";
	}




	@Override
	public boolean isActive() {
		return isActivable;
	}






	@Override
	public Object[] categories() {
		return new String[]{"Associations"};
	}






	@Override
	public String description() {
		return "<HTML> A module which creates 'BEST MATCH' between segmented objects if their labels overlap by more than 50%."
				+ "<br> It will also compute key differences between the shapes"
				+ "</HTML>";
	}



	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/matching.png"));
	}


	@Override
	public Icon[] icons() {
		return new Icon[3];
	}


	@Override
	public String[] steps() {
		return new String[]{"Inputs","Finding Members","Done"};
	}




}
