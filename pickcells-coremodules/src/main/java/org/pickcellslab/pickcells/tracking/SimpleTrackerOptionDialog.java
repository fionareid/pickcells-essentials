package org.pickcellslab.pickcells.tracking;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.commons.math3.util.FastMath;
import org.pickcellslab.foundationj.datamodel.AKey.dType;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.Dimension;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.queries.actions.Actions;
import org.pickcellslab.foundationj.queryUI.swing.MultiDimensionChoice;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.img.geometry.ImgGeometry;

import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.extended.panel.GroupPanel;

@SuppressWarnings("serial")
public class SimpleTrackerOptionDialog extends JPanel {


	private final MetaClass mc;

	private final MultiDimensionChoice choices;

	private int allowedFrameJumps = 5;
	private int minBranchDuration = 8;
	private int minDivisionGap = 100;



	public SimpleTrackerOptionDialog(MetaClass mc) {

		assert mc!=null : "The provided MetaClass is null";

		this.mc = mc;


		final JLabel frameLabel = new JLabel("Max Number of Frame Jumps");

		final SpinnerNumberModel frameModel = new SpinnerNumberModel(allowedFrameJumps, 1, 10, 1);
		final JSpinner frameSpinner = new JSpinner(frameModel);
		frameSpinner.addChangeListener(l->allowedFrameJumps=frameModel.getNumber().intValue());


		final JLabel branchLabel = new JLabel("Min Branch Duration");

		final SpinnerNumberModel branchModel = new SpinnerNumberModel(minBranchDuration, 1, 10, 1);
		final JSpinner branchSpinner = new JSpinner(branchModel);
		branchSpinner.addChangeListener(l->minBranchDuration=branchModel.getNumber().intValue());



		final JLabel divLabel = new JLabel("Min Frames between Divisions");

		final SpinnerNumberModel divModel = new SpinnerNumberModel(minDivisionGap, 10, 1000, 1);
		final JSpinner divSpinner = new JSpinner(divModel);
		divSpinner.addChangeListener(l->minDivisionGap=divModel.getNumber().intValue());



		final JLabel dimLabel = new JLabel("Additional Dimensions for cost computation");
		dimLabel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

		final List<Dimension<?,?>> toChooseFrom = new ArrayList<>();
		mc.getReadables().filter(mr->mr.dataType()==dType.NUMERIC).forEach(mr->{
			for(int d = 0; d<mr.numDimensions(); d++){
				if(!(mr.name().startsWith("bbM")||mr.name().equals("centroid")||mr.name().startsWith("Location")))
					toChooseFrom.add(mr.getDimension(d));
			}
		});

		choices = new MultiDimensionChoice((List)toChooseFrom);



		//Layout
		this.setLayout(new VerticalFlowLayout());
		this.add(new GroupPanel(10, frameLabel, frameSpinner));
		this.add(new GroupPanel(10, branchLabel, branchSpinner));
		this.add(new GroupPanel(10, divLabel, divSpinner));
		this.add(dimLabel);
		this.add(choices);

	}




	public BiFunction<Link, Double, Float> getCostFunction(DataAccess<?,?> access, double maxDistance) throws DataAccessException {
		//Build the function
		final BiFunction<Link, Double, Float> costFunction = baseFunction();
		final List<Dimension<DataItem, ? extends Number>> list = (List)choices.get();

		if(choices.get().isEmpty())
			return costFunction;
		else {

			final List<Double> weights = new ArrayList<>();

			// For each dimension get a normalisation constant
			for(int d = 0; d<list.size(); d++){
				final Dimension<DataItem, ? extends Number> dim = list.get(d);
				final SummaryStatistics stats = mc.readData(access, Actions.summaryStats(dim), 50000);
				double w = maxDistance / (stats.getMax() - stats.getMin());
				System.out.println("Weight for "+dim+" = "+w);
				weights.add(w);
			}


			return (l,d) -> {

				final ImageLocated source = (ImageLocated) l.source();
				final ImageLocated target = (ImageLocated) l.target();

				float cost = costFunction.apply(l, d);

				for(int i = 0; i<list.size(); i++){
					final Dimension<DataItem, ? extends Number> dim = list.get(i);
					final float dif = FastMath.abs(dim.apply(target).floatValue() - dim.apply(source).floatValue());
					final double normDif = dif*weights.get(i);
					//System.out.println("normDif = "+normDif);
					cost+=normDif; //We need a normalization somewhere otherwise weight will depend on scale of values
				}

				return cost;
			};
		}
	}


	private  BiFunction<Link, Double, Float> baseFunction(){

		// Cost = > inter centroid distances weighted by bbox jaccard index
		return (l,d)->{

			final ImageLocated source = (ImageLocated) l.source();
			final ImageLocated target = (ImageLocated) l.target();

			return
					(float) (d * (1-
							ImgGeometry.boundingBoxJaccard(
									source.getAttribute(Keys.bbMin).get(),
									source.getAttribute(Keys.bbMax).get(),
									target.getAttribute(Keys.bbMin).get(),
									target.getAttribute(Keys.bbMax).get())
							));
		};

	}



	public int getAllowedFrameJumps() {
		return allowedFrameJumps;
	}


	public int getMinBranchDuration() {
		return minBranchDuration;
	}


	public int getMinDivisionGap() {
		return minDivisionGap;
	}


}
