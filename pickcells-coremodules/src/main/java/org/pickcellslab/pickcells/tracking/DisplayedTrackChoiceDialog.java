package org.pickcellslab.pickcells.tracking;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.queryUI.swing.MetaRenderer;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;

public class DisplayedTrackChoiceDialog extends JPanel{

	final JComboBox<Image> imageBox = new JComboBox<>();
	
	final JComboBox<MetaClass> typeBox = new JComboBox<>();
	
	
	public DisplayedTrackChoiceDialog(DataAccess<?, ?> access) {
		
		
		
		//Get all images
		try {
					
			access.queryFactory().regenerate(Image.class).toDepth(0).traverseAllLinks()
			.includeAllNodes().regenerateAllKeys().getAll().getAllTargets().forEach(i->{
				imageBox.addItem((Image) i);
			});
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Get all links with type TRACK
		Meta.get().getMetaLinks(Links.TRACK).forEach(ml->{
			typeBox.addItem(ml.source());
		});
		
		
		GridLayout basicLayout = new GridLayout(2,2);
		basicLayout.setHgap(10);	basicLayout.setVgap(10);
		this.setLayout(basicLayout);
		
		typeBox.setRenderer(new MetaRenderer());
		
		this.add(new JLabel("Image to load"));		this.add(imageBox);
		this.add(new JLabel("Tracked Object"));		this.add(typeBox);
		
	}

	public Image getChosenImage() {
		return (Image) imageBox.getSelectedItem();
	}

	public MetaClass getChosenImageLocatedType() {
		return (MetaClass) typeBox.getSelectedItem();
	}

}
