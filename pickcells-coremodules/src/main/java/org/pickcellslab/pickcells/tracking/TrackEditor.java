package org.pickcellslab.pickcells.tracking;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.foundationj.ui.FjUI;
import org.pickcellslab.pickcells.api.app.charts.DBViewFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.charts.ViewFactoryException;
import org.pickcellslab.pickcells.api.app.modules.DefaultUIDocument;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;
import org.pickcellslab.pickcells.api.app.ui.Icons;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.conventions.UsefulQueries;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.img.io.StaticImgIO;
import org.pickcellslab.pickcells.api.img.view.ImageDisplay;
import org.pickcellslab.pickcells.api.img.view.tracks.DefaultTrackContainer;
import org.pickcellslab.pickcells.api.img.view.tracks.TrackManager;
import org.pickcellslab.pickcells.api.img.view.tracks.TrackManagerControllers;
import org.pickcellslab.pickcells.api.img.view.tracks.TrackSelectionMode;

import net.imglib2.img.Img;


@MetaInfServices(Module.class)
public class TrackEditor implements DBViewFactory{


	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}


	@Override
	public String name() {
		return "Track Editor";
	}

	@Override
	public String description() {
		return "This module allows to visualize and edit tracking outputs";
	}

	@Override
	public ViewType getType() {
		return ViewType.Numerical;
	}

	@Override
	public UIDocument createView(DataAccess<?, ?> access) throws ViewFactoryException {

		final ImageDisplay display = ImageDisplay.createDisplay();

		// Create Controllers
		final JPanel toolBar = 
				DBViewHelp.newToolBar()
				.addButton(FjUI.dataTable_16(), "Select the Tracks to be loaded", l->{
					
					//Check that there are tracks in the database
					final List<MetaLink> links = Meta.get().getMetaLinks(Links.TRACK);
					if(links.isEmpty()){
						JOptionPane.showMessageDialog(null, "There are no tracks in the database");
						return;
					}						
					
					
					final DisplayedTrackChoiceDialog dialog = new DisplayedTrackChoiceDialog(access);
					int response = JOptionPane.showConfirmDialog(display.getView(), dialog, "Image and tracks to load", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
					if(response == JOptionPane.OK_OPTION){
						//TODO close previous image and manager
						
						//Load the color image
						final Image color = dialog.getChosenImage();
						
						Img colorImg = null;
						try {
							colorImg = StaticImgIO.open(color);
						
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
							return;
						}
						
						display.addImage(color.getMinimalInfo(), colorImg);
						
						
						final MetaClass imageLocatedType = dialog.getChosenImageLocatedType();
						
						RegeneratedItems ri;
						try {
						
							ri = UsefulQueries.imageToImageLocated(access, imageLocatedType, color.getAttribute(WritableDataItem.idKey).get(),
									Integer.MAX_VALUE, Links.TRACK, Direction.OUTGOING);
						
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return;
						}
						
						final Map<Integer, List<ImageLocated>> map = ri.getDependencies(ImageLocated.class).collect(Collectors.groupingBy(il->il.frame()));
						final DefaultTrackContainer dtc = new DefaultTrackContainer(map, color.getMinimalInfo(), access);
						final TrackManager mgr = new TrackManager(dtc);
						
						display.addAnnotationLayer("Tracks", mgr);
						
						TrackManagerControllers.createControls(mgr, display);
						
						//display.getMouseModel().setSelectionMode(SelectionMode.SINGLE);
						
					}
				})				
				.build();
		
		// Z visibility control
		final SpinnerNumberModel zModel = new SpinnerNumberModel(20, 0, 100, 1);
		final JSpinner zSpinner = new JSpinner(zModel);
		toolBar.add(new JLabel("    Z Visibility"));
		toolBar.add(zSpinner);
		zSpinner.addChangeListener(l-> {
			TrackManager mgr = (TrackManager) display.getAnnotationManager(0);
			if(mgr!=null){
				mgr.setZVisibilityDepth(zModel.getNumber().intValue());
			}
		});
		
		final SpinnerNumberModel tModel = new SpinnerNumberModel(4, 0, 100, 1);
		final JSpinner tSpinner = new JSpinner(tModel);
		toolBar.add(new JLabel("Time Visibility"));
		toolBar.add(tSpinner);
		tSpinner.addChangeListener(l-> {
			final TrackManager mgr = (TrackManager) display.getAnnotationManager(0);
			if(mgr!=null){
				mgr.setFrameVisibilityDepth(tModel.getNumber().intValue());
			}
		});
		
		
		final SpinnerNumberModel sModel = new SpinnerNumberModel(2, 1, 10, 1);
		final JSpinner sSpinner = new JSpinner(sModel);
		toolBar.add(new JLabel("Node Size"));
		toolBar.add(sSpinner);
		sSpinner.addChangeListener(l-> {
			final TrackManager mgr = (TrackManager) display.getAnnotationManager(0);
			if(mgr!=null){
				mgr.setNodeSize(sModel.getNumber().intValue());
			}
		});
		
		
		
		//Selection Mode
		toolBar.add(new JLabel("   Selection Mode"));
		final JComboBox<TrackSelectionMode> modeBox = new JComboBox<>(TrackSelectionMode.values());
		modeBox.setSelectedItem(TrackSelectionMode.LINKS);
		modeBox.addActionListener(l->((TrackManager) display.getAnnotationManager(0)).setTrackSelectionMode((TrackSelectionMode) modeBox.getSelectedItem()));
		
		toolBar.add(modeBox);
		
		
		//Save (Commit Changes button)
		final JButton saveBtn = new JButton(Icons.save_16());
		toolBar.add(saveBtn);
		saveBtn.addActionListener(l->((TrackManager) display.getAnnotationManager(0)).commitChanges());
		
		final JButton reportBtn = new JButton(FjUI.dataTable_16());
		toolBar.add(reportBtn);
		reportBtn.addActionListener(l->((TrackManager) display.getAnnotationManager(0)).createReport(access));
		
		
		//TODO movie button
		
		
		
		

		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(toolBar, BorderLayout.NORTH);
		panel.add(display.getView(), BorderLayout.CENTER);
		

		return new DefaultUIDocument(panel, name(), icon());
	}

	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/tracking.png"));
	}

	
	
	
	private void createReport(){
		
	}
	
}
