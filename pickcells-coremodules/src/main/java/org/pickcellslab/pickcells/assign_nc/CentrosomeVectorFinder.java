package org.pickcellslab.pickcells.assign_nc;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.stat.Frequency;
import org.apache.commons.math3.stat.descriptive.moment.VectorialCovariance;
import org.apache.commons.math3.stat.descriptive.moment.VectorialMean;
import org.apache.commons.math3.util.MathArrays;
import org.apache.commons.math3.util.Pair;
import org.apache.log4j.Logger;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.predicates.ExplicitPredicate;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.DefaultTraversal;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.meta.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.modules.SessionConsumer;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.foundationj.optim.algorithms.HungarianAlgorithm;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.app.modules.Analysis;
import org.pickcellslab.pickcells.api.app.ui.UI;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.conventions.UsefulQueries;
import org.pickcellslab.pickcells.api.datamodel.types.Centrosome;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageDot;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.Nucleus;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.geometry.bvh.BVH;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactory;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;

import net.imglib2.Cursor;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.Views;

/**
 * <HTML> 
 * This module automatically associates nulcei with centrosomes
 * 			<br>It will try to maximize the number of cells with one or 2 centrosomes
 *				<br>however, it will allow nuclei to have more than 2 centrosomes, 
 *				<br>see <i>Marthiens et al, JCS, 2012</i> for a review on centrosome amplification.
 *				<br>Finally, a basic classification of the cells is performed and will identify
 *				<br>mitotic versus interphase cells versus cells with centrosome amplification.
 *				</HTML>
 * @author Guillaume Blin
 *
 */
@MetaInfServices(Module.class)
public class CentrosomeVectorFinder<T extends RealType<T> & NativeType<T>> extends AbstractAnalysis implements Analysis, SessionConsumer, MetaModelListener {

	private Logger log = Logger.getLogger(CentrosomeVectorFinder.class);

	static final String adjacency = "All "+Links.ADJACENT_TO;

	static final AKey<String> phase = AKey.get("Cycle Phase",String.class);
	static final AKey<String> caseKey = AKey.get("Centriole Association",String.class);
	static final AKey<Double> costKey = AKey.get("Assignment Cost",Double.class);
	static final AKey<Integer> visitKey = AKey.get("Visits",Integer.class);

	static final String interphase = "Interphase";

	private DataAccess<?,?> access;

	//Flags to check whether the module is activable or not
	private boolean isActivable = true;
	private boolean hasCentrosomes;
	private boolean hasNuclei;

	private boolean hasNeighborhood;

	
	private final int clusterSize = 5000;
	
	private final ProviderFactoryFactory pff;



	public CentrosomeVectorFinder(ProviderFactoryFactory pff) {
		this.pff = pff;
	}



	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}

	@Override
	public String name() {
		return "Nucleus/Centrosome";
	}

	@Override
	public String description() {
		return "<HTML> This module automatically associates nuclei with centrosomes"
				+ "<br>It will try to maximize the number of cells with one or 2 centrosomes"
				+ "<br>however, it will allow nuclei to have more than 2 centrosomes, "
				+ "<br>see <i>Marthiens et al, JCS, 2012</i> for a review on centrosome amplification."
				+ "<br>Finally, a basic classification of the cells is performed and will identify"
				+ "<br>mitotic versus interphase cells versus cells with centrosome amplification."
				+ "</HTML>";
	}


	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void launch() {

		try{


			//User inputs
			MetaClass nuc = Meta.get().getMetaModel("Nucleus");
			MetaClass cent = Meta.get().getMetaModel("Centrosome");



			FilterChoiceDialog dialog = new FilterChoiceDialog(nuc,cent);
			dialog.setModal(true);
			dialog.pack();
			dialog.setLocationRelativeTo(null);
			dialog.setVisible(true);

			if(dialog.wasCancelled())	return;

			float maxCC = dialog.maxCC();
			float maxCE = dialog.maxCE();
			float percentage = dialog.percentage();

			ExplicitPredicate<NodeItem> nf = null;
			ExplicitPredicate<NodeItem> cf = null;

			MetaFilter mNF = dialog.nucleusFilter(); 
			MetaFilter mCF = dialog.centFilter();

			if(null != mNF)
				nf = P.isClass(Nucleus.class).merge(Op.Bool.AND, mNF.toFilter().negate());
			else
				nf = (ExplicitPredicate)P.none().negate();
			if(null != mCF)
				cf =  P.isClass(Centrosome.class).merge(Op.Bool.AND, mCF.toFilter().negate());
			else
				cf = (ExplicitPredicate)P.none().negate();








			//Delete all the associated links between nucleus and centrosomes

			try {

				log.info("Number of deleted associations: "+
						access.queryFactory().deleteLinks(Links.ASSOCIATED)
						.completely().useFilter(P.sourcePasses(P.isClass(Nucleus.class))).run()
						);

				//TODO remove debugging version
				access.queryFactory().deleteLinks(NCSteps.POTENTIAL).completely().getAll().run();
				access.queryFactory().deleteLinks("INIT").completely().getAll().run();


			} catch (DataAccessException e1) {
				log.error("Unable to delete ASSOCIATED links",e1);
				return;
			}







			//Get Images with their segmentations in order to obtain nuclei meshes and labels
			List<Image> images = 	UsefulQueries.imagesWithSegmentations(access);

			ProviderFactory factory = pff.create(2);



			for(Image i : images){

				for(int t = 0; t<i.frames(); t++){

					log.info("Processing image "+i.toString());

					int dimensionality = i.depth() == 1 ? 2 : 3;


					//Start the production of the segmented image
					factory.addToProduction(i.getSegmentation("Nucleus").get());

					//Get a reference to the actual img
					SegmentationImageProvider<T> provider = factory.get(i.getSegmentation("Nucleus").get());
					
					

					// Get the full scene for this image
					RegeneratedItems scene = 
							access.queryFactory().regenerate(Image.class)
							.toDepth(3)
							.traverseLink(Links.COMPUTED_FROM, Direction.INCOMING) //SegmentationResult
							.and(Links.CREATED_FROM, Direction.INCOMING)//Nuclei
							.and(Centrosome.origin, Direction.INCOMING)//Centrosomes
							.and(adjacency,Direction.BOTH) //Neighbours
							.and("MITOSIS", Direction.BOTH) //Mitoses
							.withEvaluations(nf, Decision.EXCLUDE_AND_CONTINUE, Decision.INCLUDE_AND_CONTINUE)
							.lastEvaluation(cf, Decision.EXCLUDE_AND_CONTINUE)						
							.includeOneKey(WritableDataItem.idKey)
							.doNotGetAll()
							.useFilter(P.keyTest(Keys.name, P.stringEquals(i.getAttribute(Keys.name).get())))
							.run();






					//Get the nuclei
					List<Nucleus> nuclei = scene.getDependencies(Nucleus.class).collect(Collectors.toList());
					log.info("Number of nuclei : "+ nuclei.size());
					if(nuclei.size() == 0)
						continue;

					nuclei.forEach(n->n.setAttribute(phase,interphase));

					//Get the centrosomes
					List<Centrosome> cents = scene.getDependencies(Centrosome.class).collect(Collectors.toList());
					if(cents.size() == 0)
						continue;
					log.info("Number of centrosomes : "+cents.size());

					//Get the identified mitoses links
					Set<Link> mitoses = new HashSet<>();
					cents.stream().forEach(c->c.getLinks(Direction.INCOMING, "MITOSIS").forEach(mitoses::add));
					log.info("Number of mitoses : "+mitoses.size());






					//============================================================================================================================
					//Identify Metaphases


					log.info("Linking mitotic spots to the mitotic nuclei...");

					
					provider.setKnownAssociatedData(t, nuclei);

					Img<T> img = provider.provide();
					T zero = img.firstElement().createVariable();

					for(Link l : mitoses){

						//Create the bounding box of the link
						long[] min = new long[dimensionality];
						long[] max = new long[dimensionality];

						long[] src = l.source().getAttribute(Keys.location).get();
						long[] trg = l.target().getAttribute(Keys.location).get();

						for(int d = 0; d<dimensionality; d++){
							if(src[d]<trg[d]){
								min[d] = src[d];
								max[d] = trg[d];
							}
							else{
								min[d] = trg[d];
								max[d] = src[d];
							}
						}

						//Check in the image which label is the most abundant
						Frequency freq = new Frequency();
						Cursor<T> cursor = Views.interval(img, min, max).cursor();
						while(cursor.hasNext()){
							cursor.next();
							freq.addValue(cursor.get().copy());
						}

						T highest = zero.createVariable();
						T nextHighest = zero.createVariable();

						Iterator<T> list = (Iterator) freq.getMode().iterator();

						highest = list.next();
						if(highest.equals(zero) && list.hasNext())
							highest = list.next();

						if(list.hasNext())
							nextHighest = list.next();
						if(nextHighest.equals(zero) && list.hasNext())
							nextHighest = list.next();

						//Set the "Cycle Phase" string
						if(!highest.equals(zero)){
							if(nextHighest.equals(zero) || freq.getCount(highest) > freq.getCount(nextHighest)+0.1*freq.getCount(highest)){
								//Get the object for this label
								SegmentedObject so = provider.getItem(highest.getRealFloat(), t).get();
								//Set as Metaphase
								so.setAttribute(phase, "Metaphase");
								//Link to centrosomes
								new DataLink(Links.ASSOCIATED, so, l.source(),  true);
								new DataLink(Links.ASSOCIATED, so, l.target(),  true);
								//Remove from the objects to process later on
								cents.remove(l.source());
								cents.remove(l.target());
								nuclei.remove(so);
								//System.out.println("Metaphase found!");
							}
							else{//We have an anaphase
								//Get the object for these labels
								SegmentedObject s1 = provider.getItem(highest.getRealFloat(), t).get();							
								SegmentedObject s2 = provider.getItem(nextHighest.getRealFloat(), t).get();
								//Set as Anaphase
								s1.setAttribute(phase, "Anaphase");
								s2.setAttribute(phase, "Anaphase");
								//Link to centrosomes
								double d1 = MathArrays.distance(s1.getPoint(), l.source().getAttribute(Keys.centroid).get());
								double d2 = MathArrays.distance(s1.getPoint(), l.target().getAttribute(Keys.centroid).get());
								if(d1<d2){
									new DataLink(Links.ASSOCIATED, s1, l.source(),  true);
									new DataLink(Links.ASSOCIATED, s2, l.target(),  true);
								}else{
									new DataLink(Links.ASSOCIATED, s2, l.source(),  true);
									new DataLink(Links.ASSOCIATED, s1, l.target(),  true);
								}	

								cents.remove(l.source());
								cents.remove(l.target());
								nuclei.remove(s1);
								nuclei.remove(s2);
								//System.out.println("Anaphase found!");
							}
						}
						else{
							//System.out.println("***********************************");
							//System.out.println("Mitosis without a nucleus found!");
							//System.out.println("Min : "+Arrays.toString(min));
							//System.out.println("Max : "+Arrays.toString(max));
							//for(Comparable<?> c : freq.getMode())
							//	System.out.println("label : "+c.toString());
						}
					}

















					//Now resolve associations
					//-----------------------------------------------------------------------------------------------------

					long start = System.currentTimeMillis();


					// - Identify nuclei / centrosomes close to the image border which have more chance not to be assigned a centrosome
					NCSteps.checkImageBorder(i, nuclei, maxCC);
					NCSteps.checkImageDotBorder(i, cents, maxCC);



					// - find clusters of centrosomes , max = 2 microns appart
					double max = 1.5; //TODO dynamic

					List<BVH<List<ImageLocated>>> clustered = NCSteps.getClusters(cents,max);



					// - Create the list of non clustered centrosome as BVH as well
					final Function<ImageLocated,float[]> floatCentroid = (o)->{
						double[] c = o.centroid();
						float[] result = new float[c.length];
						for(int j = 0; j<c.length;j++)
							result[j] = (float)c[j];
						return result;
					};
					List<BVH<List<ImageLocated>>> singles = 
							cents.stream()
							.filter(p->!p.getAttribute(NCSteps.coloringA).isPresent())
							.map(s->BVH.create(s,floatCentroid)).collect(Collectors.toList());



					long s1 = System.currentTimeMillis();
					System.out.println("Centrioles clusters : "+(s1-start)+" ms");
					System.out.println("Singles : "+singles.size());
					System.out.println("Clustered : "+clustered.size());



					if(nuclei.size() != (singles.size()+clustered.size()))
						if(nuclei.size() > (singles.size()+clustered.size())){ // if too many nuclei compared to centrioles
							System.out.println("Not enough centrioles!");
							Pair<List<BVH<List<ImageLocated>>>,List<BVH<List<ImageLocated>>>> p = NCSteps.breakLargeClusters(clustered);
							singles.addAll(p.getFirst());
							clustered = p.getSecond();
							if(nuclei.size() > (singles.size()+clustered.size())){
								NCSteps.removeCloseToBorderCentrioles(clustered); // This directly modifies the passed list
							}
						}
					// if too many centrioles -> hungarian algorithm will select the 'best' ones ...





					// Now the graph of potential links
					List<BVH<List<ImageLocated>>> centrioles = new ArrayList<>(singles);
					centrioles.addAll(clustered);

					System.out.println("Initial size of centrioles -> "+centrioles.size());

					NCSteps.createLinks(centrioles, nuclei, maxCC, maxCE, percentage, provider);

					System.out.println("Links created ");

					// provider resources can now be released
					provider = null;
					factory.clearProduction();


					// Check the size of our sample
					int size = Math.max(centrioles.size(), nuclei.size());


					
					if(size>clusterSize){ 


						log.info(" Assignment size is large ("+size+"), now looking for subgraphs");
						TraverserConstraints tc = 
								Traversers.newConstraints().fromMinDepth().toMaxDepth()
								.doNotTraverseLink(ImageDot.origin, Direction.OUTGOING).asWellAs(Links.CREATED_FROM, Direction.OUTGOING)
								.includeAllNodes();

						List<DefaultTraversal<NodeItem, Link>> subgraphs = Traversers.disconnected(nuclei, tc);


						//Check sizes of each subgraphs if subgraphs are still too large perform clustering 
						//(been thinking about Markov clustering but memory requirements is also a problem).

						for(DefaultTraversal<NodeItem, Link> tr : subgraphs){
							if(tr.nodes().size()/2 > clusterSize){ //is this ok to just estimate by /2 (centrosomes and nuclei)

								Collection<Set<NodeItem>> clusters = performClustering(tr, clusterSize);
								clusters.forEach(c->optimize(c,maxCC));

							}
							else
								optimize(tr.nodes(),maxCC);
						}



					}
					else{
						if(nuclei.size()>0 && centrioles.size()>0)
							optimize(nuclei, centrioles, maxCC);
					}






					//Finally store
					//------------------------------------------------------------------------------------------------------

					TraverserConstraints tc = 
							Traversers.newConstraints().fromMinDepth().toMaxDepth()
							.doNotTraverseLink(NCSteps.POTENTIAL, Direction.BOTH) // Do not store potential links
							.includeAllNodes();

					//Write the classification to the database
					access.queryFactory().store().add(scene.getOneTarget(Image.class).get(),tc)
					.feed(Keys.vector, new double[3], "NCFinder", "DIRECTIONAL : A normalised vector "
							+ "representing the orientation of the cell polarity", Nucleus.class, "Nucleus")
					.run();


				}// END of loop around time frames

			}//END of loop around images

		}
		catch(DataAccessException e){
			UI.display("Database Error", "An error occured while accessing the database, check the log", e, Level.WARNING);
			return;

		} catch (Exception e) {
			UI.display("Error", "An error occured while running Nuclei / Centrosomes Associations, check the log", e, Level.WARNING);
			return;
		}

	}








	private Collection<Set<NodeItem>> performClustering(DefaultTraversal<NodeItem, Link> tr, int size) {

		log.info(" Clustering subgraph of size"+tr.nodes().size());

		LinkedList<Link> queue = new LinkedList<>(tr.edges());		
		Collections.sort( queue, (l1,l2) -> (
				Double.compare(
						(l1.getAttribute(NCSteps.dCC).get()- l1.getAttribute(NCSteps.dCE).get()),
						(l2.getAttribute(NCSteps.dCC).get() - l2.getAttribute(NCSteps.dCE).get())
						))
				);

		// Keep track of volumes
		final Map<Integer, Set<NodeItem>> volumes = new HashMap<>();

		final Iterator<Integer> gen = IntStream.range(1, tr.nodes().size()).iterator();

		while(!queue.isEmpty()){

			Link l = queue.pollLast();
			NodeItem source = l.source();	Optional<Integer> sourceColor = source.getAttribute(NCSteps.coloringB);
			NodeItem target = l.target();	Optional<Integer> targetColor = target.getAttribute(NCSteps.coloringB);

			//System.out.println("Source -> "+sourceColor);
			//System.out.println("Target -> "+targetColor);

			if(sourceColor.isPresent()){
				if(targetColor.isPresent()){ // if both have different colors
					mergeOrCreateBoundary(l, sourceColor.get(), targetColor.get(), volumes, size);

				}
				else{ // if only the source has a color
					propagateOrCreateBoundary(l, source, volumes.get(sourceColor.get()), target, volumes, gen, size);
				}
			}
			else if(targetColor.isPresent()){// if only the target has a color
				propagateOrCreateBoundary(l, target, volumes.get(targetColor.get()), source, volumes, gen, size);
			}
			else{ // if none have color
				createAndAddToMap(source, target, gen, volumes);
			}

		}
		
		
		log.info(" Clustering done!");

		return volumes.values();

	}











	private void createAndAddToMap(NodeItem source, NodeItem target, Iterator<Integer> gen,
			Map<Integer, Set<NodeItem>> volumes) {

		int i = gen.next();
		source.setAttribute(NCSteps.coloringB, i);
		target.setAttribute(NCSteps.coloringB, i);
		Set<NodeItem> set = new HashSet<>();
		set.add(source);
		set.add(target);
		volumes.put(i, set);

		//System.out.println("Created and added to map "+i);

	}



	private void propagateOrCreateBoundary(Link l, NodeItem hasColor, Set<NodeItem> set, NodeItem toAdd, Map<Integer, Set<NodeItem>> volumes, Iterator<Integer> gen, int size) {


		if(set.size()+1 <= size){
			toAdd.setAttribute(NCSteps.coloringB, hasColor.getAttribute(NCSteps.coloringB).get());
			set.add(toAdd);
			//System.out.println("1 added to set "+hasColor.getAttribute(NCSteps.coloringB).get());
		}
		else{
			int i = gen.next();
			Set<NodeItem> newSet = new HashSet<>();
			toAdd.setAttribute(NCSteps.coloringB, i);
			newSet.add(toAdd);
			volumes.put(i,newSet);
			l.delete();
			//System.out.println("1 as boundary "+hasColor.getAttribute(NCSteps.coloringB).get()+" - "+i);

		}

	}






	private void mergeOrCreateBoundary(Link l, int set1ID, int set2ID, Map<Integer, Set<NodeItem>> volumes,	int size) {

		if(set1ID != set2ID){

			Set<NodeItem> set1 = volumes.get(set1ID);
			Set<NodeItem> set2 = volumes.get(set2ID); 

			//if(set1==null)
				//System.out.println("set1 is null");
			//if(set2==null)
				//System.out.println("set2 is null");

			if(set1.size()+set2.size() <= size){
				// Merge
				set2.forEach(n->n.setAttribute(NCSteps.coloringB, set1ID));
				set1.addAll(set2);
				volumes.remove(set2ID);

				//System.out.println("Merged "+set1ID+" - "+set2ID);

			}// else -> do nothing, this create a boundary.
			else{
				l.delete();
				//System.out.println("Boundary! ");
			}
		}
	}





	private void optimize(Set<NodeItem> set, float maxCC) {


		final Function<ImageLocated,float[]> floatCentroid = (o)->{
			double[] c = o.centroid();
			float[] result = new float[c.length];
			for(int i = 0; i<c.length;i++)
				result[i] = (float)c[i];
			return result;
		};

		final List<Nucleus> nuclei = new ArrayList<>();
		final List<BVH<List<ImageLocated>>> centrosomes = new ArrayList<>();
		final Map<Float, List<ImageLocated>> map = new HashMap<>();
		set.forEach(n->{
			if(n instanceof Nucleus)
				nuclei.add((Nucleus) n);
			else{

				Optional<Float> color = n.getAttribute(NCSteps.coloringA);
				if(color.isPresent()){
					List<ImageLocated> l = map.get(color.get());
					if(null == l){
						l = new ArrayList<>();
						map.put(color.get(), l);
					}
					l.add((ImageLocated) n);
				}
				else
					centrosomes.add(BVH.create((ImageLocated)n, floatCentroid));

			}
		});


		map.values().forEach(l->centrosomes.add(BVH.create(l, floatCentroid)));

		if(nuclei.size()>0 && centrosomes.size()>0)
			optimize(nuclei, centrosomes, maxCC);

	}





	private void optimize(List<Nucleus> nuclei, List<BVH<List<ImageLocated>>> centrioles, float maxCC){


		// - Create the cost matrix
		float[][] costMatrix = NCSteps.createCosts(centrioles, nuclei);

		System.out.println("Cost Matrix created!");


		long s2 = System.currentTimeMillis();



		// - Find the optimum assignment
		HungarianAlgorithm algorithm = new HungarianAlgorithm(costMatrix);
		int[] assignment = algorithm.execute();



		long s3 = System.currentTimeMillis();
		System.out.println("Assignment time : "+(s3-s2)+" ms");

		Function<BVH<List<ImageLocated>>, Boolean> clusteringStatus = b->b.size()>1;
		Map<Boolean, List<BVH<List<ImageLocated>>>> allCents = centrioles.stream().collect(Collectors.groupingBy(clusteringStatus));

		//Make sure both entries exist
		if(allCents.get(false) == null)
			allCents.put(false, Collections.emptyList());
		if(allCents.get(true) == null)
			allCents.put(true, Collections.emptyList());


		System.out.println("Size of clustered before split -> "+allCents.size());

		// - Now filter assignments (remove long associations) and find lone nuclei
		boolean change = false;
		Map<BVH<List<ImageLocated>>, Double> tensions = new HashMap<>();				
		for(int n = 0; n<nuclei.size(); n++){
			int index = assignment[n]; // index in centrioles
			if(index== -1 || costMatrix[n][index]>maxCC){ // lone nucleus!
				if(NCSteps.breakNeighbourClusterWithHighestTension(nuclei.get(n), allCents.get(true), tensions))
					change = true;
			}
		}


		System.out.println("Change : "+change);

		// If anything has changed then create a new assignment.
		if(change){

			System.out.println("Size of clustered after split -> "+allCents.get(true).size());

			centrioles = new ArrayList<>(allCents.get(false));
			centrioles.addAll(allCents.get(true));

			System.out.println("Size of centrioles after split -> "+centrioles.size());


			costMatrix = NCSteps.createCosts(centrioles, nuclei);
			algorithm = new HungarianAlgorithm(costMatrix);
			assignment = algorithm.execute();
		}




		// - Translate indices into actual Links 
		for(int n = 0; n<nuclei.size(); n++){
			int index = assignment[n]; // index in centrioles
			if(index!=-1 && costMatrix[n][index] <= maxCC){
				final Nucleus ncl = nuclei.get(n);
				final List<Link> potentials = ncl.getLinks(Direction.OUTGOING, NCSteps.POTENTIAL).collect(Collectors.toList());
				for(ImageLocated dot : centrioles.get(index).getUserObject()){
					Link l = new DataLink(Links.ASSOCIATED, nuclei.get(n), dot, true);
					Link toRm = null;
					for(Link p : potentials){
						if(p.target() == dot){
							toRm = p;
							break;
						}							
					}

					potentials.remove(toRm);
					if(toRm!=null){
						l.setAttribute(Keys.sourceAnchor, toRm.getAttribute(Keys.sourceAnchor).get());
						l.setAttribute(NCSteps.dCC, toRm.getAttribute(NCSteps.dCC).get());	
						l.setAttribute(NCSteps.dCE, toRm.getAttribute(NCSteps.dCE).get());	
						toRm.delete();
					}
					else
						l.setAttribute(NCSteps.dCC, (float)costMatrix[n][index]);

				}
			}
			//else
			//	System.out.println("Nucleus unassigned...");
		}


		costMatrix = null;


		// - Compute the coordinates of vectors for each nucleus

		for(NodeItem n : nuclei){
			double[] centroid = n.getAttribute(AKey.get("Centroid",double[].class)).get();

			//For each vector, find the mean vector and the angle of uncertainty
			VectorialMean mean = new VectorialMean(3);
			VectorialCovariance cov = new VectorialCovariance(3,true);
			n.getLinks(Direction.OUTGOING, Links.ASSOCIATED)
			.forEach(l->{
				double[] loc = ((ImageDot) l.target()).centroid();
				Vector3D v = new Vector3D(new double[]{loc[0]-centroid[0],loc[1]-centroid[1],loc[2]-centroid[2]});
				v = v.normalize();
				mean.increment(v.toArray());	
				cov.increment(v.toArray());
			});

			Vector3D norm = new Vector3D(mean.getResult());
			norm = norm.normalize();
			n.setAttribute(Keys.vector,norm.toArray());
			//TODO RealMatrix m = cov.getResult();


		}		
	}





	@Override
	public boolean isActive() {
		return isActivable;
	}




	@Override
	public Object[] categories() {
		return new String[]{"Associations"};
	}

	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/arrow.png"));
	}



	@Override
	public void sessionClosed(DataAccess<?,?> source) {
		access = null;
	}




	@Override
	public void setSession(DataAccess<?,?> session) {
		this.access = session;
		session.addSessionListener(this);

		MetaClass mc = Meta.get().getMetaModel("Nucleus");
		if(null != mc){
			hasNuclei = true;
			hasNeighborhood = mc.metaLinks(adjacency, Direction.BOTH).findAny().isPresent();
		}
		mc = Meta.get().getMetaModel("Centrosome");
		if(null!=mc)
			hasCentrosomes = true;

		checkActivable();
	}





	private void checkActivable() {
		isActivable = 
				hasNuclei &&
				hasCentrosomes &&
				hasNeighborhood;	
		this.fireIsNowActive();
	}

	@Override
	public Icon[] icons() {
		return new Icon[1];
	}





	@Override
	public String[] steps() {
		return new String[]{"Running"};
	}








	@Override
	public void metaEvent(RegeneratedItems meta, MetaChange evt) {

		/*

		//Check classes and links
		Set<MetaQueryable> mq = meta.getAllTargets().stream()
				.filter(m->MetaQueryable.class.isAssignableFrom(m.getClass()))
				.map(m->(MetaQueryable)m)
				.collect(Collectors.toSet());

		if(evt == MetaChange.CREATED){
			if(!hasNuclei)
				hasNuclei = mq.stream().anyMatch(m->m.itemType().equals(Nucleus.class.getSimpleName()));
			if(!hasCentrosomes)
				hasCentrosomes = mq.stream().anyMatch(m->m.itemType().equals(Centrosome.class.getSimpleName()));
			if(!hasNeighborhood)
				hasNeighborhood = mq.stream().anyMatch(m->m.itemType().equals(Links.ADJACENT_TO));
		}else{
			if(hasNuclei)
				hasNuclei = !mq.stream().anyMatch(m->m.itemType().equals(Nucleus.class.getSimpleName()));
			if(hasCentrosomes)
				hasCentrosomes = !mq.stream().anyMatch(m->m.itemType().equals(Centrosome.class.getSimpleName()));
			if(hasNeighborhood)
				hasNeighborhood = !mq.stream().anyMatch(m->m.itemType().equals(Links.ADJACENT_TO));
		}

		//Check mesh
		if(hasNuclei){
			if(evt == MetaChange.CREATED){
				if(!hasMesh)
					hasMesh = meta.getAllTargets().stream()
					.filter(m->MetaKey.class.isAssignableFrom(m.getClass()))
					.map(m->(MetaKey<?>)m).anyMatch(k->k.toAKey()==Keys.mesh 
					&& k.owner().itemType().equals(Nucleus.class.getSimpleName()));
			}else{
				if(hasMesh)
					hasMesh = !meta.getAllTargets().stream()
					.filter(m->MetaKey.class.isAssignableFrom(m.getClass()))
					.map(m->(MetaKey<?>)m).anyMatch(k->k.toAKey()==Keys.mesh 
					&& k.owner().itemType().equals(Nucleus.class.getSimpleName()));
			}
		}

		checkActivable();
		 */
	}












}
