package org.pickcellslab.pickcells.assign_nc;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.DoubleAdder;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathArrays;
import org.apache.commons.math3.util.Pair;
import org.apache.log4j.Logger;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataLink;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.tools.Decision;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Centrosome;
import org.pickcellslab.pickcells.api.datamodel.types.ClusterableAdapter;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageDot;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.Nucleus;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.geometry.bvh.BVH;
import org.pickcellslab.pickcells.api.geometry.bvh.BVTTNode;
import org.pickcellslab.pickcells.api.geometry.bvh.ClosestMeshFinder;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;

public abstract class NCSteps {


	private static final Logger log = Logger.getLogger(NCSteps.class);


	public static final AKey<double[]> centroid = AKey.get("Centroid", double[].class);

	/**
	 * The type of links for potential assignments
	 */
	public static final String POTENTIAL = "POTENTIAL";
	/**
	 * The type of link between clustered centrosomes
	 */
	public static final String TEMP = "TEMP";
	/**
	 * The first assignment type
	 */
	public static final String INIT = "INIT";
	/**
	 * The color of the centrosome cluster a centrosome belongs to
	 */
	public static final AKey<Float> coloringA = AKey.get("Coloring A", Float.class);
	/**
	 * The color of a centrosome / nuclei cluster a nodeitem belongs to
	 */
	public static final AKey<Integer> coloringB = AKey.get("Coloring B", Integer.class);
	/**
	 * The distance between a centrosome and a nucleus centroid
	 */
	public static final AKey<Float> dCC = AKey.get("dCC", Float.class);
	/**
	 * The distance between the centroid of the nucleus and the anchor point of the centrosome 
	 * (barycenter of the n% closest points of the nucleus edge to the centrosome)
	 */
	public static final AKey<Float> dCE = AKey.get("dCE", Float.class);
	/**
	 * The probability of having no assignment due to the proximity to the image border(s)
	 */
	public static final AKey<Float> pNA = AKey.get("pNA", Float.class);








	/**
	 * Identifies clusters of centrosomes and assign a unique color to each cluster. 
	 * @param max 
	 * @return 
	 * @return A Map of cluster color mapped to the centroid of the cluster
	 */
	static List<BVH<List<ImageLocated>>> getClusters(List<? extends ImageDot> items, double max){

		final Function<ImageLocated,double[]> rawCentroid = (o)->o.centroid();
		final Function<ImageLocated,float[]> floatCentroid = (o)->{
			double[] c = o.centroid();
			float[] result = new float[c.length];
			for(int i = 0; i<c.length;i++)
				result[i] = (float)c[i];
			return result;
		};

		// Cluster centrioles
		List<ClusterableAdapter<ImageLocated>> list = items.stream()
				.map(s->new ClusterableAdapter<ImageLocated>(s,rawCentroid))
				.collect(Collectors.toList());

		DBSCANClusterer<ClusterableAdapter<ImageLocated>> clusterer = new DBSCANClusterer<>(max,1);
		List<Cluster<ClusterableAdapter<ImageLocated>>> clusters = clusterer.cluster(list);


		// Translate clusters as BVH and add cluster color to the underlying ImageDot

		int id = 0;
		List<BVH<List<ImageLocated>>> clustered = new ArrayList<>();

		for(Cluster<ClusterableAdapter<ImageLocated>> c : clusters){	

			// Label the centriole with the color of the cluster
			final float f = id++;
			c.getPoints().forEach(p->p.object().setAttribute(coloringA,f));

			// Translate the cluster to a BVH and add to the list
			List<ImageLocated> l = c.getPoints().stream().map(p->p.object()).collect(Collectors.toList());
			clustered.add(BVH.create(l,floatCentroid));
		}


		return clustered;		
	}



	//TODO test
	/**
	 * Split clusters until their sizes falls below 3;
	 * @param clustered
	 * @return 
	 */
	static Pair<List<BVH<List<ImageLocated>>>,List<BVH<List<ImageLocated>>>> breakLargeClusters(List<BVH<List<ImageLocated>>> clustered){

		final List<BVH<List<ImageLocated>>> singles = new ArrayList<>();
		final List<BVH<List<ImageLocated>>> broken = new ArrayList<>();

		final AtomicInteger i = new AtomicInteger();
		final Consumer<BVH<List<ImageLocated>>> labels = (c)-> {
			if(c.size() == 1){
				c.getUserObject().get(0).removeAttribute(coloringA);
				singles.add(c);
			}
			else{
				c.getUserObject().forEach(p->p.setAttribute(coloringA,(float)i.incrementAndGet()));
				broken.add(c);
			}
		};


		for(BVH<List<ImageLocated>> bvh : clustered){
			bvh.crumbs(labels, 2);
		}


		return new Pair<>(singles,broken);
	}




	static void removeCloseToBorderCentrioles(List<BVH<List<ImageLocated>>> clustered){

		Iterator<BVH<List<ImageLocated>>> it = clustered.iterator();
		while(it.hasNext()){
			BVH<List<ImageLocated>> bvh = it.next();
			int beforeSize = bvh.size();
			removeCloseToBorder(bvh.getUserObject());
			if(bvh.size()<beforeSize)
				it.remove();
		}

	}


	static void removeCloseToBorder(List<? extends ImageLocated> clustered){

		Iterator<? extends ImageLocated> it = clustered.iterator();
		while(it.hasNext()){
			ImageLocated test = it.next();
			double p = test.getAttribute(pNA).get();
			if(p > 0.5)
				if(p > Math.random())
					it.remove();
		}

	}




	static boolean breakNeighbourClusterWithHighestTension(Nucleus n, List<BVH<List<ImageLocated>>> clusters, Map<BVH<List<ImageLocated>>, Double> tensions){


		// First get all the centriole in a cluster within the neighborhood		
		TraverserConstraints tc = 
				Traversers.newConstraints().fromDepth(1).toDepth(8)
				.traverseLink(POTENTIAL, Direction.BOTH)
				.withOneEvaluation(P.hasKey(coloringA), Decision.INCLUDE_AND_CONTINUE, Decision.EXCLUDE_AND_CONTINUE);


		Set<NodeItem> set = Traversers.breadthfirst(n,tc).traverse().nodes(); 

		System.out.println("Number of clustered within the neighborhood : "+set.size());



		// Now compute the "tension" fo each cluster (probability to be a cluster when it should not)
		// -> lowest std
		// And keep the bvh with lowest
		BVH<List<ImageLocated>> lowest = null;
		double l = Double.MAX_VALUE;
		SummaryStatistics stats = new SummaryStatistics();
		for(BVH<List<ImageLocated>> bvh : clusters){

			if(set.contains(bvh.getUserObject().get(0))){ // within neighbours clusters

				stats.clear();

				System.out.println("Found -> Cluster in neighborhood");
				if(bvh.getUserObject().size()<=1)

					log.warn("NCSteps -> Found cluster with <= 1 centriole! ");


				if(tensions.get(bvh) == null){ // Compute if not already in cache
					double t = 0;
					bvh.getUserObject().get(0).getLinks(Direction.INCOMING, POTENTIAL).forEach(p->
						stats.addValue(p.getAttribute(NCSteps.dCC).get() - p.getAttribute(NCSteps.dCE).get())
					);
					t = stats.getMean();
					System.out.println("Tension -> "+t);
					tensions.put(bvh, t);
					if(t<l){
						l = t;
						lowest = bvh; 
					}
				}
				else{
					double t = tensions.get(bvh);
					if(t<l){
						l = t;
						lowest = bvh; 
					}
				}
			}
		}


		if(lowest != null){
			System.out.println("Lowest removed --> " + clusters.remove(lowest));
			tensions.remove(lowest);

			Pair<BVH<List<ImageLocated>>,BVH<List<ImageLocated>>> pair = lowest.split();
			if(pair.getFirst() !=null)
				clusters.add(pair.getFirst());
			if(pair.getSecond() != null)
				clusters.add(pair.getSecond());
		}

		return lowest != null;
	}






	static List<BVH<List<ImageLocated>>> normalizeToNuclei(List<BVH<List<ImageLocated>>> singles, List<BVH<List<ImageLocated>>> clustered,
			List<Nucleus> nuclei){

		//TODO



		List<BVH<List<ImageLocated>>> result = new ArrayList<>(singles);
		result.addAll(clustered);

		return result;
	}





	static float[][] createCostsAndLinks(List<BVH<List<ImageLocated>>> centrioles, List<Nucleus> nuclei, float maxCC, float maxCE, float percentage, SegmentationImageProvider<?> provider, boolean b) {

		// Init the matrix with maximum costs
		float[][] matrix = new float[nuclei.size()][centrioles.size()];

		System.out.println("matrix initialised");

		//for(int i = 0; i<centrioles.size(); i++)
		//	Arrays.fill(matrix[i], 0, matrix[i].length, 200); 

		//Initialise the graph
		final List<BVH<Nucleus>> cache = new ArrayList<>();
		nuclei.forEach(n->{// IMPORTANT keep same order
			List<float[]> sorted = provider.getMesh((SegmentedObject) n, 2, false, true);
			if(sorted.isEmpty()){
				double[] centroid = n.centroid();
				float[] center = new float[centroid.length];
				for(int i = 0; i<centroid.length; i++)
					center[i] = (float) centroid[i];
				sorted.add(center);
			}
			cache.add(new BVH<>(sorted, n));
		});



		DoubleAdder min = new DoubleAdder();
		min.add(Double.MAX_VALUE);

		IntStream.range(0, nuclei.size()).forEach(n->{
			IntStream.range(0, centrioles.size()).forEach(c->{		


				final BVH<List<ImageLocated>> ctl = centrioles.get(c);
				final BVH<Nucleus> ncl = cache.get(n);

				//First get all centrosome which distance from nucleus centroid is <= maxCC
				float dCC = distance(ctl.center(), ncl.getUserObject().centroid());

				matrix[n][c] = dCC;

				if(dCC<=maxCC){

					//find the anchor (closest edge point from centrosome)
					//and check that distance between centrosome and anchor is <= maxCE

					double[] anchor = edgeAnchor(ctl, ncl, percentage);
					//convert to float[]
					float[] a = new float[anchor.length];
					for(int i = 0; i<anchor.length; i++)
						a[i] = (float) anchor[i];

					float dsE = distance(ctl.center(),anchor);

					if(dsE<=maxCE){

						float dCE = (float) MathArrays.distance(ncl.getUserObject().getAttribute(centroid).get(), anchor);

						//create the link and store distances
						if(b)
							for(ImageLocated dot : ctl.getUserObject()){
								Link l = new DataLink(POTENTIAL, ncl.getUserObject(), dot, true);
								l.setAttribute(Keys.sourceAnchor, a);
								l.setAttribute(NCSteps.dCC, dCC);							
								l.setAttribute(NCSteps.dCE, dCE);
							}


						matrix[n][c] = dCC - dCE;

					}					
				}

				if(min.doubleValue()>matrix[n][c]){
					min.reset();
					min.add(matrix[n][c]);
				}

			});
		});

		//Make sure all costs are positive
		if(min.doubleValue()<0){

			double toAdd = - min.doubleValue();

			for(int i = 0; i<matrix.length; i++)
				for(int j = 0; j<matrix[i].length; j++)
					matrix[i][j] += toAdd;

		}

		return matrix;
	}







	static float createLinks(List<BVH<List<ImageLocated>>> centrioles, List<Nucleus> nuclei, float maxCC, float maxCE, float percentage, SegmentationImageProvider<?> provider) {




		//Initialise the graph
		final List<BVH<Nucleus>> cache = new ArrayList<>();
		nuclei.forEach(n->{// IMPORTANT keep same order
			List<float[]> sorted = provider.getMesh((SegmentedObject) n, 2, false, true);
			if(sorted.isEmpty()){
				double[] centroid = n.centroid();
				float[] center = new float[centroid.length];
				for(int i = 0; i<centroid.length; i++)
					center[i] = (float) centroid[i];
				sorted.add(center);
			}
			cache.add(new BVH<>(sorted, n));
		});

		System.out.println("Nuclei Cache done! ");


		DoubleAdder min = new DoubleAdder();
		min.add(Double.MAX_VALUE);

		IntStream.range(0, nuclei.size()).forEach(n->{
			IntStream.range(0, centrioles.size()).forEach(c->{		


				final BVH<List<ImageLocated>> ctl = centrioles.get(c);
				final BVH<Nucleus> ncl = cache.get(n);

				//First get all centrosome which distance from nucleus centroid is <= maxCC
				float dCC = distance(ctl.center(), ncl.getUserObject().centroid());

				float entry = dCC;

				if(dCC<=maxCC){
					//find the anchor (closest edge point from centrosome)
					//and check that distance between centrosome and anchor is <= maxCE

					double[] anchor = edgeAnchor(ctl, ncl, percentage);
					//convert to float[]
					float[] a = new float[anchor.length];
					for(int i = 0; i<anchor.length; i++)
						a[i] = (float) anchor[i];

					float dsE = distance(ctl.center(),anchor);

					if(dsE<=maxCE){

						float dCE = (float) MathArrays.distance(ncl.getUserObject().getAttribute(centroid).get(), anchor);

						//create the link and store distances

						for(ImageLocated dot : ctl.getUserObject()){
							Link l = new DataLink(POTENTIAL, ncl.getUserObject(), dot, true);
							l.setAttribute(Keys.sourceAnchor, a);
							l.setAttribute(NCSteps.dCC, dCC);							
							l.setAttribute(NCSteps.dCE, dCE);
						}
						entry = dCC -dCE;
					}					
				}

				if(min.doubleValue()>entry){
					min.reset();
					min.add(entry);
				}

			});
		});


		return min.floatValue();
	}














	private static float distance(float[] p1, double[] p2) {
		double sum = 0;
		for (int i = 0; i < p1.length; i++) {
			final double dp = p1[i] - p2[i];
			sum += dp * dp;
		}
		return (float) FastMath.sqrt(sum);
	}





	static void assignUniqueSolutions(List<Nucleus> nuclei, List<Centrosome> centrioles){
		//Now check if there are nuclei with only one possibility and remove from potential graph
		List<Nucleus> toLock = new ArrayList<>();
		List<Centrosome> toLock2 = new ArrayList<>();
		for(Nucleus n : nuclei){
			int nPossible = n.getDegree(Direction.INCOMING, POTENTIAL);
			if(nPossible == 0){
				toLock.add(n);
			}else if(nPossible == 1){
				Link pot = n.getLinks(Direction.INCOMING, POTENTIAL).iterator().next();
				NodeItem source = pot.source();
				source.getLinks(Direction.OUTGOING, POTENTIAL).forEach(l->l.delete());
				new DataLink(Links.ASSOCIATED,n,source,true);
			}
		}
		for(Centrosome n : centrioles){
			if(n.getDegree(Direction.INCOMING, Links.ASSOCIATED)>0)
				toLock2.add(n);
			else if(n.getDegree(Direction.OUTGOING, POTENTIAL) == 0){
				toLock2.add(n);
			}
		}


		toLock.forEach(n->nuclei.remove(n));
		toLock2.forEach(c->centrioles.remove(c));
	}









	static void checkImageBorder(Image img, Collection<? extends SegmentedObject> items, float max){
		if(items.isEmpty()) return;

		int w = (int) img.width();
		int h = (int) img.height();

		for(NodeItem n : items){

			float pNA1 = 0; float pNA2 = 0;

			double[] c = n.getAttribute(centroid).get();			
			float dTop = (float) c[1];
			float dLeft = (float) c[0];
			float dBottom = (float) (h-c[1]);
			float dRight = (float) (w-c[0]);
			if(dTop<=max)
				pNA1 = 0.5f*(1-dTop/max);
			else if(dBottom<=max)
				pNA1 = 0.5f*(1-dBottom/max);
			if(dLeft<=max)
				pNA2 = 0.5f*(1-dLeft/max);
			else if(dRight<=max)
				pNA2 = 0.5f*(1-dRight/max);

			float p = 0;
			if(pNA1>0){ 
				p = pNA1 + pNA1*pNA2;
			}else if(pNA2>0){
				p = pNA2;
			}
			n.setAttribute(pNA,p);
		}
	}


	static void checkImageDotBorder(Image img, Collection<? extends ImageDot> items, float max){
		if(items.isEmpty()) return;

		int w = (int) img.width();
		int h = (int) img.height();

		for(ImageDot n : items){

			float pNA1 = 0; float pNA2 = 0;

			double[] c = n.centroid();			
			float dTop = (float) c[1];
			float dLeft = (float) c[0];
			float dBottom = (float) (h-c[1]);
			float dRight = (float) (w-c[0]);
			if(dTop<=max)
				pNA1 = 0.5f*(1-dTop/max);
			else if(dBottom<=max)
				pNA1 = 0.5f*(1-dBottom/max);
			if(dLeft<=max)
				pNA2 = 0.5f*(1-dLeft/max);
			else if(dRight<=max)
				pNA2 = 0.5f*(1-dRight/max);

			float p = 0;
			if(pNA1>0){ 
				p = pNA1 + pNA1*pNA2;
			}else if(pNA2>0){
				p = pNA2;
			}

			n.setAttribute(pNA,p);
		}
	}








	private static double[] edgeAnchor(BVH<List<ImageLocated>> dotBvh, BVH<Nucleus> bvh, float percentage){

		//Get the required percentage of the closest edge points and compute the centroid
		double[] edgeAnchor = new double[dotBvh.center().length];
		List<BVTTNode<List<ImageLocated>, Nucleus>> list = ClosestMeshFinder.closestPoints(dotBvh, bvh, percentage, 500);
		for(BVTTNode<List<ImageLocated>, Nucleus> node : list)
			for(int i = 0; i<dotBvh.center().length; i++){
				edgeAnchor[i]+=node.source().center()[i];
				edgeAnchor[i]+=node.target().center()[i];
			}


		for(int i = 0; i<dotBvh.center().length; i++)
			edgeAnchor[i] /= (double)(list.size()*2);

		return edgeAnchor;

	}



	public static float[][] createCosts(List<BVH<List<ImageLocated>>> centrioles, List<Nucleus> nuclei) {

		// Store in each nucleus its index in the list
		AKey<Integer> listId = AKey.get("listId",Integer.class);

		for(int n = 0; n<nuclei.size(); n++)
			nuclei.get(n).setAttribute(listId, n);

		// Init the matrix with maximum costs
		float[][] matrix = new float[nuclei.size()][centrioles.size()];

		System.out.println("matrix initialised");

		// Fill the matrix with max cost
		for(int i = 0; i<nuclei.size(); i++)
			Arrays.fill(matrix[i], 0, matrix[i].length, 5000000f); 
		
		
		//Now for each centriole assign the cost in the matrix
		
		// Keep track of the minimum cost
		DoubleAdder min = new DoubleAdder();
		min.add(Double.MAX_VALUE);
		
		
		for(int c = 0; c<centrioles.size(); c++){//can be parallized
			Iterator<Link> links = centrioles.get(c).getUserObject().get(0).getLinks(Direction.INCOMING, POTENTIAL).iterator();
			
			while(links.hasNext()){
				Link l = links.next();
				NodeItem nucleus = l.source();
				int n = nucleus.getAttribute(listId).get();
				matrix[n][c] = l.getAttribute(dCC).get() - l.getAttribute(dCE).get();
				
				if(min.doubleValue()>matrix[n][c]){
					min.reset();
					min.add(matrix[n][c]);
				}
			}
		}
		
		// Erase ids in nuclei
		for(int n = 0; n<nuclei.size(); n++)
			nuclei.get(n).removeAttribute(listId);
		
		
		//Make sure all costs are positive
		if(min.doubleValue()<0){

			double toAdd = - min.doubleValue();

			for(int i = 0; i<matrix.length; i++)
				for(int j = 0; j<matrix[i].length; j++)
					matrix[i][j] += toAdd;

		}

		return matrix;


	}















}
