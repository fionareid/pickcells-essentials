package org.pickcellslab.pickcells.picking;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jdesktop.swingx.JXTable;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.DataPointer;
import org.pickcellslab.foundationj.dbm.meta.Dimension;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.meta.MetaReadable;
import org.pickcellslab.foundationj.dbm.modules.SessionConsumer;
import org.pickcellslab.foundationj.dbm.queries.ReadTable;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;
import org.pickcellslab.foundationj.dbm.queries.actions.Actions;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.pickcells.api.app.picking.SeriesPickConsumer;
import org.pickcellslab.pickcells.api.app.ui.UI;

import com.alee.laf.filechooser.WebFileChooser;
import com.alee.managers.language.data.TooltipWay;
import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotification;
import com.alee.managers.tooltip.TooltipManager;


@MetaInfServices(Module.class)
public class TableSeriesPicker implements SeriesPickConsumer, SessionConsumer {


	private DataAccess<?, ?> access;


	@Override
	public void seriesPicked(String seriesId, DataPointer v) {

		//null check
		Objects.requireNonNull(seriesId,"SeriesId is null");
		Objects.requireNonNull(v,"vignette is null");

		ReadTable model = null;


		try{

			model = v.readData(access, Actions.table(getDimensions(v)), 50);


		}catch(Exception e){
			UI.display("Error", "An exception occured while getting info for object in series "+seriesId, e, Level.WARNING);
			return;
		}


		if(model!=null){

			JFrame f = new JFrame(seriesId+" (preview)");

			JXTable table = new JXTable();			
			table.setModel(model);

			JScrollPane scroll = new JScrollPane(table);			
			table.setColumnControlVisible(true);
			table.setHorizontalScrollEnabled(true);
			table.packAll();

			JPanel panel = new JPanel();
			panel.setLayout(new BorderLayout());			
			panel.add(scroll,BorderLayout.CENTER);

			JPanel options = new JPanel();
			options.setLayout(new FlowLayout(FlowLayout.RIGHT));

			JButton close = new JButton("Close");
			close.addActionListener(l->	f.dispose()	);
			options.add(close);

			JButton save = new JButton("Save");
			TooltipManager.addTooltip(save, "Export to .txt", TooltipWay.left, 0);
			save.addActionListener(l->{

				File file = WebFileChooser.showSaveDialog();
				if(file == null)
					return;				


				Thread t = new Thread(()->{
					try {
						v.readData(access, new ToFileAction(getDimensions(v), file), -1);
					} catch (Exception e1) {
						UI.display("Error", "An exception occured while getting info for object in series "+seriesId, e1, Level.WARNING);
						return;
					}


				});

				UI.waitAnimation(t,"Saving");

				try {
					t.join();
				} catch (Exception e) {
					JOptionPane.showMessageDialog(table, "Could not save to file", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}

				
				WebNotification wn = NotificationManager.showNotification(seriesId+" Saved!");
				wn.setDisplayTime(1000);


			});
			options.add(save);

			panel.add(options, BorderLayout.SOUTH);

			f.setContentPane(panel);
			f.pack();
			f.setLocationRelativeTo(null);
			f.setVisible(true);

		}


	}
	
	
	private List<Dimension<DataItem,?>> getDimensions(DataPointer v){
		
		MetaQueryable mq =  v.queriedMetaObjects().get(0);
		List<MetaReadable<?>> mrs = mq.getReadables().collect(Collectors.toList());

		System.out.println("TableSeriesPicker : picking received, getting dimensions");

		if(mq instanceof MetaLink){
			System.out.println("TableSeriesPicker : MetaLink Detected");
			mrs.add(((MetaLink)mq).readOnSource(WritableDataItem.idKey));
			mrs.add(((MetaLink)mq).readOnTarget(WritableDataItem.idKey));
		}
		
		List<Dimension<DataItem,?>> dims = new ArrayList<>();
		for(MetaReadable<?> mr : mrs){
			for(int i = 0; i<mr.numDimensions(); i++){
				Dimension<DataItem, ?> d = mr.getDimension(i);
				dims.add(d);
			}
		}
		
		return dims;
	}
	
	
	
	
	
	

	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}

	@Override
	public String name() {
		return "Table Set";
	}

	@Override
	public void sessionClosed(DataAccess<?, ?> source) {}

	@Override
	public void setSession(DataAccess<?, ?> session) {
		this.access = session;;
	}


	private class ToFileAction implements Action<DataItem, Void>{

		ToFileMechanism m;


		public ToFileAction(List<Dimension<DataItem,?>> dims, File f) throws FileNotFoundException {
			m = new ToFileMechanism(dims,f);
		}

		@Override
		public ActionMechanism<DataItem> createCoordinate(Object key) {
			return m;
		}

		@Override
		public Set<Object> coordinates() {
			return Collections.emptySet();
		}

		@Override
		public Void createOutput(Object key) throws IllegalArgumentException {
			return null;
		}

		@Override
		public String description() {
			return "Printing series to file";
		}

	}



	private class ToFileMechanism implements ActionMechanism<DataItem>{

		private final PrintWriter os;
		private final List<Dimension<DataItem, ?>> dims;

		public ToFileMechanism(List<Dimension<DataItem,?>> dims, File file) throws FileNotFoundException {

			this.os = new PrintWriter(file);
			this.dims = dims;

			//Print Headers
			for (Dimension<DataItem,?> dim : dims) {
				os.print(dim.name()+"\t");
			}
		}


		@Override
		public void performAction(DataItem i) throws DataAccessException {

			// Print a new row
			os.println();	
			for (Dimension<DataItem,?> dim : dims) {
				os.print(AKey.asString(i, dim)+"\t");
			}

		}


		public void lastAction(){
			os.flush();
			os.close();
		}

	}


}
