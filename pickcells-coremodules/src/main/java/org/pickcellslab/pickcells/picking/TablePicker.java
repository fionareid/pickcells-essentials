package org.pickcellslab.pickcells.picking;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.FlowLayout;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import org.apache.log4j.Logger;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.meta.Dimension;
import org.pickcellslab.foundationj.dbm.meta.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.modules.SessionConsumer;
import org.pickcellslab.foundationj.dbm.queries.Paradigm;
import org.pickcellslab.foundationj.dbm.queries.Vignette;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.foundationj.queryUI.swing.QueryUI;
import org.pickcellslab.pickcells.api.app.picking.SingleDataPickConsumer;
import org.pickcellslab.pickcells.api.app.ui.Icons;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.img.io.ImgIO;

import com.alee.extended.image.WebImage;
import com.alee.managers.notification.NotificationManager;
import com.alee.managers.notification.WebNotification;

@MetaInfServices(Module.class)
public class TablePicker implements SingleDataPickConsumer, SessionConsumer {

	private final Logger log = Logger.getLogger(getClass());

	private final ImgIO io;
	private DataAccess<?,?> access;

	public TablePicker(ImgIO io){
		this.io = io;
	}

	@Override
	public String name(){
		return "Table";
	}

	@Override
	public void singleDataPicked(Vignette<?> v, int dbId) {

		//TODO in new thread

		try {


			//Init pop over
			final JFrame popOver = new JFrame();
			popOver.setLayout ( new FlowLayout () );

			ImageIcon icon = null;

			//TextPane for the description of the object
			JTextPane pane = new JTextPane();

			//Check if we have an ImageLocated
			if(v.paradigm() != Paradigm.PATH){

				MetaQueryable mc = null;
				if(v.paradigm() == Paradigm.LINK){
					mc = Meta.get().getMetaLinks(v.shortType()).get(0);
				}
				else
					mc = Meta.get().getMetaModel(v.shortType());

				//Create the top panel displaying a view if it is an ImageLocated object
				if(mc instanceof MetaClass && SegmentedObject.class.isAssignableFrom(((MetaClass) mc).itemClass())){

					@SuppressWarnings("unchecked")
					Vignette<NodeItem> vg = (Vignette<NodeItem>) v;

					SegObjectMechanism m = new SegObjectMechanism((MetaClass)mc);
					access.queryFactory().readOne(vg, dbId).using(m);

					icon = (ImageIcon) m.icon;
					pane.setText(m.text);
				}
				//If not ImageLocated simply display the description of the object
				else {

					@SuppressWarnings("unchecked")
					Vignette<DataItem> vg = (Vignette<DataItem>) v;

					icon = (ImageIcon) QueryUI.getMetaIcon(mc);

					final List<Dimension<DataItem,?>> functions = mc.getReadables(p->true, Collectors.toList()).stream()
							.map(mr->mr.getRaw()).collect(Collectors.toList());

					//TODO fetch string and ids / type of source and target
					access.queryFactory().readOne(vg, dbId).using(item -> {
						StringBuilder b = new StringBuilder();
						b.append(item.toString()+"\n");
						functions.forEach(func-> b.append(func.name()+ " : "+AKey.asString(item,func)+"\n"));
						pane.setText(b.toString());
					});
				}
			}else{

				//We are looking at a path
				//icon = new WebImage ( WebLookAndFeel.getIcon ( 16 ) ); //TODO set icon
				access.queryFactory().readOne(v, dbId).using(item -> pane.setText(item.toString()));
			}

			popOver.setTitle( v.shortType());
			if(icon!=null){
				icon = (ImageIcon) Icons.resize(icon, 128, 128);
				WebImage img = new WebImage(icon);
				popOver.add(img);
			}
			popOver.add ( new JScrollPane(pane) );
			popOver.pack();
			popOver.setLocationRelativeTo(null);
			popOver.setVisible(true);


		} catch (Exception e) {
			WebNotification wn = NotificationManager.showNotification("Unable to display the table, see the log");
			wn.setDisplayTime(1000);
			log.warn("Unable to display the table", e);
		}

	}


	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}


	@Override
	public void sessionClosed(DataAccess<?, ?> source) {
		access = null;
	}


	@Override
	public void setSession(DataAccess<?, ?> session) {
		this.access = session;
	}




	private class SegObjectMechanism implements ActionMechanism<NodeItem>{

		private Icon icon;
		private String text;
		private final List<Dimension<DataItem,?>> functions;

		public SegObjectMechanism(MetaClass mc) {
			functions = mc.getReadables(p->true, Collectors.toList()).stream()
					.map(mr->mr.getRaw()).collect(Collectors.toList());
		}

		@Override
		public void performAction(NodeItem item) throws DataAccessException {

			NodeItem i = item.getLinks(Direction.OUTGOING, Links.CREATED_FROM).iterator().next().target()//SegmentationResult
					.getLinks(Direction.OUTGOING, Links.COMPUTED_FROM).iterator().next().target();//Image


			StringBuilder b = new StringBuilder();


			try {

				Image im = Image.createInstance(i).get();
				b.append("Found in "+im.toString()+"\n");

				functions.forEach(f-> b.append(f.name()+ " : "+AKey.asString(item,f)+"\n"));

				icon = io.thumbnail(im, item.getAttribute(Keys.bbMin).get(), item.getAttribute(Keys.bbMax).get(), item.getAttribute(ImageLocated.frameKey).orElse(0));

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			text = b.toString();
		}


	}




}
