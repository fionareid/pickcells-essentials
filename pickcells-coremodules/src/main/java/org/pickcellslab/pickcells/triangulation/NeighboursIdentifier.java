package org.pickcellslab.pickcells.triangulation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.log4j.Logger;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.tools.DefaultTraversal;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.meta.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaClass;
import org.pickcellslab.foundationj.dbm.meta.MetaFilter;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.modules.SessionConsumer;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.StorageBox.BoxBuilder;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.app.ui.UI;
import org.pickcellslab.pickcells.api.datamodel.conventions.DataModel;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.conventions.UsefulQueries;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.geometry.bvh.BVH;
import org.pickcellslab.pickcells.api.geometry.bvh.BVTTNode;
import org.pickcellslab.pickcells.api.geometry.bvh.ClosestMeshFinder;
import org.pickcellslab.pickcells.api.geometry.triangulation.DataTriangulation;
import org.pickcellslab.pickcells.api.img.providers.NotInProductionException;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactory;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;

@MetaInfServices(Module.class)
public class NeighboursIdentifier extends AbstractAnalysis implements MetaModelListener, SessionConsumer {


	static final Predicate<MetaQueryable> allowed = 
			(mq) -> MetaClass.class.isAssignableFrom(mq.getClass()) 
			&& ImageLocated.class.isAssignableFrom(((MetaClass) mq).itemClass());

			private boolean isActivable;
			private DataAccess<?,?> session;


			private final ProviderFactoryFactory factory;

			private boolean flat = false;


			private static Logger log = Logger.getLogger(NeighboursIdentifier.class);


			public NeighboursIdentifier(ProviderFactoryFactory factory) {
				this.factory = factory;
			}



			@Override
			public void sessionClosed(DataAccess<?,?> source) {
				session = null;
			}




			@Override
			public void setSession(DataAccess<?,?> session) {
				session.addMetaListener(this);
				if( Meta.get().getQueryable(allowed).size() != 0)
					isActivable = true;

				this.session = session;
				session.addSessionListener(this);
			}





			@Override
			public Scope getScope() {
				return Scope.ALL_USERS;
			}

			@Override
			public String name() {
				return "Neighbours Identifier";
			}

			@Override
			public String description() {
				return "<HTML>This module creates links between adjacent objects.<br> "
						+ "You can decide the maximum distance between adjacent objects<br>"
						+ "and the maximum variation allowed in the distance between neighbouring objects.<br>"
						+ "The distance and contact area between objects will be stored in the created links <br>"
						+ "A vector identifying the center of mass of the neighbourhood will also be computed</HTML>";
			}

			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
			public void launch() {

				setStep(0);

				//Start dialog similar to data content controller to select the group
				NeighbourhoodDialog dialog = new NeighbourhoodDialog(session);
				dialog.pack();
				dialog.setLocationRelativeTo(null);
				dialog.setVisible(true);


				if(dialog.wasCancelled()){
					setCancelled();
					return;
				}


				setStep(1);

				//Build the name of vectors which will be created
				String vBase = "NNSkew";
				String vType = "All "+vBase;
				//Build the type of the links which will be created
				String linkType = "All "+Links.ADJACENT_TO;

				if(dialog.getSourceFilter().isPresent()){
					linkType = dialog.getSourceFilter().get().name()+" "+Links.ADJACENT_TO+" ";
					vType = dialog.getSourceFilter().get()+" "+vBase+" ";
				}

				final String lt = linkType;
				final String vt = vType; 
				final String vtNorm = vType+" Norm";



				//Now run for each image
				List<Image> images = null;
				try {

					images = UsefulQueries.imagesWithSegmentations(session);

				} catch (Exception e) {
					log.error("Unable to read the database",e);
					return;
				}




				setStep(2);

				final MetaClass choice = dialog.getTargetType();
				final Class<? extends ImageLocated> choiceClazz = (Class<? extends ImageLocated>) choice.itemClass();

				System.out.println("Choice = "+choice);




				//First delete the previously created "ADJACENT TO" links for the filtered items			
				Optional<MetaLink> toDel = Meta.get().getMetaLinks(linkType).stream().filter(ml->ml.source().itemDeclaredType().equals(choice)).findFirst();

				toDel.ifPresent(ml -> {
					try {
						int deletes = ml.deleteData(session);
						log.info("The analysis replaces previously identified links ("+deletes+" were performed)");
					} catch (DataAccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				});








				int counter = 0;
				float total = images.size();

				ProviderFactory pf = factory.create(1);

				for(Image i : images){

					flat = i.depth()==1;


					//Get the source list
					Map<Integer, List<ImageLocated>> sources;

					try{

						Optional<MetaFilter> mf = dialog.getSourceFilter();
						if(mf.isPresent())
							sources = UsefulQueries.imageToImageLocated(
									session,
									dialog.getTargetType(),
									mf.get().toFilter(),
									i.getAttribute(DataItem.idKey).get())
							.getAllItemsFor(choiceClazz)
							.map(il->(ImageLocated)il)
							.collect(Collectors.groupingBy(il->((ImageLocated)il).frame()));

						else
							sources = UsefulQueries.imageToImageLocated(
									session,
									dialog.getTargetType(),
									i.getAttribute(DataItem.idKey).get())
							.getAllItemsFor(choiceClazz)
							.collect(Collectors.groupingBy(il->((ImageLocated)il).frame()));

					} catch (Exception e) {
						log.error("Unable to read the database",e);
						return;
					}



					// Check sources
					if(sources.isEmpty()){
						log.warn("no item found in "+i.toString());
						continue;
					}


					final ImageLocated prototype = sources.get(0).get(0);
					LabelsImage labels = null;
					SegmentationImageProvider<?> sp = null;

					if(SegmentedObject.class.isAssignableFrom(prototype.getClass())){

						// Get the outline or mesh to refine using BVH
						// Get the segmented image to obtain the mesh/outline of the object
						labels = ((SegmentedObject)prototype).origin().get();


						//Obtain ref to segmented image
						pf.clearProduction();
						pf.addToProduction(labels);

						try{

							sp = pf.get(labels);
							sp.setKnownAssociatedData((Map)sources);

						}
						catch (InterruptedException | ExecutionException | NotInProductionException e1) {
							System.out.println("Error!");
							UI.display("Error", "Producing the segmentation result failed for "+labels.fileName(), e1, Level.WARNING);
							return;
						}

					}



					for(int frame : sources.keySet()){

						// Check sources
						if(sources.get(frame).isEmpty()){
							log.warn("no item found at frame "+frame);
							continue;
						}






						//Create the delaunay triangulation using centroids
						DataTriangulation.triangulate(sources.get(frame),
								p->p.getAttribute(Keys.centroid).get(),
								prototype.getAttribute(Keys.centroid).get().length,
								dialog.getMaxDistance()*5, lt);




						//-------------------------------------------------------------------------------------------------
						// Now add an extra step for Segmented Objects
						if(SegmentedObject.class.isAssignableFrom(prototype.getClass())){

							final SegmentationImageProvider<?> fsp = sp;
							
							// Get the outline or mesh to refine using BVH

							//Build the list of BVH
							final Map<DataItem,BVH<DataItem>> BVHMap = new HashMap<>();

							sources.get(frame).forEach(s -> {
								List<float[]> sMesh = fsp.getMesh((SegmentedObject) s,2,false, true);  
								if(sMesh.isEmpty()){
									double[] p = s.getAttribute(Keys.centroid).get();
									float[] f = new float[p.length];
									for(int j = 0; j<p.length; j++)
										f[j] = (float) p[j];
									sMesh.add(f);
								}
								BVHMap.put(s,new BVH<DataItem>(sMesh, s));
							});	

							System.out.println("Neighbours : number of sources : "+sources.get(frame).size());
							System.out.println("Neighbours : number of source BVH : "+BVHMap.size());




							//Now for each item
							//Iterate over neighbour and test the edge to edge distance

							float max = (float) dialog.getMaxDistance();
							float var = (float) dialog.getMaxVariation();
							BVHMap.values().forEach(s ->{
								//System.out.println("New Value!");
								long start = System.currentTimeMillis();

								((NodeItem) s.getUserObject()).getLinks(Direction.OUTGOING,lt).forEach(l ->{
									//System.out.println("finding closest!");
									List<BVTTNode<DataItem,DataItem>> shared = 
											ClosestMeshFinder.closestPoints(s, BVHMap.get(l.target()),var, max);
									//System.out.println("closest found!");
									if(shared.isEmpty()){
										//System.out.println("deleting!");
										l.delete();
										//System.out.println("deleted!");
									}
									else
										computeLinkFeatures(l,shared);
								});
								//same for incoming
								//System.out.println("Outgoing done!");
								((NodeItem) s.getUserObject()).getLinks(Direction.INCOMING,lt).forEach(l ->{
									List<BVTTNode<DataItem,DataItem>> shared = 
											ClosestMeshFinder.closestPoints(BVHMap.get(l.source()), s, var, max);
									if(shared.isEmpty())
										l.delete();
									else
										computeLinkFeatures(l,shared);
								});

								//System.out.println("Incoming Done!");
								//log.debug("Time to compute one source : "+(System.currentTimeMillis()-start)+" ms");

							});





						}

						System.out.println("Nearest Neighbours identified computing features");

						setProgress(((float)++counter/(total*2f)));




						//Now for each source compute the mean vector which will tell us the skew of neighbours distribution

						long start = System.currentTimeMillis();

						if(flat){

							System.out.println("Flat!");

							sources.get(frame).forEach(s -> {
								Vector2D v = new Vector2D(0, 0);
								Iterator<Link> it = ((NodeItem)s).getLinks(Direction.OUTGOING, lt).iterator();
								while(it.hasNext()){
									Link l = it.next();
									double[] sV = ((ImageLocated) l.source()).getAttribute(Keys.centroid).get();
									double[] tV = ((ImageLocated) l.target()).getAttribute(Keys.centroid).get();					
									Vector2D v2 = new Vector2D(tV[0]-sV[0],tV[1]-sV[1]);
									if(!v2.equals(Vector2D.ZERO))
										v = v.add(v2.normalize());
								}
								it = ((NodeItem)s).getLinks(Direction.INCOMING, lt).iterator();
								while(it.hasNext()){
									Link l = it.next();
									double[] sV = ((ImageLocated) l.target()).getAttribute(Keys.centroid).get();
									double[] tV = ((ImageLocated) l.source()).getAttribute(Keys.centroid).get();				
									Vector2D v2 = new Vector2D(tV[0]-sV[0],tV[1]-sV[1]);
									if(!v2.equals(Vector2D.ZERO))
										v = v.add(v2.normalize());
								}
								if(!v.equals(Vector2D.ZERO)){
									s.setAttribute(AKey.get(vtNorm,Double.class),v.getNorm());
									s.setAttribute(AKey.get(vt,double[].class),v.normalize().toArray());
								}
							});
						}
						else
							sources.get(frame).stream().parallel().forEach(s -> {
								Vector3D v = new Vector3D(0, 0, 0);
								Iterator<Link> it = ((NodeItem)s).getLinks(Direction.OUTGOING, lt).iterator();
								while(it.hasNext()){
									Link l = it.next();
									double[] sV = ((ImageLocated) l.source()).getAttribute(Keys.centroid).get();
									double[] tV = ((ImageLocated) l.target()).getAttribute(Keys.centroid).get();					
									Vector3D v2 = new Vector3D(tV[0]-sV[0],tV[1]-sV[1],tV[2]-sV[2]);
									if(!v2.equals(Vector3D.ZERO))
										v = v.add(v2.normalize());
								}
								it = ((NodeItem)s).getLinks(Direction.INCOMING, lt).iterator();
								while(it.hasNext()){
									Link l = it.next();
									double[] sV = ((ImageLocated) l.target()).getAttribute(Keys.centroid).get();
									double[] tV = ((ImageLocated) l.source()).getAttribute(Keys.centroid).get();				
									Vector3D v2 = new Vector3D(tV[0]-sV[0],tV[1]-sV[1],tV[2]-sV[2]);
									if(!v2.equals(Vector3D.ZERO))
										v = v.add(v2.normalize());
								}
								if(!v.equals(Vector3D.ZERO)){
									s.setAttribute(AKey.get(vtNorm,Double.class),v.getNorm());
									s.setAttribute(AKey.get(vt,double[].class),v.normalize().toArray());
								}
							});



						log.info("Time to process neighborhood features : "+(System.currentTimeMillis()-start)+" ms");

						//Now compute graph features.











						//Now write to the database, storing from the segmented result (or image)


						try {

							final TraverserConstraints tc = Traversers.newConstraints()
									.fromMinDepth().toMaxDepth()
									.traverseLink(lt, Direction.BOTH).includeAllNodes();

							//Only store the current frame
							final List<DefaultTraversal<NodeItem, Link>> list = Traversers.disconnected(sources.get(frame), tc);


							final BoxBuilder box = session.queryFactory().store();

							for(DefaultTraversal<NodeItem, Link> dt : list)
								box.add(dt.nodes.iterator().next(), tc);


							box.feed(
									AKey.get(vt,double[].class)
									, new double[flat ? 2 : 3],
									"Neighbours Identifier",
									DataModel.DIRECTIONAL+" : Direction of neighbourhood distribution skew",
									dialog.getTargetType())
							.feed(
									AKey.get(vtNorm,Double.class)
									, new Double(0),
									"Neighbours Identifier",
									" The norm of the vector for the neighbourhood distribution skew",
									dialog.getTargetType())
							.run();


						} catch (DataAccessException e) {
							UI.display("Error", "Could not update the database", e, Level.SEVERE);
							setFailure("Could not update the database");

						}



					}//END of loop over frames

					setProgress(((float)++counter/(total*2f)));

				}//END of loop over images

				setStep(3);

			}







			private void computeLinkFeatures(Link l, List<BVTTNode<DataItem, DataItem>> bvtt) {

				//System.out.println("Summarizing pairs for : " + l.toString());
				//System.out.println("Size : " + bvtt.size());

				int dim = flat ? 2 : 3;

				float[] sBary = new float[dim];
				float[] tBary = new float[dim];
				SummaryStatistics stats = new SummaryStatistics();	



				for(int p = 0; p<bvtt.size(); p++){

					// a- add to barycenter
					float[] sPoint = bvtt.get(p).source().center();						
					float[] tPoint = bvtt.get(p).target().center();
					double[] row = new double[dim];
					for(int d = 0; d<dim; d++){
						sBary[d] += sPoint[d];
						tBary[d] += tPoint[d];
						row[d] = tPoint[d];
					}						
					// b- add to distance stats
					stats.addValue(bvtt.get(p).distance());		

				}

				for(int d = 0; d<dim; d++){
					sBary[d] /= (float)bvtt.size();
					tBary[d] /= (float)bvtt.size();
				}	
				double mDist = stats.getMean();
				double vDist = stats.getStandardDeviation();

				//Create a link between the source and the target which will store the data
				l.setAttribute(AKey.get("Shared edge", Integer.class), bvtt.size());
				l.setAttribute(AKey.get("Edges distances", Double.class), mDist);
				l.setAttribute(AKey.get("Distance deviation", Double.class), vDist);
				l.setAttribute(Keys.sourceAnchor, sBary); //TODO swap if link is incoming
				l.setAttribute(Keys.targetAnchor, tBary);
			}




			@Override
			public boolean isActive() {
				return isActivable;
			}




			@Override
			public Object[] categories() {
				return new String[]{"Associations"};
			}






			@Override
			public Icon icon() {
				return new ImageIcon(getClass().getResource("/icons/hub.png"));
			}




			@Override
			public Icon[] icons() {
				return new Icon[4];
			}




			@Override
			public String[] steps() {
				return new String[]{"Inputs","Reading DB","Running","Done"};
			}





			/**
			 * Number of types supported by this module
			 */
			private int allowedCount = 0;



			@Override
			public void metaEvent(RegeneratedItems meta, MetaChange evt) {

				int change = meta.getTargets(MetaClass.class).filter(mq->ImageLocated.class.isAssignableFrom(mq.itemClass()))
						.collect(Collectors.counting()).intValue();
				if(evt == MetaChange.CREATED)
					allowedCount += change;
				else
					allowedCount -= change;

				isActivable = allowedCount!=0;
				fireIsNowActive();
			}

}
