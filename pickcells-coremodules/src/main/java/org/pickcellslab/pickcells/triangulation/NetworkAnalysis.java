package org.pickcellslab.pickcells.triangulation;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.TraversalStep;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.datamodel.tools.Traversers.BreadthFirstWithConstraints;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.meta.MetaLink;
import org.pickcellslab.foundationj.dbm.modules.SessionConsumer;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.foundationj.modules.ModuleActivationListener;
import org.pickcellslab.foundationj.queryUI.swing.MetaRenderer;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.app.ui.UI;

@MetaInfServices(Module.class)
public class NetworkAnalysis extends AbstractAnalysis implements MetaModelListener, SessionConsumer  {

	private DataAccess<?, ?> access;

	private boolean isActive;

	private List<ModuleActivationListener> lstrs = new ArrayList<>();




	@Override
	public void launch() {


		try {

			//Display the dialog allowing to choose the link type


			List<MetaLink> valid = 
					access.queryFactory().regenerate(MetaLink.class).toDepth(1).traverseAllLinks()
					.includeAllNodes().regenerateAllKeys().getAll()
					.getTargets(MetaLink.class).collect(Collectors.partitioningBy(ml-> ((MetaLink)ml).source().equals(((MetaLink)ml).target())))
					.get(true);

			if(valid.isEmpty()){
				JOptionPane.showMessageDialog(null, "There are no valid links for this analysis yet");
				return;
			}
			
			
			JComboBox<MetaLink> choice = new JComboBox<>();
			choice.setRenderer(new MetaRenderer());
			valid.forEach(ml -> choice.addItem(ml));

			
			int i = JOptionPane.showOptionDialog(null, "Choose the network to analyse", "Basic Network Analysis",
					JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, icon(), valid.toArray(), valid.get(0));
			
			if(i == JOptionPane.CANCEL_OPTION)
				return;
			
			
			//Now get objects from the database
			
			setStep(0);

			MetaLink l = (MetaLink) choice.getSelectedItem();
			
			
			
			List<? extends NodeItem> vertices = access.queryFactory().regenerate(l.source().itemClass())
			.toDepth(1).traverseLink(l.linkType(), Direction.BOTH).includeAllNodes()
			.includeOneKey(WritableDataItem.idKey).getAll().getTargets(l.source().itemClass()).collect(Collectors.toList());
			

			//Keys which will be created
			final AKey<Integer> degree = AKey.get(" Degree "+l.linkType(), Integer.class);
			final AKey<Double> avgD1 = AKey.get("Degree "+l.linkType()+" (Avg Depth 1)", Double.class);
			final AKey<Double> avgD2 = AKey.get("Degree "+l.linkType()+" (Avg Depth 2)", Double.class);
			final AKey<Double> stdD1 = AKey.get("Degree "+l.linkType()+" (Std Depth 1)", Double.class);
			final AKey<Double> stdD2 = AKey.get("Degree "+l.linkType()+" (Std Depth 2)", Double.class);			
			final AKey<Double> cluster = AKey.get("Clustering "+l.linkType(), Double.class);
			
			final TraverserConstraints tc = Traversers.newConstraints().fromDepth(1).toDepth(2).traverseAllLinks().includeAllNodes();
			
			
			setStep(1);
			
			vertices.stream().parallel().forEach(v->{
				
				v.setAttribute(degree, v.getDegree(Direction.BOTH, l.linkType()));
				
				final BreadthFirstWithConstraints tr = Traversers.breadthfirst(v,tc);
				
				TraversalStep<NodeItem,Link> step1 = tr.nextStep();
				TraversalStep<NodeItem,Link> step2 = tr.nextStep();
				
				if(step1 != null){
					//Compute the average degree
					final SummaryStatistics stats = new SummaryStatistics();
					step1.nodes.stream().forEach(n->stats.addValue(n.getDegree(Direction.BOTH, l.linkType())));
					v.setAttribute(avgD1, stats.getMean());
					v.setAttribute(stdD1, stats.getStandardDeviation());
					//Compute clustering
					final Set<NodeItem> set = new HashSet<>(step1.nodes);
					double e = step1.edges.stream().filter(p-> set.contains(p.source()) && set.contains(p.target())).count();
					double n = step1.nodes.size();
					v.setAttribute(cluster, 2d*e / (n*(n-1)));					
				}
				
				if(step2 != null){
					//Compute the average degree
					final SummaryStatistics stats = new SummaryStatistics();
					step2.nodes.stream().forEach(n->stats.addValue(n.getDegree(Direction.BOTH, l.linkType())));
					v.setAttribute(avgD2, stats.getMean());
					v.setAttribute(stdD2, stats.getStandardDeviation());
				}
				
				
			});
			
			
			setStep(2);
			
			access.queryFactory().store().addAll(vertices)
			.feed(avgD1, 1d, "Network Analysis", "Average Degree of Neighbours (Depth 1)", l.source() )
			.feed(avgD2, 1d, "Network Analysis", "Average Degree of Neighbours (Depth 2)", l.source())
			.feed(stdD1, 1d, "Network Analysis", "Std Degree of Neighbours (Depth 1)", l.source())
			.feed(stdD2, 1d, "Network Analysis", "Std Degree of Neighbours (Depth 2)", l.source())
			.feed(degree, 1d, "Network Analysis", "Degree of node for links of type "+l.linkType(), l.source())
			.feed(cluster, 1d, "Network Analysis", "Clustering Coefficient for links of type "+l.linkType(), l.source())
			.run();
			
			
			setStep(3);
			


		} catch (Exception e) {
			setFailure(e.getMessage());
			UI.display("Error", "An Error has occured while running Network Analysis, see the log", e, Level.WARNING);			
		}

	}






	@Override
	public void sessionClosed(DataAccess<?, ?> source) {
		access = null;	
	}

	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}

	@Override
	public Icon[] icons() {
		return new Icon[3];
	}

	@Override
	public String[] steps() {
		return new String[]{"Reading DB","Computing Features","Saving","Done"};
	}

	

	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/hub.png")); //TODO change
	}



	@Override
	public Object[] categories() {
		return new String[]{"Topology"};
	}

	@Override
	public String name() {
		return "Network Analysis";
	}

	@Override
	public String description() {
		return "<HTML> A basic network analysis : <br>"
				+ "Given a type of link accessible from Images which connects objects of the same type, <br>"
				+ "This module will compute the degree of each node, the average degree of neighbouring <br>"
				+ "nodes at depth 1 and 2, as well as the local clustering coefficient for each node. <br>";
	}

	@Override
	public void registerListener(ModuleActivationListener lst) {
		lstrs.add(lst);
	}

	@Override
	public boolean isActive() {
		return isActive;
	}


	@Override
	public void setSession(DataAccess<?, ?> session) {
		access = session;
		session.addSessionListener(this);
	}




	@Override
	public void metaEvent(RegeneratedItems meta, MetaChange evt) {
		try {
			
			isActive = 
					! access.queryFactory().regenerate(MetaLink.class).toDepth(1).traverseAllLinks()
					.includeAllNodes().regenerateAllKeys().getAll()
					.getTargets(MetaLink.class).collect(Collectors.partitioningBy(ml-> ((MetaLink)ml).source().equals(((MetaLink)ml).target())))
					.get(true).isEmpty();
		
			this.fireIsNowActive();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



}
