package org.pickcellslab.pickcells.features;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataNode;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.builders.P;
import org.pickcellslab.foundationj.datamodel.graph.ListenableGraph;
import org.pickcellslab.foundationj.datamodel.predicates.Op;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.meta.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.modules.SessionConsumer;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.dbm.queries.StorageBox.BoxBuilder;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.foundationj.optim.csp.GraphOptimization;
import org.pickcellslab.pickcells.api.app.modules.AbstractAnalysis;
import org.pickcellslab.pickcells.api.app.modules.Demand;
import org.pickcellslab.pickcells.api.app.modules.Dependency;
import org.pickcellslab.pickcells.api.app.modules.DependencyFactory;
import org.pickcellslab.pickcells.api.app.modules.FeaturesComputer;
import org.pickcellslab.pickcells.api.app.ui.UI;
import org.pickcellslab.pickcells.api.datamodel.conventions.DataModel;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ImageLocated;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.ProcessedLabels;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedPrototype;
import org.pickcellslab.pickcells.api.img.providers.ChannelImageConsumer;
import org.pickcellslab.pickcells.api.img.providers.ChannelImageProvider;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactory;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryFactory;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageConsumer;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;

import net.imglib2.type.numeric.RealType;


@MetaInfServices(Module.class)
public class FeatureComputerManager<T extends RealType<T>,S extends RealType<S>> extends AbstractAnalysis implements MetaModelListener, SessionConsumer {

	private Logger log = Logger.getLogger(FeatureComputerManager.class);

	//Session fields
	private DataAccess<?,?> session;
	private boolean isActivable;	
	private final List<Demand<Collection<AKey<?>>>> computers;
	private ListenableGraph<Demand<Collection<AKey<?>>>,Dependency<Collection<AKey<?>>>> graph ;

	private final ProviderFactoryFactory factory;



	public FeatureComputerManager(ProviderFactoryFactory factory){    
		this(new ArrayList<>(), factory);

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public FeatureComputerManager(List<FeaturesComputer> computers, ProviderFactoryFactory factory){


		this.computers = (List)computers;

		System.out.println("Number of computers : "+computers.size());

		this.factory = factory;


	}


	@Override
	public void sessionClosed(DataAccess<?,?> source) {
		session = null;
	}




	@Override
	public void setSession(DataAccess<?,?> session) {
		this.session = session;
		this.session.addMetaListener(this);


		//build the graph of computers (potential) dependencies 
		BiFunction<Demand<Collection<AKey<?>>>, Demand<Collection<AKey<?>>>, Boolean> p 
		= (f,s)->!Collections.disjoint(f.requirements(), s.capabilities());

		graph = GraphOptimization.createGraphModel(this.computers, p, new DependencyFactory<Collection<AKey<?>>>());

		System.out.println("Number of computers in graph : "+graph.nodeSet().size());

		//to be activable there must be at least one SegmentationResult

		try {

			isActivable = !session.queryFactory().read(SegmentationResult.class).makeList(WritableDataItem.idKey).inOneSet().getAll().run().isEmpty();	

			log.debug("Is activable = "+isActivable);


		} catch (DataAccessException e) {
			log.error("Could not read the database -> cause : "+e.getMessage());
			return;
		}

		session.addSessionListener(this);
	}
















	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void launch() {

		/*
		try {
			session.runQuery(
					QueryFactory.delete(MetaKey.class)
					.filter(P.keyTest(MetaKey.name, P.stringEquals(AKey.get("Mean Intensity", double[].class).name))).build()
					);
		} catch (DataAccessException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		 */

		//TODO UI.setProgress();
		ExecutorService dbExecutor = Executors.newFixedThreadPool(6);


		try{

			fireIsNowActive();
			setStep(0);

			// Get user input
			//---------------------------------------------------------------------------------


			// build dialog
			FeaturesDialog dialog = new FeaturesDialog(session, graph);
			dialog.setVisible(true);

			//User choice of FeaturesComputers
			List<FeaturesComputer> toRun = dialog.getSelectedComputers();



			//Show the dialog and get inputs
			if(!dialog.wasCancelled() && !toRun.isEmpty()){
				log.debug("Inputs received");
				setStep(1);

				log.debug(Arrays.toString(toRun.toArray(new FeaturesComputer[0])));
				//Configure the analysis

				//User choice of type
				String type = dialog.getSelectedObjectType();

				final SegmentedPrototype prototype = DataModel.getPrototype(type);

				if(type.equals(FeaturesDialog.CUSTOM)){
					// Create dialog to ask for the custom type
					type = showNewTypeDialog();
					if(type == null) // Cancelled
						return;		
					else
						prototype.setAttribute(DataNode.declaredTypeKey, type);
				}



				//if A MetaQueryable for the selected object already exists in the database,
				//then we will need to fetch instances attached to the current image.
				//Keep a reference to the MetaQueryable, if null then a prototype is required
				MetaQueryable mq = Meta.get().getMetaModel(type);



				//Check if chosen computers require channel image and segmentation image
				boolean channelRequired = false;
				boolean segRequired = false;
				for(FeaturesComputer fc : toRun){
					if(ChannelImageConsumer.class.isAssignableFrom(fc.getClass()))
						channelRequired = true;
					if(SegmentationImageConsumer.class.isAssignableFrom(fc.getClass()))
						segRequired = true;
					if(channelRequired && segRequired)
						break;
				}

				//load images (data types) to run the computers for each one of them
				//reconstruct also segresult and processedlabels
				List<Image> images =
						session.queryFactory()
						.regenerate(Image.class)
						.toDepth(2)
						.traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
						.and(ProcessedLabels.TO_SEG, Direction.INCOMING)
						.includeAllNodes().regenerateAllKeys()
						.getAll()
						.getTargets(Image.class).collect(Collectors.toList());

				log.info("The database contains "+images.size()+" images registered");

				//References to providers
				ProviderFactory f = factory.create(4);
				SegmentationImageProvider<S> sp = null;
				ChannelImageProvider<T> cp = null;
				LabelsImage labels = null;	

				setStep(2);
				if(mq == null){

					log.info("New data types will be created from this intrinsic features action : " + prototype.getClass().getSimpleName());

					//We are going to create new datatypes
					//In order to speed up the process, we will let the SegProviders identify the objects
					//Commit these new objects to the database concurrently for each image 
					//and create an index in parallel, so future reads and update are speed up.
					Iterator<Image> itr = images.iterator();
					SegmentedObject example = null;//Serve as a prototype to document features created by SegProvider
					int counter = 0;
					while(itr.hasNext()){

						LabelsImage i1 = null;	
						LabelsImage i2 = null;	


						//Load label images
						i1 = itr.next().segmentations().stream().filter(sr->sr.fileName().startsWith(dialog.getSelectedPrefix())).findFirst().get();
						counter++;

						log.info("i1 = "+i1.fileName());

						//Add the associated object to the ImageLabels
						i1.addAssociated(type);
						f.addToProduction(i1);
						if(itr.hasNext()){
							counter++;
							i2 = itr.next().segmentations().stream().filter(sr->sr.fileName().startsWith(dialog.getSelectedPrefix())).findFirst().get();
							//Add the associated object to the ImageLabels
							i2.addAssociated(type);
							f.addToProduction(i2);
						}

						//log.info("segmentation images loaded");

						//Setup the providers
						final ExecutorService ex = Executors.newFixedThreadPool(2);
						final SegmentationImageProvider<S> sp1 = f.get(i1);
						final LabelsImage l1 = i1;
						sp = sp1;//In case only one image then we don't need to reload later on

						ex.execute(()->sp1.setPrototype(l1, prototype, true));

						if(i2==null){	

							//f.clearProduction();
							ex.shutdown();
							ex.awaitTermination(30, TimeUnit.MINUTES);

							setProgress(counter/images.size());

							if(example == null){//Objects created by the seg provider so get example from the provider
								example = sp1.getAnyItem().orElse(null);
							}

							if(example!=null)
								storeNewData(sp1, dbExecutor, !itr.hasNext(), example);


						}

						else{//if a second image is to be processed in this round

							final SegmentationImageProvider<S> sp2 = f.get(i2);
							final LabelsImage l2 = i2;
							ex.execute(()->sp2.setPrototype(l2, prototype, true));
							//f.clearProduction();
							ex.shutdown();
							ex.awaitTermination(30, TimeUnit.MINUTES);

							setProgress(counter/images.size());

							if(example == null){
								example = sp1.getAnyItem().orElse(null);
							}

							if(example!=null){// Can be null if not items are found in the segmentation
								storeNewData(sp1, dbExecutor, false, example);
								storeNewData(sp2, dbExecutor, !itr.hasNext(), example);
								System.out.println("Sp1 : "+sp1.origin().fileName());
								System.out.println("Sp2 : "+sp2.origin().fileName());
							}
							else{
								Iterator<Float> lItr = sp2.labels(0).iterator();
								if(lItr.hasNext())
									example = sp2.getItem(lItr.next(),0).get();
								if(example!=null)// Can be null if no items are found in the segmentation
									storeNewData(sp2, dbExecutor, !itr.hasNext(), example);
							}


						}

						//log.info("data types created");
					}

					example = null;
					dbExecutor.shutdown();
					dbExecutor.awaitTermination(4, TimeUnit.HOURS);


					log.info("Creating new types done!");

				}




				setStep(3);
				int progress = 0;
				float total = images.size()*2;
				ExecutorService service = Executors.newCachedThreadPool();

				for(Image img : images){


					/********* Image Production ****************/

					if(segRequired || channelRequired){
						//Load the channel image only if it will be required by at least one computer
						if(channelRequired)
							f.addToProduction(img);
						//Load the ImageLabels only if it will be required by at least one computer		
						if(segRequired){							

							labels = img.getSegmentation(type).get();

							if(sp == null || !labels.equals(sp.origin()))//Reload if NOT already loaded by prototyping (see above)
								f.addToProduction(labels);
							else 
								log.info("Reusing provider for "+img);

						}

						//Load image
						try {
							log.info("Loading image...");
							f.produceAndWait(20, TimeUnit.MINUTES);
							log.info("Loading image done!");
						} catch (InterruptedException e) {
							log.error("Unable to load images ; cause -> "+e.getMessage());
							return;
						}

						log.info("Actual images loaded for "+img.getAttribute(Keys.name).orElse("Unknown"));


						/******************** Provider Setup ****************/


						if(channelRequired)
							cp = f.get(img);	

						if(segRequired){

							if(sp == null || !labels.equals(sp.origin())){//Reload if NOT already loaded by prototyping (see above)

								log.info("getting data from the database");

								sp = f.get(labels);


								//Now we need to regenerate known data
								Collection<AKey<?>> toRetrieve = dialog.keysNotComputedBySelectedComputers();
								toRetrieve.add(ImageLocated.frameKey);
								toRetrieve.add(Keys.bbMin);
								toRetrieve.add(Keys.bbMax);
								toRetrieve.add(Keys.volume);

								System.out.println("keys to retrieve = "+Arrays.toString(toRetrieve.toArray()));

								
								RegeneratedItems set = 
										session.queryFactory()
										.regenerate(labels.getClass())
										.toDepth(2)
										.traverseLink(labels.linkType(),Direction.INCOMING)
										.includeAllNodes()
										.includeKeys(toRetrieve)
										.doNotGetAll()
										.useFilter(P.keyTest(Keys.name, Op.TextOp.EQUALS, labels.fileName()))
										.run();
								//.withRule()


								labels = set.getOneTarget(labels.getClass()).get();

								System.out.println("Labels : "+labels.fileName());
								
								System.out.println("Set dependencies size : "+set.getAllDependencies().size());
								System.out.println("Set targets size : "+set.getAllTargets().size());

								final Map<Integer,List<SegmentedObject>> known = 
										set.getDependencies(SegmentedObject.class)
										.collect(Collectors.groupingBy(so->so.frame()));


								sp.setKnownAssociatedData(known);
								log.info("Getting data from database done! Number of objects : "+known.size());
							}
						}			
					}//END of providers setup 

					f.clearProduction();

					log.info("Providers successfully setup!");

					log.info("Computing features...");



					/******************** Features Computations ****************/

					//Run computers sequentially
					for(FeaturesComputer computer : toRun){
						//check if it needs a labels image
						if(SegmentationImageConsumer.class.isAssignableFrom(computer.getClass()))
							((SegmentationImageConsumer<S>) computer).setProvider(sp);
						//check if it needs a channel image as well
						if(ChannelImageConsumer.class.isAssignableFrom(computer.getClass()))
							((ChannelImageConsumer<T>)computer).setProvider(cp);						
						computer.run();
					}

					log.info("Computing features for "+img.getAttribute(Keys.name).get()+" done!");

					//SetProgress
					setProgress(((float)++progress)/total);



					//Update the database
					log.info("Sending write request to the database");




					//Create a documented query
					BoxBuilder builder = session.queryFactory().store().add(labels);

					//TODO ? Remove all the attributes that were previously know to 

					//Document keys
					//use the first item attached to labels as a prototype to document the MetaModel
					final SegmentedObject i = sp.getAnyItem().orElse(null);
					if(i!=null){
						for(FeaturesComputer computer : toRun){
							for(Entry<AKey<?>, String> e : computer.featuresDescription().entrySet()){
								AKey<?> k = e.getKey();
								Object p = i.getAttribute(k).get();
								String creator = computer.name();
								String description = e.getValue();
								builder.feed(k, p, creator, description, i);					    
							}
						}

						//The objects created by computers maybe large so add a limit on the size of transactions
						builder.setMaxCommitSize(5000);

						service.execute(()->{
							try {

								builder.run();

							} catch (DataAccessException e) {
								UI.display("Error","Unable to update the data graph", e, Level.SEVERE);
								setFailure("Data Access Exception");
								return;
							}	
						});
					}
					else{
						log.warn("No labels found in"+img);
					}

				}
				service.shutdown();
				if(!service.awaitTermination(12, TimeUnit.HOURS)){
					setFailure("Too long computation time");
					return;
				};

			}else{
				setCancelled();
				return;
			}

		}

		catch(Exception e){
			UI.display("Error","Intrinsic Features encountered a problem", e, Level.WARNING);
			setFailure(e.getClass().getSimpleName());
			return;
		}
		setStep(4);
	}







	private String showNewTypeDialog() {

		String type = JOptionPane.showInputDialog(null, "Enter the name of your custom object", "Custom type...");
		if(type == null)
			return null;

		// Check that this name is not already taken
		if(Meta.get().getMetaModel(type) != null){
			JOptionPane.showMessageDialog(null, type + "already exists.");
			return showNewTypeDialog();
		}

		return type;
	}










	/**
	 * Update the database with the LabelsImage found in the given SegmentationImageProvider.
	 * Expected: sp1 created its datamap already 
	 * @param sp
	 * @param dbExecutor 
	 * @param prototype 
	 */
	private void storeNewData(final SegmentationImageProvider<S> sp, ExecutorService dbExecutor, boolean meta, SegmentedObject prototype) {

		final LabelsImage labels = sp.origin();

		log.debug("Storing data for "+labels.fileName());
		log.debug("Number of objects "+labels.links().count());

		final TraverserConstraints c = Traversers.newConstraints().fromMinDepth().toDepth(1)
				.traverseLink(labels.linkType(), Direction.INCOMING)
				.includeAllNodes();

		BoxBuilder b = session.queryFactory().store().add(labels, c);
		if(meta){			
			for(Entry<AKey<?>, String> i : sp.computedFeatures().entrySet()){
				b.feed(i.getKey(), prototype.getAttribute(i.getKey()).get(), "Label Image Handler", i.getValue(), prototype);
			}
		}else
			b.turnMetaModelOff();

		//dbExecutor.execute(()->{
		try {
			b.run();
		} catch (Exception e) {
			log.error("Unable to update the database with "+labels.fileName(),e);
		}
		//});
	}






	@Override
	public boolean isActive() {
		return isActivable;
	}




	@Override
	public Object[] categories() {
		return new String[]{"Features Computation"};
	}




	@Override
	public String description() {
		return "Analyse Intrinsic Features in Segmented Shapes";
	}





	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/feats.png"));
	}




	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}






	@Override
	public String name() {
		return "Intrinsic Features";
	}






	@Override
	public void metaEvent(RegeneratedItems meta, MetaChange evt) {
		if(	meta.getAllTargets().stream().filter(m -> m.getAttribute(MetaModel.recognition).get().contains(SegmentationResult.class.getSimpleName()))
				.collect(Collectors.counting()).intValue() != 0 ){

			if(evt == MetaChange.CREATED)
				isActivable = true;
			else isActivable = false;

			fireIsNowActive();

		}

	}







	@Override
	public Icon[] icons() {
		return new Icon[4];
	}

	@Override
	public String[] steps() {
		return new String[]{"Inputs", "Reading Images Data", "Initialising Modules", "Computing Features", "Done"};
	}
















}
