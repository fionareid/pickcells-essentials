package org.pickcellslab.pickcells.features;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.log4j.Logger;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.pickcells.api.app.modules.FeaturesComputer;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.img.providers.ChannelImageConsumer;
import org.pickcellslab.pickcells.api.img.providers.ChannelImageProvider;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageConsumer;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;

import net.imglib2.Cursor;
import net.imglib2.RandomAccess;
import net.imglib2.type.numeric.RealType;
import net.imglib2.view.IntervalView;
import net.imglib2.view.composite.Composite;


@MetaInfServices(Module.class)
public class BasicComputer<T extends RealType<T>,S extends RealType<S>> extends FeaturesComputer implements SegmentationImageConsumer<S>, ChannelImageConsumer<T> {

	//private Logger log = Logger.getLogger(BasicComputer.class);
	private static final Logger log = Logger.getLogger(BasicComputer.class);

	private int statsCount;

	private SummaryStatistics statsTotal = new SummaryStatistics();
	private SummaryStatistics statsInit = new SummaryStatistics();
	private SummaryStatistics statsCoords = new SummaryStatistics();
	private SummaryStatistics statsStats = new SummaryStatistics();

	private ChannelImageProvider<T> channelProvider;
	private List<SegmentationImageProvider<S>> segProviders = new ArrayList<>();

	public static AKey<double[]> minInt = AKey.get("Min Intensity", double[].class);
	public static AKey<double[]> maxInt = AKey.get("Max Intensity", double[].class);
	public static AKey<double[]> kurtInt = AKey.get("Kurtosis Intensity", double[].class);
	public static AKey<double[]> meanInt = AKey.get("Mean Intensity", double[].class);
	public static AKey<double[]> sumInt = AKey.get("Sum Intensity", double[].class);
	public static AKey<double[]> medianInt = AKey.get("Median Intensity", double[].class);
	public static AKey<double[]> varInt = AKey.get("Intensity Variance", double[].class);



	private static Map<AKey<?>, String> features = new HashMap<>();


	//Note here we also document keys that are automatically created by the SegmentationImageProvider (The one found in Keys)

	static{		
		features.put(minInt, "The minimum intensity found in an object for each channel");
		features.put(maxInt, "The maximum intensity found in an object for each channel");
		features.put(meanInt, "The mean intensity of all the pixels/voxels belonging to an object for each channel");
		features.put(kurtInt, "The kurtosis (\"peakedness\") of the intensities each channel");
		features.put(medianInt, "The median of intensities in the object for each channel");
		features.put(varInt, "The variance of intensities in an object for each channel");
		features.put(sumInt, "The sum of intensities in an object for each channel");
		features.put(Keys.bbMin, "An array containing the coordinates of the minimum of the bounding box (in px)");
		features.put(Keys.bbMax, "An array containing the coordinates of the maximum of the bounding box (in px)");
		features.put(Keys.volume, "The volume of the object (in real space)");
	}



	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}

	@Override
	public String name() {
		return "Basic Features";
	}

	@Override
	public Map<AKey<?>, String> featuresDescription() {
		return features;
	}

	@Override
	public void setProvider(ChannelImageProvider<T> provider) {
		this.channelProvider = provider;		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setProvider(SegmentationImageProvider<S> one, SegmentationImageProvider<S>... others) {
		segProviders.clear();
		segProviders.add(one);
		for(SegmentationImageProvider<S> p: others)
			segProviders.add(p);
	}

	@Override
	public Collection<AKey<?>> requirements() {
		return Collections.emptyList();
	}




	@Override
	public Collection<AKey<?>> capabilities() {
		return features.keySet();
	}




	@Override
	public void run() {

		if(channelProvider == null)
			throw new IllegalStateException("BasicComputer needs a ChannelImageProvider");

		Image i = channelProvider.info();

		final int numChannels = i.channels().length;


		//Iterate over the segmentation images
		segProviders.forEach(provider->{

			IntStream.range(0, (int)i.frames()).forEach(t->{


				//For each label in the segmentation image 
				provider.labels(t).forEach(l->{ //parallel removed in case of CellImg


					long points = System.currentTimeMillis();

					final IntervalView<S> interval = provider.interval(l,t);
					final Cursor<S> cursor = interval.localizingCursor();
					final RandomAccess<? extends Composite<T>> colors = channelProvider.getFrame(t).randomAccess(interval); 


					//And get the DataItem associated with this list of point
					provider.getItem(l,t).ifPresent(item->{//make sure this is present


						//Init a map to store channel values temporarily
						Map<Integer, DescriptiveStatistics> values = new HashMap<>(numChannels);
						IntStream.range(0, numChannels)
						.forEach(c -> values.put(c , new DescriptiveStatistics()));


						long init = System.currentTimeMillis();
						statsInit.addValue(init-points);

						S value = provider.variable(l);

						while(cursor.hasNext()){

							cursor.fwd();
							if(cursor.get().equals(value)){

								colors.setPosition(cursor);
								Composite<T> composite = colors.get();	

								IntStream.range(0, numChannels)
								.forEach(channel ->	{
									values.get(channel).addValue(
											composite.get(channel).getRealDouble());								
								});

							}
						}


						long endCoords = System.currentTimeMillis();
						statsCoords.addValue(endCoords-init);


						//Stats on channels
						double[] minI = new double[numChannels];
						double[] maxI = new double[numChannels];
						double[] mean = new double[numChannels];
						double[] median = new double[numChannels];
						double[] sum = new double[numChannels];
						double[] var = new double[numChannels];
						double[] kurtosis = new double[numChannels];

						IntStream.range(0, numChannels)
						.forEach(c->{		
							DescriptiveStatistics stats = values.get(c);

							minI[c] = stats.getMin();
							maxI[c] = stats.getMax();
							mean[c] = stats.getMean();
							kurtosis[c] = stats.getKurtosis();
							sum[c] = stats.getSum();
							var[c] = stats.getVariance();

							//median
							if(stats.getN()!=0)
								median[c] = stats.getSortedValues()[(int) (stats.getN()/2)];							

						});

						item.setAttribute(minInt,minI);
						item.setAttribute(maxInt,maxI);
						item.setAttribute(meanInt,mean);
						item.setAttribute(medianInt,median);
						item.setAttribute(varInt,var);
						item.setAttribute(kurtInt,kurtosis);
						item.setAttribute(sumInt,sum);
						//values = null;//garbage

						long endStats = System.currentTimeMillis();
						statsStats.addValue(endStats-endCoords);


					});



					long done = System.currentTimeMillis();
					statsTotal.addValue(done-points);

					updateCount();


				}); // END of labels itr


			}); // END of time itr


		}); // END of providers itr





	}


	private void updateCount() {

		if(++statsCount == 10000){
			log.info("-------------------------- Stats on 10000 last items ------------------------------");
			log.info("Init: "+statsInit.getMean()+" ms");
			log.info("Coordinates: "+statsCoords.getMean()+" ms");
			log.info("Stats: "+statsStats.getMean()+" ms");
			log.info("Total: "+statsTotal.getMean()+" ms");
			statsCount = 0;
			statsInit.clear();
			statsCoords.clear();
			statsTotal.clear();
		}


	}







	@Override
	public String description() {
		return "Computes simple intrinsic features in labeled shapes in a segmented image";
	}






}
