package org.pickcellslab.pickcells.features;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.apache.log4j.Level;
import org.jdesktop.swingx.JXTaskPaneContainer;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.DataTypes;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.graph.ListenableGraph;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.meta.Meta;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.optim.csp.GraphOptimization;
import org.pickcellslab.foundationj.optim.csp.OptimizationListener;
import org.pickcellslab.foundationj.optim.csp.Solver;
import org.pickcellslab.foundationj.optim.cspImpl.DefaultEnumerationFactory;
import org.pickcellslab.foundationj.optim.cspImpl.DefaultEnumerator.SpanPolicy;
import org.pickcellslab.foundationj.optim.cspImpl.DefaultEnumerator.ordering;
import org.pickcellslab.foundationj.ui.renderers.ClassOrLinkRenderer;
import org.pickcellslab.pickcells.api.app.modules.Demand;
import org.pickcellslab.pickcells.api.app.modules.Dependency;
import org.pickcellslab.pickcells.api.app.modules.FeaturesComputer;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.ProcessedLabels;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedPrototype;


@SuppressWarnings({ "serial" })
public class FeaturesDialog  extends JDialog implements OptimizationListener<Demand<Collection<AKey<?>>>,Dependency<Collection<AKey<?>>>> {


	//UI components
	private final JXTaskPaneContainer tpc;
	private final JLabel lblSegmentedChannel;
	private final JButton okButton;
	private final JButton cancelButton;
	private final JComboBox<SegmentationResult> segBox;
	private final JComboBox<String>  objectTypeBox;

	
	final static String CUSTOM = "Custom Type";
	

	//Model components
	private final GraphOptimization<Demand<Collection<AKey<?>>>,Dependency<Collection<AKey<?>>>> view;	
	private final List<String> unused;
	private final Image image;
	private final DataAccess<?,?> session;
	private final Map<FeatureComputerUI,FeaturesComputer> map = new HashMap<>();

	//User choice dependent fields
	private Collection<AKey<?>> keys;
	private String selectedComponent;
	private boolean wasCancelled = true;//init as true if user closes the window
	private List<FeaturesComputer> selected;

	private ActionListener l = l->updateFCGraph();

	@SuppressWarnings({ "unchecked" })
	public FeaturesDialog(DataAccess<?,?> session, ListenableGraph<Demand<Collection<AKey<?>>>, Dependency<Collection<AKey<?>>>> graph) throws Exception { 

		this.session = session;


		//Setup the containers

		setTitle("Intrinsic Features...");
		setModal(true);
		setBounds(100, 100, 610, 529);
		JPanel contentPanel = new JPanel();
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		contentPanel.add(scrollPane, BorderLayout.CENTER);
		tpc = new JXTaskPaneContainer();

		scrollPane.setViewportView(tpc);





		//Setup controllers to choose segmented channel and type of object
		//-------------------------------------------------------------------------------------------------------


		//Get one Image (data type) and reconstruct with segresult and processed image
		image =
				session.queryFactory().regenerate(Image.class)
				.toDepth(2)
				.traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
				.and(ProcessedLabels.TO_SEG, Direction.INCOMING)
				.includeAllNodes().regenerateAllKeys().getAll().getOneTarget(Image.class).get();


		//Get all the unused types which are SegmentedPrototypes
		unused = DataTypes.dataTypes()
				.filter(t->SegmentedPrototype.class.isAssignableFrom(t))
				.map(t->t.getSimpleName())
				.filter(c-> Meta.get().getMetaModel(c) == null) //check that the class is not already used
				.collect(Collectors.toList());


		unused.add(CUSTOM);

		//get the possible segmentation names and add those in a JComboBox
		SegmentationResult[] segs = image.segmentations().toArray(new SegmentationResult[0]);
		segBox = new JComboBox<>(segs);
		segBox.setRenderer((a,b,c,d,e)->{
			JLabel label =  new JLabel( b.segmentationType());
			label.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
			return label;
		});
	

		//Create a JComboBox to select the type of SegmentedObject to associate with this channel if the associated SegmentedResult
		//does not already have an associated object, or to select the objects which already exist
		objectTypeBox = new JComboBox<>();
		objectTypeBox.setRenderer(new ClassOrLinkRenderer());


		//Fill the objectTypeBox
		//Get the selected SegResult
		SegmentationResult sr = (SegmentationResult) segBox.getSelectedItem();
		String[] available = sr.associatiedObjects();

		//if the array is empty, then propose unused objects
		if(available.length == 0 )
			unused.forEach(u->objectTypeBox.addItem(u));
		else{

			//Check if there are processed labels associated
			Optional<ProcessedLabels> processed = 
					sr.getLinks(Direction.INCOMING, ProcessedLabels.TO_SEG)
					.map(l->(ProcessedLabels)l.source()).findFirst();

			if(processed.isPresent()){
				String[] toAdd = processed.get().associatiedObjects();

				available = concat(available,toAdd);
			}

			for(int s = 0; s<available.length; s++){
				objectTypeBox.addItem(available[s]);
			}
		}
		
		
		




		// Setup a GraphOptimization which will dynamically resolve which computers can be used and which ones cannot
		//------------------------------------------------------------------------------------------------------------

		//Check which Computers were already done for the current type
		//To do so -> look in the Metamodel which keys are already known and compare with computers capabilities
		graph.nodeSet().forEach(fc->fc.setSatisfied(false,true));

		if(!unused.contains(objectTypeBox.getSelectedItem())){
			MetaQueryable mq = Meta.get().getMetaModel((String)objectTypeBox.getSelectedItem());
			keys = mq.keys().stream().map(mk->mk.toAKey()).collect(Collectors.toList());
			//If done set satisfied
			graph.nodeSet().stream()
			.filter(d->checkDone(d))
			.forEach(d->((FeaturesComputer) d).setDone(true));

			graph.nodeSet().forEach(fc->System.out.println("FeaturesComputer is done : "+((FeaturesComputer) fc).isDone()));

		}

		//Create the optimization
		view = new GraphOptimization<>(
				DemandValidation.factory(),
				DemandCost.factory(), 
				new DefaultEnumerationFactory<>(new SpanPolicy(0, 10, ordering.RANDOM)),
				graph //TODO define args
				);

		GraphOptimization.setLogLevel(Level.WARN);

		//Solve hierarchy
		view.solve(1000,0.05);

		//Now for each computer check if solver has solution, if yes, then set computer as satisfied
		//And create the associated UI
		Collection<Solver<Demand<Collection<AKey<?>>>, Dependency<Collection<AKey<?>>>>> solvers = view.getSortedSolvers();

		System.out.println("Sorted solver size : "+solvers.size());
		
		
		for(Solver<Demand<Collection<AKey<?>>>, Dependency<Collection<AKey<?>>>> s : solvers){			
			FeaturesComputer fc = (FeaturesComputer) s.getNode();
			fc.setSatisfied(s.hasSolution(),true);
			FeatureComputerUI ui = new FeatureComputerUI(fc);
			ui.addPropertyChangeListener("chosen", l->updateFCGraph());
			map.put(ui,fc);
			tpc.add(ui);			
		}

		view.addListener(this);




		//Events 
		//------------------------------------------------------------------------------------------------------------
		segBox.addActionListener(e->updateTypeBox());
		objectTypeBox.addActionListener(l);









		//Layout
		//----------------------------------------------------------------------------------------------------------


		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.NORTH);
			{
				lblSegmentedChannel = new JLabel("Segmented Images :");
			}



			JLabel lblObjectType = new JLabel("Object Type:");


			GroupLayout gl_panel = new GroupLayout(panel);
			gl_panel.setHorizontalGroup(
					gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblSegmentedChannel)
							.addGap(18)
							.addComponent(segBox, GroupLayout.PREFERRED_SIZE, 173, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lblObjectType, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(objectTypeBox, 0, 162, Short.MAX_VALUE)
							.addContainerGap())
					);
			gl_panel.setVerticalGroup(
					gl_panel.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
									.addComponent(lblSegmentedChannel)
									.addComponent(objectTypeBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(segBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(lblObjectType))
							.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					);
			panel.setLayout(gl_panel);
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);

			JLabel lblChooseTheFeatures = new JLabel("<HTML>Choose the features you wish to compute : "
					+ "<br>\r\nUnfold the panes corresponding to the desired features and close the ones you don't need. "
					+ "<br>Some features may require others to be precomputed and might automatically unfold other feature panes");
			lblChooseTheFeatures.setHorizontalAlignment(SwingConstants.LEFT);
			{
				okButton = new JButton("Compute");
				okButton.addActionListener(e->{
				
					selectedComponent = (String) objectTypeBox.getSelectedItem();
					selected = view.getSortedNodes(true).stream()
							.map(s->(FeaturesComputer)s)
							.filter(fc->!fc.isDone() && !getUI(fc).isCollapsed())
							.collect(Collectors.toList());

					//Determine which keys are needed by the computers which are not computed by 
					//them and need to be retrieved from the database by the manager
					keys = new HashSet<>();
					selected.forEach(fc->keys.addAll(fc.requirements()));
					//selected.forEach(fc->keys.removeAll(fc.capabilities()));					

					wasCancelled = false;
					this.dispose();
				});
				getRootPane().setDefaultButton(okButton);
			}
			{
				cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(e->{
					wasCancelled = true;
					this.dispose();
				});
			}
			GroupLayout gl_buttonPane = new GroupLayout(buttonPane);
			gl_buttonPane.setHorizontalGroup(
					gl_buttonPane.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_buttonPane.createSequentialGroup()
							.addGap(7)
							.addComponent(lblChooseTheFeatures))
					.addGroup(Alignment.TRAILING, gl_buttonPane.createSequentialGroup()
							.addContainerGap(437, Short.MAX_VALUE)
							.addComponent(okButton)
							.addGap(5)
							.addComponent(cancelButton)
							.addContainerGap())
					);
			gl_buttonPane.setVerticalGroup(
					gl_buttonPane.createParallelGroup(Alignment.LEADING)
					.addGroup(gl_buttonPane.createSequentialGroup()
							.addGap(5)
							.addComponent(lblChooseTheFeatures)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGroup(gl_buttonPane.createParallelGroup(Alignment.LEADING)
									.addComponent(okButton)
									.addComponent(cancelButton))
							.addContainerGap())
					);
			buttonPane.setLayout(gl_buttonPane);
		}
	}



	private FeatureComputerUI getUI(FeaturesComputer fc){
		for(Entry<FeatureComputerUI, FeaturesComputer> e : map.entrySet())
			if(e.getValue() == fc)
				return e.getKey();
		return null;
	}



	private void updateFCGraph() {

		//TODO start new Thread with a please wait icon


		//Reset
		view.getNodes().forEach(d->d.setSatisfied(false, true));
		view.getNodes().forEach(d->d.setEnabled(!getUI((FeaturesComputer) d).isCollapsed()));

		//Check which Computers were already done for the current type
		//To do so -> look in the MetaModel which keys are already known and compare with computers capabilities
		if(!unused.contains(objectTypeBox.getSelectedItem())){
			MetaQueryable mq = Meta.get().getMetaModel((String) objectTypeBox.getSelectedItem());
			keys = mq.keys().stream().map(mk->mk.toAKey()).collect(Collectors.toList());			
		}
		else 
			keys = new ArrayList<>();

		//updtate computers satisfaction based on the new set of keys
		view.getNodes().forEach(d->((FeaturesComputer) d).setDone(checkDone(d)));
		
		//Solve hierarchy again
		view.solve(1000,0.05);		
	}

	private boolean checkDone(Demand<Collection<AKey<?>>> d){
		Set<AKey<?>> test = new HashSet<>(d.capabilities());
		test.removeAll(keys);
		return test.isEmpty();		
	}


	private void updateTypeBox() {

		//Clear all items
		objectTypeBox.removeActionListener(l);
		objectTypeBox.removeAllItems();		


		//Get the object associated with the selected channel
		SegmentationResult sr = (SegmentationResult) segBox.getSelectedItem();

		String[] available = sr.associatiedObjects();

		//if the array is empty, then propose unused objects
		if(available.length == 0)
			unused.forEach(u->objectTypeBox.addItem(u));
		else{

			for(int s = 0; s<available.length; s++){
				objectTypeBox.addItem(available[s]);
			}

		}

		updateFCGraph();

		objectTypeBox.addActionListener(l);
	}


	public List<FeaturesComputer> getSelectedComputers(){
		return selected;
	}

	public String getSelectedPrefix() {
		String text = ((SegmentationResult) segBox.getSelectedItem()).fileName();
		return text.split("_")[0];
	}
	
	public String getSelectedObjectType(){
		return selectedComponent;
	}

	public Collection<AKey<?>> keysNotComputedBySelectedComputers(){
		return keys;
	}


	public boolean wasCancelled(){
		return wasCancelled;
	}


	@SuppressWarnings("unchecked")
	private static <T> T[] concat(T[] t1, T[] t2){
		T[] arr = (T[]) Array.newInstance(t1.getClass().getComponentType(), t1.length+t2.length);
		arr= Arrays.copyOf(t1, t1.length+t2.length);
		for(int i = 0; i<t2.length; i++)
			arr[i+t1.length] = t2[i];
		return arr;
	}




	@Override
	public void uptodateStateChanged(
			GraphOptimization<? extends Demand<Collection<AKey<?>>>, ? extends Dependency<Collection<AKey<?>>>> source,
					boolean uptodate) {
		if(uptodate){
			System.out.println("Event received!");
			//Now for each computer check if solver has solution, if yes, then set computer as satisfied
			view.getSortedSolvers().forEach(s->{
				FeaturesComputer fc = (FeaturesComputer) s.getNode();
				if(!fc.isDone())
					fc.setSatisfied(s.hasSolution(), false);
			});
			map.keySet().forEach(ui->ui.updateStatus());
		}		
	}



	




}
