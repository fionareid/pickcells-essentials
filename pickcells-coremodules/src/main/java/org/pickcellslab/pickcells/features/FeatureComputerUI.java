package org.pickcellslab.pickcells.features;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.Map.Entry;

import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.jdesktop.swingx.JXCollapsiblePane;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.pickcells.api.app.modules.FeaturesComputer;

@SuppressWarnings("serial")
public class FeatureComputerUI extends JPanel{

	private Font fFont = new Font("Arial Bold", Font.BOLD, 14);
	private FeaturesComputer computer;
	private JXCollapsiblePane collapsible;
	private JButton toggle;

	private static final Icon openIcon = new ImageIcon(FeatureComputerUI.class.getResource("/icons/open.png"));
	private static final Icon closeIcon = new ImageIcon(FeatureComputerUI.class.getResource("/icons/closed.png"));




	public enum STATUS{
		DONE, SATISFIED_INACTIVE, SATISFIED_ACTIVE, UNSATISFIED
	}


	public FeatureComputerUI(FeaturesComputer computer) {

		super(new BorderLayout());

		collapsible = new JXCollapsiblePane();
		this.add(collapsible,BorderLayout.CENTER);

		toggle = new JButton(computer.name());
		toggle.setHorizontalAlignment(SwingConstants.LEFT);
		toggle.setIconTextGap(50);
		toggle.addActionListener(l->{
			this.setCollapsed(!isCollapsed());
			this.firePropertyChange("chosen", !isCollapsed(), isCollapsed());
		});
		this.add(toggle,BorderLayout.NORTH);

		this.computer = computer;


		if(computer.isDone())
			setStatus(STATUS.DONE);
		else if(computer.isSatisfied())
			setStatus(STATUS.SATISFIED_ACTIVE);
		else setStatus(STATUS.UNSATISFIED);



		Box verticalBox = Box.createVerticalBox();
		verticalBox.setBorder(new EmptyBorder(15, 5, 15, 5));

		for(Entry<AKey<?>, String> e : computer.featuresDescription().entrySet()){

			JPanel panel = new JPanel(new BorderLayout());
			JLabel feature = new JLabel(e.getKey().name);
			feature.setFont(fFont);
			panel.add(feature, BorderLayout.NORTH);

			JLabel description = new JLabel(e.getValue());
			panel.add(description, BorderLayout.CENTER);

			verticalBox.add(panel);			

		}
		collapsible.add(verticalBox);
	}


	public boolean isCollapsed(){
		return collapsible.isCollapsed();
	}

	private void setCollapsed(boolean b){
		collapsible.setCollapsed(b);
		if(!b)
			toggle.setIcon(openIcon);
		else
			toggle.setIcon(closeIcon);
	}


	public STATUS getStatus(){
		if(!isCollapsed() && !isEnabled())
			return STATUS.DONE;
		else if(!isCollapsed() && isEnabled())
			return STATUS.SATISFIED_ACTIVE;
		else if(isCollapsed() && isEnabled())
			return STATUS.SATISFIED_INACTIVE;
		return STATUS.UNSATISFIED;
	}


	public void updateStatus(){
		if(computer.isDone())
			setStatus(STATUS.DONE);
		else if(computer.isSatisfied()){
			setEnabled(true);
		}
		else setStatus(STATUS.UNSATISFIED);
	}


	private void setStatus(STATUS s){
		switch(s){
		case DONE: this.setCollapsed(false); this.setEnabled(false);
		break;
		case SATISFIED_ACTIVE: this.setCollapsed(false); this.setEnabled(true);
		break;
		case SATISFIED_INACTIVE: this.setCollapsed(true); this.setEnabled(true);
		break;
		case UNSATISFIED: this.setCollapsed(true); this.setEnabled(false);
		break;
		default:
			break;		
		}
	}


	@Override
	public void setEnabled(boolean b){
		toggle.setEnabled(b);
	}

	//opened and enabled means to be computed
	//opened and disabled means already done
	//closed and enabled means deactivated but satisfiable
	//closed and disabled means deactivated and not satisfied
	


	public FeaturesComputer getComputer() {
		return computer;
	}










}
