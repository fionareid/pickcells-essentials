package org.pickcellslab.pickcells.features;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.HashSet;

import org.pickcellslab.foundationj.optim.csp.OptimizationPartsFactory;
import org.pickcellslab.foundationj.optim.csp.Solver;
import org.pickcellslab.foundationj.optim.csp.Validator;
import org.pickcellslab.pickcells.api.app.modules.Demand;
import org.pickcellslab.pickcells.api.app.modules.Dependency;

public class DemandValidation<T> implements Validator<Demand<Collection<T>>,Dependency<Collection<T>>>{


	@Override
	public boolean isValid(Solver<Demand<Collection<T>>, Dependency<Collection<T>>> source,
			Collection<Solver<Demand<Collection<T>>, Dependency<Collection<T>>>> neighbours) {		


		//If any of neighbours is not both enabled and hasSolution or is satisfied, return false;
		boolean b = neighbours
				.stream()
				.filter(s->{
					if(!s.getNode().isEnabled())
						return true;
					if(s.hasSolution() || s.getNode().isSatisfied())
						return false;
					return true;
				}).count() ==0;

		if(!b)
			return false;
		//Now check that the source requirements are satisfied
		Collection<T> offer = new HashSet<>();
		neighbours.forEach(s->offer.addAll(s.getNode().capabilities()));
		return source.getNode().evaluate(offer) >= 1;

	}

	@Override
	public boolean potential(Solver<Demand<Collection<T>>, Dependency<Collection<T>>> source, Solver<Demand<Collection<T>>, Dependency<Collection<T>>> neighbour) {

		if(!neighbour.getNode().isEnabled()) return false;		
		return source.getNode().evaluate(neighbour.getNode().capabilities())>=1;

	}



	public static <T> OptimizationPartsFactory<Validator<Demand<Collection<T>>,Dependency<Collection<T>>>,Demand<Collection<T>>,Dependency<Collection<T>>> factory(){
		return new OptimizationPartsFactory<Validator<Demand<Collection<T>>,Dependency<Collection<T>>>,Demand<Collection<T>>,Dependency<Collection<T>>>(){

			private DemandValidation<T> v = new DemandValidation<>();

			@Override
			public DemandValidation<T> getInstanceFor(Solver<Demand<Collection<T>>, Dependency<Collection<T>>> solver) {
				return v;
			}			
		};		
	}



}
