package org.pickcellslab.pickcells.features;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.HashSet;

import org.pickcellslab.foundationj.optim.csp.CostFunction;
import org.pickcellslab.foundationj.optim.csp.OptimizationPartsFactory;
import org.pickcellslab.foundationj.optim.csp.Solver;
import org.pickcellslab.pickcells.api.app.modules.Demand;
import org.pickcellslab.pickcells.api.app.modules.Dependency;


public class DemandCost<T> implements CostFunction<Demand<Collection<T>>,Dependency<Collection<T>>> {

	@Override
	public double compute(Solver<Demand<Collection<T>>, Dependency<Collection<T>>> source,
			Collection<Solver<Demand<Collection<T>>, Dependency<Collection<T>>>> neighbours) {
		
		if(source.getNode().isSatisfied())
			return 1;
		
		Collection<T> offer = new HashSet<>();
		neighbours.forEach(s->offer.addAll(s.getNode().capabilities()));
		
		double cost1 = source.getNode().evaluate(offer);
		if(cost1<1)
			return 10;			
		double cost2 = neighbours.stream().mapToDouble(n->n.currentCost()).sum();//can add a probability to stop
		
		return cost1+cost2;
	}

	@Override
	public double minCost() {
		return -Integer.MAX_VALUE;
	}

	
	public static <T> OptimizationPartsFactory<CostFunction<Demand<Collection<T>>,Dependency<Collection<T>>>,Demand<Collection<T>>,Dependency<Collection<T>>> factory(){
		return new OptimizationPartsFactory<CostFunction<Demand<Collection<T>>,Dependency<Collection<T>>>,Demand<Collection<T>>,Dependency<Collection<T>>>(){

			private DemandCost<T> v = new DemandCost<>();
			
			@Override
			public DemandCost<T> getInstanceFor(Solver<Demand<Collection<T>>, Dependency<Collection<T>>> solver) {
				return v;
			}			
		};
	}
	
	
}
