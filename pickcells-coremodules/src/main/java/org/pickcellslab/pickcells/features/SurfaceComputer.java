package org.pickcellslab.pickcells.features;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import javax.vecmath.Vector3f;

import org.apache.log4j.Logger;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.pickcells.api.app.modules.FeaturesComputer;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentedObject;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageConsumer;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;

import net.imglib2.type.numeric.RealType;

@MetaInfServices(Module.class)
public class SurfaceComputer<T extends RealType<T>> extends FeaturesComputer implements SegmentationImageConsumer<T> {

	private Logger log = Logger.getLogger(SurfaceComputer.class);


	private List<SegmentationImageProvider<T>> segProviders = new ArrayList<>();


	public static AKey<Double> surface = AKey.get("Surface", Double.class);
	public static AKey<Double> compact = AKey.get("Compactness", Double.class);

	private static Map<AKey<?>, String> features = new HashMap<>();

	static{		
		features.put(surface, "The surface of the shape (the length of the periphery if the image is 2D) in image unit\u00B2");
		features.put(compact, "The ratio of surface over volume of an object");
		//features.put(Keys.mesh, "An ordered collection of points defining the mesh of the object used for 3D rendering (triangles in clockwise order), in real space coordinates!");
	}



	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}

	@Override
	public String name() {
		return "Surface Extractor";
	}

	@Override
	public String description() {
		return "Creates meshes from segmented objects and computes several features about the surface";
	}


	@Override
	public void run() {

		for(SegmentationImageProvider<T> provider : segProviders){

			log.debug("new provider");

			Image i = provider.origin().origin();

			log.debug("Checking "+i.getAttribute(Keys.name));

			//Use an out of bound strategy to avoid nullPointerException
			//RandomAccessible<RealType> interval = Views.extendZero(img);
			IntStream.range(0, (int)i.frames()).forEach(t->{

				//For each label get the interval defined by the bounding box
				provider.labels(t).forEach(label->{

					log.trace("new label");
					//get the item corresponding to the current label
					SegmentedObject item = provider.getItem(label, t).get();

					log.trace("item obtained");


					List<float[]> points = provider.getMesh(item, 1, false, true);


					if(points.isEmpty())
						return;

					double area = 0;

					if(points.get(0).length == 2){//If 2D


						//FIXME for the moment inaccurate, points are not sorted
						//In the future Segmentation provider must use http://udel.edu/~mm/code/marchingSquares/

						int p = 1;
						while(p<points.size()){
							float[] p1 = points.get(p-1);
							float[] p2 = points.get(p);

							area += Math.sqrt(Math.pow(p1[0]-p2[0],2)+Math.pow(p1[1]-p2[1],2));

							p+=2;
						}

					}

					else{//if 3D

						//Obtain the mesh from the Marching cubes method				
						//Store the points into mesh and compute features

						int p = 2;
						while(p<points.size()){

							//Get the summit of the triangle
							float[] p1 = points.get(p-2);	 
							float[] p2 = points.get(p-1);
							float[] p3 = points.get(p);

							//Calculate the area of the triangle
							Vector3f v0 = new Vector3f(p1);
							Vector3f v1 = new Vector3f(p2);
							v1.sub(v0);
							Vector3f v2 = new Vector3f(p3);
							v2.sub(v0);
							v1.cross(v1,v2);
							double ca = Math.abs(v1.length()/2d);
							area = area + ca;

							p+=3;
						}
					}
					item.setAttribute(surface, area);
					item.setAttribute(compact, area/item.getAttribute(Keys.volume).get());
				});

			});

		}
	}

	@Override
	public Collection<AKey<?>> requirements() {
		return Collections.emptyList();
	}





	@Override
	public Collection<AKey<?>> capabilities() {
		return features.keySet();
	}



	@Override
	public void setProvider(SegmentationImageProvider<T> one,
			@SuppressWarnings("unchecked") SegmentationImageProvider<T>... others) {
		segProviders.clear();
		segProviders.add(one);
		for(SegmentationImageProvider<T> p: others)
			segProviders.add(p);
	}

	@Override
	public Map<AKey<?>, String> featuresDescription() {
		return features;
	}





}
