package org.pickcellslab.pickcells.features;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Map;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.pickcells.api.app.modules.FeaturesComputer;


public class TestComputer extends FeaturesComputer{

	private final Collection<AKey<?>> requirements;
	private final Collection<AKey<?>> capabilities;
	private final String name;

	TestComputer(String name, Collection<AKey<?>> requirements, Collection<AKey<?>> capabilities){
		this.name = name;
		this.requirements = requirements;
		this.capabilities = capabilities;
	}
	
	
	@Override
	public Scope getScope() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String name() {
		return name;
	}

	@Override
	public String description() {
		// TODO Auto-generated method stub
		return null;
	}

	

	@Override
	public Collection<AKey<?>> capabilities() {
		return capabilities;
	}

	@Override
	public Collection<AKey<?>> requirements() {
		return requirements;
	}

	@Override
	public Map<AKey<?>, String> featuresDescription() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}



}
