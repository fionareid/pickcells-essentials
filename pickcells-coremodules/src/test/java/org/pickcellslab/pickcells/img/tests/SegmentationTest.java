package org.pickcellslab.pickcells.img.tests;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.apache.commons.math3.util.Pair;
import org.junit.Assert;
import org.junit.Test;
import org.pickcellslab.foundationj.datamodel.graph.TreeNode;

public class SegmentationTest {








	@Test
	public void treeNodeTest(){

		// Build a tree --> b:branch, n: node, a: ancestor, l: leaf
		TestNode root = new TestNode(null);

		//First branch
		TestNode b1n1 = root.createChild();
		TestNode l1 = b1n1.createChild();

		//Second branch
		TestNode b1 = root.createChild();
		TestNode b2a1n1 = b1.createChild();
		TestNode l2 = b2a1n1.createChild();

		TestNode b2 = b1.createChild();
		TestNode b2a2n1 = b2.createChild();
		TestNode l3 = b2a2n1.createChild();

		TestNode b2a2n2 = b2.createChild();
		TestNode b2a2n3 = b2a2n2.createChild();
		TestNode b3 = b2a2n3.createChild();
		TestNode b2a3n1 = b3.createChild();
		TestNode l4 = b2a3n1.createChild();
		TestNode l5 = b3.createChild();

		TestNode l6 = b1.createChild();

		Assert.assertTrue(TreeNode.commonAncestor(l1,l2).get() == root);
		Assert.assertTrue(TreeNode.commonAncestor(l2,l3).get() == b1);
		Assert.assertTrue(TreeNode.commonAncestor(l3,l4).get() == b2);
		Assert.assertTrue(TreeNode.commonAncestor(l2,l5).get() == b1);

		System.out.println("Path to Parent : "+l5.pathToParent(b1).size());
		Assert.assertTrue(l5.pathToParent(b1).size() == 5);


		TestRidge r = new TestRidge(l2, l5);
		Assert.assertTrue(r.ancestor == b1);
		Assert.assertTrue(r.left.size() == 2);
		Assert.assertTrue(r.right.size() == 5);

	}











	private class TestRidge{

		private final List<TestNode> left;
		private final List<TestNode> right;
		private final TestNode ancestor;


		public TestRidge(TestNode rp1, TestNode rp2) {
			// First we need to identify the least common ancestor
			Optional<Pair<List<TestNode>, List<TestNode>>> pair = TreeNode.pathsToCommonAncestor(rp1, rp2);
			if(pair.isPresent()){
				left = pair.get().getFirst();
				right = pair.get().getSecond();
				ancestor = left.get(left.size()-1).getParent();		
			}
			else throw new IllegalArgumentException("rp1 and rp2 do not share a common ancestor");
		}




	}






	private class TestNode implements TreeNode<TestNode>{

		private TestNode parent;
		private List<TestNode> children = new ArrayList<>();
		private HashSet<TestNode> ancestors;

		public TestNode(TestNode parent) {
			this.parent = parent;
		}

		public TestNode createChild(){
			TestNode child = new TestNode(this);
			children.add(child);
			return child;
		}


		@Override
		public TestNode getParent() {
			return parent;
		}

		@Override
		public int numChildren() {
			return children.size();
		}

		@Override
		public TestNode get(int i) {
			return children.get(i);
		}

		@Override
		public boolean hasAncestor(TestNode node) {
			if(ancestors == null){
				ancestors = new HashSet<>();
				Iterator<TestNode> it = ancestorIterator();
				while(it.hasNext())
					ancestors.add(it.next());
			}
			return ancestors.contains(node);
		}

	}



}
