# README #

PickCells is an image analysis platform. It has been designed with the following objectives in mind:

1. To be a flexible and powerful framework which allows developers to bring together generic image analysis algorithms, visualisation modules and data mining tasks. 
2. To provide conventions and a rich API ensuring that modules remain as generic as possible (notably via use of Predicates) and have access to generic features of the application without direct dependencies to specific implementation (via inversion of control designs). 
4. To be a user friendly solution for biologists with no programming knowledge to perform complex image analyses such as quantifying patterning and cell-cell interactions in complex tissues. A self descriptive property graph model resides at the core of the application. "Intelligent" graphical query builders allow the users to formulate hypothesis while exploring information contained in images and directly test ideas by querying and enriching the property graph data model.   

### What is this repository for? ###
 
* This repository contains the PickCells Core modules.

### How do I get set up? ###

* The complete PickCells application is spread over several repositories. PickCells is mavenized, in order to create a PickCells distribution, one needs to obtain [the Assembly POM](https://framagit.org/pickcellslab/pickcells-assembly) and run the maven-assembly plugin.