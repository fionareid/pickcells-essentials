package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.util.Collection;
import java.util.function.Predicate;

import javax.swing.Icon;

import io.scif.FormatException;
import io.scif.ImageMetadata;
import io.scif.Writer;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;

public interface DiskImgHandler {

		
	public Collection<ImageMetadata> imageMetaData();

	public ImageMetadata imageMetadata(int index) throws ArrayIndexOutOfBoundsException;		
		
	public int numImages();

	public String getDataSetPath();
	
	public String getDataSetName();
	
	public String getName(int i);

	public <T extends RealType<T> & NativeType<T>> Img<T> load(int image);
	
	public DiskImgHandler create(Predicate<Integer> subset);
	
	public Icon thumbnail(int i);
	
	public Writer createWriterHandle(int i) throws FormatException, IOException;
}
