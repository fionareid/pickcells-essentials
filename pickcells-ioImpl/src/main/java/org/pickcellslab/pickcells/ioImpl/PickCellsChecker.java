package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgsChecker;

import io.scif.ImageMetadata;

public class PickCellsChecker implements ImgsChecker {

	private Logger log = Logger.getLogger(getClass());

	private PickCellsImgFiles list;
	private final Set<AKey<?>> inconsistencies = new HashSet<>();

	private final Map<AKey<?>, Consistency<?>> options;
	private final Map<AKey<?>, Object> fixedValues;
	private final Map<AKey<?>, Consistency<?>> fixedChecks;


	PickCellsChecker(ImgFileList list, 
			Map<AKey<?>, Consistency<?>> options,
			Map<AKey<?>, Object> fixedValues,
			Map<AKey<?>, Consistency<?>> fixedChecks) {

		this.options = options;
		this.fixedValues = fixedValues;
		this.fixedChecks = fixedChecks;

		setList(list);
	}




	@Override
	public void setList(ImgFileList l) {
		log.debug("Checking new file list");
		this.list = (PickCellsImgFiles) l;
		inconsistencies.clear();
		List<ImageMetadata> mds = list.metadata();
		log.debug("Number of metadata to check : "+mds.size());
		for(AKey<?> k : options.keySet()){
			log.debug("Undetermined Check -> Current key : "+k.name);
			if(!options.get(k).check(mds))
				inconsistencies.add(k);
		}
		for(AKey<?> k : fixedChecks.keySet()){
			log.debug("Fixed Check -> Current key : "+k.name);
			if(!fixedChecks.get(k).check(mds,fixedValues.get(k)))
				inconsistencies.add(k);
		}
	}

	@Override
	public ImgFileList currentList() {
		return list;
	}

	@Override
	public boolean isConsistent() {
		return inconsistencies.size() == 0;
	}

	@Override
	public Set<AKey<?>> inconsistencies() {
		return Collections.unmodifiableSet(inconsistencies);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <V> V value(AKey<V> property) {
		if (!options.containsKey(property))
			throw new IllegalArgumentException("The given property is not part of the check list of this checker");
		if(inconsistencies.contains(property)) return null;	
		return (V) options.get(property).findValue(list.metadata().get(0));
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public <V> V value(AKey<V[]> property, int index) {		
		if (!options.containsKey(property))
			throw new IllegalArgumentException("The given property is not part of the check list of this checker");
		if(property != (AKey)ImgsChecker.calibration)
			return null;
		return (V) (Double) list.metadata().get(0).getAxes().get(index).calibratedValue(1);
	}

	@Override
	public Set<AKey<?>> tested() {
		Set<AKey<?>> keys = new HashSet<>(options.keySet());
		keys.addAll(fixedValues.keySet());
		return keys;
	}




	@SuppressWarnings("unchecked")
	@Override
	public <V> V value(AKey<V> property, ImgFileList list, int dataset, int index) {
		Consistency<?> c = repository.get(property);
		if(null == c)
			throw new IllegalArgumentException("The given property is unknown");
		PickCellsImgFiles pList = (PickCellsImgFiles) list;
		return (V) c.findValue(pList.metadata(dataset, index));
	}
	
	
	@SuppressWarnings("unchecked")
	static <V> V value(AKey<V> property, ImageMetadata imd) {
		Consistency<?> c = repository.get(property);
		if(null == c)
			throw new IllegalArgumentException("The given property is unknown");
		return (V) c.findValue(imd);
	}
	
	
	
	
	static final Map<AKey<?>,Consistency<?>> repository = new HashMap<>();
	static{
		repository.put(ImgsChecker.bitDepth,Consistency.pxType());
		repository.put(ImgsChecker.calibration,Consistency.calibration());
		repository.put(ImgsChecker.ordering,Consistency.ordering());
		repository.put(ImgsChecker.dimensions,Consistency.dimensions());
		repository.put(ImgsChecker.extension,Consistency.ext());
		repository.put(ImgsChecker.nChannels,Consistency.channels());
		repository.put(ImgsChecker.units,Consistency.units());
		repository.put(ImgsChecker.sUnit,Consistency.sUnit());
		repository.put(ImgsChecker.tUnit,Consistency.tUnit());
	}


}
