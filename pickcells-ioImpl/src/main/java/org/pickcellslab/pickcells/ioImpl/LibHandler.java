package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import io.scif.FormatException;
import io.scif.ImageMetadata;
import io.scif.Plane;
import io.scif.Reader;
import io.scif.Writer;
import io.scif.config.SCIFIOConfig;
import io.scif.config.SCIFIOConfig.ImgMode;
import io.scif.gui.AWTImageTools;
import io.scif.img.IO;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;

public class LibHandler implements DiskImgHandler {



	private final Reader reader;	
	private final int[] subset ;
	private final File file;




	LibHandler(File file, int[] subset) throws IOException {		
		this.file = file;
		this.subset = subset;
		this.reader = Context.createReader(file.getPath());
	}






	@Override
	public ImageMetadata imageMetadata(int index) throws ArrayIndexOutOfBoundsException {
		return reader.getMetadata().get(subset[index]);
	}




	@Override
	public int numImages() {
		return subset.length;
	}

	@Override
	public String getDataSetName() {
		return file.getName();
	}

	@Override
	public String getName(int i) {
		String name = imageMetadata(i).getName();
		if(name != null) return name;
		else
			return (String) reader.getMetadata().get(subset[i])
					.getTable().getOrDefault("Image name", "Series "+i);
	}

	@Override
	public String getDataSetPath() {
		return file.getPath();
	}

	@Override
	public <T extends RealType<T> & NativeType<T>> Img<T> load(int image) {
		//duplicate reader to use for image loading
		Reader duplicate = null;
		try {
			duplicate = Context.createReader(file.getPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		ImageMetadata imd = imageMetadata(image);
		
		long size = imd.getSize();
		System.out.println("Image size = "+size);
		
		SCIFIOConfig config = Context.newConfig();
		if(size>=4000000000l )//|| Runtime.getRuntime().freeMemory()<size+512000000)
			config.imgOpenerSetImgModes(ImgMode.CELL);
		
		T type = Context.createType(imd);
		config.imgOpenerSetIndex(subset[image]);
		
		Img<T> img = IO.openImgs(duplicate,type,config).get(0);
		
		try {
			duplicate.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return img;
	}



	@Override
	public Collection<ImageMetadata> imageMetaData() {
		Collection<ImageMetadata> list = new ArrayList<>();
		for(int i = 0; i<numImages(); i++)
			list.add(imageMetadata(i));
		return list;
	}






	@Override
	public DiskImgHandler create(Predicate<Integer> p) {
		List<Integer> sub = new ArrayList<>();
		for(int i = 0; i<subset.length; i++)
			if(p.test(i))
				sub.add(subset[i]);		
		int[] result = new int[sub.size()];
		for(int i = 0; i<subset.length; i++)
			result[i] = sub.get(i);
		try {
			return new LibHandler(file,result);
		} catch (IOException e) {
			//This won't happen as we control the files
			throw new RuntimeException("Problem with LibHandler implementation",e);
		}
	}






	@Override
	public synchronized Icon thumbnail(int i) {

		ImageMetadata iMeta  = imageMetadata(i);
		final long planeIndex = iMeta.getPlaneCount() / 2;
		Plane plane = null;
		BufferedImage thumb = null;
		try {
			plane = reader.openPlane(subset[i], planeIndex);
			thumb =	AWTImageTools.openThumbImage(plane, reader, subset[i], iMeta
					.getAxesLengthsPlanar(), (int) iMeta.getThumbSizeX(), (int) iMeta
					.getThumbSizeY(), false);
		} catch (FormatException | IOException e) {
			e.printStackTrace();
		}
		if(thumb == null)
			return new ImageIcon(getClass().getResource("/icons/failed.png"));
		return new ImageIcon(thumb);
	}

	
	@Override
	public void finalize(){
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	

	public String toString(){
		return "Library : "+getDataSetName();
	}






	@Override
	public Writer createWriterHandle(int i) throws FormatException, IOException {
		Writer w = reader.getFormat().createWriter();
		w.setMetadata(reader.getMetadata());
		w.setDest(file, subset[i]);
		return w;
	}

}
