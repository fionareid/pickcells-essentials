package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.pickcellslab.pickcells.api.app.ui.AlphanumComparator;

import io.scif.FormatException;
import io.scif.ImageMetadata;
import io.scif.Plane;
import io.scif.Reader;
import io.scif.Writer;
import io.scif.config.SCIFIOConfig;
import io.scif.config.SCIFIOConfig.ImgMode;
import io.scif.gui.AWTImageTools;
import io.scif.img.IO;
import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;


/**
 * A {@link DiskImgHandler} which handles images in a folder. Each file in the folder is assumed
 * to be one  single image, if the files are multi images, only the first one will be handled
 * 
 * @author Guillaume Blin
 *
 */
public class FolderHandler implements DiskImgHandler{


	private final File[] files;
	private final Reader[] readers; 
	//private final ImageMetadata[] mds; 

	/**
	 * Initialise a new {@link FolderHandler} object with the specified files to handle.
	 * Note that directories are forbidden, and any file that is not valid (as a file) will be ignored
	 * @throws IOException 
	 */
	FolderHandler(File[] files) throws IOException {

		//Sort files in ascending order
		Arrays.sort(files, new AlphanumComparator<File>(f -> f.getName()));

		this.files = files;
		readers = new Reader[files.length];
		//mds = new ImageMetadata[files.length];
		for(int i = 0; i<files.length; i++){
			// System.out.println("FolderHandler : Creating Reader");
			readers[i] = Context.createReader(files[i].getPath());
			// make sure the name is set properly in metadata
			// System.out.println("FolderHandler : Obtaining Metadata");
			readers[i].getMetadata().get(0).setName(files[i].getName());
			// System.out.println("Image "+i+" -> "+files[i].getName());
		}
	}




	@Override
	public Collection<ImageMetadata> imageMetaData() {
		Collection<ImageMetadata> md = new ArrayList<>(readers.length);
		for(int i = 0; i<files.length; i++)
			md.add(imageMetadata(i));
		return md;
	}


	@Override
	public ImageMetadata imageMetadata(int i) throws ArrayIndexOutOfBoundsException {
		if(null == readers[i].getMetadata()){// this means the reader was closed by a previous call to load;
			try {
				readers[i] = Context.createReader(files[i].getPath());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			readers[i].getMetadata().get(0).setName(files[i].getName());
		}
		return readers[i].getMetadata().get(0);
	}




	@Override
	public int numImages() {
		return files.length;
	}




	@Override
	public String getDataSetName() {
		return files[0].getParentFile().getName();
	}




	@Override
	public String getName(int i) {
		return files[i].getName();
	}



	@Override
	public <T extends RealType<T> & NativeType<T>> Img<T> load(int image) {

		Reader r = readers[image];		
		ImageMetadata imd = imageMetadata(image);
		long size = imd.getSize();
		System.out.println("Image size = "+size);

		SCIFIOConfig config = Context.newConfig();
		if(size>=4000000000l || Runtime.getRuntime().freeMemory()<size+512000000)
			config.imgOpenerSetImgModes(ImgMode.CELL);

		T type = Context.createType(imd);	

		return IO.openImgs(r, type, config).get(0);
	}




	@Override
	public String getDataSetPath() {
		return files[0].getParentFile().getPath();
	}




	@Override
	public DiskImgHandler create(Predicate<Integer> subset) {
		List<File> sub = new ArrayList<>();
		for(int i = 0; i<files.length; i++)
			if(subset.test(i))
				sub.add(files[i]);		
		try {
			return new FolderHandler(sub.toArray(new File[sub.size()]));
		} catch (IOException e) {
			//This won't happen as we control the files
			throw new RuntimeException("Problem with FolderHandler implementation",e);
		}
	}




	@Override
	public synchronized Icon thumbnail(int i) {
		System.out.println("Initiating thumbnailing");
		ImageMetadata iMeta  = imageMetadata(i);
		final long planeIndex = iMeta.getPlaneCount() / 2;
		Plane plane = null;
		BufferedImage thumb = null;
		try {
			plane = readers[i].openPlane(0, planeIndex);
			thumb =	AWTImageTools.openThumbImage(plane, readers[i], 0, iMeta
					.getAxesLengthsPlanar(), (int) iMeta.getThumbSizeX(), (int) iMeta
					.getThumbSizeY(), false);
		} catch (Exception e) {
			e.printStackTrace();
			return new ImageIcon(getClass().getResource("/icons/failed.png"));
		}
		if(thumb == null)
			return new ImageIcon(getClass().getResource("/icons/failed.png"));
		return new ImageIcon(thumb);
	}

	public String toString(){
		return "Folder : "+getDataSetName();
	}



	@Override
	public void finalize(){
		try {
			for(int i = 0; i<readers.length; i++)
				readers[i].close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}




	@Override
	public Writer createWriterHandle(int i) throws FormatException, IOException {
		Writer w = readers[i].getFormat().createWriter();
		imageMetadata(i);//Ensure not closed
		w.setMetadata(readers[i].getMetadata());
		w.setDest(files[i].getPath(), 0, Context.newConfig());	
		return w;
	}



}
