package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;

import org.pickcellslab.pickcells.api.img.io.ImgWriter;
import org.scijava.util.Bytes;

import io.scif.ByteArrayPlane;
import io.scif.FormatException;
import io.scif.ImageMetadata;
import io.scif.Metadata;
import io.scif.Writer;
import io.scif.util.FormatTools;
import io.scif.util.SCIFIOMetadataTools;
import net.imagej.axis.Axes;
import net.imagej.axis.CalibratedAxis;
import net.imglib2.RandomAccess;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.exception.ImgLibException;
import net.imglib2.img.Img;
import net.imglib2.img.array.ArrayImg;
import net.imglib2.img.basictypeaccess.array.ByteArray;
import net.imglib2.img.basictypeaccess.array.CharArray;
import net.imglib2.img.basictypeaccess.array.DoubleArray;
import net.imglib2.img.basictypeaccess.array.FloatArray;
import net.imglib2.img.basictypeaccess.array.IntArray;
import net.imglib2.img.basictypeaccess.array.LongArray;
import net.imglib2.img.basictypeaccess.array.ShortArray;
import net.imglib2.img.planar.PlanarImg;
import net.imglib2.img.planar.PlanarImgFactory;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.ComplexType;
import net.imglib2.type.numeric.integer.GenericByteType;
import net.imglib2.type.numeric.integer.GenericIntType;
import net.imglib2.type.numeric.integer.GenericShortType;
import net.imglib2.type.numeric.integer.LongType;
import net.imglib2.type.numeric.real.DoubleType;
import net.imglib2.type.numeric.real.FloatType;

class PickCellsImgWriter<T extends NativeType<T>> implements ImgWriter<T> {


	private final T type;
	private final Writer w;
	private final long[] channelFrameDims;
	private final long[] npLength;


	PickCellsImgWriter(Writer w, T type) {
		this.type = type;
		this.w = w;
		// Get dimensions (x,y,z)
		ImageMetadata imd = w.getMetadata().get(0);
		if(imd.getAxisLength(Axes.Z)==1)
			channelFrameDims = new long[]{imd.getAxisLength(Axes.X), imd.getAxisLength(Axes.Y)};
		else
			channelFrameDims = new long[]{imd.getAxisLength(Axes.X), imd.getAxisLength(Axes.Y), imd.getAxisLength(Axes.Z)};

		npLength = w.getMetadata().get(0).getAxesLengthsNonPlanar();

	}



	@Override
	public int sliceNumber() {
		return (int) w.getMetadata().get(0).getAxisLength(Axes.Z);
	}



	@Override
	public int channelNumber() {
		return (int) w.getMetadata().get(0).getAxisLength(Axes.CHANNEL);
	}


	@Override
	public int frameNumber() {
		return (int) w.getMetadata().get(0).getAxisLength(Axes.TIME);
	}





	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Img<T> createNewChannelFrame() {
		return new PlanarImgFactory().create(channelFrameDims, type);
		//return Util.getArrayOrCellImgFactory(channelFrameDims, type).create(channelFrameDims, type);
	}

	@Override
	public void writeChannelFrame(RandomAccessibleInterval<T> frame, int channel, int time) throws IOException {

		//Itr over all planes in this frame and save to disk
		try {
			for(int p = 0; p<getPlaneCount(frame); p++){
				w.savePlane(0, getPlaneIndex(p, channel, time), getPlane(frame, 1, 0, p));
			}
		} catch (FormatException e) {
			throw new IOException(e);
		}

	}
	
	@Override
	public void writePlane(RandomAccessibleInterval<T> plane, int slice, int channel, int frame) throws IOException {
		try {
			w.savePlane(0, getPlaneIndex(slice, channel, frame), getPlane(plane, 1, 0, 0));
		} catch (FormatException e) {
			throw new IOException(e);
		}
	}



	private int getPlaneIndex(int currentZ, int currentC, int currentT){

		// This depends on order
		// equation : 1L*2L*3 + 1L*2 + 1
		int index = 0;
		Iterator<CalibratedAxis> it = w.getMetadata().get(0).getAxesNonPlanar().iterator();
		CalibratedAxis ax = it.next();	
		switch(ax.type().getLabel()){
		case "Z": index+=currentZ;	break;
		case "Channel" : index+=currentC; break;
		case "T" : index+=currentT; break;
		case "Time" : index+=currentT; break;
		}
		if(it.hasNext()){
			ax = it.next();	
			switch(ax.type().getLabel()){
			case "Z": index += currentZ*npLength[0]; break;
			case "Channel" : index += currentC*npLength[0]; break;
			case "T" : index += currentT*npLength[0]; break;
			case "Time" : index += currentT*npLength[0]; break;
			}
			if(it.hasNext()){

				ax = it.next();	
				switch(ax.type().getLabel()){
				case "Z": index += currentZ*npLength[0]*npLength[1]; break;
				case "Channel" : index += currentC*npLength[0]*npLength[1]; break;
				case "T" : index += currentT*npLength[0]*npLength[1]; break;
				case "Time" : index += currentT*npLength[0]*npLength[1]; break;
				}
			}
		}

		//System.out.println("PIW: Plane Index for : "+currentC+" ; "+currentZ+" ; "+currentT);
		//System.out.println("PIW: ---> : "+index);

		return index;
	}



	/**
	 * @return The number of planes in the provided {@link Img}.
	 */
	private int getPlaneCount(final RandomAccessibleInterval<?> img) {
		// PlanarImg case
		if (PlanarImg.class.isAssignableFrom(img.getClass())) {
			final PlanarImg<?, ?> planarImg = (PlanarImg<?, ?>) img;
			return planarImg.numSlices();
		}
		// General case
		int count = 1;

		for (int d = 2; d < img.numDimensions(); d++) {
			count *= img.dimension(d);
		}
		//System.out.println("PIW: planecount in one frame : "+count);
		return count;
	}


	/**
	 * Method from ImgSaver
	 * @return An array of data corresponding to the given plane and channel
	 *         indices.
	 */
	private ByteArrayPlane getPlane(final RandomAccessibleInterval<T> img, final int rgbChannelCount, final int cIndex, final int planeIndex)
	{

		Object array = null;

		// PlanarImg case
		if (PlanarImg.class.isAssignableFrom(img.getClass())) {
			final PlanarImg<?, ?> planarImg = (PlanarImg<?, ?>) img;
			array = planarImg.getPlane(cIndex + (planeIndex * rgbChannelCount))
					.getCurrentStorageArray();
		}
		else{

			final int planeSize = (int) (img.dimension(0) * img.dimension(1));

			// ArrayImg case
			if (ArrayImg.class.isAssignableFrom(img.getClass())) {
				final ArrayImg<?, ?> arrayImg = (ArrayImg<?, ?>) img;
				final Object store = arrayImg.update(null);

				// For each array type, just create an appropriate container array
				// and System.arraycopy the relevant data.
				if (store instanceof ByteArray) {
					final byte[] source = ((ByteArray) store).getCurrentStorageArray();
					final byte[] bytes = new byte[planeSize];
					System.arraycopy(source, planeSize *
							(cIndex + (planeIndex * rgbChannelCount)), bytes, 0, bytes.length);
					array = bytes;
				}
				else if (store instanceof ShortArray) {
					final short[] source = ((ShortArray) store).getCurrentStorageArray();
					final short[] shorts = new short[planeSize];
					System.arraycopy(source, planeSize *
							(cIndex + (planeIndex * rgbChannelCount)), shorts, 0, shorts.length);
					array = shorts;
				}
				else if (store instanceof LongArray) {
					final long[] source = ((LongArray) store).getCurrentStorageArray();
					final long[] longs = new long[planeSize];
					System.arraycopy(source, planeSize *
							(cIndex + (planeIndex * rgbChannelCount)), longs, 0, longs.length);
					array = longs;
				}
				else if (store instanceof CharArray) {
					final char[] source = ((CharArray) store).getCurrentStorageArray();
					final char[] chars = new char[planeSize];
					System.arraycopy(source, planeSize *
							(cIndex + (planeIndex * rgbChannelCount)), chars, 0, chars.length);
					array = chars;
				}
				else if (store instanceof DoubleArray) {
					final double[] source = ((DoubleArray) store).getCurrentStorageArray();
					final double[] doubles = new double[planeSize];
					System
					.arraycopy(source, planeSize *
							(cIndex + (planeIndex * rgbChannelCount)), doubles, 0,
							doubles.length);
					array = doubles;
				}
				else if (store instanceof FloatArray) {
					final float[] source = ((FloatArray) store).getCurrentStorageArray();
					final float[] floats = new float[planeSize];
					System.arraycopy(source, planeSize *
							(cIndex + (planeIndex * rgbChannelCount)), floats, 0, floats.length);
					array = floats;
				}
				else if (store instanceof IntArray) {
					final int[] source = ((IntArray) store).getCurrentStorageArray();
					final int[] ints = new int[planeSize];
					System.arraycopy(source, planeSize *
							(cIndex + (planeIndex * rgbChannelCount)), ints, 0, ints.length);
					array = ints;
				}
			}

			// Fallback default case - SLOW

			// Get dimensions array
			final long[] dimensions = new long[img.numDimensions()];
			img.dimensions(dimensions);

			// Truncate X, Y axes
			final long[] lengths = Arrays.copyOfRange(dimensions, 2, dimensions.length);

			// Get non-X,Y position array
			final long[] planePosition =
					FormatTools.rasterToPosition(lengths, planeIndex);

			// Copy plane positions back to dimensions array and set X, Y to start
			// at 0
			System.arraycopy(planePosition, 0, dimensions, 2, planePosition.length);
			dimensions[0] = dimensions[1] = 0;

			// Create a primitive array appropriate for the ImgPlus type
			final Class<?> typeClass = type.getClass();


			if (GenericIntType.class.isAssignableFrom(typeClass)) {
				array = new int[planeSize];
			}
			else if (GenericByteType.class.isAssignableFrom(typeClass)) {
				array = new byte[planeSize];
			}
			else if (GenericShortType.class.isAssignableFrom(typeClass)) {
				array = new short[planeSize];
			}
			else if (LongType.class.isAssignableFrom(typeClass)) {
				array = new long[planeSize];
			}
			else if (DoubleType.class.isAssignableFrom(typeClass)) {
				array = new double[planeSize];
			}
			else if (FloatType.class.isAssignableFrom(typeClass)) {
				array = new float[planeSize];
			}

			// Ensure we have a compatible type
			if (array == null) {
				throw new IllegalArgumentException("Unsupported ImgPlus data type: " +
						typeClass);
			}

			// Create a cursor and move it to the first position of the requested
			// plane
			RandomAccess<?> randomAccess = img.randomAccess();
			randomAccess.setPosition(dimensions);

			//TODO jump ahead to the requested channel? Not sure if that is needed or not..

			// Iterate over the positions in this plane, copying the values at
			// each position to the output array.
			int idx = 0;
			for (int i = 0; i < img.dimension(1); i++) {
				for (int j = 0; j < img.dimension(0); j++) {
					final Object value = randomAccess.get();

					if (GenericIntType.class.isAssignableFrom(typeClass)) {
						((int[]) array)[idx++] = (int) ((ComplexType<?>) value)
								.getRealDouble();
					}
					else if (GenericByteType.class.isAssignableFrom(typeClass)) {
						((byte[]) array)[idx++] = (byte) ((ComplexType<?>) value)
								.getRealDouble();
					}
					else if (GenericShortType.class.isAssignableFrom(typeClass)) {
						((short[]) array)[idx++] = (short) ((ComplexType<?>) value)
								.getRealDouble();
					}
					else if (LongType.class.isAssignableFrom(typeClass)) {
						((long[]) array)[idx++] = (long) ((ComplexType<?>) value)
								.getRealDouble();
					}
					else if (DoubleType.class.isAssignableFrom(typeClass)) {
						((double[]) array)[idx++] = ((ComplexType<?>) value).getRealDouble();
					}
					else if (FloatType.class.isAssignableFrom(typeClass)) {
						((float[]) array)[idx++] = (float) ((ComplexType<?>) value)
								.getRealDouble();
					}
					randomAccess.fwd(0);
				}
				dimensions[1]++;
				randomAccess.setPosition(dimensions);
			}

		}


		//-------------------------------------------------------------  Convert current plane if necessary


		final Metadata meta = w.getMetadata();
		final boolean interleaved =	meta.get(0).getInterleavedAxisCount() > 0;

		final long[] planarLengths = meta.get(0).getAxesLengthsPlanar();
		final long[] planarMin = SCIFIOMetadataTools.modifyPlanar(0, meta, new long[planarLengths.length]);
		final ByteArrayPlane destPlane = new ByteArrayPlane(Context.get().getContext(), meta.get(0), planarMin, planarLengths);

		Object curPlane = array;
		Class<?> planeClass = curPlane.getClass();
		byte[] sourcePlane = null;


		if (planeClass == int[].class) {
			sourcePlane = Bytes.fromInts((int[]) curPlane, false);
		}
		else if (planeClass == byte[].class) {
			sourcePlane = (byte[]) curPlane;
		}
		else if (planeClass == short[].class) {
			sourcePlane = Bytes.fromShorts((short[]) curPlane, false);
		}
		else if (planeClass == long[].class) {
			sourcePlane = Bytes.fromLongs((long[]) curPlane, false);
		}
		else if (planeClass == double[].class) {
			sourcePlane = Bytes.fromDoubles((double[]) curPlane, false);
		}
		else if (planeClass == float[].class) {
			sourcePlane = Bytes.fromFloats((float[]) curPlane, false);
		}
		else {
			throw new RuntimeException("Plane data type: " + planeClass + " not supported.", new ImgLibException());
		}

		if (interleaved) {
			final int bpp =
					FormatTools.getBytesPerPixel(meta.get(0).getPixelType());

			// TODO: Assign all elements in a for loop rather than
			// using many small System.arraycopy calls. Calling
			// System.arraycopy is less efficient than
			// element-by-element
			// copying for small array lengths (~24 elements or
			// less).
			// See: http://stackoverflow.com/a/12366983
			for (int i = 0; i < sourcePlane.length / bpp; i += bpp) {
				System.arraycopy(sourcePlane, i, destPlane.getData(),
						((i * rgbChannelCount) + cIndex) * bpp, bpp);
			}
		}
		else {
			// TODO: Consider using destPlane.setData(sourcePlane)
			// instead.
			// Ideally would also make modifications to avoid the
			// initial
			// allocation overhead of the destPlane's internal
			// buffer.
			System.arraycopy(sourcePlane, 0, destPlane.getData(), cIndex *
					sourcePlane.length, sourcePlane.length);
		}




		return destPlane;
	}



	@Override
	public void close() {
		try {
			w.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	




}
