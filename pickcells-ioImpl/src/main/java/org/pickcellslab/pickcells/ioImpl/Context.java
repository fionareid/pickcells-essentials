package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.IOException;

import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.bushe.swing.event.EventService;
import org.scijava.log.LogService;

import io.scif.FormatException;
import io.scif.ImageMetadata;
import io.scif.Reader;
import io.scif.SCIFIO;
import io.scif.config.SCIFIOConfig;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;

/**
 * Makes A Scifio instance singleton for this module
 * and centralise configuration (log level, checker config...)
 * 
 * @author Guillaume Blin
 *
 */
enum Context {
	INSTANCE;

	//Init Scifio
	private static final SCIFIO scif = new SCIFIO();	
	static{scif.log().setLevel(LogService.ERROR);}

	private static final SCIFIOConfig config =
			new SCIFIOConfig()
			.checkerSetOpen(true)
			.imgSaverSetWriteRGB(false);

	private static final String[] libs = {"lei", "lif", "lsm","mvd2","ome","ome.tiff","zvi"};
	private static final String[] folders = {"tif", "tiff", "ome","ome.tiff", "ics", "ids"};

	private Context(){
		Logger.getLogger(EventService.class).setLevel(Level.ERROR);
		//Logger.getLogger(FormatHandler.class).setLevel(Level.ERROR);
		//Logger.getLogger(Location.class).setLevel(Level.ERROR);
	}


	public static SCIFIO get(){
		return scif;
	};

	public static SCIFIOConfig config(){
		return config;
	}

	
	static SCIFIOConfig newConfig(){
		return new SCIFIOConfig()
				.checkerSetOpen(true)
				.imgSaverSetWriteRGB(false);
	}
	

	public static FileFilter libsFilter(){
		return new FileNameExtensionFilter("Image Libraries", libs);
	}
	
	
	public static FileFilter foldersFilter(){
		return new FileNameExtensionFilter("Image files", folders);
	}

	
	@SuppressWarnings("unchecked")
	public static <T extends RealType<T> & NativeType<T>> T createType(ImageMetadata md){
		return (T) scif.imgUtil().makeType(md.getPixelType());
	}


	public static Reader createReader(String path) throws IOException{
		Reader r;
		try {
			r = scif.initializer().initializeReader(path,config);
		} catch (FormatException e) {
			throw new IOException("Not a supported format",e);
		}
		return r;
	}



}
