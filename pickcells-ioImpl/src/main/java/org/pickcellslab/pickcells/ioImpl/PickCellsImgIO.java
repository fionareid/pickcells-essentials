package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryUsage;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.ImageIcon;

import org.apache.log4j.Logger;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.datamodel.types.MinimalImageInfo;
import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.io.ImgWriter;
import org.pickcellslab.pickcells.api.img.io.ImgsCheckerBuilder;
import org.pickcellslab.pickcells.api.img.io.ImgsChooserBuilder;

import io.scif.Format;
import io.scif.FormatException;
import io.scif.ImageMetadata;
import io.scif.Metadata;
import io.scif.Parser;
import io.scif.Plane;
import io.scif.Reader;
import io.scif.SCIFIO;
import io.scif.Writer;
import io.scif.config.SCIFIOConfig;
import io.scif.config.SCIFIOConfig.ImgMode;
import io.scif.gui.AWTImageTools;
import io.scif.img.IO;
import io.scif.img.ImgIOException;
import io.scif.img.ImgSaver;
import io.scif.img.ImgUtilityService;
import io.scif.util.FormatTools;
import net.imagej.axis.Axes;
import net.imagej.axis.AxisType;
import net.imagej.axis.CalibratedAxis;
import net.imagej.axis.DefaultLinearAxis;
import net.imglib2.Dimensions;
import net.imglib2.RandomAccessibleInterval;
import net.imglib2.exception.IncompatibleTypeException;
import net.imglib2.img.Img;
import net.imglib2.img.ImgFactory;
import net.imglib2.img.ImgView;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;
import net.imglib2.type.numeric.integer.ByteType;
import net.imglib2.type.numeric.integer.IntType;
import net.imglib2.type.numeric.integer.ShortType;
import net.imglib2.type.numeric.integer.UnsignedByteType;
import net.imglib2.type.numeric.integer.UnsignedIntType;
import net.imglib2.type.numeric.integer.UnsignedShortType;
import net.imglib2.type.numeric.real.FloatType;
import net.imglib2.util.Util;

@MetaInfServices(Module.class)
public class PickCellsImgIO implements ImgIO  {

	private static Logger log = Logger.getLogger(PickCellsImgIO.class);


	private static File tempFolder;

	public PickCellsImgIO() {


		URL u = getClass().getProtectionDomain().getCodeSource().getLocation();	    
		File dir = null;
		try {

			dir = new File(u.toURI()).getParentFile().getParentFile();
			tempFolder = new File(dir.getAbsolutePath()+File.separator+"tmp");
			if(!tempFolder.exists())
				tempFolder.mkdir();

			log.debug("Temp file : "+tempFolder);

			//	Runtime.getRuntime().addShutdownHook(new Thread(()->deleteTempFiles()));


		} catch (URISyntaxException e1) {
			log.error("Unable to obtain a temp folder", e1);
		}


	}





	private long getSize(long[] dims){
		long size = 1;
		for(int d = 0; d<dims.length; d++)
			size *= dims[d];
		return size;
	}


	private <T extends RealType<T>> boolean fitsInMemory(long[] dims, T type){

		long size = getSize(dims);



		log.trace("Checking if ("+type.getBitsPerPixel()+"bits) image fits into memory");
		int bitDepth = type.getBitsPerPixel()/8;
		log.trace("The bit depth is "+type.getBitsPerPixel());
		long memorySize = size * bitDepth;
		log.trace("The size in memory will be "+ memorySize/1000000 +" Mb");

		System.gc();

		final MemoryUsage mu = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage ();
		long free = mu.getMax() - mu.getUsed();

		long expected = free - memorySize;

		if(expected<0)
			return false;

		long thresh = (long) ((1f - (float)expected/(float)free) * (float)memorySize) ;

		//System.out.println("Current free memory" + free/1000000);
		//System.out.println("Image size = " + memorySize/1000000);
		//System.out.println("Expected Free Memory after loading = " + expected/1000000);
		//System.out.println("Threshold = " + thresh/1000000);

		return expected >= thresh;
	}



	//TODO there are a few verification we may add when opening images
	//Such as checking that the info in Image and the info in Img are consistent

	@SuppressWarnings("unchecked")
	public synchronized <T extends RealType<T> & NativeType<T>> Img<T> open(Image img) throws IOException{

		Img<T> opened = null;

		long[] dims = img.dimensions();
		long size = getSize(dims);

		//Find an appropriate type
		T type = (T) Context.get().getContext().getService(ImgUtilityService.class).makeType(img.bitDepth());

		if ( size > Integer.MAX_VALUE || !fitsInMemory(dims, type))
			opened = openAsCellImg(img.path(), type);
		else 
			opened = openInMemory(img.path(), type);

		return opened;
	}






	@SuppressWarnings("unchecked")
	public synchronized <T extends RealType<T> & NativeType<T>> Img<T> open(LabelsImage result) throws IOException{

		Img<T> opened = null;

		Image img = result.origin();

		long[] dims = result.dimensions();
		long size = getSize(dims);

		//Find an appropriate type
		T type = (T) Context.get().getContext().getService(ImgUtilityService.class).makeType(result.bitDepth());

		if ( size > Integer.MAX_VALUE || !fitsInMemory(dims, type))
			opened = openAsCellImg(img.getAttribute(Image.loc).get()+File.separator+result.fileName(), type);
		else 
			opened = openInMemory(img.getAttribute(Image.loc).get()+File.separator+result.fileName(), type);

		return opened;
	}





	@Override
	public <T extends NativeType<T>> String standard(RandomAccessibleInterval<T> img) {

		long[] dims = new long[img.numDimensions()];
		img.dimensions(dims);		
		return standard(dims, pixelType(img.randomAccess().get()));

	}



	@Override
	public String standard(long[] dims, int pxType){

		return ".ids";
		/*
		long size = getSize(dims);

		size *= FormatTools.getBytesPerPixel(pxType);

		if(size > 4200000000l)
			return ".ics";//TODO change to nrrd
		else
			return ".tif";
		 */

	}







	public <T extends NativeType<T>> int byteSize(T type) {

		if(type instanceof UnsignedByteType || type instanceof ByteType)
			return 1;
		if(type instanceof UnsignedShortType || type instanceof ShortType)
			return 2;
		if(type instanceof UnsignedIntType || type instanceof IntType || type instanceof FloatType)
			return 3;

		return 4;
	}





	public <T extends NativeType<T>> int pixelType(T type) {
		if(type instanceof UnsignedByteType)
			return FormatTools.UINT8;
		if(type instanceof ByteType)
			return FormatTools.INT8;
		if(type instanceof UnsignedShortType)
			return FormatTools.UINT16;
		if(type instanceof ShortType)
			return FormatTools.INT16;
		if(type instanceof UnsignedIntType)
			return FormatTools.UINT32;
		if(type instanceof IntType)
			return FormatTools.INT32;
		if(type instanceof FloatType)
			return FormatTools.FLOAT;
		return FormatTools.DOUBLE;
	}




	@Override
	public <T extends RealType<T> & NativeType<T>> Img<T> openAsCellImg(String path, T type) throws IOException {

		log.info("Opening as CellImg");

		SCIFIO scif = Context.get();
		SCIFIOConfig config = Context.newConfig().imgOpenerSetImgModes(ImgMode.CELL);

		Reader r;
		try {
			r = scif.initializer().initializeReader(path,config);
		} catch (FormatException e) {
			throw new IOException("Not a supported format",e);
		}

		long before = System.currentTimeMillis();

		Img<T> img = (Img<T>) IO.openImgs(r,type.createVariable(),config).get(0);
		long after = System.currentTimeMillis();

		log.debug("Image import time : "+(after-before)+"ms");

		return img;

	}





	@Override
	public <T extends RealType<T> & NativeType<T>> Img<T> openInMemory(String path, T type) throws IOException {


		SCIFIO scif = Context.get();
		SCIFIOConfig config = Context.newConfig().imgOpenerSetImgModes(ImgMode.ARRAY);



		Reader r;
		try {
			r = scif.initializer().initializeReader(path,config);
		} catch (FormatException e) {
			throw new IOException("Not a supported format",e);
		}

		long before = System.currentTimeMillis();

		Img<T> img = (Img<T>) IO.openImgs(r,type.createVariable(),config).get(0);

		long after = System.currentTimeMillis();

		log.debug("Image import time : "+(after-before)+"ms");

		return img;
	}







	@Override
	public <T extends NativeType<T>> void save(Image i, RandomAccessibleInterval<T> data, String path) throws IOException {

		Objects.requireNonNull(i,"The provided image is null");
		Objects.requireNonNull(data, "The provided RandomAccessible is null");

		save(i.getMinimalInfo(),data,path);

	}




	@Override
	public <T extends NativeType<T>> void save(LabelsImage info, RandomAccessibleInterval<T> data, String path) throws IOException {
		Objects.requireNonNull(info,"The provided LabelsImage is null");
		MinimalImageInfo i = info.origin().getMinimalInfo().removeDimension(Image.c);
		save(i,data,path);
	}





	@SuppressWarnings({"unchecked","rawtypes"})
	@Override
	public void importImages(ImgFileList list, String destination, String namePattern) throws IOException {		


		int counter = 0;
		for(int i = 0; i<list.numDataSets(); i++){
			for(int j = 0; j<list.numImages(i); j++){
				
				final Img img = list.load(i, j);
				String file = null;
				if(namePattern==null)
					file = destination+File.separator+list.name(i,j).substring(0, list.name(i,j).lastIndexOf('.'))+standard(img);
				else
					file = destination+File.separator+namePattern+"_"+ counter++ +standard(img);
					
				
				//TODO use system file copy if ics
				//String name = list.name(i, j);
				//if(name.endsWith(".ics") || name.endsWith(".ids")){
				//	File in = new File(list.path(i)+File.separator+list.name(i,j));
				//	File out = new File(destination+File.separator+namePattern+"_"+ counter++ +name.substring(name.length()-4, name.length()));
				//	Files.copy(in.toPath(), out.toPath());
				//}else{
				
				save(list.getMinimalInfo(i, j), img, file);					
				//}
			}
		}
	}



	@Override
	public ImageIcon thumbnail(Image img, long[] min, long[] max, int timeFrame) throws IOException {

		SCIFIO scif = Context.get();
		SCIFIOConfig config = Context.newConfig()/*.imgOpenerSetImgModes(ImgMode.CELL)*/.imgOpenerSetComputeMinMax(true);


		// Initialise the Reader
		Reader r;
		try {
			r = scif.initializer().initializeReader(img.path(),config);
		} catch (FormatException e) {
			throw new IOException("Not a supported format",e);
		}


		try {

			// Find out which plane(s) to load

			ImageMetadata iMeta = r.getMetadata().get(0);

			int depth = img.depthIndex();
			if(img.channelIndex()!=-1 && depth>img.channelIndex())
				depth--;

			int planeIndex = img.depth() == 1 ? 0 : (int) (min[depth] + (max[depth] - min[depth])/2);
			planeIndex *= timeFrame+1;
			planeIndex = iMeta.getPlanarAxisCount() == 2 ? planeIndex*img.channels().length : planeIndex;

			System.out.println("Using plane :"+planeIndex+" for thumbnail");

			int w = (int) (max[Image.x] - min[Image.x]);		
			int h = (int) (max[Image.y] - min[Image.y]);	

			Plane plane = null;
			BufferedImage thumb = null;


			System.out.println("Channel in meta : "+ iMeta.getAxisIndex(Axes.CHANNEL));
			System.out.println("Planar axe count in meta : "+iMeta.getPlanarAxisCount());

			long[] axes = img.channelIndex() == -1 ? new long[2] : new long[3];
			axes[0] = img.width(); axes[1] = img.height();


			// Handle single/multi-channel case / planar / sequential case
			if(img.channelIndex() != -1)
				axes[2] = img.channels().length;


			if(img.channelIndex() != -1 && iMeta.getPlanarAxisCount() == 2){



				BufferedImage[] planes = new BufferedImage[img.channels().length];
				for(int c = 0; c<planes.length; c++){
					planes[c] = AWTImageTools.openImage( r.openPlane(0, planeIndex+c), r, axes, 0);
					planes[c] = planes[c].getSubimage((int)min[Image.x], (int)min[Image.y], w, h);
					planes[c] = AWTImageTools.scale(planes[c], (int) iMeta.getThumbSizeX(), (int) iMeta.getThumbSizeY(), false);
					planes[c] = AWTImageTools.autoscale( planes[c] );
				}

				thumb = AWTImageTools.mergeChannels(planes);
				System.out.println("Channels merged!");


			}
			else{	 



				plane = r.openPlane(0, planeIndex);

				System.out.println("Plane lengths : "+Arrays.toString(plane.getLengths()));


				thumb = AWTImageTools.openImage(plane, r, axes, 0);
				thumb = thumb.getSubimage((int)min[Image.x], (int)min[Image.y], w, h);
				thumb = AWTImageTools.scale(thumb, (int) iMeta.getThumbSizeX(), (int) iMeta.getThumbSizeY(), false);
				thumb = AWTImageTools.autoscale(thumb);
			}

			System.out.println("axes : "+Arrays.toString(axes));


			if(thumb == null)
				return new ImageIcon(getClass().getResource("/icons/failed.png"));
			return new ImageIcon(thumb);


		} catch (Exception e) {
			e.printStackTrace();
			return new ImageIcon(getClass().getResource("/icons/failed.png"));
		}



	}










	@Override
	public <T extends NativeType<T>> void save(MinimalImageInfo info,  RandomAccessibleInterval<T> data, String dest) throws IOException{

		long before = System.currentTimeMillis();


		Img<T> img = null;
		if(data instanceof Img)
			img = (Img<T>) data;
		else{
			//Wrap into an ImgView
			ImgFactory<T> factory = Util.getArrayOrCellImgFactory(data, data.randomAccess().get().createVariable());
			img = ImgView.wrap(data, factory);
		}


		saveImg(info, img, dest);


		long after = System.currentTimeMillis();
		log.info("Done saving image -> "+(after-before)+"ms");		

	}







	@Override
	public <T extends NativeType<T> & RealType<T>> Img<T> createImg(Dimensions dim, T type){


		// Estimate the amount required :
		long[] dims = new long[dim.numDimensions()];
		dim.dimensions(dims);



		if(fitsInMemory(dims, type)){
			return Util.getArrayOrCellImgFactory(dim, type).create(dim, type);
		}
		else{

			try{

				log.info("A temporary image ( which does not fit in RAM) is being created on the file system...");

				int[] order = new int[5];
				Arrays.fill(order, -1);
				for(int d = 0; d<dims.length; d++)
					order[d] = d;

				log.trace("Order = "+Arrays.toString(order));

				String path = this.saveTmpImg(new MinimalImageInfo(order,dims), type, standard(dims, pixelType(type) ));

				log.info("Image created on disk");

				return this.openAsCellImg(path, type);

			}
			catch(IOException e){
				throw new RuntimeException("ImgIO unable to create a temporary disk image", e);
			}
		}


	}






	private <T extends NativeType<T>> void saveImg(MinimalImageInfo info, Img<T> img, String dest) throws IOException {




		System.out.println("saving the image");


		if (!RealType.class.isAssignableFrom(img.firstElement().getClass()))
			throw new IOException("Unsuported type : "+img.firstElement().getClass().getSimpleName());


		File destination = new File(dest);
		//TODO if ics also delete companion file
		if(destination.exists())
			if(!destination.delete())
				throw new IOException("No permission to overwrite the already existing file : "+destination);


		Writer w = this.createScifioWriter(info, img.firstElement(), dest);

		try {

			new ImgSaver().saveImg(w, img);

		} catch (ImgIOException | IncompatibleTypeException e) {
			throw new IOException(e);
		}
		finally{
			w.close();
		}


	}







	private AxisType getAxisType(int i) {
		switch(i){
		case Image.x : return Axes.X;	
		case Image.y : return Axes.Y;	
		case Image.z : return Axes.get("Z");	
		case Image.c : return Axes.CHANNEL;
		case Image.t : return Axes.TIME;	
		}
		return null;
	}



	private static AtomicInteger temp = new AtomicInteger();


	private <T extends NativeType<T> & RealType<T>> String saveTmpImg(MinimalImageInfo info, T type, String suffix) throws IOException {

		System.out.println("Creating Image on disk");

		int id = temp.incrementAndGet();
		String path = tempFolder+File.separator+"tmpImg"+id+suffix;
		Writer sw = createScifioWriter(info, type, path);
		ImgWriter<T> w = new PickCellsImgWriter<>(sw, type);
		for(int c = 0; c<w.channelNumber(); c++){		
			for(int t = 0; t<w.frameNumber(); t++){
				RandomAccessibleInterval<T> frame = w.createNewChannelFrame();	
				w.writeChannelFrame(frame, c, t);
			}
		}
		System.out.println("Image Created!");

		try {
			Parser parser = sw.getFormat().createParser();
			System.out.println("Parser created!");
			parser.parse(path);
			String[] toDel = parser.getUsedFiles();
			
			for(int i = 0; i<toDel.length; i++){
				File file = new File(toDel[i]);
				System.out.println("Deleting : "+file);
				file.deleteOnExit();
			}

		} catch (FormatException e) {
			throw new IOException(e);
		}


		System.out.println("Done creating image");

		return path;


	}






	@Override
	public ImgsChooserBuilder createChooserBuilder() {
		return new PickCellsChooserBuilder();
	}





	@Override
	public ImgsCheckerBuilder createCheckerBuilder() {
		return new PickCellsCheckerBuilder();
	}


	@Override
	public ImgFileList emptyList() {
		return new PickCellsImgFiles(Collections.emptyList());
	}






	@Override
	public <T extends NativeType<T> & RealType<T>> ImgWriter<T> createWriter(MinimalImageInfo info, T type, String dest) throws IOException {
		return new PickCellsImgWriter<>(createScifioWriter(info, type, dest), type);
	}



	@Override
	public <T extends NativeType<T> & RealType<T>> ImgWriter<T> createWriter(String file) throws IOException {
		return imgFileListFrom(Collections.singletonList(new File(file))).createWriterHandle(0, 0);
	}




	private <T> Writer createScifioWriter(MinimalImageInfo info, T type, String dest) throws IOException{
		// Create writer

		System.out.println("saving the image");


		if (!RealType.class.isAssignableFrom(type.getClass()))
			throw new IOException("Unsuported type : "+type.getClass().getSimpleName());


		Writer w = null;

		try{


			File destination = new File(dest);



			//Check the format and delete if file already exists
			Format f = Context.get().format().getFormat(dest);
			if(destination.exists()){
				Parser parser = f.createParser();
				parser.parse(destination);
				String[] toDel = parser.getUsedFiles();
				System.out.println("Number of files to delete : "+toDel.length);
				for(int i = 0; i<toDel.length; i++){
					File file = new File(toDel[i]);
					System.out.println("Deleting : "+file);
					if(file.exists() && !file.delete())
						throw new IOException("No permission to overwrite the already existing file : "+file);
				}
			}




			// Create Metadata
			Metadata md = f.createMetadata();

			md.createImageMetadata(1);
			ImageMetadata imd = md.get(0);


			List<CalibratedAxis> axes = new ArrayList<>();


			imd.setName(destination.getName());


			//Create Axes
			long[] dims = info.imageDimensions();
			System.out.println("dims from minimal -> "+Arrays.toString(dims));
			System.out.println("cal from minimal -> "+Arrays.toString(info.imageCalibration()));
			System.out.println("units from minimal -> "+Arrays.toString(info.imageUnits()));
			for(int i = 0; i<dims.length; i++){ 
				if(dims[i]!=-1){
					int at = info.axisType(i);
					DefaultLinearAxis axis = new DefaultLinearAxis(getAxisType(at), dims[i]);
					axis.setScale(info.calibration(at));
					axis.setUnit(info.unit(at));
					axes.add(axis);
				}
			}

			/*
					for(int i = 0; i<order.length; i++){ 
						if(order[i]!=-1){
							DefaultLinearAxis axis = new DefaultLinearAxis(getAxisType(order[i]), dims[order[i]]);
							//TODO add cal axis.setScale(cal[icsOrder[i]]);
							axes.add(axis);
						}
					}
					//reorder according to ordering
					Collections.sort(axes,(a1,a2)->Integer.compare(order[getIndex(a1.type())], order[getIndex(a2.type())]));
			 */




			@SuppressWarnings("rawtypes")
			int bits = ((RealType)type).getBitsPerPixel();


			imd.populate(destination.getName(),
					axes,
					info.imageDimensions(),
					Context.get().imgUtil().makeType(type),
					bits,
					true, false,
					false, false, true);




			log.debug("Dims in metadata : " +Arrays.toString(imd.getAxesLengths()));
			log.debug("MultiChannel : "+imd.isMultichannel());


			imd.getAxes().forEach(a->{
				System.out.println("Axis Type = "+a.type()+" ; Axis Length "+imd.getAxisLength(a.type()));			
			});



			System.out.println(md.get(0).toString());



			w = f.createWriter();
			log.debug("Writer class : "+f.getWriterClass());



			SCIFIOConfig config = Context.newConfig();

			System.out.println(dest);


			w.setMetadata(md);
			w.setDest(dest, 0, config);	

			return w;

		}catch(FormatException | ImgIOException e){
			throw new IOException(e);
		}
	}





	@Override
	public ImgFileList imgFileListFrom(List<File> files) throws IOException {
		//FIXME for each file check it exists and determine if files in folder or if library
		// For now we just assume all the files can be hanled by one FolderHandler
		List<DiskImgHandler> list = new ArrayList<>(1);
		list.add(new FolderHandler(files.toArray(new File[files.size()])));
		return new PickCellsImgFiles(list);
	}











}
