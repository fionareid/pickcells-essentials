package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;

import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgsChooser;

import io.scif.ImageMetadata;
import io.scif.Reader;
import io.scif.SCIFIO;



@SuppressWarnings("serial")
public class PickCellsImgsChooser extends JDialog implements ImgsChooser {

	private final JComboBox<DiskImgHandler> comboBox;
	private boolean wasCancelled = true;
	private final int[] coords = {0,-1};

	public PickCellsImgsChooser(String title, String homeFolder) {//TODO add message string to document the use of the chooser

		
		JButton btnOk = new JButton("Ok");
		btnOk.setEnabled(false);
		btnOk.addActionListener(l->{
			wasCancelled = false;
			this.dispose();
		});

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(l->{
			this.dispose();
		});

		JScrollPane scrollPane = new JScrollPane();
		JList<JLabel> list = new JList<>();
		list.setCellRenderer(new JLabelRenderer());
		DefaultListModel<JLabel> model = new DefaultListModel<>();
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setModel(model);
		scrollPane.setViewportView(list);


		comboBox = new JComboBox<>();
		comboBox.addActionListener(l->{
			coords[0] = comboBox.getSelectedIndex();
			model.removeAllElements();
			DiskImgHandler h = (DiskImgHandler) comboBox.getSelectedItem();
			if(h!=null){

				for(int i = 0; i<h.numImages(); i++){

					// Create the text info and wrap in a JLabel					
					ImageMetadata imd = h.imageMetadata(i);
					String info = "<HTML><b>"+h.getName(i)+"</b>";
					for(int a = 0; a<imd.getAxes().size(); a++){
						info += "<br>"+imd.getAxis(a).type().getLabel()
								+ " : "+imd.getAxis(a).calibratedValue(1)+"</br>"
								+" ("+imd.getAxisLength(a)+")";
					}
					JLabel label = new JLabel();
					label.setText(info);
					label.setBorder(new EmptyBorder(5,5,5,5));
					System.out.println(getClass().getResource("").toString());
					label.setIcon(new ImageIcon(getClass().getResource("/icons/loading.png")));
					model.addElement(label);


					final int j = i;
					try{
						new Thread(()->{
							label.setIcon(h.thumbnail(j));
							this.revalidate();
							this.repaint();
						}).start();
					}catch(Exception e){
						label.setIcon(new ImageIcon(getClass().getResource("/icons/failed.png")));
						this.revalidate();
						this.repaint();
					}
				}
				btnOk.setEnabled(true);
			}
		});
		
		list.addListSelectionListener(l -> coords[1] = list.getSelectedIndex());
		


		JButton libBtn = new JButton("Add Library");
		libBtn.addActionListener(l->{
			SCIFIO scif = Context.get();	
			JFileChooser chooser = scif.gui().buildFileChooser(new FileFilter[]{Context.libsFilter()}, false);
			chooser.setMultiSelectionEnabled(false);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setDialogTitle(title);
			chooser.setCurrentDirectory(new File(homeFolder));
			int r = chooser.showDialog(null, "Select");
			if (r == JFileChooser.APPROVE_OPTION) {	
				int[] choice = askForSubset(chooser.getSelectedFile());
				if(choice.length == 0)
					return;
				else
					try {
						comboBox.addItem(new LibHandler(chooser.getSelectedFile(), choice));
					} catch (Exception e) {
						e.printStackTrace();
						JOptionPane.showMessageDialog(null, "An error occured : "+e.getMessage());
					}
			}
		});

		
		JButton imBtn = new JButton("Add Images");
		imBtn.addActionListener(l->{
			JFileChooser chooser =  new JFileChooser();
			chooser.setFileFilter(Context.foldersFilter());
			chooser.setMultiSelectionEnabled(true);
			chooser.setDialogTitle(title);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setCurrentDirectory(new File(homeFolder));
			int r = chooser.showDialog(null, "Select");
			if (r == JFileChooser.APPROVE_OPTION) {
				try {
					comboBox.addItem(new FolderHandler(chooser.getSelectedFiles()));
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "An error occured : "+e.getMessage());
				}
			}
		});






		//Layout
		//----------------------------------------------------------------------------------------------------------

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 174, GroupLayout.PREFERRED_SIZE)
										.addGap(18)
										.addComponent(libBtn)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(imBtn))
								.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 411, GroupLayout.PREFERRED_SIZE))
						.addContainerGap(23, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
						.addContainerGap(223, Short.MAX_VALUE)
						.addComponent(btnOk)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnCancel)
						.addGap(19))
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(libBtn)
								.addComponent(imBtn))
						.addGap(18)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnOk)
								.addComponent(btnCancel))
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);


		getContentPane().setLayout(groupLayout);
	}


	private int[] askForSubset(File selectedFile) {

		Reader r = null;
		try {
			r = Context.createReader(selectedFile.getPath());
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Unable to open this file, sorry!");
			return new int[]{};
		}

		SubsetDialog d = new SubsetDialog(r);
		d.setModal(true);
		d.setPreferredSize(new Dimension(400,400));
		d.pack();
		d.setLocationRelativeTo(null);
		d.setVisible(true);		


		return d.getSelected();
	}


	public void setVisible(boolean isVisible){
		this.pack();
		this.setLocationRelativeTo(null);		
		super.setVisible(isVisible);
	}

	@Override
	public boolean wasCancelled() {
		return wasCancelled;
	}

	@Override
	public ImgFileList getChosenList() {
		if(!wasCancelled){
			List<DiskImgHandler> list = new ArrayList<>();
			for(int i = 0; i<comboBox.getItemCount(); i++)
				list.add(comboBox.getItemAt(i));
			return new PickCellsImgFiles(list);
		}
		return null;
	}


	@Override
	public int[] getSelectedImage() {
		return coords;
	}
}
