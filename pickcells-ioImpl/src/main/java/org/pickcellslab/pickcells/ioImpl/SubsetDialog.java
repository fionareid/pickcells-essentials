package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import io.scif.FormatException;
import io.scif.ImageMetadata;
import io.scif.Metadata;
import io.scif.Plane;
import io.scif.Reader;
import io.scif.gui.AWTImageTools;

@SuppressWarnings("serial")
public class SubsetDialog extends JDialog{



	private final Reader reader;
	private int[] selected = new int[]{};
	private boolean wasCanceled = true;



	public SubsetDialog(Reader r) {


		this.reader = r;


		getContentPane().setLayout(new BorderLayout(0, 0));


		JPanel scrollPanel = new JPanel();
		scrollPanel.setBorder(new TitledBorder(null, "Library Content", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(scrollPanel, BorderLayout.CENTER);
		scrollPanel.setLayout(new BoxLayout(scrollPanel, BoxLayout.X_AXIS));

		JScrollPane scroll = new JScrollPane();
		scroll.getVerticalScrollBar().setUnitIncrement(16);
		scrollPanel.add(scroll);

		JPanel cbPanel = new JPanel();

		//Put in the scroll tickboxeswith preview
		Metadata md = r.getMetadata();
		

		List<JCheckBox> boxList = new ArrayList<>(r.getImageCount());
		
		
		for(int n = 0; n<r.getImageCount(); n++){

			//Panel holding the preview
			JPanel preview = new JPanel();
			preview.setLayout(new BoxLayout(preview, BoxLayout.X_AXIS));
			
			
			
			
			//Obtain a summary of the image
			ImageMetadata imd = md.get(n);
			String info = "<HTML><b>"+imd.getTable().getOrDefault("Image name", "Series "+n)+"</b>";
			for(int a = 0; a<imd.getAxes().size(); a++)
				info += "<br>"+imd.getAxis(a).type().getLabel()
				+ " : "+imd.getAxis(a).calibratedValue(1)+"</br>"
				+" ("+imd.getAxisLength(a)+")";

			JCheckBox checkBox = new JCheckBox(info);
			boxList.add(checkBox);
			checkBox.setSelected(true);
			checkBox.setBorder(new EmptyBorder(5, 5, 5, 5));
			
			
			preview.add(checkBox);
			
			JLabel icon = new JLabel(new ImageIcon(getClass().getResource("/icons/loading.png")));
			
			preview.add(icon);

			int j = n;
			try{
				new Thread(()->icon.setIcon(thumbnail(imd,j))).start();
			}catch(Exception e){
				icon.setIcon(new ImageIcon(getClass().getResource("/icons/failed.png")));
			}			
			
			
			cbPanel.add(preview);
		}

		scroll.setViewportView(cbPanel);
		cbPanel.setLayout(new BoxLayout(cbPanel, BoxLayout.Y_AXIS));



		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.SOUTH);

		JButton btnOk = new JButton("OK");
		panel.add(btnOk);

		btnOk.addActionListener(l->{
			ArrayList<Integer> subset = new ArrayList<>(r.getImageCount());
			for(int c = 0; c<r.getImageCount(); c++)
				if(boxList.get(c).isSelected())
					subset.add(c);
			selected = new int[subset.size()];
			for(int i = 0; i<selected.length; i++)
				selected[i] = subset.get(i);
			wasCanceled = false;
			
			System.out.println(Arrays.toString(selected));
			
			
			this.dispose();
		});



		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(l->{
			selected = new int[]{};
			this.dispose();
		});
		panel.add(btnCancel);




		//Layout Components



	}


	
	public boolean wasCanceled(){
		return wasCanceled;
	}
	
	

	public int[] getSelected(){
		return selected;
	}





	synchronized Icon thumbnail(ImageMetadata iMeta, int i) {

		final long planeIndex = iMeta.getPlaneCount() / 2;
		Plane plane = null;
		BufferedImage thumb = null;
		try {
			plane = reader.openPlane(i, planeIndex);
			thumb =	AWTImageTools.openThumbImage(plane, reader, i, iMeta
					.getAxesLengthsPlanar(), (int) iMeta.getThumbSizeX(), (int) iMeta
					.getThumbSizeY(), false);
		} catch (FormatException | IOException e) {
			e.printStackTrace();
		}
		if(thumb == null)
			return new ImageIcon(getClass().getResource("/icons/failed.png"));
		return new ImageIcon(thumb);
	}




}
