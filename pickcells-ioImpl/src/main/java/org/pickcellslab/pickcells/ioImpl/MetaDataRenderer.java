package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.Component;
import java.util.Arrays;

import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.pickcellslab.foundationj.ui.renderers.SLabel;

import io.scif.ImageMetadata;


public class MetaDataRenderer implements ListCellRenderer<ImageMetadata> {

	@Override
	public Component getListCellRendererComponent(JList<? extends ImageMetadata> list, ImageMetadata value, int index,
			boolean isSelected, boolean cellHasFocus) {

		SLabel label = new SLabel();
		
		
		if(value != null){
			System.out.println("Name = " + value.getName());
			label.setText(value.getName());
			
			//Tooltip	
			StringBuilder b = new StringBuilder();
			b.append("<HTML><b> Details: </b>");
			value.getTable().forEach((s,o)-> {
				String objectString = o.toString();
				if(o.getClass().isArray())
					objectString = Arrays.toString((String[])o);
				b.append("<br>"+s+" : "+ objectString);
			});
			b.append("</HTML>");
			
			label.setToolTipText(b.toString());
			
			if(isSelected)
				label.setForeground(Color.BLACK);
			else
				label.setForeground(Color.LIGHT_GRAY);			
		}


		return label;
	}

	
	
	
	
	
	
}
