package org.pickcellslab.pickcells.ioImpl;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.ExecutionException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.pickcells.api.app.charts.Charts;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.LabelsImage;
import org.pickcellslab.pickcells.api.img.editors.SegmentationEditorControllers;
import org.pickcellslab.pickcells.api.img.editors.SegmentedObjectManager;
import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.io.ImgsChecker;
import org.pickcellslab.pickcells.api.img.io.ImgsChooser;
import org.pickcellslab.pickcells.api.img.io.StaticImgIO;
import org.pickcellslab.pickcells.api.img.providers.NotInProductionException;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactory;
import org.pickcellslab.pickcells.api.img.providers.ProviderFactoryUtils;
import org.pickcellslab.pickcells.api.img.providers.SegmentationImageProvider;
import org.pickcellslab.pickcells.api.img.view.ImageDisplay;
import org.pickcellslab.pickcells.api.img.view.MouseDisplayEventModel.SelectionMode;
import org.pickcellslab.pickcells.impl.providers.PickCellsProviderFactory;
import org.pickcellslab.pickcells.ioImpl.PickCellsImgIO;
import org.pickcellslab.pickcells.jfree.JFSimpleHistogramFactory;

import com.alee.laf.WebLookAndFeel;

import net.imglib2.img.Img;
import net.imglib2.type.NativeType;
import net.imglib2.type.numeric.RealType;

public class AnnotatedViewTest<T extends RealType<T> & NativeType<T>> {


	private static final ImgIO io = new PickCellsImgIO();


	public void run() throws InterruptedException, ExecutionException, NotInProductionException{

		new StaticImgIO(io);
		new Charts(Collections.singletonList(new JFSimpleHistogramFactory()));

		ProviderFactory pf = new PickCellsProviderFactory(null, 2);

		// Choose color image
		ImgsChooser chooser = io.createChooserBuilder()
				.setTitle("Choose a color image for the test")
				.build();
		chooser.setModal(true);
		chooser.setVisible(true);


		if(!chooser.wasCancelled()){

			ImgFileList list = chooser.getChosenList();

			System.out.println("Number of datasets = "+list.numDataSets());
			System.out.println("Number of images = "+list.numImages());

			ImgsChecker checker = 
					io.createCheckerBuilder()
					.addCheck(ImgsChecker.calibration)
					.addCheck(ImgsChecker.dimensions)
					.addCheck(ImgsChecker.ordering)
					.addCheck(ImgsChecker.nChannels)
					.addCheck(ImgsChecker.extension)
					.addCheck(ImgsChecker.sUnit)
					.addCheck(ImgsChecker.bitDepth)
					.build(list);

			System.out.println("Chosen list is consistent : "+checker.isConsistent());

			for(AKey<?> k : checker.tested())
				System.out.println(k.name+" = "+checker.value(k));



			int[] order = ImgsChecker.convert(checker.value(ImgsChecker.ordering,list,0,0));
			double[] cal = ImgsChecker.convert(order, checker.value(ImgsChecker.calibration,list,0,0));

			System.out.println("Order = " + Arrays.toString(order));
			System.out.println("Calibration = " + Arrays.toString(cal));


			// Build an Image object

			Image image = ProviderFactoryUtils.createFakeImage(checker, list, 0, 0);
			Img<T> color = list.load(0, 0);

			// Choose segmentation image

			ImgsChooser chooser2 = io.createChooserBuilder()
					.setTitle("Choose the associated segmentation result")
					.build();
			chooser2.setModal(true);
			chooser2.setVisible(true);


			if(!chooser2.wasCancelled()){

				ImgFileList list2 = chooser2.getChosenList();

				ImgsChecker checker2 = 
						io.createCheckerBuilder()
						.addCheck(ImgsChecker.calibration)
						.addCheck(ImgsChecker.dimensions)
						.addCheck(ImgsChecker.ordering)
						.addCheck(ImgsChecker.nChannels)
						.addCheck(ImgsChecker.extension)
						.addCheck(ImgsChecker.sUnit)
						.addCheck(ImgsChecker.bitDepth)
						.build(list2);



				LabelsImage segResult = ProviderFactoryUtils.createLabels(image, checker2, list2, 0, 0);
				// Now get the providers
				SegmentationImageProvider<T> provider = pf.createFrom(segResult, (Img)list2.load(0, 0));
				provider.setPrototype(segResult, SegmentedObjectManager.createPrototype(), false);
				// Now setup the view
				SegmentedObjectManager<T> manager = new SegmentedObjectManager<>(provider);

				ImageDisplay view = ImageDisplay.createDisplay();
				view.addImage(image.getMinimalInfo(), color);
				view.addAnnotationLayer("", manager);
				
				view.getMouseModel().setRotationEnabled(false);
				view.getMouseModel().setSelectionMode(SelectionMode.SINGLE);
				
				JPanel panel = new JPanel();
				panel.setLayout(new BorderLayout());
				panel.add(SegmentationEditorControllers.createControls(manager, view),BorderLayout.NORTH);
				panel.add(view.getView(), BorderLayout.CENTER);

				JFrame f = new JFrame();
				f.setContentPane(panel);
				f.pack();
				f.setLocationRelativeTo(null);
				f.setVisible(true);

				view.refreshDisplay();


				Thread.sleep(300000);

			}

		}



	}

















	public static void main(String[] args) throws InterruptedException, ExecutionException, NotInProductionException {



		// Setup the look and feel
		try {
			UIManager.setLookAndFeel ( WebLookAndFeel.class.getCanonicalName () );
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebLookAndFeel.initializeManagers ();


		new AnnotatedViewTest<>().run();

	}

}
