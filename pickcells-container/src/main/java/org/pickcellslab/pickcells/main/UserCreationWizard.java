package org.pickcellslab.pickcells.main;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.util.Collection;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.pickcellslab.foundationj.dbm.Credentials;
import org.pickcellslab.foundationj.dbm.User;
import org.pickcellslab.foundationj.dbm.User.priviledge;

@SuppressWarnings("serial")
class UserCreationWizard extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private User created;


	/**
	 * Create the dialog.
	 */
	UserCreationWizard(Collection<String> forbiddenNames, priviledge p, boolean cancellable) {

		setTitle("New User...");
		setModal(true);
		setLocationRelativeTo(null);
		if(!cancellable)
			this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		setBounds(100, 100, 275, 192);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		JLabel lblUserName = new JLabel("User Name");

		textField = new JTextField();
		textField.setColumns(10);

		JLabel lblPassword = new JLabel("Enter Password");

		passwordField = new JPasswordField();
		passwordField.setEchoChar('*');

		JLabel lblConfirmPassword = new JLabel("Confirm Password");

		passwordField_1 = new JPasswordField();
		passwordField_1.setEchoChar('*');

		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_contentPanel.createSequentialGroup()
												.addComponent(lblConfirmPassword, GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE)
												.addPreferredGap(ComponentPlacement.RELATED))
												.addGroup(gl_contentPanel.createSequentialGroup()
														.addComponent(lblPassword, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
														.addPreferredGap(ComponentPlacement.RELATED)))
														.addGroup(gl_contentPanel.createSequentialGroup()
																.addComponent(lblUserName)
																.addPreferredGap(ComponentPlacement.RELATED)))
																.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING, false)
																		.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
																		.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
																		.addComponent(passwordField_1, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE))
																		.addGap(180))
				);
		gl_contentPanel.setVerticalGroup(
				gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
						.addContainerGap()
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPanel.createSequentialGroup()
										.addGap(6)
										.addComponent(lblUserName)
										.addGap(18)
										.addComponent(lblPassword)
										.addGap(18)
										.addComponent(lblConfirmPassword))
										.addGroup(gl_contentPanel.createSequentialGroup()
												.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(passwordField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
												.addContainerGap(114, Short.MAX_VALUE))
				);
		gl_contentPanel.linkSize(SwingConstants.HORIZONTAL, new Component[] {textField, passwordField, passwordField_1});
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(e->{

					String userName = textField.getText();
					if(userName.isEmpty()){
						JOptionPane.showMessageDialog(null, "The user name cannot be empty");
						return;
					}
					if(forbiddenNames.contains(userName)){
						JOptionPane.showMessageDialog(null, "This user name already exists, please choose another name");
						textField.setText("");
						return;
					}

					String password = new String(passwordField.getPassword());
					String confirm = new String(passwordField_1.getPassword());
					if(password.isEmpty()){
						JOptionPane.showMessageDialog(null, "Please enter a password");
						return;
					}
					if(!password.equals(confirm)){
						JOptionPane.showMessageDialog(null, "The password fields do not match please re-enter your password");
						passwordField.setText("");
						passwordField_1.setText("");
						return;
					}

					created = new User(new Credentials(userName, password), p);
					
					this.dispose();

				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(e->{					
					this.dispose();
				});
				if(cancellable)
				buttonPane.add(cancelButton);
			}
		}
	}
	
	

	/**
	 * Display this modal dialog
	 * @return The created user (Maybe {@code null} if this dialog was cancelled)
	 */
	User showDialog(){
		this.setVisible(true);
		return created;
	}
	
	
	
	
}
