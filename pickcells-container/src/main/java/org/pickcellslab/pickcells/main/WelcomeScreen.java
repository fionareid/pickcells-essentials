package org.pickcellslab.pickcells.main;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.util.Objects;
import java.util.Optional;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;

import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.RootItem;
import org.pickcellslab.foundationj.dbm.User;
import org.pickcellslab.foundationj.dbm.User.priviledge;
import org.pickcellslab.foundationj.ui.tree.DataTreeTable;
import org.pickcellslab.foundationj.ui.tree.DataTreeTableModel;
import org.pickcellslab.pickcells.api.app.ui.UI;
import org.pickcellslab.pickcells.api.datamodel.types.Experiment;

import com.alee.extended.layout.VerticalFlowLayout;
import com.alee.laf.filechooser.WebFileChooser;

@SuppressWarnings("serial")
public class WelcomeScreen extends JPanel implements ListSelectionListener {

	private final JButton btnCreate;
	private final JButton btnLoad;
	private final JButton btnNew;
	private final JButton btnDelete;
	private final JButton btnSwitchUser;
	private final JButton btnLink;
	private final JButton btnSb;

	private DataTreeTable table;
	private User currentUser;

	private final CenteredDropShadowPane centeredDropShadowPane;
	private final JPanel btnPanel;
	private final Image img;



	public WelcomeScreen(PickCells pc) {


		//Controllers
		//----------------------------------------------------------------------------------------


		//Create Experiment
		btnNew = new JButton("New Experiment");
		btnNew.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNew.addActionListener(a->{
			Optional<Experiment> e = new ExperimentWizard().showDialog();
			if(e.isPresent()){

				//Create the database
				Thread t = new Thread(()->pc.createExperiment(e.get()));

				UI.waitAnimation(t, "Creating Experiment...");

				try {
					t.join();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				//Update the table				
				table.getModel().addNode(currentUser, e.get());						
				System.out.println(e.get().getDBName()+" should be added");
			}
		});






		//Load
		btnLoad = new JButton("         Load          ");
		btnLoad.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnLoad.addActionListener(e->{			
			WritableDataItem item = table.getSelectedItem();
			if(RootItem.class.isAssignableFrom(item.getClass()))
				pc.launchExperiment((Experiment) item);

		});



		//Delete experiment or user
		btnDelete = new JButton("        Delete        ");
		btnDelete.setFont(new Font("Dialog", Font.PLAIN, 20));		
		btnDelete.addActionListener(e->{			
			WritableDataItem item = table.getSelectedItem();
			if(RootItem.class.isAssignableFrom(item.getClass())){
				String message = "<HTML>Are you sure you want to delete the following experiment:"
						+ "<br>"+((Experiment)item).getName()+
						"<br><b> This operation cannot be undone! </b></HTML>";

				if(JOptionPane.showConfirmDialog(null, message)==JOptionPane.YES_OPTION){	

					Thread t = new Thread(()->pc.deleteExperiment((Experiment) item));

					UI.waitAnimation(t, "Deleting Experiment...");	

					table.getModel().deleteNode((NodeItem) item);
				}
			}
			else{// we have a user
				User selectedUser = (User) item;
				String message = "<HTML>Are you sure you want to delete the following user:"
						+ "<br>"+selectedUser.userName()+" and the experiments he possesses?"+
						"<br><b> This operation cannot be undone! </b></HTML>";

				if(JOptionPane.showConfirmDialog(null, message)==JOptionPane.YES_OPTION){	

					Thread t = new Thread(()->pc.deleteUser(selectedUser));

					UI.waitAnimation(t, "Deleting User...");	

					for(RootItem i: selectedUser.getAccessibleRootItem())
						table.getModel().deleteNode(i);
					table.getModel().deleteNode(selectedUser);
				}


			}
		});


		//Create User
		btnCreate = new JButton("    Create User    ");
		btnCreate.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnCreate.addActionListener(l->{

			User novel = pc.createUser();
			if(novel != null){
				table.getModel().addNode(currentUser, novel);
				System.out.println("User created: "+novel);
			}

		});




		//Change User
		btnSwitchUser = new JButton("    Switch User    ");
		btnSwitchUser.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnSwitchUser.addActionListener(l->{
			pc.launchAuthentification();
		});



		//Link to a new db
		btnLink = new JButton("         Link DB        ");
		btnLink.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnLink.addActionListener(l->{
			File f = WebFileChooser.showOpenDialog(c->{
				c.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				c.setFileFilter(new FileFilter(){

					@Override
					public boolean accept(File f) {
						if(f.isDirectory() && f.getName().endsWith("db"))
							return true;
						return false;
					}

					@Override
					public String getDescription() {
						return "db file";
					}

				});
			});
			if(f == null)
				return;
			Thread t = new Thread(()->{
				pc.linkDB(f).ifPresent(exp -> table.getModel().addNode(currentUser, exp));
			});
			UI.waitAnimation(t, "Linking...");

		});




		//Go to Sandbox Mode
		btnSb = new JButton("   Utilities    ");
		btnSb.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnSb.addActionListener(l->	PickCells.goToSandBox());






		//Layout
		//-----------------------------------------------------------------------------------------
		//setBounds(100, 100, 651, 547);
		setLayout(new BorderLayout());


		JPanel panelWelcome = new JPanel();
		add(panelWelcome, BorderLayout.NORTH);



		JLabel lblWelcome = new JLabel("Welcome to PickCells!");
		lblWelcome.setBorder(BorderFactory.createEmptyBorder(20, 50, 20, 50));
		panelWelcome.add(lblWelcome);
		lblWelcome.setFont(new Font("Calibri", Font.BOLD, 30));


		centeredDropShadowPane = new CenteredDropShadowPane(100,50,500,800);

		add(centeredDropShadowPane.getHolderPanel(), BorderLayout.CENTER);
		centeredDropShadowPane.getInsidePanel().setBackground(Color.WHITE);

		btnPanel = new JPanel();
		btnPanel.setBorder(BorderFactory.createEmptyBorder(80, 0, 50, 120));
		add(btnPanel, BorderLayout.EAST);


		btnPanel.setLayout(new VerticalFlowLayout(20,30));
		btnPanel.add(btnNew);
		btnPanel.add(btnLink);
		btnPanel.add(btnLoad);
		btnPanel.add(btnCreate);
		btnPanel.add(btnSwitchUser);
		btnPanel.add(btnDelete);
		btnPanel.add(btnSb);

		panelWelcome.setOpaque(false);
		btnPanel.setOpaque(false);
		centeredDropShadowPane.getHolderPanel().setOpaque(false);

		//Load background
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		img = new ImageIcon(getClass().getResource("/welcomeBG3.jpg")).getImage()
				.getScaledInstance((int)screenSize.getWidth(),(int)screenSize.getHeight(), Image.SCALE_SMOOTH);

	}







	public void setUser(User user){


		if(user!=null){

			//Create the treetable

			TraverserConstraints tc = Traversers.newConstraints()
					.fromMinDepth().toDepth(2)
					.traverseLink("AUTHORIZES", Direction.OUTGOING)
					.and("HAS ACCESS", Direction.OUTGOING).includeAllNodes();

			DataTreeTableModel model = new DataTreeTableModel(
					Traversers.breadthfirst(user, tc),
					(n,k)->{
						if(n instanceof Experiment && k == AKey.get("privilege", String.class))
							return "EXPERIMENT";
						return Objects.toString(n.getAttribute(k).orElse(null),"");
					});


			model.hideColumn(AKey.get("password", String.class));
			model.hideColumn(WritableDataItem.idKey);

			if(table == null){
				table = new DataTreeTable(model);
				centeredDropShadowPane.getInsidePanel().setLayout(new BorderLayout());
				centeredDropShadowPane.getInsidePanel().add("Center",table);
				centeredDropShadowPane.getInsidePanel().revalidate();						
				table.addSelectionListener(this);
			}
			else
				table.setModel(model);

			if(user.privilege() == priviledge.USER)
				btnCreate.setEnabled(false);
			else
				btnCreate.setEnabled(true);

			currentUser = user;		




		}
		else{
			if(table != null)
				table.setModel(new DataTreeTableModel());			
		}
	}






	//********************** Enable buttons only when option is available ************************		
	@Override
	public void valueChanged(ListSelectionEvent e) {

		if(table.getSelectedItem() == null)
			return;

		if(table.getSelectedItem() instanceof User){
			this.btnLoad.setEnabled(false);
			this.btnDelete.setEnabled(currentUser.privilege() == priviledge.ADMIN 
					&& ((User)table.getSelectedItem()).privilege() == priviledge.USER);
		}
		else{
			this.btnLoad.setEnabled(true);
			this.btnDelete.setEnabled(true);
		}
	}


	@Override
	public void paintComponent(Graphics g) {
		g.drawImage(img, 0, 0, this);
	}



}
