package org.pickcellslab.pickcells.main;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.apache.log4j.Logger;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.StorageBox.BoxBuilder;
import org.pickcellslab.pickcells.api.app.ui.AlphanumComparator;
import org.pickcellslab.pickcells.api.app.ui.UI;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Experiment;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.io.ImgWriter;

import com.alee.managers.tooltip.TooltipManager;

import net.imglib2.RandomAccessibleInterval;
import net.imglib2.type.numeric.integer.UnsignedByteType;

@SuppressWarnings("serial")
public class CanvasMaker extends JDialog{

	private static Logger log = Logger.getLogger(CanvasMaker.class);

	private DataAccess<?,?> session;
	private File home;


	private List<Image> images; 

	private boolean wasCancelled = true;

	private boolean readImagesFailed;


	private JTextField textField;

	private final ImgIO io;


	public CanvasMaker(DataAccess<?,?> session, ImgIO factory) {

		this.session = session;
		this.io = factory;

		try {

			if(home == null){
				//Get the image folder
				String f = session.queryFactory()
						.read(Experiment.class)
						.makeList(Experiment.dbPathKey)
						.inOneSet().getAll().run().get(0);

				home = new File(f);		
			}

			//Build input dialog 
			//First get the image data
			images = 					
					session.queryFactory().regenerate(Image.class)
					.toDepth(1)
					.traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
					.and(Image.RAW_FILE_LINK, Direction.OUTGOING)
					.includeAllNodes()
					.regenerateAllKeys()
					.getAll()
					.getTargets(Image.class).collect(Collectors.toList());



			//Important sort the list based on the file names to maintain the same order as the one obtained from DiskImageandler
			Collections.sort(images, new AlphanumComparator<Image>(I -> I.getAttribute(Keys.name).get()));

			if(images.size() == 0)
				return;

		} catch (Exception e) {
			readImagesFailed = true;
			log.error("Unable to read images in the database", e);
			return;
		}







		JLabel lblThisDialogAllows = new JLabel(
				"<HTML><p align=\"justify\"> "
						+ "This dialog allows you to generate \"blank\" segmentation results.<br>"
						+ "<br>"+images.size()+" color images have been registered in the database, "
						+ "therefore "+images.size()+" segmentation images will be created."
						+ "</p> </HTML>");

		JLabel lblSegmentedChannel = new JLabel("Segmentation Prefix");
		TooltipManager.setTooltip(lblSegmentedChannel, "Define a prefix to name the generated images");
		textField = new JTextField("canvas");
		TooltipManager.setTooltip(textField, "Define a prefix to name the generated images");

		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(l->{			

			String text = textField.getText();
			if(text.isEmpty()){
				JOptionPane.showMessageDialog(null, "Define a prefix for the segmented images to be imported");
				return;
			}

			images.get(0).segmentations().forEach(s->{
				if(s.fileName().startsWith(text)){
					JOptionPane.showMessageDialog(null, "This prefix is already taken");
					return;
				}
			});

			new Thread(()->{
				try {
					this.setVisible(false);

					if(launchCanvasMaker()){			
						wasCancelled = false;
						this.dispose();
					}else
						this.setVisible(true);
				} catch (IOException e) {
					log.error("An error occured while processing the image files",e);
					this.dispose();
				}
			}).start();
		});

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(l-> this.dispose());




		//Layout
		//-------------------------------------------------------------------------------------------------

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
						.addGap(20)
						.addComponent(lblThisDialogAllows, GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
						.addGap(11))
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
						.addGap(71)
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(btnOk)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(btnCancel))
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(lblSegmentedChannel)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(textField, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(58, Short.MAX_VALUE))
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblThisDialogAllows)
						.addGap(18)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblSegmentedChannel)
								.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnOk)
								.addComponent(btnCancel))
						.addGap(56))
				);
		getContentPane().setLayout(groupLayout);
	}









	@Override
	public void setVisible(boolean visible){
		if(readImagesFailed){
			JOptionPane.showMessageDialog(null, "An error occured while reading the database", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		else if(images.size() == 0){
			JOptionPane.showMessageDialog(null,
					"There are no image registered in the database, please register color images first.");
			return;
		}
		else super.setVisible(visible);
	}







	public boolean wasCancelled(){
		return wasCancelled;
	}








	private boolean launchCanvasMaker() throws IOException {

		log.info("Started : Generating canvas...");
		
		final String pfx = textField.getText();
		int counter = -1;		
		for(Image img : images){
			
			final long[] dims = img.getMinimalInfo().removeDimension(Image.c).imageDimensions();
			final int bd = 1;

			// Create the writer
			final ImgWriter<UnsignedByteType> w = io.createWriter(img.getMinimalInfo().removeDimension(Image.c), 
					new UnsignedByteType(), home.getPath()+File.separator+pfx+"_"+ ++counter +io.standard(dims, bd));

			// Write blank for each frame
			for(int f = 0; f< img.frames(); f++){
				RandomAccessibleInterval<UnsignedByteType> frame = w.createNewChannelFrame();
				w.writeChannelFrame(frame, 0, f);
			}

			
			// Generate a Segmentation Result
			
			new SegmentationResult(// Automatically creates a link to the parent image
					img,
					pfx+"_"+counter+io.standard(dims, bd),
					pfx,
					bd,
					"Generated Canvas");
			
			log.debug("Canvas generated!");
					
		}


		//Now save to the database
		final TraverserConstraints tc = 
				Traversers.newConstraints()
				.fromMinDepth().toDepth(1).traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
				.includeAllNodes();
		
		BoxBuilder b = session.queryFactory().store();
		for(Image i : images)
			b.add(i, tc);

		try {

			b.run();

		} catch (DataAccessException e) {
			UI.display("Error", "Could not update the database", e, Level.SEVERE);	
			return false;
		}
		
		return true;
	}

}
