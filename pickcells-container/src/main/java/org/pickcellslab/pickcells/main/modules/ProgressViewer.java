package org.pickcellslab.pickcells.main.modules;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.apache.log4j.Logger;
import org.kohsuke.MetaInfServices;

import com.alee.extended.panel.GroupPanel;
import com.alee.extended.progress.WebStepProgress;
import com.alee.utils.SwingUtils;

import org.pickcellslab.foundationj.modules.Module;
import org.pickcellslab.foundationj.modules.ProgressPublisher;
import org.pickcellslab.foundationj.modules.StepwiseProcess;
import org.pickcellslab.pickcells.api.app.modules.DefaultDocument;
import org.pickcellslab.pickcells.api.app.ui.Icons;

@MetaInfServices(Module.class)
public class ProgressViewer implements ProgressPublisher, DefaultDocument {

	private static final Logger log = Logger.getLogger(ProgressViewer.class);


	private List<StepwiseProcess> registered = new ArrayList<>();
	private Map<StepwiseProcess, WebStepProgress> active = new ConcurrentHashMap<>();
	private Map<StepwiseProcess, JLabel> results = new ConcurrentHashMap<>();

	private JScrollPane scroll = new JScrollPane();
	private GroupPanel panel = new GroupPanel(10, false);
	private int counter;

	public ProgressViewer(List<StepwiseProcess> proc){
		proc.forEach(sp->register(sp));
		scroll.setName("Progress Viewer");
		panel.setBackground(Color.WHITE);
		scroll.setViewportView(panel);
	}



	@Override
	public Component getScene() {
		return scroll;
	}

	@Override
	public Scope getScope() {
		return Scope.ALL_USERS;
	}

	@Override
	public String name() {
		return "Progress Viewer";
	}

	@Override
	public String description() {
		return "Displays the progress of concurrently running processes";
	}

	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/progress.png"));
	}

	

	@Override
	public int preferredLocation() {
		return DefaultDocument.DOWN;
	}

	@Override
	public void register(StepwiseProcess p) {
		registered.add(p);
		p.addPublisher(this);
	}

	@Override
	public void setStep(StepwiseProcess source, int step) {

		log.info("Received: from "+source.getClass().getSimpleName()+" setStep: "+step);
		SwingUtils.invokeLater(()->{

			WebStepProgress web = active.get(source);
			if(web == null){
				

				String[] steps = source.steps();
				String longest = "";
				for(String s: steps)
					if(s.length()>longest.length())
						longest = s;
			
							
				
				
				web = new WebStepProgress (steps);
				web.setSelectionEnabled(false);
				int w = longest.length()*steps.length*6;
				web.setMinimumWidth(w);


				active.put(source, web);			
				JPanel pane = new JPanel();
				pane.setBackground(Color.WHITE);
				//pane.setLayout(new BoxLayout(pane,BoxLayout.X_AXIS));
				pane.setPreferredSize(new Dimension(w+500,100));

				JLabel time = new JLabel(source.getClass().getSimpleName()+"  ");
				JLabel end = new JLabel();
				pane.add(time, BorderLayout.EAST);
				pane.add(web,BorderLayout.CENTER);
				pane.add(end,BorderLayout.WEST);						
				panel.add(pane);

				panel.setPreferredSize(new Dimension(w+500,++counter*85));

				scroll.revalidate();
				scroll.repaint();
				results.put(source,end);
			}
			web.setSelectedStepIndex(step);

			if(web.getStepsAmount() == step+1){
				JLabel l = results.get(source);
				l.setIcon(Icons.valid_16());
				l.setText(LocalDateTime.now().toString());
				active.remove(source);
				results.remove(source);
			}
		});
	}

	@Override
	public void setProgress(StepwiseProcess source, float progress) {

		WebStepProgress web = active.get(source);
		if(web == null){
			log.warn("Received a setProgress notification from an inactive process : "+source.getClass().getSimpleName());
			return;
		}
		web.setProgress(progress);
	}

	@Override
	public void cancelled(StepwiseProcess source) {
		WebStepProgress web = active.get(source);
		if(web == null){
			log.warn("Received faiure notification from an inactive process: "+source.getClass().getSimpleName());
			return;
		}

		JLabel l = results.get(source);
		l.setIcon(Icons.cancelled_16());
		l.setText("Cancelled "+LocalDateTime.now().toString());
		active.remove(source);
		results.remove(source);
	}

	@Override
	public void failure(StepwiseProcess source, String message) {

		WebStepProgress web = active.get(source);
		if(web == null){
			log.warn("Received faiure notification from an inactive process!");
			return;
		}

		JLabel l = results.get(source);
		l.setIcon(Icons.error_16());
		l.setText("Failure : "+message);
		active.remove(source);
		results.remove(source);

	}






	@Override
	public List<StepwiseProcess> activeProcesses() {
		return new ArrayList<>(active.keySet());
	}




}
