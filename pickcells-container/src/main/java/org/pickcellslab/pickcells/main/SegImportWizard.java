package org.pickcellslab.pickcells.main;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.apache.log4j.Logger;
import org.pickcellslab.foundationj.datamodel.Direction;
import org.pickcellslab.foundationj.datamodel.tools.TraverserConstraints;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.StorageBox.BoxBuilder;
import org.pickcellslab.pickcells.api.app.ui.AlphanumComparator;
import org.pickcellslab.pickcells.api.app.ui.UI;
import org.pickcellslab.pickcells.api.datamodel.conventions.Keys;
import org.pickcellslab.pickcells.api.datamodel.conventions.Links;
import org.pickcellslab.pickcells.api.datamodel.types.Experiment;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.datamodel.types.SegmentationResult;
import org.pickcellslab.pickcells.api.img.io.ImgFileList;
import org.pickcellslab.pickcells.api.img.io.ImgIO;
import org.pickcellslab.pickcells.api.img.io.ImgsChecker;
import org.pickcellslab.pickcells.api.img.io.ImgsChooser;

import com.alee.managers.tooltip.TooltipManager;

@SuppressWarnings("serial")
public class SegImportWizard extends JDialog{

	private static Logger log = Logger.getLogger(SegImportWizard.class);

	private DataAccess<?,?> session;
	private File home;


	private List<Image> images; 

	private boolean wasCancelled = true;

	private boolean readImagesFailed;


	private JTextField textField;

	private final ImgIO io;


	public SegImportWizard(DataAccess<?,?> session, ImgIO factory) {

		this.session = session;
		this.io = factory;

		try {

			if(home == null){
				//Get the image folder
				String f = session.queryFactory()
						.read(Experiment.class)
						.makeList(Experiment.dbPathKey)
						.inOneSet().getAll().run().get(0);

				home = new File(f);		
			}

			//Build input dialog 
			//First get the image data
			images = 					
					session.queryFactory().regenerate(Image.class)
					.toDepth(1)
					.traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
					.and(Image.RAW_FILE_LINK, Direction.OUTGOING)
					.includeAllNodes()
					.regenerateAllKeys()
					.getAll()
					.getTargets(Image.class).collect(Collectors.toList());



			//Important sort the list based on the file names to maintain the same order as the one obtained from DiskImageandler
			Collections.sort(images, new AlphanumComparator<Image>(I -> I.getAttribute(Keys.name).get()));

			if(images.size() == 0)
				return;

		} catch (Exception e) {
			readImagesFailed = true;
			log.error("Unable to read images in the database", e);
			return;
		}







		JLabel lblThisDialogAllows = new JLabel(
				"<HTML><p align=\"justify\"> "
						+ "This dialog allows you to import segmentation results.<br>"
						+ "<br>"+images.size()+" color images have been registered in the database, "
						+ "therefore "+images.size()+" segmentation images are required."
						+ "The dimensions of the segmentation images will be checked "
						+ "to make sure they match the dimensions of the registered images."
						+ "</p> </HTML>");

		JLabel lblSegmentedChannel = new JLabel("Segmentation Prefix");
		TooltipManager.setTooltip(lblSegmentedChannel, "Define a prefix to name the imported images");
		textField = new JTextField("seg");
		TooltipManager.setTooltip(textField, "Define a prefix to name the imported images");

		JButton btnOk = new JButton("Ok");
		btnOk.addActionListener(l->{			

			String text = textField.getText();
			if(text.isEmpty()){
				JOptionPane.showMessageDialog(null, "Define a prefix for the segmented images to be imported");
				return;
			}

			images.get(0).segmentations().forEach(s->{
				if(s.fileName().startsWith(text)){
					JOptionPane.showMessageDialog(null, "This prefix is already taken");
					return;
				}
			});

			new Thread(()->{
				try {
					this.setVisible(false);

					if(launchSegImporter()){			
						wasCancelled = false;
						this.dispose();
					}else
						this.setVisible(true);
				} catch (IOException e) {
					log.error("An error occured while processing the image files",e);
					this.dispose();
				}
			}).start();
		});

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(l-> this.dispose());




		//Layout
		//-------------------------------------------------------------------------------------------------

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
				groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
						.addGap(20)
						.addComponent(lblThisDialogAllows, GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
						.addGap(11))
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
						.addGap(71)
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(btnOk)
										.addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(btnCancel))
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(lblSegmentedChannel)
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(textField, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(58, Short.MAX_VALUE))
				);
		groupLayout.setVerticalGroup(
				groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblThisDialogAllows)
						.addGap(18)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblSegmentedChannel)
								.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnOk)
								.addComponent(btnCancel))
						.addGap(56))
				);
		getContentPane().setLayout(groupLayout);
	}









	@Override
	public void setVisible(boolean visible){
		if(readImagesFailed){
			JOptionPane.showMessageDialog(null, "An error occured while reading the database", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		else if(images.size() == 0){
			JOptionPane.showMessageDialog(null,
					"There are no image registered in the database, please register color images first.");
			return;
		}
		else super.setVisible(visible);
	}







	public boolean wasCancelled(){
		return wasCancelled;
	}








	private boolean launchSegImporter() throws IOException {

		log.info("Started : Choosing a set of images to import...");


		//Ask the user to choose a set of images
		ImgsChooser chooser = io.createChooserBuilder().setTitle("Image Dataset").setHomeDirectory(UI.getLastDirectory(session)).build();
		chooser.setModal(true);
		chooser.setVisible(true);
		if(chooser.wasCancelled())	return false;

		ImgFileList list = chooser.getChosenList();
		if(list.numImages()==0){
			JOptionPane.showMessageDialog(null, "The dataset is empty!");
			return false;
		}else if(list.numImages()!=images.size()){
			JOptionPane.showMessageDialog(null, "You need to import exactly "+images.size()+" segmentation results");
			return launchSegImporter();
		}

		//Check the last directory and store in preferences
		File f = new File(list.path(list.numDataSets()-1));
		if(f.isDirectory())
			UI.setLastDirectory(session, f);
		else
			UI.setLastDirectory(session, f.getParentFile());


		log.info("Checking that the segmentations dimensions meet the raw images dimensions");

		int counter = 0;
		for(int i = 0; i < list.numDataSets(); i++){
			log.debug("imgSet size ="+ list.numImages(i));

			for(int j = 0; j < list.numImages(i); j++){

				//Get the raw image dimensions

				Image im = images.get(counter);

				System.out.println("Checking Image data : "+im.getAttribute(Keys.name).get());

				log.debug("current segmentation result ="+ list.name(i,j));


				//Check that the segmentation has only one channel
				//and that the other dimensions are the same as the color image 
				long[] colorDims = im.dimensions(false, true);

				final int k = j;
				ImgsChecker check = 
						io.createCheckerBuilder()
						.addCheck(ImgsChecker.nChannels,(Integer)1)
						//.addCheck(ImgsChecker.dimensions, colorDims)
						.build(list.subList(i, p -> p==k ));

				if(!check.isConsistent()){
					JOptionPane.showMessageDialog(null, "The segmentation Image must have only one channel");
					return launchSegImporter();
				}


				long[] segDims = check.value(ImgsChecker.dimensions,list,i,j);


				//Check that the segmentation image has the same dimensions as the original image
				if(!Arrays.equals(colorDims, segDims)){
					JOptionPane.showMessageDialog(null, 
							"<HTML>The segmentation result selected for image "+counter+" does"
									+ "<br> not have the same dimensions as the parent image :"
									+ "<br><b>Color dimensions:</b> "+Arrays.toString(colorDims)
									+ "<br><b>Segmentation dimensions :</b> "+Arrays.toString(segDims)+"/HTML");
					return launchSegImporter();
				}

				counter++;
			}
		}

		Thread importImages = new Thread(()->{


			//Now import and create SegmentationResult in the data graph		
			ImgsChecker check = 
					io.createCheckerBuilder().addCheck(ImgsChecker.bitDepth).addCheck(ImgsChecker.dimensions).build(list);

			String pfx = textField.getText();

			try {
				io.importImages(list, home.getAbsolutePath(), pfx);





				int c = 0;
				for(int i = 0; i < list.numDataSets(); i++){
					for(int j = 0; j < list.numImages(i); j++){

						assert list!=null : "list is null";
						assert check!=null : "check is null";
						assert io!=null : "io is null";
						long[] dims = check.value(ImgsChecker.dimensions, list, i, j);
						int bd = check.value(ImgsChecker.bitDepth);
						new SegmentationResult(// Automatically creates a link to the parent image
								images.get(c),
								pfx+"_"+c+io.standard(dims, bd),
								pfx,
								bd,
								"Direct import");	
						log.debug("SegResultCreated");
						c++;
					}
				}

			} catch (Exception e) {
				UI.display("Error", "Error copying images", e, Level.SEVERE);
				Thread.currentThread().interrupt();
				return;
			}



			final TraverserConstraints tc = 
					Traversers.newConstraints()
					.fromMinDepth().toDepth(1).traverseLink(Links.COMPUTED_FROM, Direction.INCOMING)
					.includeAllNodes();

			BoxBuilder b = session.queryFactory().store();
			for(Image i : images)
				b.add(i, tc);

			try {

				b.run();

			} catch (DataAccessException e) {
				UI.display("Error", "Could not update to the database", e, Level.SEVERE);		
				return;
			}
			return;




		});


		UI.waitAnimation(importImages, "Importing images");

		try {
			importImages.join();
		} catch (InterruptedException e) {
			UI.display("Error", "An error occured while importing images",e, Level.WARNING);
			return false;
		}


		return true;
	}

}
