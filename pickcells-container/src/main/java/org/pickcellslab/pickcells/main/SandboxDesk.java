package org.pickcellslab.pickcells.main;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.logging.Level;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.pickcellslab.pickcells.api.app.modules.DesktopModule;
import org.pickcellslab.pickcells.api.app.ui.UI;

import com.alee.laf.button.WebButton;
import com.alee.laf.desktoppane.WebDesktopPane;
import com.alee.managers.tooltip.TooltipManager;

@SuppressWarnings("serial")
public class SandboxDesk extends WebDesktopPane{

	private final Image bg;
	private final Dimension screenSize;
	private int numIcons = 0;

	public SandboxDesk() {

		screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		bg =  new ImageIcon(getClass().getResource("/welcomeBG3.jpg")).getImage()
				.getScaledInstance((int)screenSize.getWidth(),(int)screenSize.getHeight(), Image.SCALE_SMOOTH);

		addReturnButton();
	}



	private void addReturnButton() {


		final WebButton moduleIcon = new WebButton (resize( (ImageIcon) PickCellsIcons.switch_24(), 48) );
		TooltipManager.setTooltip(moduleIcon, "Return to the welcome screen");
		moduleIcon.setRolloverDecoratedOnly ( true );
		moduleIcon.setHorizontalTextPosition ( WebButton.CENTER );
		moduleIcon.setVerticalTextPosition ( WebButton.BOTTOM );
		moduleIcon.addActionListener ( l-> {
			if ( moduleIcon.getClientProperty ( DesktopPaneIconMoveAdapter.DRAGGED_MARK ) != null )
				return;
			PickCells.goToWelcome();
		});

		final DesktopPaneIconMoveAdapter ma1 = new DesktopPaneIconMoveAdapter ();
		moduleIcon.addMouseListener ( ma1 );
		moduleIcon.addMouseMotionListener ( ma1 );
		moduleIcon.setBounds ( screenSize.width-125, screenSize.height-125, 70, 70 );
		this.add ( moduleIcon );
	}



	public void addModule(DesktopModule m){

		final WebButton moduleIcon = new WebButton ( m.name(), resize((ImageIcon) m.icon(), 48) );
		TooltipManager.setTooltip(moduleIcon, m.description());
		//moduleIcon.setRolloverDecoratedOnly ( true );
		moduleIcon.setHorizontalTextPosition ( WebButton.CENTER );
		moduleIcon.setVerticalTextPosition ( WebButton.BOTTOM );
		moduleIcon.addActionListener ( l-> {
			if ( moduleIcon.getClientProperty ( DesktopPaneIconMoveAdapter.DRAGGED_MARK ) != null )
				return;
			try{
				m.launch();
			}catch(Exception e) {
				UI.display("Error", "Something went wrong when launching this module, check the log", e, Level.SEVERE);
			}
		});

		final DesktopPaneIconMoveAdapter ma1 = new DesktopPaneIconMoveAdapter ();
		moduleIcon.addMouseListener ( ma1 );
		moduleIcon.addMouseMotionListener ( ma1 );
		
		int x = (int) ((numIcons / (screenSize.getHeight() / 100)) * 100); 
		int y = (int) ((numIcons * 75) % screenSize.getHeight()) + 50;

		System.out.println("x = "+x+" ; y = "+y);
		
		moduleIcon.setBounds ( x, y, 100, 75 );
		this.add ( moduleIcon );
		numIcons++;
	}


	private Icon resize(ImageIcon i, int s){
		java.awt.Image img = i.getImage() ;  
		java.awt.Image newimg = img.getScaledInstance( s, s,  java.awt.Image.SCALE_SMOOTH ) ;  
		return new ImageIcon( newimg );
	}



	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(bg, 0, 0, this);
	}










}
