package org.pickcellslab.pickcells.main;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.log4j.Logger;
import org.jdesktop.swingx.auth.LoginService;
import org.pickcellslab.foundationj.dbm.Credentials;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.Persistence;
import org.pickcellslab.foundationj.dbm.User;

public class LoginManager extends LoginService {

	private Logger log = Logger.getLogger(LoginManager.class);
	private Persistence persistence;
	private User user;

	LoginManager(Persistence persistence){
		this.persistence = persistence;
	}

	
	User getAuthenticatedUser(){
		return user;
	}
	
	@Override
	public boolean authenticate(String name, char[] password, String server)
			throws Exception {
		try{
			log.debug("Asking persistence");
			user = persistence.getUser(new Credentials(name, new String(password))).get();
			log.debug("user is null " + (user == null));
		}
		catch(DataAccessException e){
			log.debug(e.getMessage());
			return false;
		}
		return user != null;
	}

}
