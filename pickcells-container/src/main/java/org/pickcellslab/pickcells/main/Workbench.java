package org.pickcellslab.pickcells.main;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;
import java.util.logging.Level;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.error.ErrorLevel;
import org.pickcellslab.foundationj.dbm.DataAccess;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.Session;
import org.pickcellslab.foundationj.dbm.events.MetaModelListener;
import org.pickcellslab.foundationj.dbm.meta.ManuallyStored;
import org.pickcellslab.foundationj.dbm.meta.MetaModel;
import org.pickcellslab.foundationj.dbm.meta.MetaQueryable;
import org.pickcellslab.foundationj.dbm.queries.RegeneratedItems;
import org.pickcellslab.foundationj.modules.Categorized;
import org.pickcellslab.foundationj.modules.StepwiseProcess;
import org.pickcellslab.foundationj.queryUI.dialogs.ChoicePanel;
import org.pickcellslab.foundationj.queryUI.dialogs.ChoicePanels;
import org.pickcellslab.foundationj.ui.wizard.Wizard;
import org.pickcellslab.pickcells.api.app.modules.DefaultDocument;
import org.pickcellslab.pickcells.api.app.modules.DocumentFactory;
import org.pickcellslab.pickcells.api.app.modules.Launchable;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;
import org.pickcellslab.pickcells.api.app.ui.UI;
import org.pickcellslab.pickcells.api.datamodel.conventions.UsefulQueries;
import org.pickcellslab.pickcells.api.datamodel.types.Experiment;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.img.io.ImgIO;

import com.alee.extended.layout.ToolbarLayout;
import com.alee.extended.statusbar.WebMemoryBar;
import com.alee.extended.statusbar.WebStatusBar;
import com.alee.extended.tab.DocumentData;
import com.alee.extended.tab.DocumentListener;
import com.alee.extended.tab.PaneData;
import com.alee.extended.tab.WebDocumentPane;
import com.alee.laf.button.WebButton;
import com.alee.laf.progressbar.WebProgressBar;
import com.alee.laf.toolbar.ToolbarStyle;
import com.alee.laf.toolbar.WebToolBar;
import com.alee.managers.tooltip.TooltipManager;


public class Workbench {

	private static Logger log = Logger.getLogger(Workbench.class);


	// Load preferences and default geometry
	//private final Preferences cfg = Preferences.userNodeForPackage(Workbench.class);
	public static final Rectangle GEOMETRY = new Rectangle(0,0,640,480);


	private final WebDocumentPane<DocumentData<Component>> web = new WebDocumentPane<>();

	private final JFrame parentFrame;
	private final JPanel mainContainer;
	private JMenuBar menuBar;

	//private final JPanel toolBarUp;
	//private final JToolBar toolBar;
	//private final JPanel bottomPanel;

	private boolean sceneLoaded = false;

	//Stores the components to place into the toolbar grouped by categories
	private final TreeMap<String,Map<Categorized,Component>> tools = new TreeMap<>();

	private final ImgIO io;


	private Map<String, UIDocument> activeDocuments = new HashMap<>();









	/**
	 * Initialize the workbench
	 */
	public Workbench(JFrame parent, Session<?,?> session, ImgIO factory) {

		this.io = factory;
		this.parentFrame = parent;	

		mainContainer = new JXPanel();
		mainContainer.setLayout(new MultiBorderLayout());


		//Setup the Menu
		menuBar = buildMenuBar(session);
		parent.setJMenuBar(menuBar);

		//Views toolbar
		//toolBarUp = new JPanel(new FlowLayout(FlowLayout.LEFT));
		//mainContainer.add(toolBarUp, BorderLayout.NORTH);

		//Init toolBar - will be fully built when getScene gets called
		//toolBar = new JToolBar();
		//toolBar.setOrientation(JToolBar.VERTICAL);
		//toolBar.setFloatable(false);

		//JScrollPane scroll = new JScrollPane(toolBar);
		//scroll.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		//scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		//scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		//mainContainer.add(scroll, BorderLayout.WEST);


		//Setup the document pane
		mainContainer.add(web, BorderLayout.CENTER);
		web.setBorder(BorderFactory.createLineBorder(Color.GRAY));

		//Status bar


		// Simple status bar
		WebStatusBar statusBar = new WebStatusBar ();

		// Simple memory bar
		WebMemoryBar memoryBar = new WebMemoryBar ();
		memoryBar.setPreferredWidth ( memoryBar.getPreferredSize ().width + 20 );
		memoryBar.setPreferredHeight(24);
		statusBar.add ( memoryBar, ToolbarLayout.END );

		WebToolBar bar = new WebToolBar();
		bar.setToolbarStyle(ToolbarStyle.attached);
		bar.add(memoryBar);
		mainContainer.add(bar, BorderLayout.SOUTH);

		//bottomPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		//bottomPanel.add(statusBar);

		//mainContainer.add(bottomPanel, BorderLayout.SOUTH);



		//Add the frame to the parent frame
		//parent.getContentPane().add(mainContainer);


	}



	public void addDefault(DefaultDocument factory) {



		DocumentData<Component> p = new DocumentData<>( 
				factory.name(),
				resize((ImageIcon) factory.icon(),12),
				factory.name(),
				Color.WHITE,
				factory.getScene());
		p.setCloseable(false);

		if(web.getAllSplitPanes().isEmpty()){
			if(factory.preferredLocation() == DefaultDocument.UP){
				web.openDocument (p);		
				web.setSelected(p);
			}
			else{
				web.openDocument (p);
				web.split(p, WebDocumentPane.BOTTOM);
			}
		}

		else{
			if(factory.preferredLocation() == DefaultDocument.UP)
				web.setSelected(web.getAllSplitPanes().get(0).getFirst().findClosestPane().get(0));
			else
				web.setSelected(web.getAllSplitPanes().get(0).getLast().findClosestPane().get(0));

			web.openDocument (p);		
			web.setSelected(p);
		}



	}






	public void addDocument(DocumentFactory view) {



		//Add to document pane when a document is created
		view.addDocumentFactoryListener(doc->{
			//Set the top pane selected to make sure we add the new document to the top view
			if(web.getAllSplitPanes().size()!=0)
				web.setSelected(web.getAllSplitPanes().get(0).getFirst().findClosestPane().get(0));
			String id = doc.id();
			id += " "+web.getDocuments().stream().filter(d->d.getId().contains(doc.id())).count()+1;
			DocumentData<Component> p = new DocumentData<>( 
					id,
					resize((ImageIcon) doc.icon(),12),
					doc.id(),
					Color.WHITE,
					doc.scene());
			activeDocuments.put(id,doc);
			web.openDocument (p);
			web.setSelected(p);
		});



		//Add the panel to the toolBar
		WebToolBar bar = new WebToolBar(WebToolBar.VERTICAL);
		bar.setToolbarStyle(ToolbarStyle.attached);
		bar.setFloatable(false);
		Component comp = view.toolBar();
		if(comp instanceof JPanel && ((JPanel) comp).getLayout() instanceof FlowLayout)
			((FlowLayout) ((JPanel) comp).getLayout()).setVgap(0);
		bar.add(view.toolBar());
		mainContainer.add(bar,BorderLayout.NORTH);

		//toolBarUp.add(bar);
		//toolBarUp.add(new JToolBar.Separator());



	}



	private Icon resize(ImageIcon i, int s){
		java.awt.Image img = i.getImage() ;  
		java.awt.Image newimg = img.getScaledInstance( s, s,  java.awt.Image.SCALE_SMOOTH ) ;  
		return new ImageIcon( newimg );
	}









	public void addTask(Launchable task){

		Objects.requireNonNull(task, "The provided ViewFactory is null");

		//Check the factory does not return a null component or a component that is too large
		Icon comp = task.icon();
		if(null == comp)
			log.warn("The Analysis with name \""+task.name()+"\" has returned a null icon and was therefore disabled");

		WebButton btn = new WebButton(this.resize((ImageIcon) comp,24));
		TooltipManager.setTooltip(btn, task.description());
		//btn.setRolloverDecoratedOnly(true);

		btn.addActionListener(l-> new Thread(()->{

			WebProgressBar progressBar = new WebProgressBar ();
			WebToolBar bar = new WebToolBar();
			bar.setToolbarStyle(ToolbarStyle.attached);
			bar.add(progressBar);
			try{

				progressBar.setIndeterminate ( true );
				progressBar.setStringPainted ( true );
				progressBar.setString ( task.name()+" is running..." );				
				mainContainer.add(bar,BorderLayout.SOUTH);
				task.launch();				

			}catch(Exception e){
				UI.display("Error", "An exception occured while running "+task.name(), e, Level.WARNING);
				mainContainer.remove(bar);
				mainContainer.revalidate();
				mainContainer.repaint();
				if(StepwiseProcess.class.isAssignableFrom(task.getClass())){
					((StepwiseProcess)task).getPublishers().forEach(p->p.failure((StepwiseProcess) task, e.getMessage()));
				}
				return;
			}

			mainContainer.remove(bar);
			mainContainer.revalidate();
			mainContainer.repaint();

			if(StepwiseProcess.class.isAssignableFrom(task.getClass())){
				((StepwiseProcess)task).getPublishers().forEach(pub->pub.setStep((StepwiseProcess) task, ((StepwiseProcess) task).steps().length));
			}

			//NotificationManager.showNotification(task.name()+" Done!", task.icon());			

		}).start());
		sortCategory(task,btn);


	}



	public void setSession(Session<?, ?> userSession) {
		for(DocumentData<Component> i : web.getDocuments()){
			if(i.isCloseable())
				web.closeDocument(i);
		}
		menuBar = buildMenuBar(userSession);
		parentFrame.setJMenuBar(menuBar);
	}






	private void sortCategory(Categorized c, Component comp){		
		Map<Categorized,Component> cat = tools.get(c.categories()[0]);
		if(null == cat){
			cat = new HashMap<>();
			cat.put( c, comp);
			tools.put((String) c.categories()[0], cat);
		}
		else
			cat.put(c, comp);		
		log.debug("Added : "+c.name());
	}








	public Component getScene(){

		//Organise the toolBar				

		if(!sceneLoaded){

			log.debug("Number of object in the ToolBar = "+tools.size());

			for(String category : tools.navigableKeySet()){			

				log.debug("Adding category in tool bar : " + category);

				Map<Categorized, Component> categoryMap = tools.get(category);
				WebToolBar bar = new WebToolBar(JToolBar.VERTICAL);
				bar.setToolbarStyle(ToolbarStyle.attached);

				for(Entry<Categorized, Component> module : categoryMap.entrySet())
					bar.add(module.getValue());

				mainContainer.add(bar,BorderLayout.WEST);
			}

			//toolBar.addSeparator();

			//Add the JButton allowing to return to the welcome screen at the right of the toolBar

			//	toolBarUp.add( Box.createHorizontalGlue() );
			WebButton back = new WebButton(PickCellsIcons.switch_24());
			TooltipManager.setTooltip(back, "Return to the welcome screen");
			//back.setRolloverDecoratedOnly(true);
			back.addActionListener(l->{
				parentFrame.setJMenuBar(null);				
				PickCells.goToWelcome();				
			});
			WebToolBar bar = new WebToolBar(JToolBar.VERTICAL);
			bar.setToolbarStyle(ToolbarStyle.attached);
			bar.add(back);
			mainContainer.add(bar,BorderLayout.WEST);




			//Setup listener to reactivate btn if only one doc is allowed

			web.addDocumentListener(new DocumentListener<DocumentData<Component>>(){
				@Override
				public void closed(DocumentData<Component> arg0, PaneData<DocumentData<Component>> arg1, int arg2) {
					UIDocument doc = activeDocuments.get(arg0.getId());
					if(doc!=null){
						doc.close();
						activeDocuments.remove(arg0.getId());
					}
				}

				@Override
				public boolean closing(DocumentData<Component> arg0, PaneData<DocumentData<Component>> arg1, int arg2) {
					return true;
				}

				@Override
				public void opened(DocumentData<Component> arg0, PaneData<DocumentData<Component>> arg1, int arg2) {}

				@Override
				public void selected(DocumentData<Component> document, PaneData<DocumentData<Component>> pane, int index) {}

			});

		}


		//layout defaultdocuments
		web.getAllSplitPanes().get(0).setDividerLocation(0.9);

		sceneLoaded = true;


		return mainContainer;
	}






	//MenuBar
	//-------------------------------------------------------------------------------------------------
	private JMenuBar buildMenuBar(Session<?,?> session) { 

		JMenuBar menuBar = new JMenuBar();

		// File
		//-----------------------------------------


		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		JMenu mntmImport = new JMenu("Import...");
		mnFile.add(mntmImport);

		JMenuItem imgImport = new JMenuItem("Images");
		mntmImport.add(imgImport);

		//Add the possibility to import images if first time
		try {			

			//Test if the session already contains Images if not this is the first run				
			if(session.queryFactory().read(Image.class,1).makeList(Image.idKey).inOneSet().getAll().run().isEmpty()){			

				session.addMetaListener(new MetaModelListener(){

					@Override
					public void sessionClosed(DataAccess<?, ?> source) {}

					@Override
					public void metaEvent(RegeneratedItems meta, MetaChange evt) {
						// TODO Auto-generated method stub
						if(evt == MetaChange.CREATED){
							if(meta.getAllTargets().stream().anyMatch(i->i.getAttribute(MetaModel.name).get().equals(Image.class.getSimpleName()))){
								imgImport.setEnabled(false);
								session.removeMetaListener(this);	
							}
						}
					}

				});


				imgImport.addActionListener(l->{	

					try {

						Optional<Experiment> experiment = 
								session.queryFactory().regenerate(Experiment.class)
								.toDepth(0).traverseAllLinks().includeAllNodes().regenerateAllKeys()
								.getAll().getOneTarget(Experiment.class);
						new Thread(new ImageOrganiser(experiment.get(), session, io)).start();	


					} catch (Exception e) {
						UI.display("Error", "Unable to import images, check the log", e, Level.WARNING);			
					}

				});

			}
			else //if there are already images in the database disable the menu item
				imgImport.setEnabled(false);


		} catch (DataAccessException e) {
			UI.display(
					"Error", "A problem occured while accessing the database",
					e, ErrorLevel.SEVERE);		

		}



		JMenuItem segImport = new JMenuItem("Segmentation Results");
		mntmImport.add(segImport);
		segImport.addActionListener(l->{			
			SegImportWizard w = new SegImportWizard(session, io);	
			w.pack();
			w.setLocationRelativeTo(null);			
			w.setVisible(true);			
		});


		JMenuItem annotImport = new JMenuItem("Annotations");
		mntmImport.add(annotImport);
		annotImport.addActionListener(l->{			
			JOptionPane.showMessageDialog(null, "Coming soon (Workbench.class)");		
			//TODO PickCells.userDBChoice();
		});

		
		
		JMenuItem generateMenuItem = new JMenuItem("Generate Canvas");
		TooltipManager.addTooltip(generateMenuItem, "Generate empty Segmentation Result for each registered image. "
				+ "\nThis allows to paint regions of interests using the Segmentation Editor");
		mnFile.add(generateMenuItem);
		generateMenuItem.addActionListener(l->{			
			CanvasMaker w = new CanvasMaker(session, io);	
			w.pack();
			w.setLocationRelativeTo(null);			
			w.setVisible(true);	
		});
		
		
		
		

		JMenuItem mntmSwitchWorkspace = new JMenuItem("switch workspace...");
		mntmSwitchWorkspace.addActionListener(l->{
			parentFrame.setJMenuBar(null);				
			PickCells.goToWelcome();
		});
		mnFile.add(mntmSwitchWorkspace);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(l->{
			PickCells.exit();
		});
		mnFile.add(mntmExit);





		// Data
		//-----------------------------------------

		JMenu mnData = new JMenu("Data");
		menuBar.add(mnData);

		JMenuItem filterMenuItem = new JMenuItem("New Filter");
		mnData.add(filterMenuItem);

		filterMenuItem.addActionListener(l->{

			// First choose the MetaQueryable
			try {

				ChoicePanel<MetaQueryable> mqChoice = ChoicePanels.allQueryable(session, "Choose a type of object" );
				mqChoice.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

				if(JOptionPane.showConfirmDialog(null, mqChoice, "Custom Filter ...",JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION){
					Wizard.showWizardDialog(
							ChoicePanels.customFilter(session, mqChoice.getSelected().get()).create())
					.ifPresent(mf ->{
						try {
							session.queryFactory().meta(mf).run();
						} catch (Exception e) {
							UI.display("Error", "Unable to save the new Attribute", e, Level.WARNING);
						}
					});
				}			

			} catch (Exception e) {
				UI.display("Error", "Unable to create a new Filter", e, Level.WARNING);
			}

		});


		JMenuItem attMenuItem = new JMenuItem("New Attribute");
		mnData.add(attMenuItem);

		attMenuItem.addActionListener(l->{

			// First choose the MetaQueryable
			try {

				ChoicePanel<MetaQueryable> mqChoice = ChoicePanels.allQueryable(session, "Choose a type of object" );
				mqChoice.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

				if(JOptionPane.showConfirmDialog(null, mqChoice, "Custom Attribute ...",JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION){
					Wizard.showWizardDialog(
							ChoicePanels.customFeatureFactory(session, () -> mqChoice.getSelected().get()).create())
					.ifPresent(mr ->{
						try {
							session.queryFactory().meta((ManuallyStored) mr).run();
						} catch (Exception e) {
							UI.display("Error", "Unable to save the new Attribute", e, Level.WARNING);
						}
					});
				}			

			} catch (Exception e) {
				UI.display("Error", "Unable to create a new Attribute", e, Level.WARNING);
			}


		});


		JMenuItem hideMenuItem = new JMenuItem("MetaModel Visibility");
		mnData.add(hideMenuItem);

		hideMenuItem.addActionListener(l->{
			//TODO
			JOptionPane.showMessageDialog(null, "Coming soon (Workbench.class)");		
		});




		// Locations
		//-----------------------------------------



		JMenu mnLocs = new JMenu("Locations");
		menuBar.add(mnLocs);

		JMenuItem openMenuItem = new JMenuItem("Logs Folder");
		openMenuItem.addActionListener(l->{

			File file = null;

			try {

				if (Desktop.isDesktopSupported()) {

					Desktop desktop = Desktop.getDesktop();
					String path = System.getProperty("user.dir")+File.separator+"logs";
					file = new File(path);
					desktop.open(file);
				}
				else {
					JOptionPane.showMessageDialog(null, "Sorry, not supported on your plateform");
				}
			}
			catch (Exception e){
				UI.display("Error", "Unable to open "+file.toString(), e, Level.WARNING);
			}

		});
		mnLocs.add(openMenuItem);

		JMenuItem openDBItem = new JMenuItem("Database Folder");
		openDBItem.addActionListener(l->{

			File file = null;

			try {

				if (Desktop.isDesktopSupported()) {

					Desktop desktop = Desktop.getDesktop();
					String path = UsefulQueries.experimentFolder(session);
					file = new File(path);
					desktop.open(file);
				}
				else {
					JOptionPane.showMessageDialog(null, "Sorry, not supported on your plateform");
				}
			}
			catch (Exception e){
				UI.display("Error", "Unable to open "+file.toString(), e, Level.WARNING);
			}

		});
		mnLocs.add( openDBItem);






		// Help
		//-----------------------------------------


		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);


		JMenuItem wikiMenuItem = new JMenuItem("Wiki");
		mnHelp.add(wikiMenuItem);
		wikiMenuItem.addActionListener(l->{
			if(Desktop.isDesktopSupported()){
				try {
					Desktop.getDesktop().browse(new URI("https://www.wiki.ed.ac.uk/display/PCUW/PickCells+Users+Wiki+Home"));
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Sorry this operation is not supported for your OS. The URL is : https://www.wiki.ed.ac.uk/display/PCUW/PickCells+Users+Wiki+Home");
				}
			}
			else
				JOptionPane.showMessageDialog(null, "Sorry this operation is not supported for your OS. The URL is : https://www.wiki.ed.ac.uk/display/PCUW/PickCells+Users+Wiki+Home");
		});



		JMenuItem mailMenuItem = new JMenuItem("Mail Author...");
		mailMenuItem.addActionListener(l->{
			try {

				if (Desktop.isDesktopSupported()) { //TODO replace by bug tracker
					mailto(
							Arrays.asList("guillaume.blin@ed.ac.uk"), 
							"PickCells Help Request",
							"Describe your problem..."
							);
				}
				else {
					JOptionPane.showMessageDialog(null, "Sorry, not supported on your plateform");
				}
			}
			catch (Exception e){
				UI.display("Error", "", e, Level.WARNING);
			}
		});
		mnHelp.add(mailMenuItem);

		JMenuItem mntmAbout = new JMenuItem("About...");
		mntmAbout.addActionListener(l->JOptionPane.showMessageDialog(null, "Not yet implemented")); //TODO licenses per module
		mnHelp.add(mntmAbout);

		return menuBar;
	}



	public void setMenuBarVisible(boolean b) {
		if(b)
			parentFrame.setJMenuBar(menuBar);
		else
			parentFrame.setJMenuBar(null);
	}







	/*
	 * 
	 * Obtained from http://www.2ality.com/2010/12/simple-way-of-sending-emails-in-java.html
	 * 
	 */


	private static void mailto(List<String> recipients, String subject,
			String body) throws IOException, URISyntaxException {
		String uriStr = String.format("mailto:%s?subject=%s&body=%s",
				join(",", recipients), // use semicolon ";" for Outlook!
				urlEncode(subject),
				urlEncode(body));
		Desktop.getDesktop().browse(new URI(uriStr));
	}

	private static final String urlEncode(String str) {
		try {
			return URLEncoder.encode(str, "UTF-8").replace("+", "%20");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	public static final String join(String sep, Iterable<?> objs) {
		StringBuilder sb = new StringBuilder();
		for(Object obj : objs) {
			if (sb.length() > 0) sb.append(sep);
			sb.append(obj);
		}
		return sb.toString();
	}

	


}
