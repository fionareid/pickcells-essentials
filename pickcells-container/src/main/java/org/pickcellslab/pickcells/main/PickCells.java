package org.pickcellslab.pickcells.main;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.CardLayout;
import java.awt.Frame;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.log4j.Logger;
import org.jdesktop.swingx.JXErrorPane;
import org.jdesktop.swingx.JXLoginPane;
import org.jdesktop.swingx.JXLoginPane.Status;
import org.jdesktop.swingx.error.ErrorInfo;
import org.jdesktop.swingx.error.ErrorLevel;
import org.kohsuke.MetaInfServices;
import org.pickcellslab.foundationj.appli.Application;
import org.pickcellslab.foundationj.appli.ModuleManager;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.Traversers;
import org.pickcellslab.foundationj.dbm.Credentials;
import org.pickcellslab.foundationj.dbm.DataAccessException;
import org.pickcellslab.foundationj.dbm.Persistence;
import org.pickcellslab.foundationj.dbm.RootItem;
import org.pickcellslab.foundationj.dbm.Session;
import org.pickcellslab.foundationj.dbm.User;
import org.pickcellslab.foundationj.dbm.User.priviledge;
import org.pickcellslab.foundationj.dbm.meta.Meta;
import org.pickcellslab.foundationj.dbm.queries.actions.AKeyInfo;
import org.pickcellslab.foundationj.dbm.queries.actions.Updater;
import org.pickcellslab.foundationj.dbm.queries.adapters.Updatable;
import org.pickcellslab.pickcells.api.app.modules.Analysis;
import org.pickcellslab.pickcells.api.app.modules.DefaultDocument;
import org.pickcellslab.pickcells.api.app.modules.DesktopModule;
import org.pickcellslab.pickcells.api.app.modules.DocumentFactory;
import org.pickcellslab.pickcells.api.app.modules.Organiser;
import org.pickcellslab.pickcells.api.app.ui.UI;
import org.pickcellslab.pickcells.api.datamodel.types.Experiment;
import org.pickcellslab.pickcells.api.datamodel.types.Image;
import org.pickcellslab.pickcells.api.img.io.ImgIO;

import com.alee.laf.WebLookAndFeel;

/**
 * The main application container in PickCells
 * TODO reorganize this class with a model and a view
 */
@MetaInfServices(Application.class)
public class PickCells extends Application{

	private static final Logger log = Logger.getLogger(PickCells.class);

	//UI related fields
	private final static String WORKBENCH = "Workbench";
	private final static String WELCOME = "Welcome";
	private final static String SANDBOX = "Sandbox";
	private static JFrame frame;
	private static WelcomeScreen welcome;
	private static Workbench workbench;
	private static SandboxDesk sandbox;

	private static boolean wbAdded;


	//Data related fields
	private static User currentUser;
	private static Experiment currentExperiment;
	private static Session<?,?> userSession;

	private static ImgIO imgIO;	



	public PickCells(ModuleManager domain, Persistence persistence) {

		super(domain, persistence);

		imgIO = domain.get(ImgIO.class).get();

		//Setup the look and feel
		try {
			UIManager.setLookAndFeel ( WebLookAndFeel.class.getCanonicalName () );
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebLookAndFeel.initializeManagers();


		//ToolTip Management
		ToolTipManager.sharedInstance().setInitialDelay(300);
		ToolTipManager.sharedInstance().setDismissDelay(10000);


		frame = new JFrame("PickCells");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setIconImage(new ImageIcon(getClass().getResource("/icons/Pickcells_Icon.png")).getImage());

		frame.getContentPane().setLayout(new CardLayout());



		welcome = new WelcomeScreen(this);
		frame.getContentPane().add(welcome, WELCOME);
		((CardLayout)frame.getContentPane().getLayout()).show(frame.getContentPane(), WELCOME);




		frame.pack();
		frame.setExtendedState( frame.getExtendedState()|Frame.MAXIMIZED_BOTH );
		frame.setVisible(true);


		if(!firstTime())
			launchAuthentification();

	}






	private boolean firstTime() {


		// Testing if 1st time -> if yes create admin user
		List<String> users = persistence.getAllUsers();
		if (users.size() == 0) {     

			log.debug("No users in the admin, ask for the creation of an admin user");
			currentUser = new UserCreationWizard(users, priviledge.ADMIN, false).showDialog();
			try {
				persistence.createUser(currentUser, null);
				welcome.setUser(currentUser);
			} catch (DataAccessException e) {
				ErrorInfo error = new ErrorInfo("Error", 
						"A fatal error has occured", null, null,e, ErrorLevel.FATAL, null);
				JXErrorPane.showDialog(null, error);
			}

			return true;
		}
		return false;

	}


	User createUser(){

		List<String> users = persistence.getAllUsers();   
		User novel = new UserCreationWizard(users, priviledge.USER, true).showDialog();
		try {
			if(novel!=null){
				String name = currentUser.getAttribute(AKey.get("name", String.class)).get();
				String password = currentUser.getAttribute(AKey.get("password", String.class)).get();

				persistence.createUser(novel, new Credentials(name, password));
			}
		} catch (DataAccessException e) {
			ErrorInfo error = new ErrorInfo("Error", 
					"A fatal error has occured", null, null,e, ErrorLevel.FATAL, null);
			JXErrorPane.showDialog(null, error);
		}
		return novel;
	}





	void launchAuthentification(){

		//Show the login dialog
		LoginManager logManager = new LoginManager(persistence);
		Status status = JXLoginPane.showLoginDialog(null, logManager);

		//If authentification succeeds then get the scope, and the experiments it possesses
		if(status == Status.SUCCEEDED){
			currentUser = logManager.getAuthenticatedUser();
			welcome.setUser(currentUser);

		}else if(status == Status.FAILED){
			currentUser = null;
			welcome.setUser(null);
		}

	}




	static void goToWelcome(){
		((CardLayout)frame.getContentPane().getLayout()).show(frame.getContentPane(), WELCOME);
		if(workbench!=null)
			workbench.setMenuBarVisible(false);
	}


	static void goToWorkbench(){		
		if(!wbAdded){
			frame.getContentPane().add(workbench.getScene(), WORKBENCH);
			wbAdded = true;
		}
		((CardLayout)frame.getContentPane().getLayout()).show(frame.getContentPane(), WORKBENCH);	
		workbench.setMenuBarVisible(true);		
	}


	static void goToSandBox(){		
		if(sandbox==null){
			sandbox = new SandboxDesk();
			frame.getContentPane().add(sandbox, SANDBOX);
			domain.getModulesWithvalidScope(DesktopModule.class).forEach(module -> sandbox.addModule(module));
		}
		((CardLayout)frame.getContentPane().getLayout()).show(frame.getContentPane(), SANDBOX);			
	}










	void deleteExperiment(Experiment experiment) {

		try {

			if(currentExperiment == experiment){
				//TODO shutdown database and reload workbench
				userSession.close();
				userSession = null;
				currentExperiment = null;
			}			

			Credentials cre = new Credentials(currentUser.userName(), currentUser.getAttribute(AKey.get("password", String.class)).get());
			persistence.delete(cre, experiment);

		} catch (DataAccessException e) {
			JXErrorPane.showDialog(
					null, new ErrorInfo("Failed to delete the experiment", e.getMessage(), null, null, e, ErrorLevel.SEVERE, null));
		}

	}


	public  void deleteUser(User selectedUser) {

		//First delete all experiments
		List<RootItem> experiments = selectedUser.getAccessibleRootItem();
		experiments.forEach(e->this.deleteExperiment((Experiment) e));

		//now delete the user
		try {
			persistence.deleteUser(selectedUser, new Credentials(currentUser.userName(), currentUser.getAttribute(AKey.get("password", String.class)).get()));
		} catch (IllegalArgumentException | DataAccessException e1) {
			JXErrorPane.showDialog(
					null, new ErrorInfo("Failed to delete the experiment", e1.getMessage(), null, null, e1, ErrorLevel.SEVERE, null));
		}

	}





	/**
	 * Creates a new database for the current user and stores the given experiment into this database
	 * The experiment {@link WritableDataItem#idKey} will conserved if it already exists
	 * @param experiment The Experiment to be created
	 * @return The newly created session
	 */
	void createExperiment(Experiment experiment){


		try {

			if(userSession != null) {
				userSession.close();
				userSession = null;
				currentExperiment = null;
			}
			
			Credentials cre = new Credentials(currentUser.userName(), currentUser.getAttribute(AKey.get("password", String.class)).get());

			Session<?,?> session = persistence.startSession(cre, experiment);
			Meta.get().setSession(session);
			
			session.queryFactory().store()
			.add(experiment, Traversers.newConstraints().fromMinDepth().toDepth(0).traverseAllLinks().includeAllNodes())
			.forceUnknownId()
			.feed(experiment, "PickCells Core", "An Experiment englobes the full analysis")
			.run();

			session.close();


		}catch(DataAccessException e){
			UI.display(
					"Error", "A problem occured while accessing the database",
					e, ErrorLevel.SEVERE);
		}



	}





	void launchExperiment(Experiment experiment) {

		//System.out.println("Launching experiment");

		if(experiment != currentExperiment){
			if(currentExperiment != null){				
				userSession.close();
				userSession = null;
			}

			currentExperiment = experiment;			

			Credentials cre = new Credentials(currentUser.userName(), currentUser.getAttribute(AKey.get("password", String.class)).get());
			try {			

				userSession = persistence.startSession(cre, experiment);

			} catch (DataAccessException e) {
				UI.display(
						"Error", "A problem occured while accessing the database",
						e, ErrorLevel.SEVERE);		
				return;
			}




			//Load the modules in the workbench
			//Check the workbench
			if(workbench == null){

				workbench = new Workbench(frame, userSession, imgIO);

				//	Thread t = new Thread(()->{
				domain.setActiveSession(userSession,currentUser.privilege());

				domain.getModulesWithvalidScope(DefaultDocument.class).forEach(factory -> workbench.addDefault(factory));
				domain.getModulesWithvalidScope(DocumentFactory.class).forEach(factory -> workbench.addDocument(factory));
				domain.getModulesWithvalidScope(Analysis.class).forEach(analysis -> workbench.addTask(analysis));
				domain.getModulesWithvalidScope(Organiser.class).forEach(analysis -> workbench.addTask(analysis));
				//	});


				//	UI.waitAnimation(t, "Loading the workbench...");

				//	try {
				//		t.join();
				//	} catch (InterruptedException e) {
				//		
				//		e.printStackTrace();
				//	}
			}
			else{
				workbench.setSession(userSession);	
				domain.setActiveSession(userSession,currentUser.privilege());
			}



			//Display the workbench
			goToWorkbench();

			log.info("Workbench loaded !");



		}
		else
			goToWorkbench();
	}





	public Optional<Experiment> linkDB(File f) {

		//Check if file is valid
		//First get the Experiment in the database
		Optional<Experiment> exp = persistence.investigateDB(f.getPath(), Experiment.class);
		if(!exp.isPresent()){
			JOptionPane.showMessageDialog(null, "The chosen database does not contain a valid experiment");
			return exp;
		}

		//Now get the session for the current user
		try {

			//Make sure the values for the path in experiment are correct
			String dbPath = f.getParent();
			String dbName = f.getName();
			System.out.println("dbPath : "+dbPath);
			System.out.println("dName : "+dbName);


			boolean differ = !dbPath.equals(exp.get().getDBPath());
			if(differ)
				exp.get().setAttribute(Experiment.dbPathKey, dbPath);

			exp.get().removeAttribute(WritableDataItem.idKey);

			//Now create in the admin database which will also register in the admin db
			Session<?,?> s =
					persistence.startSession(new Credentials(
							currentUser.userName(), currentUser.getAttribute(AKey.get("password", String.class)).get()), exp.get());
			
			//We need to specify which session the MetaModel will be built from
			Meta.get().setSession(s);
			
			
			//If the dbPath is different update the database with the new path (must have been copied from somewhere else)	
			if(differ){

				//Update the experiment dbPath
				s.queryFactory().update(Experiment.class, new Updater(){

					@Override
					public void performAction(Updatable i) throws DataAccessException {
						i.setAttribute(Experiment.dbPathKey,dbPath);
					}

					@Override
					public List<AKeyInfo> updateIntentions() {
						List<AKeyInfo> updates = new ArrayList<>(1);						
						updates.add(new AKeyInfo(Experiment.dbPathKey, "Database Path"));
						return updates;
					}

				}).getAll().run();


				//Update the location of images
				//TODO check if not from cloud
				s.queryFactory().update(Image.class, new Updater(){

					@Override
					public void performAction(Updatable i) throws DataAccessException {
						i.setAttribute(Image.loc,dbPath);
					}

					@Override
					public List<AKeyInfo> updateIntentions() {
						List<AKeyInfo> updates = new ArrayList<>(1);						
						updates.add(new AKeyInfo(Image.loc, "Location"));
						return updates;
					}

				}).getAll().run();

			}

			return exp;

		} catch (IllegalArgumentException | DataAccessException e) {
			JOptionPane.showMessageDialog(null, "Linking to the specified database has failed, check the log");
			log.warn("Could not obtain a session from an unbound db", e);
			return exp;
		}

	}



}




