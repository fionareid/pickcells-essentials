package org.pickcellslab.pickcells.main;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;

/**
 * ToolbarWithOverflow provides a component which is useful for displaying commonly used
 * actions.  It adds an overflow button when the toolbar becomes too small to show all the
 * available actions.
 *
 */
@SuppressWarnings("serial")
public class ToolBar extends JToolBar {

	private JButton overflowButton;
	private JPopupMenu popup;
	private JToolBar overflowToolbar;
	private boolean displayOverflowOnHover = true;
	
	private AWTEventListener awtEventListener;
	private ComponentAdapter componentAdapter;

	/**
	 * Creates a new tool bar; orientation defaults to
	 * <code>HORIZONTAL</code>.
	 */
	public ToolBar() {
		this(VERTICAL);
	}

	/**
	 * Creates a new tool bar with the specified
	 * <code>orientation</code>. The
	 * <code>orientation</code> must be either
	 * <code>HORIZONTAL</code> or
	 * <code>VERTICAL</code>.
	 *
	 * @param orientation the orientation desired
	 */
	public ToolBar(int orientation) {
		this(null, orientation);
	}

	/**
	 * Creates a new tool bar with the specified
	 * <code>name</code>. The name is used as the title of the undocked tool
	 * bar. The default orientation is
	 * <code>HORIZONTAL</code>.
	 *
	 * @param name the name of the tool bar
	 */
	public ToolBar(String name) {
		this(name, HORIZONTAL);
	}

	/**
	 * Creates a new tool bar with a specified
	 * <code>name</code> and
	 * <code>orientation</code>. All other constructors call this constructor.
	 * If
	 * <code>orientation</code> is an invalid value, an exception will be
	 * thrown.
	 *
	 * @param name the name of the tool bar
	 * @param orientation the initial orientation -- it must be     *        either <code>HORIZONTAL</code> or <code>VERTICAL</code>
	 * @exception IllegalArgumentException if orientation is neither
	 * <code>HORIZONTAL</code> nor <code>VERTICAL</code>
	 */
	public ToolBar(String name, int orientation) {
		super(name, orientation);
		setupOverflowButton();
		popup = new JPopupMenu();
		popup.setInvoker(this);
		popup.setBorderPainted(false);
		popup.setBorder(BorderFactory.createEmptyBorder());
		overflowToolbar = new JToolBar("overflowToolbar", orientation == HORIZONTAL ? VERTICAL : HORIZONTAL);
		overflowToolbar.setFloatable(false);
		overflowToolbar.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
	}

	private ComponentListener getComponentListener() {
		if (componentAdapter == null) {
			componentAdapter = new ComponentAdapter() {
				@Override
				public void componentResized(ComponentEvent e) {
					maybeAddOverflow();
				}
			};
		}
		return componentAdapter;
	}

	private AWTEventListener getAWTEventListener() {
		if (awtEventListener == null) {
			awtEventListener = new AWTEventListener() {
				@Override
				public void eventDispatched(AWTEvent event) {
					MouseEvent e = (MouseEvent) event;
					if(isVisible() && !isShowing() && popup.isShowing()) {
						popup.setVisible(false);
						return;
					}
					if (event.getSource() == popup) {
						if (popup.isShowing() && e.getID() == MouseEvent.MOUSE_EXITED) {
							int minX = popup.getLocationOnScreen().x;
							int maxX = popup.getLocationOnScreen().x + popup.getWidth();
							int minY = popup.getLocationOnScreen().y;
							int maxY = popup.getLocationOnScreen().y + popup.getHeight();
							if (e.getXOnScreen() < minX || e.getXOnScreen() >= maxX || e.getYOnScreen() < minY || e.getYOnScreen() >= maxY) {
								popup.setVisible(false);
							}
						}
					} else {
						if (popup.isShowing() && overflowButton.isShowing() && (e.getID() == MouseEvent.MOUSE_MOVED || e.getID() == MouseEvent.MOUSE_EXITED)) {
							int minX = overflowButton.getLocationOnScreen().x;
							int maxX_ob = minX + overflowButton.getWidth();
							int maxX = getOrientation() == HORIZONTAL ? minX + popup.getWidth()
									: minX + overflowButton.getWidth() + popup.getWidth();
							int minY = overflowButton.getLocationOnScreen().y;
							int maxY_ob = minY + overflowButton.getHeight();
							int maxY = getOrientation() == HORIZONTAL ? minY + overflowButton.getHeight() + popup.getHeight()
									: minY + popup.getHeight();
							if (e.getXOnScreen() < minX || e.getYOnScreen() < minY || e.getXOnScreen() > maxX || e.getYOnScreen() > maxY
									|| (getOrientation() == HORIZONTAL && e.getXOnScreen() > maxX_ob
									&& e.getYOnScreen() >= minY && e.getYOnScreen() < maxY_ob)
									|| (getOrientation() == VERTICAL && e.getXOnScreen() >= minX && e.getXOnScreen() < maxX_ob
									&& e.getYOnScreen() > maxY_ob)) {
								popup.setVisible(false);
							}
						}
					}
				}
			};
		}
		return awtEventListener;
	}

	@Override
	public void addNotify() {
		super.addNotify();
		
			addComponentListener(getComponentListener());
			Toolkit.getDefaultToolkit().addAWTEventListener(getAWTEventListener(), AWTEvent.MOUSE_EVENT_MASK | AWTEvent.MOUSE_MOTION_EVENT_MASK);
		
	}

	@Override
	public void removeNotify() {
		super.removeNotify();
		if (componentAdapter != null) {
			removeComponentListener(componentAdapter);
		}
		if (awtEventListener != null) {
			Toolkit.getDefaultToolkit().removeAWTEventListener(awtEventListener);
		}
	}

	/**
	 * Returns whether the overflow should be displayed on hover or not. The
	 * default value is <code>true</code>.
	 *
	 * @return <code>true</code> if overflow is displayed on hover; <code>false</code> otherwise
	 */
	 public boolean isDisplayOverflowOnHover() {
		 return displayOverflowOnHover;
	 }

	 /**
	  * Sets whether the overflow should be displayed on hover or not. The
	  * default value is <code>true</code>.
	  *
	  * @param displayOverflowOnHover if <code>true</code>, the overflow will be displayed on hover;
	  * <code>false</code> otherwise
	  */
	  public void setDisplayOverflowOnHover(boolean displayOverflowOnHover) {
		  this.displayOverflowOnHover = displayOverflowOnHover;
		  setupOverflowButton();
	  }

	  @Override
	  public Dimension getPreferredSize() {
		  Component[] comps = getAllComponents();
		  Insets insets = getInsets();
		  int width = null == insets ? 0 : insets.left + insets.right;
		  int height = null == insets ? 0 : insets.top + insets.bottom;
		  for (int i = 0; i < comps.length; i++) {
			  Component comp = comps[i];
			  if (!comp.isVisible()) {
				  continue;
			  }
			  width += getOrientation() == HORIZONTAL ? comp.getPreferredSize().width : comp.getPreferredSize().height;
			  height = Math.max( height, getOrientation() == HORIZONTAL ? comp.getPreferredSize().height : comp.getPreferredSize().width );
		  }
		  if(overflowToolbar.getComponentCount() > 0) {
			  width += getOrientation() == HORIZONTAL ? overflowButton.getPreferredSize().width : overflowButton.getPreferredSize().height;
		  }
		  Dimension dim = getOrientation() == HORIZONTAL ? new Dimension(width, height) : new Dimension(height, width);
		  return dim;        
	  }

	  @Override
	  public void setOrientation(int o) {
		  super.setOrientation(o);
		  setupOverflowButton();
	  }

	  @Override
	  public void removeAll() {
		  super.removeAll();
		  overflowToolbar.removeAll();
	  }

	  @Override
	  public void validate() {
		  
			  int visibleButtons = computeVisibleButtons();
			  if (visibleButtons == -1) {
				  handleOverflowRemoval();
			  } else {
				  handleOverflowAddittion(visibleButtons);
			  }
		
		  super.validate();
	  }

	  private void setupOverflowButton() {
		  overflowButton = new JButton(new ImageIcon(getClass().getResource("/icons/Down.png")));
		  overflowButton.addMouseListener(new MouseAdapter() {
			  @Override
			  public void mouseClicked(MouseEvent e) {
				  if(popup.isShowing()) {
					  popup.setVisible(false);
				  } else {
					  displayOverflow();
				  }
			  }

			  @Override
			  public void mouseEntered(MouseEvent e) {
				  if(displayOverflowOnHover) {
					  displayOverflow();
				  }
			  }
		  });
	  }

	  private void displayOverflow() {
		  int x = getOrientation() == HORIZONTAL ? overflowButton.getLocationOnScreen().x : overflowButton.getLocationOnScreen().x + overflowButton.getWidth();
		  int y = getOrientation() == HORIZONTAL ? overflowButton.getLocationOnScreen().y + overflowButton.getHeight() : overflowButton.getLocationOnScreen().y;
		  popup.setLocation(x, y);
		  popup.setVisible(true);
	  }

	  /**
	   * Determines if an overflow button should be added to or removed from the toolbar.
	   */
	   private void maybeAddOverflow() {
		   validate();
		   repaint();
	   }

	  private int computeVisibleButtons() {
		  if (isShowing()) {
			  int w = getOrientation() == HORIZONTAL ? overflowButton.getIcon().getIconWidth() + 4 : getWidth() - getInsets().left - getInsets().right;
			  int h = getOrientation() == HORIZONTAL ? getHeight() - getInsets().top - getInsets().bottom : overflowButton.getIcon().getIconHeight() + 4;
			  overflowButton.setMaximumSize(new Dimension(w, h));
			  overflowButton.setMinimumSize(new Dimension(w, h));
			  overflowButton.setPreferredSize(new Dimension(w, h));
		  }

		  Component[] comps = getAllComponents();
		  int sizeSoFar = 0;
		  int maxSize = getOrientation() == HORIZONTAL ? getWidth() : getHeight();
		  int overflowButtonSize = getOrientation() == HORIZONTAL ? overflowButton.getPreferredSize().width : overflowButton.getPreferredSize().height;
		  int showingButtons = 0; // all that return true from isVisible()
				  int visibleButtons = 0; // all visible that fit into the given space (maxSize)
				  Insets insets = getInsets();
				  if( null != insets ) {
					  sizeSoFar = getOrientation() == HORIZONTAL ? insets.left+insets.right : insets.top+insets.bottom;
				  }
				  for (int i = 0; i < comps.length; i++) {
					  Component comp = comps[i];
					  if( !comp.isVisible() ) {
						  continue;
					  }
					  if (showingButtons == visibleButtons) {
						  int size = getOrientation() == HORIZONTAL ? comp.getPreferredSize().width : comp.getPreferredSize().height;
						  if (sizeSoFar + size <= maxSize) {
							  sizeSoFar += size;
							  visibleButtons++;
						  }
					  }
					  showingButtons++;
				  }
				  if (visibleButtons < showingButtons && visibleButtons > 0 && sizeSoFar + overflowButtonSize > maxSize) {
					  // overflow button needed but would not have enough space, remove one more button
					  visibleButtons--;
				  }
				  if (visibleButtons == 0 && comps.length > 0
						  && comps[0] instanceof JComponent) {
					  visibleButtons = 1; // always include the dragger if present
				  }
				  if (visibleButtons == showingButtons) {
					  visibleButtons = -1;
				  }
				  return visibleButtons;
	  }

	  private void handleOverflowAddittion(int visibleButtons) {
		  Component[] comps = getAllComponents();
		  removeAll();
		  overflowToolbar.setOrientation(getOrientation() == HORIZONTAL ? VERTICAL : HORIZONTAL);
		  popup.removeAll();

		  for (Component comp : comps) {
			  if (visibleButtons > 0) {
				  add(comp);
				  if (comp.isVisible()) {
					  visibleButtons--;
				  }
			  } else {
				  overflowToolbar.add(comp);
			  }
		  }
		  popup.add(overflowToolbar);
		  add(overflowButton);
	  }

	  private void handleOverflowRemoval() {
		  if (overflowToolbar.getComponents().length > 0) {
			  remove(overflowButton);
			  for (Component comp : overflowToolbar.getComponents()) {
				  add(comp);
			  }
			  overflowToolbar.removeAll();
			  popup.removeAll();
		  }
	  }

	

	  private Component[] getAllComponents() {
		  Component[] toolbarComps;
		  Component[] overflowComps = overflowToolbar.getComponents();
		  if (overflowComps.length == 0) {
			  toolbarComps = getComponents();
		  } else {
			  if (getComponentCount() > 0) {
				  toolbarComps = new Component[getComponents().length - 1];
				  System.arraycopy(getComponents(), 0, toolbarComps, 0, toolbarComps.length);
			  } else {
				  toolbarComps = new Component[0];
			  }
		  }
		  Component[] comps = new Component[toolbarComps.length + overflowComps.length];
		  System.arraycopy(toolbarComps, 0, comps, 0, toolbarComps.length);
		  System.arraycopy(overflowComps, 0, comps, toolbarComps.length, overflowComps.length);
		  return comps;
	  }
}
