package org.pickcellslab.pickcells.main;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.TreeTableNode;
import org.pickcellslab.foundationj.datamodel.AKey;
import org.pickcellslab.foundationj.datamodel.Link;
import org.pickcellslab.foundationj.datamodel.NodeItem;
import org.pickcellslab.foundationj.datamodel.WritableDataItem;
import org.pickcellslab.foundationj.datamodel.tools.BreadthFirst;
import org.pickcellslab.foundationj.datamodel.tools.TraversalStep;

public abstract class DataUtils {


	public static TreeTableNode createTreeModel(BreadthFirst<NodeItem,Link> traverser){

		Map<WritableDataItem, DefaultMutableTreeTableNode> sources = new LinkedHashMap<>();
		Map<WritableDataItem, DefaultMutableTreeTableNode> targets = new LinkedHashMap<>();
		DefaultMutableTreeTableNode root;

		List<Link> linksToStore = new ArrayList<>();

		TraversalStep<NodeItem,Link> step = traverser.nextStep();
		root = new DefaultMutableTreeTableNode();
		root.setUserObject((step.nodes.get(0)));
		linksToStore = step.edges;
		sources.put(step.nodes.get(0), root);

		while ((step = traverser.nextStep()) != null) {

			System.out.println("Something happens");

			for(WritableDataItem i : step.nodes){
				DefaultMutableTreeTableNode n = new DefaultMutableTreeTableNode();
				n.setUserObject(i);
				targets.put(i, n);


				System.out.println("added : "+i.getAttribute(AKey.get("name", String.class)));
			}

			for(Link l : linksToStore){
				// creating parent/child relationship
				// get Source node and target node
				DefaultMutableTreeTableNode source = sources.get(l.source());
				DefaultMutableTreeTableNode target = targets.get(l.target());

				if(source== null || target == null){// Traverser returns both incoming and outgoing links
					target = targets.get(l.target());
					source = targets.get(l.source());
					//target = sources.get(l.target());

					if(source == null)//selfReferencing!
						source = sources.get(l.source());
					else if(target == null)
						target = targets.get(l.target());
				}

				source.add(target);
				System.out.println("source = "+((WritableDataItem) source.getUserObject()).getAttribute(AKey.get("name", String.class)));
				System.out.println("target = "+((WritableDataItem) target.getUserObject()).getAttribute(AKey.get("name", String.class)));
			}

			sources.putAll(targets);
			targets.clear();
			linksToStore = step.edges;

		}

		System.out.println("root = "+((WritableDataItem) root.getUserObject()).getAttribute(AKey.get("name", String.class)));
		
		return root;

	}




}
