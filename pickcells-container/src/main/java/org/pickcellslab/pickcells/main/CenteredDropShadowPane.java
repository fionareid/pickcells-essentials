package org.pickcellslab.pickcells.main;

/*-
 * #%L
 * Pickcells
 * %%
 * Copyright (C) 2016 - 2017 Guillaume Blin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jdesktop.swingx.border.DropShadowBorder;

/**
 * Creates a Panel displaying a Component in the middle with a drop shadow border
 */
public class CenteredDropShadowPane {
	
	private final JPanel main;
	private final JPanel fixed;

	public CenteredDropShadowPane(int borderWidth, int borderHeight, int prefWidth, int prefHeight) {
		
		main = new JPanel();
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{borderWidth, prefWidth, borderWidth};
		gridBagLayout.rowHeights = new int[]{borderHeight, prefHeight, borderHeight};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		main.setLayout(gridBagLayout);
		
		
		
		//Panel resizing with the parent container to place the fixed ratio in the middle
		fixed = new JPanel();
		DropShadowBorder b =new DropShadowBorder();
		b.setShowLeftShadow(true);
		b.setShowTopShadow(true);
		b.setShowBottomShadow(true);
		b.setShowRightShadow(true);
		b.setShadowSize(10);
		
		fixed.setBorder(b);
		
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 1;
		gbc_panel.gridy = 1;
		main.add(fixed, gbc_panel);
		
		
	}

	public JPanel getHolderPanel(){
		return main;
	}
	
	public JPanel getInsidePanel(){
		return fixed;
	}
	
	
	
	public static void main(String[] args){
		JFrame container = new JFrame();
		container.setBackground(new Color(255,255,255));
		container.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		container.getContentPane().add(new CenteredDropShadowPane(50, 50, 600, 400).getHolderPanel());
		container.pack();
		container.setVisible(true);
	}
	
	
}
